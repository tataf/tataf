
(*----------------------------------------------------------------------------*)
(* options currently not set by command line arguments: *)

let default_timeout = 10
let timeout = ref default_timeout

let callbohne = ref false
let usedps = ref []
let minasm = ref false
let novcsplit = ref false
let tryHard = ref false

let concurrent_provers = ref true
let maxvcpar = ref 0

let verify = ref true
let failfast = ref false

let outfile = ref ""

