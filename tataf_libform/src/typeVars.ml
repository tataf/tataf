(** Get free type variables in a type formula.   *)

(** This file has been adapted from the Jahob project:
 * http://lara.epfl.ch/w/jahob_system
  
 *  Andreas Podelski, Thomas Wies: Counterexample-guided focus. POPL 2010: 249-260

 * Karen Zee, Viktor Kuncak, Martin C. Rinard: Full functional verification of linked data structures. PLDI 2008: 349-361

 *)

open Form

let ftv (f : typeForm) : ident list =
   let add v acc = 
    if List.mem v acc then acc
    else v::acc in
  let rec fv1 f acc = match f with
    | TypeUniverse -> acc
    | TypeVar v -> add v acc
    | TypeApp(_,fs) -> fv_list fs acc
    | TypeFun(fs,f1) -> fv_list (f1::fs) acc
    | TypeProd fs -> fv_list fs acc
  and fv_list fs acc = match fs with
    | [] -> acc
    | f0::fs1 -> fv_list fs1 (fv1 f0 acc)
  in fv1 f []

