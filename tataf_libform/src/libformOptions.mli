(** 
  This module exports the variables, which are filled by the command line
  parser {!CmdLine}.  The command line options and the subcommands (aka.
  optionblocks) are defined in module {!optionblocks}.

  This module should not define further dependencies, since it is supposed to
  be usable from everywhere else to access these variables (and is thus listed
  very early for linking).

  {3 See {!CmdLine} documentation for an overview over the interplay of these
     three modules and a HOWTO for adding new subcommands, flags, etc.}
*)

(** 
  Options, which are not set by command line arguments.
*)

val timeout : int ref
(** used by {!SmtCvcl}. *)

val callbohne : bool ref
(** used by {!Decider}. *)

val usedps : (string * Common.options_t) list ref
(** used by {!Decider}. *)

val minasm : bool ref
(** used by {!Decider}. *)

val novcsplit : bool ref
(** used by {!Decider}. *)

val tryHard : bool ref
(** used by {!Decider}. Split the antecedent disjunctively in case the 
  original sequent can't be proven. *)

val concurrent_provers : bool ref
(** used by {!Decider}. *)

val maxvcpar : int ref
(** used by {!Decider}. *)

val verify : bool ref
(** used by {!Decider}. *)

val failfast : bool ref
(** used by {!Decider}. *)

