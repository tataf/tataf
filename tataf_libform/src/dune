;;
;; Copyright (C) 2021 University of Freiburg
;;
;; This file is part of tataf.
;;
;; Tataf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; Tataf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with tataf.  If not, see <https://www.gnu.org/licenses/>.
;;

; stanza to define compilation & linking features of the library
; generate library by: $ dune build
;
(library
 (public_name tataf_libform)
; (wrapped false)
;   remove ';' on prev. line if wanting to disable type-level module aliasing
;   when compiling this library
 (libraries
  str unix
  tataf_libcore tataf_libuxu)
 (modules
  (:standard \ parseForm parsingFormAux tataf_utop))
 (modes
    byte native)
 (flags -annot -g -w @7@9@40-23 -bin-annot)
 (library_flags -g))

;TODO:@gen_doc, should find a way to exclude:
;form/{parseForm,parsingFormAux}.ml

; stanza so that dune considers source files (ml,mli,etc.) within
; subdirectories as sources for the library too
;
(include_subdirs unqualified)

; stanza to generate library-version-ml file via a make call
; is run automatically when doing: $ dune build
;
(rule
 (targets libformVersion.ml)
 (deps
  (:mk Makefile Makefile.tataf)
  (:in libformVersion.ml.in))
 (action
  (run make -s update_version_info OMIT_OMK=y)))

; following two stanzas generate a utop executable by doing:
; $ dune build @tataf_utop
;
(executable
 (name tataf_utop)
 (modules tataf_utop)
 (link_flags -linkall -custom)
 (modes byte)
 (libraries utop tataf_libform))

(alias
 (name   tataf_utop)
 (deps   tataf_utop.bc))

