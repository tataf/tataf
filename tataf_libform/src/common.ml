(** Common Jahob definitions *)
(** This file has been adapted from the Jahob project:
 * http://lara.epfl.ch/w/jahob_system
  
 *  Andreas Podelski, Thomas Wies: Counterexample-guided focus. POPL 2010: 249-260

 * Karen Zee, Viktor Kuncak, Martin C. Rinard: Full functional verification of linked data structures. PLDI 2008: 349-361

 *) 

(** String used for padding by all Jahob printing. Identation mamagement is
    left free to each implementation. *)
let padding_string = ref ""

(** Set parseall to true if you want all methods in the given files to be
    parsed . *)
let parseall = ref false

type analysis_task = {
  task_lemmas : (string * string) list; (* (class,ident) list of lemmas *)
  task_methods : (string * string) list; (* (class,ident) list of methods *)
  task_classes : string list;
}

type source_pos = {
  pos_class : string;
  pos_method : string;
  pos_place : string;
}

let string_of_pos (pos : source_pos) : string =
  "[" ^ pos.pos_class ^ "." ^ pos.pos_method ^ ", " ^ pos.pos_place ^ "]"

(** The unknown position: unknown class, method and empty place. *)
let unknown_pos = {
  pos_class = "UnknownClass";
  pos_method = "UnknownMethod";
  pos_place = "";
}

(** {6 These are useful to deal with command-line options} *)

module StringMap = Map.Make(String)

type options_t = string StringMap.t

(** [merge_opts default provided] merges the two mappings [default] and
    [provided]. When a key appears in both [default] and [provided], the
    associated value in the result is the value found in [provided]. *)
let merge_opts (default:options_t) (provided:options_t) : options_t = 
  StringMap.fold (fun key value oldmap -> StringMap.add key value oldmap) provided default

(** transforms a list of [(a1, b1);(a2, b2);...] in a maping [a1 => b1 ; a2 => b2 ; ....] *)
let map_of_list : (string*string) list -> options_t = 
  ListLabels.fold_left ~f:(fun m (a,b) -> StringMap.add a b m) ~init:StringMap.empty

(** [glag_positive ~o flag] is [true] iff [flag] appear as a key in [o], with an associated value different from "no". *)
let flag_positive ~(o:options_t) (flag:string) : bool = 
  try 
    not (StringMap.find flag o = "no")
  with
    | Not_found -> false

let string_of_options (o:options_t) : string = 
 String.concat ":" (StringMap.fold (fun key value s -> (key ^ "=" ^ value) :: s) o [])

