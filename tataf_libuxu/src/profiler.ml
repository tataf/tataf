(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Unix
let dbg = Tataf_libcore.Debug.register_debug_module "Profiler"


class profiler =
    object (self)

    val time_stack = Stack.create()

    method start_clock =
        let times = Unix.times () in
        let start_time = times.tms_utime in
        Stack.push start_time time_stack

    method stop_clock =
        (* NOTE: Can raise Stack.Empty exception below, maybe catch and warn
         * instead? *)
        let times = Unix.times () in
        let stop_time = times.tms_utime in
        let recovered_start_time = Stack.pop time_stack in
        let execution_time = string_of_float(stop_time -. recovered_start_time)
        in 
        let s = "Execution time: " ^ execution_time ^ " s"  in
        dbg#ldebug_endline 2 s; 
        (stop_time -. recovered_start_time)
end
