(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)


(* load this package dependencies (based on META) to UTop *)
UTop.require ["str";"unix";"tataf_libcore"];

(* add this-package path of compiled sources to the load path of UTop
   paths are relative to where the tataf_utop executable is called,
   so the executable is expected to be called from tataf_lib*/ *)

(*path for dune*)
Topdirs.dir_directory "_build/default/src/.tataf_libuxu.objs/";
(*path for omk*)
Topdirs.dir_directory "src/";

(* Start utop. It never returns. *)
UTop_main.main ()

