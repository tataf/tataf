(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(** 
  This module exports the variables, which are filled by the command line
  parser {!CmdLine}.  The command line options and the subcommands (aka.
  optionblocks) are defined in module {!optionblocks}.

  This module should not define further dependencies, since it is supposed to
  be usable from everywhere else to access these variables (and is thus listed
  very early for linking).

  {3 See {!CmdLine} documentation for an overview over the interplay of these
     three modules and a HOWTO for adding new subcommands, flags, etc.}
*)

(** 
  tataf for Uppaal options.
*)

(**
  {1 Options}
  List of all variables, which are filled by the specified subcommands.
*)

(**
  {2 analysepaths}
*)

val analysepaths : bool ref
(** Indicates if the [analysepaths] subcommand was specified. *)

val analysepaths_depth : int ref
(** Specifies the depth of the path analysis. Defaults to [3]. *)

val analysepaths_source : string ref
(** Filename of the Uppaal xml file. *)

val analysepaths_template : string ref
(** Name of the starting template. *)

val analysepaths_startTransition : string ref
(** Name of the starting transition. *)


(**
  {2 dump}
*)

(**
  {3 sub modules.}
*)

val dumptransitions : bool ref
(** 
  Indicates if the [dump transitions] subcommand was specified.
*)

val dumptransitions_source : string ref
(** source Uppaal-file, which shall be considered for transition dump.  *)

val dumptransitions_template : string ref
(** Name of the template for transition dump. *)

val dumpsymboltable : bool ref
(** 
  Indicates if the [dump symboltable] subcommand was specified.
*)


(**
  {2 flattenarrays}
*)

val flattenarrays: bool ref
(** Indicates if the [flattenarrays] subcommand was specified. *)

val flattenarrays_source : string ref
(** Filename of the source Uppaal xml file. *)

(**
  {2 flattencustomtypes}
*)

val flattencustomtypes: bool ref
(** Indicates if the [flattencustomtypes] subcommand was specified. *)
val flattencustomtypes_remove_typedefs: bool ref
(** Indicates if all typedefs should be removed from the Uppaal model. *)


(**
  {2 prettyprint_uppaal}
*)

val ppuppaal : bool ref
(** Indicates if the [prettyprint_uppaal] subcommand was specified.  *)

val ppuppaal_source : string ref
(** source Uppaal-file, which shall be pretty printed.  *)

val ppuppaal_transition_details : bool ref

(** TODO: docu *)

val ppuppaalprintquery : bool ref
val ppuppaalprintquery_nta : string ref  
val ppuppaalprintquery_query : string ref

(**
  {3 Example sub modules.}
*)

val defineconstant : bool ref
(**
  Indicates if the [transform define] subcommand was specified.
*)
val defineconstant_varname : string ref
val defineconstant_exprstring : string ref

val globalize : bool ref
(**
  Indicates if the [transform define] subcommand was specified.
*)
val globalize_source : string ref
val globalize_separator : string ref


val explicatesystem : bool ref
(**
  Indicates if the [transform explicatesystem] subcommand was
  specified.
*)
val es_separator : string ref 
val es_queryfile_in : string ref 
val es_queryfile_out : string ref 

val maxstep : bool ref
(**
  Indicates if the [transform maxstep] subcommand was specified.
*)
val maxstep_meta : bool ref
val maxstep_step : string ref
val maxstep_step_max : string ref
val maxstep_queryfile_out : string ref
val maxstep_maxsteps : int ref

val makelocalclocks : bool ref
(**
  Indicates if the [transform makelocalclocks] subcommand was specified.
*)
val makelocalclocks_clocks : string ref
(**
  Specifies a comma-separated list of (globally declared) clock ID's to 
  turn into local clocks in the given input model.
*)

val split_disjunctions : bool ref
(**
  Indicates if the [transform splitdisjunctions] subcommand was specified.
*)

val propagate_constants : bool ref
(**
  Indicates if the [transform propagate_constants] subcommand was
  specified.
*)

val propagate_constants_out : string ref
(** Specifies where we want to print the output of this subcommand *)

val colorize : bool ref
(**
  Indicates if the [transform colorize] subcommand was
  specified.
*)
val colorize_ids : string ref
(**
  Specifies a comma-separated list of transition ID's of transitions to colorize
  in the given input model.
*)

val colorize_color : string ref
(**
  Specifies the colour to colorize edges with, default is #ff0000, i.e. red
*)

val removeedges : bool ref
(**
  Indicates if the [transform removeEdges] subcommand was
  specified.
*)
val removeedges_ids : string ref
(**
  Specifies a comma-separated list of transition ID's of transitions to remove
  in the given input model.
*)

val randomquery : bool ref
(**
  Indicates if the [generate randomquery] subcommand was
  specified.
*)

val random_query_seed : int ref
(**
  Specifies the seed to use for the random component.
  (default value '-1' indicates random seed chosen in system-dependent way)
*)

val random_query_no_queries : int ref
(**
  Specifies how many locations should maximally be used in one 
  reachability query.
*)

val random_query_max_locs : int ref
(**
  Specifies how many minimal expressions (e.g. P.l for Process P and 
  respective Location l of this Process) one query should maximally contain. 
  Note that the real number can be lower. *)

val addtransitionids : bool ref
(**
  Indicates if the [transform add_transition_ids] subcommand was specified.
*)


val addlocationnames : bool ref
(**
  Indicates if the [transform add_location_names] subcommand was specified.
*)

val ppuppaalexampletransformation : bool ref
(**
  Indicates if the [transform example_transformation] subcommand was
  specified.
*)




val undeclaredvars : bool ref
(** Indicates if the [undeclaredvars] subcommand was specified. *)

val undeclaredvars_indent : int ref
(**
  Specifies the number of spaces used to indent the trace to the undeclared 
  variable.
*)
val undeclaredvars_line_width : int ref
(**
  Specifies the maximum number of chars of a single line of trace output.
*)
val undeclaredvars_max_print_chars : int ref
(**
  Specifies the maximum number of chars to print of expressions before
  abbreviating the rest. Example: 
      - If undeclaredvars_max_print_chars = 3 then:
          Guard (x >= 3 && y < 2) is turned into:
          Guard "[x >= ...]"
        in the resulting trace
*)

