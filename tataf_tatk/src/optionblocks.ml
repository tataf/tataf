(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libcore.CmdLine
open Options

(* Seems as if

    validate_empty_or_file_exists "<FLAG>" !<VARIABLE>

   also exists; seems that it checks that the argument to <FLAG> is given and
   that the denoted file does not exist; not used any more because we are
   following the standard UNIX tool convention: do what the user requested, 
   if the given file is there, it's overwritten (of course).
*)

(*----------------------------------------------------------------------------*)
(*- utataf -------------------------------------------------------------------*)
(*----------------------------------------------------------------------------*)

(*----------------------------------------------------------------------------*)
(*- dump -> paths ------------------------------------------------------------*)

let opt_analysepaths : optionblock = "paths",
  "[OPTION]... SOURCE TEMPLATE ID",
  SCSet analysepaths,
  "Enumerate all paths up to maximal length ending with transition ID in the
  given Uppaal model.  
   Nowadays basically used with --depth 1 to print invariants and guards in 
   prefix normal form.",
  [("-d","--depth","DEPTH", Arg.Set_int analysepaths_depth,
    "Set the maximum path length to DEPTH.  Defaults to 3.");
  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string analysepaths_source,
     "Required: Specifies the source Uppaal file.  This file must be in XTA 4
     (Xml format).");
    ("TEMPLATE", Arg.Set_string analysepaths_template,
     "Required: Specifies the template name.");  
    ("ID", Arg.Set_string analysepaths_startTransition,
     "Required: Specifies the id of the transition with which the enumerated
     paths should end.");  
  ]),
  (fun () ->
    validate_notempty "SOURCE" !analysepaths_source;
    validate_notempty "TEMPLATE" !analysepaths_template;
    validate_notempty "ID" !analysepaths_startTransition
  ),
  [("Fischer protocol with transition IDs (from supporting edge paper):",
	  "dump paths examples/Uppaal/SupportingEdges/OrFischer4DD_2.xml Process t6");]

(*- dump -> symboltable ------------------------------------------------------*)

let opt_dumpsymboltable : optionblock = "symboltable",
  "SOURCE",
  SCSet dumpsymboltable,
  "Print symbol-table.",
  [],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []

(*- dump -> transitions ------------------------------------------------------*)

let opt_dumptransitions : optionblock = "transitions",
  "SOURCE [TEMPLATE]",
  SCSet dumptransitions,
  "Print all transitions of an Uppaal model (obsolete, see 'dump paths').",
  [],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string dumptransitions_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
    ("TEMPLATE", Arg.Set_string dumptransitions_template,
     "Optional: Consider only transitions of the specified template.");  
  ]),
  (fun () ->
    validate_notempty "SOURCE" !dumptransitions_source;
  ),
  []

(*- dump ---------------------------------------------------------------------*)

let opt_dump : optionblock = "dump",
  "[OPTION]... MODULE [...]",
  SCNothing,
  "Dump various information.",
  [],
  Subcommands([
    opt_analysepaths;
    opt_dumpsymboltable;
    opt_dumptransitions;
  ]),
  (fun () ->
    if !analysepaths
    || !dumpsymboltable
    || !dumptransitions 
      then () 
      else raise (InvalidOption("dump","No information to dump specified."))
  ),
  []

(*----------------------------------------------------------------------------*)
(*- analyze ------------------------------------------------------------------*)

(*- analyze -> undeclaredvars ------------------------------------------------*)


let opt_undeclaredvars : optionblock = "undeclaredvars",
  "[OPTION]... SOURCE",
  SCSet undeclaredvars,
  "Raise warnings for undeclared variables in the given Uppaal model.",
  [("--indent","","", Arg.Set_int undeclaredvars_indent,
    "Number of chars to indent the trace output. (Default: 2)");
   ("--line-width","","", Arg.Set_int undeclaredvars_line_width,
    "Maximum number of chars of a line of trace output. (Default: 80)");
   ("--exp-width","","", Arg.Set_int undeclaredvars_max_print_chars,
    "Maximum number of chars to print of an expression (e.g. a guard) in a "
    ^ "trace before abbreviating the rest of it. (Default: 10)");
  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []

(*- analyze ------------------------------------------------------------------*)

let opt_analyze : optionblock = "analyze",
  "[OPTION]... MODULE [...]",
  SCNothing,
  "Various analyses.",
  [],
  Subcommands([
    opt_undeclaredvars;
  ]),
  (fun () ->
    if !undeclaredvars
      then () 
      else raise (InvalidOption("analyze","No analysis specified."))
  ),
  []

(*----------------------------------------------------------------------------*)
(*- print --------------------------------------------------------------------*)

let opt_ppuppaal : optionblock = "print",
  "[OPTION]... SOURCE",
  SCSet ppuppaal,
  "Prettyprint the given Uppaal model.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");
   ("","--transition-details","", Arg.Set ppuppaal_transition_details,
    "Instead of printing in XTA4 format, print out the symbol_table creation.");
   ("-w","--width","WIDTH", Arg.Set_int Tataf_libta.LibtaOptions.ppuppaal_xta4_linewidth,
    "Wrap annotations of edges and locations after WIDTH characters (default
    0, no linewrap)." );
  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
    validate_gteq "width" !Tataf_libta.LibtaOptions.ppuppaal_xta4_linewidth 0;
  ),
  [("Just pretty print Uppaal model:",
    "examples/Uppaal/demo/fischer.xml");
  ]

(*----------------------------------------------------------------------------*)
(*- printquery ---------------------------------------------------------------*)

let opt_ppuppaalprintquery : optionblock = "printquery",
  "[OPTION]... SOURCE",
  SCSet ppuppaalprintquery,
  "Prettyprint Uppaal Query file.",
  [("","--model","", Arg.Set_string ppuppaalprintquery_nta,
    "Specifies the corresponding Uppaal model for the query file. This is
    sometimes necessary to resolve custom type definitions.")  
  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaalprintquery_query,
     "Required: Specifies the source query file (usually a .q-file).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaalprintquery_query;
  ),
  [("Just pretty print Uppaal query (needs model in this case):",
    "--model examples/Uppaal/demo/fischer.xml examples/Uppaal/demo/fischer.q");]  

(*----------------------------------------------------------------------------*)
(*- transform -> addlocationnames --------------------------------------------*)

let opt_addlocationnames : optionblock = "addlocationnames",
  "[OPTION]... SOURCE IDS",
  SCSet addlocationnames,
  "Adds a fresh, unused name to each location in the resulting XML (leaves
  existing location names unchanged).",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []  
  
(*- transform -> add_transition_ids ------------------------------------------*)

let opt_addtransitionids : optionblock = "addtransitionids",
  "SOURCE",
  SCSet addtransitionids,
  "Add a unique id to each transition node in the resulting XML.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []  


(*- transform -> colorize ----------------------------------------------------*)

let opt_colorize : optionblock = "colorize",
  "[OPTION]... SOURCE IDS",
  SCSet colorize,
  "Colorize edges specified by their ID's.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout)."); 
      ("","--color", "COLOR", Arg.Set_string colorize_color,
    "Hexadecimal color code specifying the color
    used to colorize pre-/post delayed edges (default: 0xff0000, i.e. red).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
    ("IDS", Arg.Set_string colorize_ids,
     "Required: Specifies the comma-seperated list of ids of the edges to
     colorize in the given model.");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
    validate_notempty "IDS" !colorize_ids;
  ),
  []  

(*- transform -> define ------------------------------------------------------*)

let opt_defineconstant : optionblock = "define",
  "[OPTION]... SOURCE VARNAME EXPR",
  SCSet defineconstant,
  "Preprocessor-like definition of the initializer value 
  of constants in a given XML model. Anything that can be parsed as Uppaal
  Expression can be used as initializer.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
    ("VARNAME", Arg.Set_string defineconstant_varname,
     "Required: Specifies the (template-qualified) name of the constant
     variable the preprocessor should transform.");
    ("EXPR", Arg.Set_string defineconstant_exprstring,
     "Required: value of the new initializer for VARNAME as
     Tataf_libta.UppaalSystem.Expression-parsable expression.");
  ]),
  (fun () ->
    validate_notempty "SOURCE"  !ppuppaal_source;
    validate_notempty "VARNAME" !defineconstant_varname;
    validate_notempty "EXPR"    !defineconstant_exprstring;
  ),
  [("Initialise constant 'slowest' with value 27 (instead of original 25):",
    "slowest 27 examples/Uppaal/demo/bridge.xml");]  

(*- transform -> example_transformation --------------------------------------*)

let opt_ppuppaalexampletransformation : optionblock = "example_transformation",
  "SOURCE",
  SCSet ppuppaalexampletransformation,
  "Example how to transform an Uppaal model (does not do anything useful).",
  [],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []  

(*- transform -> explicatesystem ---------------------------------------------*)

let opt_explicatesystem : optionblock = "explicatesystem",
  "SOURCE",
  SCSet explicatesystem,
  "Unfold template names in system declaration into process declarations.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");
   ("-p","--queryout", "FILENAME", Arg.Set_string es_queryfile_out,
    "Filename for the transformed query output (only effective with '-q').");
   ("-q","--query", "FILENAME", Arg.Set_string es_queryfile_in,
    "Query-File for the given Uppaal model.  If this option is used, an output
     filename for the transformed query must be specified with '-p'.");
   ("-S","--separator","SEP", Arg.Set_string es_separator,
    "Separate template name and suffix with SEP (default: _).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
    if !es_queryfile_in <> "" && !es_queryfile_out = "" then
      raise (InvalidOption("-p","Output filename for the transformed query missing."));
    if !es_queryfile_out <> "" && !es_queryfile_in = ""  then
      raise (InvalidOption("-q","Query file missing."))
  ),
  []  

(*- transform -> flatten arrays ----------------------------------------------*)

let opt_flattenarrays : optionblock = "flattenarrays",
  "[OPTION]... SOURCE",
  SCSet flattenarrays,
  "Flatten arrays to normal variables of the same type in the given Uppaal model.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");]
      ,
  AnonymousParameters([
    ("SOURCE", Arg.Set_string flattenarrays_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !flattenarrays_source;
  ),
  (* TODO: Add examples below as soon as they exist.. *)
  []

(*- transform -> flatten custom types ----------------------------------------*)

let opt_flattencustomtypes : optionblock = "flattencustomtypes",
  "[OPTION]... SOURCE",
  SCSet flattencustomtypes,
  "Flatten custom type definitions in the given Uppaal model.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");
    ("-r","--remove","", Arg.Set flattencustomtypes_remove_typedefs,
    "Remove all (now uneeded) typedefs from the resulting model.");],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []

(*- transform -> globalize ---------------------------------------------------*)

let opt_globalize : optionblock = "globalize",
  "[OPTION]... SOURCE",
  SCSet globalize,
  "Move local variables to global scope (prefixed with template name).
  Template name, separator, and old name must form a system-wide fresh name.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");
   ("-S","--separator","SEP", Arg.Set_string globalize_separator,
    "Separate template name and original name with SEP (default: _).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string globalize_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !globalize_source;
  ),
  []

(*- transform -> makelocalclocks ---------------------------------------------*)

let opt_makelocalclocks : optionblock = "makelocalclocks",
  "SOURCE",
  SCSet makelocalclocks,
  "Turn given list of globally declared clocks into list of local clocks (one 
   for each template using the globally declared clock).",
   [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
    ("CLOCKS", Arg.Set_string makelocalclocks_clocks,
     "Required: Comma-seperated list of clock ids.");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
    validate_notempty "CLOCKS" !makelocalclocks_clocks;
  ),
  []  

(*- transform -> maxstep -----------------------------------------------------*)

let opt_max_step : optionblock = "maxstep",
  "SOURCE",
  SCSet maxstep,
  "Add guards and updates which enforce a limited number of step.",
  [("-m","--meta","", Arg.Set maxstep_meta,
    "Declare step counter as 'meta' (default: not meta).");
   ("-N","--maxsteps","NUM", Arg.Set_int maxstep_maxsteps,
    "Initialise step bound with NUM (default: 0).");
   ("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");
   ("-p","--queryout", "FILENAME", Arg.Set_string maxstep_queryfile_out,
    "Generate a query and write it to FILENAME.");
   ("-s","--step", "NAME", Arg.Set_string maxstep_step,
    "Call step counter NAME (default: step).");
   ("-x","--stepmax", "NAME", Arg.Set_string maxstep_step_max,
    "Call step bound NAME (default: STEP_MAX).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []  

(*- transform -> propagateconstants ------------------------------------------*)

let opt_propagate_constants : optionblock = "propagateconstants",
  "SOURCE",
  SCSet propagate_constants,
  "Replace constant variables by their value in the given source model
   and (after replacement) pre-evaluate expressions involving only literal
   values.  The pre-evaluation supports the most common operators (like
   addition) and some, e.g., associativity rules, but not all.",
  [("-o","--output","FILENAME", Arg.Set_string propagate_constants_out,
    "Write result to FILENAME (instead of stdout).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  []  

(*- transform -> removeedges -------------------------------------------------*)

let opt_removeedges : optionblock = "removeedges",
  "[OPTION]... SOURCE IDS",
  SCSet removeedges,
  "Remove edges specified by their ID's.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout)."); 
  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
    ("IDS", Arg.Set_string removeedges_ids,
     "Required: Specifies the comma-seperated list of ids of the edges to
     remove in the given model.");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
    validate_notempty "IDS" !removeedges_ids;
  ),
  []  

(*- transform -> split clock disjunctions ------------------------------------*)

let opt_split_disjunctions : optionblock = "splitdisjunctions",
  "[OPTION]... SOURCE",
  SCSet split_disjunctions,
  "Transform disjunctive clock constraints into multiple edges or locations.",
  [("-o","--output","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).")],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file for the simulated network.
     This file must be in XTA 4 (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
  ),
  (* TODO: Add examples below as soon as they exist.. *)
  []

(*- generate -> randomquery ------------------------------------------*)

let opt_randomquery : optionblock = "randomquery",
  "[OPTION]... SOURCE",
  SCSet randomquery,
  "Generate random queries for sanity checking of transformations.",
  [("-o","","FILENAME", Arg.Set_string Tataf_libta.LibtaOptions.ppuppaal_out,
    "Write result to FILENAME (instead of stdout).");
   ("-s", "--seed", "SEED", Arg.Set_int random_query_seed,
    "Use specific seed. '-1' (default) for random seed chosen in 
     system-dependent way.");
    ("-q", "--queries", "NO_QUERIES", Arg.Set_int random_query_no_queries,
     "Specifies how many individual queries should be generated.");
    ("-l", "--max-length", "MAX_LENGTH", Arg.Set_int random_query_max_locs,
     "Specifies how many minimal expressions (e.g. P.l for Process P and 
  respective Location l of this Process) one query should maximally contain. 
  Note that the real number is randomized.");

  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string ppuppaal_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).")
  ]),
  (fun () ->
    validate_notempty "SOURCE" !ppuppaal_source;
    validate_notempty "NO_QUERIES" (string_of_int(!random_query_no_queries));
    validate_notempty "MAX_EXP_PER_QUERY"
                      (string_of_int(!random_query_max_locs));
  ),
  []
(*----------------------------------------------------------------------------*)

(* - generate ----------------------------------------------------------------*)
let opt_generate : optionblock = "generate",
  "MODULE [...]",
  SCNothing,
  "Miscelleanous generation commands.",
  [],
  Subcommands([opt_randomquery;
  ]),
  (fun () ->
    if !randomquery
      then () 
      else raise (InvalidOption("generate","No generation specified."))
  ),
  []

(*----------------------------------------------------------------------------*)



(*- transform ----------------------------------------------------------------*)

let opt_transform : optionblock = "transform",
  "MODULE [...]",
  SCNothing,
  "Miscelleanous transformations.",
  [],
  Subcommands([
    opt_addtransitionids;
    opt_addlocationnames;
    opt_colorize;
    opt_defineconstant;
    opt_ppuppaalexampletransformation;
    opt_explicatesystem;
    opt_flattenarrays;
    opt_flattencustomtypes;
    opt_globalize;
    opt_max_step;
    opt_makelocalclocks;
    opt_propagate_constants;
    opt_removeedges;
    opt_split_disjunctions;
  ]),
  (fun () ->
    if !addtransitionids
    || !addlocationnames
    || !colorize
    || !defineconstant
    || !ppuppaalexampletransformation
    || !explicatesystem
    || !flattenarrays
    || !flattencustomtypes
    || !globalize
    || !maxstep
    || !makelocalclocks
    || !propagate_constants
    || !removeedges
    || !split_disjunctions
      then () 
      else raise (InvalidOption("transform","No transformation specified."))
  ),
  []

(*----------------------------------------------------------------------------*)
(*- utataf options -----------------------------------------------------------*)

(* Reference of user input; used for validity check *)
let utataf_debug_modules = ref ""
let opt_utataf : optionblock = "",
  "[OPTION]... SUBCOMMAND [...]",
  SCNothing,
  "tataf for Uppaal is a tool capable to work with networks of timed automata.",
  [
   ("","--debuglevel", "LEVEL", Arg.Int (fun debuglevel -> 
      Tataf_libcore.Debug.set_debug_level debuglevel
    ),
    "Sets the verbosity of the debug messages.  Higher is more.  
     Only effective together with --debugmodules.
     ");    
    ("","--debugmodules", "M[,M]",  Arg.String (fun modules ->
      (* Keep track of modules requested on the commandline for 
       * validity check below. *)
      utataf_debug_modules := modules;
      if modules = ""
      then Tataf_libcore.Debug.debugOn := false
      else Tataf_libcore.Debug.debugOn := true;
           Tataf_libcore.Debug.set_debug_modules modules
    ),
    "Output debugging information for specified modules (listed below).  The
     modules must be given as comma seperated list.  Supported modules: "
     ^ Tataf_libcore.Debug.registered_debug_modules ());
   ("-D","--default-dtd","VERSION", Arg.Set_string Tataf_libta.LibtaOptions.dtd_version_string,
    "Parse wrt. DTD version VERSION if the given XTA file does not specify one.
     Accepted values: 1.1 (default), 1.2.");
   ("","--halt-on-warning","", Arg.Set Tataf_libcore.Debug.halt_on_warning,
    "If given, the program will immediately halt if a warning occurs.");
   ("","--nocolor","", Arg.Clear Tataf_libcore.Color.colors_enabled,
    "Disables ANSI escape codes in console output.");    
   ("-v","--verbose","", Arg.Set Tataf_libta.LibtaOptions.verbose,
    "explain what is being done.");
   (*-----------------*)
   ("","--help","", Arg.Unit (fun () -> raise (Arg.Help "command line argument") ),
    "Display the best matching help page.  This flag is always applicable.");
   ("","--version","", Arg.Unit
	 		(fun () -> print_endline TatkVersion.version_string;
			           print_endline Tataf_libtatk.LibtatkVersion.version_string;
			           print_endline Tataf_libta.LibtaVersion.version_string;
			           print_endline Tataf_libcore.LibcoreVersion.version_string;
								 exit 0 ),
    "Display version information and exit.");
  ],
  Subcommands([
    opt_analyze;
    opt_dump;
    opt_generate;
    opt_ppuppaal;
    opt_ppuppaalprintquery;
    opt_transform;
    opt_help;
  ]),
  (* "validate_modules_exist" validates that all modules requested by the user 
   * actually exist (are registered) in the Debugging Module. *)
  (fun () ->
    validate_modules_exist 
    "MODULES" !utataf_debug_modules (Tataf_libcore.Debug.registered_debug_modules ());
    let open Tataf_libta.UppaalSystem.DtdVersion in
    match !Tataf_libta.LibtaOptions.dtd_version_string with
    | ""    -> ()
    | "1.1" -> Tataf_libta.LibtaOptions.dtd_version := DTD_1_1 ()
    | "1.2" -> Tataf_libta.LibtaOptions.dtd_version := DTD_1_2 ()
    | _     -> raise_invalid_option "-D"
                 ("'" ^ !Tataf_libta.LibtaOptions.dtd_version_string ^ "': invalid DTD version")
  ),
  [("Output debug information for the workflow of the Uppaal module:",
    "--debuglevel 1 --debugmodules UppaalPrint,UppaalParser ...");
   ("Disable ANSI escape codes:",
    "--nocolor ...");
   ("Display how the help system works:",
    "help");
  ]

