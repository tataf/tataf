(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

(* utataf *********************************************************************)
(******************************************************************************)

(* analyze * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

let undeclaredvars () =
    Tataf_libtatk.UndeclaredVariables.check_undeclared_variables_external
      !Options.ppuppaal_source
      !Options.undeclaredvars_indent
      !Options.undeclaredvars_line_width
      !Options.undeclaredvars_max_print_chars

(* dump  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

let analysepaths () =
  Tataf_libtatk.PathAnalysis.analysePaths 
    !Options.analysepaths_source
    !Options.analysepaths_template
    !Options.analysepaths_startTransition
    !Options.analysepaths_depth

let dump_symboltable () =
  Tataf_libta.LibtaOptions.ppuppaal_symbol_table := true;
  Tataf_libta.PrettyPrintUppaal.ppuppaal !Options.ppuppaal_source

let dump_transitions () =
  Tataf_libtatk.PrintTransitions.print_all_transitions
    !Options.dumptransitions_source
    !Options.dumptransitions_template  

(* print * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

let ppuppaal () = Tataf_libta.PrettyPrintUppaal.ppuppaal !Options.ppuppaal_source

(* printquery  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

let ppuppaal_query () =
  let stable = 
    if !Options.ppuppaalprintquery_nta <> "" then
    begin
      let nta = Tataf_libta.UppaalParser.parse_xta4_file !Options.ppuppaalprintquery_nta in
      let walker = new Tataf_libta.UppaalWalker.unit_walker in
      walker#visit nta;
      match walker#global_frame with
      | None -> new Tataf_libta.SymbolTable.frame None
      | Some gf -> gf
    end
    else 
      new Tataf_libta.SymbolTable.frame None
  in
  let queries = Tataf_libta.UppaalParser.parse_queries_file 
    ~symbol_table:stable 
    !Options.ppuppaalprintquery_query
  in
  (new Tataf_libta.UppaalPrint.query_printer)#visit_queries queries |> print_endline

(* generate * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
let random_query () =
  Tataf_libtatk.RandomQueryGenerator.generate_random_queries
    ~seed:!Options.random_query_seed
    ~no_queries:!Options.random_query_no_queries
    ~max_locs:!Options.random_query_max_locs
    !Options.ppuppaal_source
    !Tataf_libta.LibtaOptions.ppuppaal_out

(* transform * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

let add_location_names () =
  Tataf_libtatk.AddLocationNames.generate_location_names
    !Options.ppuppaal_source
    !Tataf_libta.LibtaOptions.ppuppaal_out

let add_transition_ids () =
  Tataf_libtatk.AddTransitionIds.generate_transition_ids
    !Options.ppuppaal_source
    !Tataf_libta.LibtaOptions.ppuppaal_out

let colorize() =
    let id_string = !Options.colorize_ids in
    let id_list = Str.split (Str.regexp_string(",")) id_string in
    Tataf_libtatk.Colorize.colorize_system 
        !Options.ppuppaal_source
        !Tataf_libta.LibtaOptions.ppuppaal_out
        id_list
        !Options.colorize_color

let define_constant() =
	Tataf_libtatk.DefineConstant.define_constant_system 
                   !Options.ppuppaal_source !Tataf_libta.LibtaOptions.ppuppaal_out
                   !Options.defineconstant_varname
                   !Options.defineconstant_exprstring

let ppuppaal_example_transformation () =
  Tataf_libta.ExampleTransformation.example_transformation
    !Options.ppuppaal_source

let explicate_system () =
  Tataf_libtatk.ExplicateSystem.explicate_system
    ~query:!Options.es_queryfile_in ~queryout:!Options.es_queryfile_out
    ~separator:!Options.es_separator
    !Options.ppuppaal_source !Tataf_libta.LibtaOptions.ppuppaal_out

let flattenarrays () =
    let new_nta = 
        Tataf_libtatk.Flattenarrays.flattenArrays 
        !Options.flattenarrays_source
    in
    let ntaString = (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta new_nta
    in
    let _ = print_string ntaString in ()

let flattencustomtypes () =
    Tataf_libtatk.FlattenCustomTypes.flatten_custom_types 
        !Options.ppuppaal_source
        !Tataf_libta.LibtaOptions.ppuppaal_out
        !Options.flattencustomtypes_remove_typedefs

let globalize () =
  Tataf_libtatk.Globalize.globalize !Options.globalize_separator
                      !Options.globalize_source !Tataf_libta.LibtaOptions.ppuppaal_out

let max_step() =
	Tataf_libtatk.MaxStep.max_step !Options.maxstep_meta
                   !Options.maxstep_step !Options.maxstep_step_max
                   !Options.maxstep_maxsteps
                   !Options.maxstep_queryfile_out
                   !Options.ppuppaal_source !Tataf_libta.LibtaOptions.ppuppaal_out

let make_local_clocks() =
        !Options.makelocalclocks_clocks
        |>Str.split (Str.regexp_string(",")) 
        |> Tataf_libtatk.Makelocalclocks.makelocal_clock_system
                   !Options.ppuppaal_source !Tataf_libta.LibtaOptions.ppuppaal_out

let propagate_constants() =
    Tataf_libta.UppaalParser.parse_xta4_file !Options.ppuppaal_source 
    |> (new Tataf_libtatk.Simplifier.simplifier
        ~evaluate_exp:true ~constant_propagation:true ())#transform
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file !Options.propagate_constants_out

let split_clock_disjunctions () =
  Tataf_libtatk.SplitDisjunctions.split_disjunctions_system !Options.ppuppaal_source
                                              !Tataf_libta.LibtaOptions.ppuppaal_out


(* main ***********************************************************************)
(******************************************************************************)

let main() =  
  Tataf_libcore.CmdLine.program_cmd := Sys.argv.(0) |> Filename.basename;
  Tataf_libcore.CmdLine.help_examples := true;
  try begin
    Tataf_libcore.CmdLine.process Optionblocks.opt_utataf;
    (* dump *)
    if !Options.analysepaths        then analysepaths ();
    if !Options.dumpsymboltable     then dump_symboltable ();
    if !Options.dumptransitions     then dump_transitions ();
    (* print *)
    if !Options.ppuppaal            then ppuppaal ();
    (* printquery *)
    if !Options.ppuppaalprintquery  then ppuppaal_query ();
    (* transform *)
    if !Options.addlocationnames    then add_location_names ();
    if !Options.addtransitionids    then add_transition_ids ();
    if !Options.colorize            then colorize();
    if !Options.defineconstant      then define_constant ();
    if !Options.ppuppaalexampletransformation
                                    then ppuppaal_example_transformation ();
    if !Options.explicatesystem     then explicate_system ();
    if !Options.flattenarrays       then flattenarrays();
    if !Options.flattencustomtypes  then flattencustomtypes();
    if !Options.globalize           then globalize();
    if !Options.makelocalclocks     then make_local_clocks ();
    if !Options.maxstep             then max_step ();
    if !Options.propagate_constants then propagate_constants ();
    if !Options.randomquery         then random_query ();
    if !Options.split_disjunctions  then split_clock_disjunctions();
    if !Options.undeclaredvars      then undeclaredvars ();
  end with 
  | Arg.Help(_) -> () (* silently quit *)
;;

(* exception handling *********************************************************)
(******************************************************************************)

let print_exception2 exn =
  let print_exn s = output_string stderr (s ^ "\n"); flush stderr in 
  match exn with
  | Tataf_libcore.CmdLine.InvalidOption(arg,msg) ->
    "Invalid Argument '" ^ arg ^ "': " ^ msg 
    |> Tataf_libcore.Color.red |> print_exn; 1 
  | Invalid_argument(msg) -> msg |> Tataf_libcore.Color.red |> print_exn ; 2
  | Xml.Error(msg,pos) ->
      "Xml.Error:  " ^ Xml.error( msg, pos )
      |> Tataf_libcore.Color.red
      |> print_exn; 10
  | Dtd.Prove_error(error) -> 
      let open Dtd in
      let msg = match error with
        | UnexpectedPCData -> "unexpected PCDATA"
        | UnexpectedTag(s) -> "unexpected Tag: '" ^ s ^ "'" 
        | UnexpectedAttribute(s) -> "unexpected Attribute: '" ^ s ^ "'" 
        | InvalidAttributeValue(s) -> "invalid Attribute value: '" ^ s ^ "'"
        | RequiredAttribute(s) -> "Required Attribute: '" ^ s ^ "'"
        | ChildExpected(s) -> "child expected: '" ^ s ^ "'"
        | EmptyExpected -> "empty element expected"
        | DuplicateID(s) -> "duplicate id: '" ^ s ^ "'"
        | MissingID(s) -> "missing id: '" ^ s ^ "'" 
      in
      "Dtd.Prove_error -- " ^ msg
      |> Tataf_libcore.Color.red
      |> print_exn; 10
  | Tataf_libta.UppaalParser.Parse_error(p1,p2,msg,s) ->
      let open Lexing in
      let hs = Tataf_libcore.Color.highlight_string Tataf_libcore.Color.red p1.pos_cnum p2.pos_cnum s in
      let line = string_of_int p1.pos_lnum in
      let offset = string_of_int (p1.pos_cnum - p1.pos_bol + 1) in
      "UppaalParser.Parse_error in '" ^ 
          p1.pos_fname ^ "' at position " ^ 
          line ^ "; " ^ offset ^ " (" ^ (string_of_int p1.pos_cnum) ^ ")" ^
          " -- " ^ msg
      |> Tataf_libcore.Color.red
      |> fun x -> x ^ "\n" ^ hs
      |> print_exn ; 11
  | Tataf_libcore.CmdLine.ValidationError msg -> 
      "ValidationError: " ^ msg |> Tataf_libcore.Color.red |> print_exn ; 2
  | Failure(msg) -> "Failure: '" ^ msg ^ "'" |> Tataf_libcore.Color.red |> print_exn; 254
  (*---------*)
  | e -> "An unknown exception was thrown!\n" |> print_exn;
        (* use ignore to avoid warning 21 - function does not return *)
        ignore (raise e);
        (* Is this print_backtrace reachable at all?  After raise? *)
        Printexc.print_backtrace stderr; 250
;;

let print_exception ex = print_exception2 ex |> ignore;;

(* toplevel *******************************************************************)
(******************************************************************************)

try main (); exit 0 with ex -> print_exception2 ex |> exit
;;

