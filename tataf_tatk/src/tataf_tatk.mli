(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  Main entry point of tataf.
  
  If started this module is loaded from an interactive shell, then the main
  function is not called automatically. This allows to build a toplevel with
  all tataf-modules preloaded.
*)

(** 
  Main entry point.
*)
val main : unit -> unit

(**
  [print_exceptions ex] pretty prints the exception [ex] and 
  returns the return code for this exception. Possible return values are:
  - 0: no error occurred.
  - 1: an unkonwn exception was thrown
  - 2: [Invalid_argument] occurred. Error in command line?
  - 10: Error in XML parsing (Xml.Error or Dtd.Prove_error)
  - 11: {!UppaalParser.Parse_error} occurred.
  
  This is useful to use in the toplevel, to get nicer exception output.
*)
val print_exception2 : exn -> int

(**
  Same as {!print_exception2}, but does not return a code.
*)
val print_exception : exn -> unit
