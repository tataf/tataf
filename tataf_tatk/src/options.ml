(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

(*----------------------------------------------------------------------------*)
(*- utataf -------------------------------------------------------------------*)
(*----------------------------------------------------------------------------*)

(*----------------------------------------------------------------------------*)
(*- enumeratepaths -----------------------------------------------------------*)

let analysepaths = ref false
let analysepaths_depth = ref 3
let analysepaths_source = ref ""
let analysepaths_template = ref ""
let analysepaths_startTransition = ref ""

(*----------------------------------------------------------------------------*)
(*- dump ---------------------------------------------------------------------*)

(*- dump > symboltable -------------------------------------------------------*)

(* sets 'ppuppaal_source' for historical reasons: implemented in pretty
   printer *)
let ppuppaal_source = ref ""

let dumpsymboltable = ref false  

(*- dump > transitions -------------------------------------------------------*)

let dumptransitions_source = ref ""
let dumptransitions_template = ref ""

let dumptransitions = ref false  

(*----------------------------------------------------------------------------*)
(*- print --------------------------------------------------------------------*)

let ppuppaal = ref false
(* ppuppaal_source declared within dump! *)

let ppuppaal_transition_details = ref false

(*----------------------------------------------------------------------------*)
(*- printquery ---------------------------------------------------------------*)

let ppuppaalprintquery = ref false
let ppuppaalprintquery_nta = ref ""  
let ppuppaalprintquery_query = ref ""

(*----------------------------------------------------------------------------*)
(*- transform > addtransitionids ---------------------------------------------*)

let addtransitionids = ref false  

(*- transform > addlocationnames -------------------------------------------*)
let addlocationnames = ref false  

(*- transform > colorize -----------------------------------------------------*)

let colorize = ref false
let colorize_ids = ref ""
let colorize_color = ref "#ff0000" (* default color = red *)

(*- transform > define ---------- -------------------------------------------*)

let defineconstant = ref false  
let defineconstant_varname = ref ""
let defineconstant_exprstring = ref ""

(*- transform > example_transformation ---------------------------------------*)

let ppuppaalexampletransformation = ref false  

(*- transform > explicatesystem ----------------------------------------------*)

let explicatesystem = ref false  
let es_separator = ref "_"
let es_queryfile_in = ref ""
let es_queryfile_out = ref ""
(*----------------------------------------------------------------------------*)
(*- transform > flattenarrays ------------------------------------------------*)

let flattenarrays = ref false
let flattenarrays_source = ref ""

(*----------------------------------------------------------------------------*)
(*- transform > flattencustomtypes -------------------------------------------*)

let flattencustomtypes = ref false
let flattencustomtypes_remove_typedefs = ref false

(*- transform > globalize ---------------------------------------------------*)

let globalize = ref false  
let globalize_source = ref ""
let globalize_separator = ref "_"

(*- transform > maxstep ------------------------------------------------------*)

let maxstep = ref false
let maxstep_meta = ref false
let maxstep_step = ref ""
let maxstep_step_max = ref ""
let maxstep_queryfile_out = ref ""
let maxstep_maxsteps = ref 0

(*- transform > makelocalclocks ----------------------------------------------*)

let makelocalclocks = ref false
let makelocalclocks_clocks = ref ""

(*- transform > propagate_constants ------------------------------------------*)

let propagate_constants = ref false
let propagate_constants_out = ref ""

(*- transform > splitclockdisjunctions ---------------------------------------*)
let split_disjunctions = ref false

(*- transform > removeedges --------------------------------------------------*)

let removeedges = ref false
let removeedges_ids = ref ""

(*- generate > randomquery --------------------------------------------------*)
let randomquery = ref false
let random_query_seed = ref (-1)
let random_query_no_queries = ref 1
let random_query_max_locs = ref 2

(*----------------------------------------------------------------------------*)
(*- undeclaredvars --------------------------------------------------------------------*)
let undeclaredvars = ref false
let undeclaredvars_indent = ref 2
let undeclaredvars_line_width = ref 80
let undeclaredvars_max_print_chars = ref 10

