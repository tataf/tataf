
The following build instructions are for a clean environment, i.e., without
opam installed.  Install opam by, e.g., following the instructions over here:
https://opam.ocaml.org/doc/Install.html

`$ opam init --bare --disable-sandboxing --no-setup`  
`$ opam switch create 4071 4.07.1`  
`$ opam switch 4071`  
``$ eval `opam env` ``  
`$ opam update `  
`$ opam upgrade`  
`$ opam install ocamlfind.1.8.0 xml-light.2.4 ocaml-makefile.6.39.2 dune.1.6.3 odoc.1.4.0 utop.2.4.0`  
`$ git clone https://gitlab.com/tataf/tataf`  
`$ cd tataf`  
`$ ./build.sh`  

