(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Template
open Tataf_libta.UppaalSystem.XY

class fix_layout_simple nta = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable

    val mutable location_positions = []
    val mutable location_ids = []

    (* HashTable storing the positions of all edge nails of the edges going 
     * into a given target location.
     *)
    val nail_positions = Hashtbl.create 0; (* location.id -> XY list *)

    val lookup = new Tataf_libta.UppaalWalker.lookup nta

    method! transform_template 
        ({Template. locations; edges; _;} as tmpl) =
            let t_locs = self#transform_locations locations in
            let t_edges = self#transform_edges edges in
            { tmpl with locations = t_locs;
                        edges = t_edges;
            }

    method relocate x_offset y_offset ({XY.x; y } as reference_pose)  =
        let open XY in
        let x = reference_pose.x in 
        let y = reference_pose.y in
        XY.make (x - x_offset) (y - y_offset)

        method! transform_edge ({Edge. source; target; xy; guard; nails; _} 
                                as trans) =
        let open Location in let open Edge in let open XY in
        let rec find_target_index n = function
            | hd :: tl -> if hd = target then n else find_target_index (n+1) tl 
            | [] -> failwith("Layouter failed to find location with id " ^
                                (target))
        in 
        let target_index = find_target_index 0 location_ids in
        let target_loc_pose = List.nth location_positions target_index in
        let nail_poses = try Hashtbl.find nail_positions target
                         with Not_found -> []
        in
        let (x_offset, y_offset) = (0,50) in
        let xy = self#relocate 20 0 target_loc_pose in
        let rel = self#relocate x_offset y_offset in
        let rec find_nail_pose curr_xy =
            let rec aux = function
                | (n_xy : XY.t option) :: tl -> begin match n_xy with
                    | None -> aux tl
                    | Some n_xy -> if n_xy.x <> curr_xy.x || n_xy.y <> curr_xy.y
                                   then
                                       aux tl
                                   else
                                       find_nail_pose (rel curr_xy)
                end
                | [] -> curr_xy
            in aux nail_poses
        in 
        let new_position = (find_nail_pose xy) in
        let guard_position = XY.make (new_position.x - 60) (new_position.y - 20) 
        in
        let nail = Nail.make ~x:new_position.x ~y:new_position.y in
        let open Guard in 
        let new_guard = match guard with
                        | None -> None
                        | Some g -> Some { g with xy = Some guard_position; }
        in
        Hashtbl.add nail_positions target (Some new_position :: nail_poses);
        { trans with nails = [nail];
                     guard = new_guard;
        }
                        

    method! transform_location ({Location. xy; id; _} as loc) =
        let open Location in let open XY in
        let xy = match xy with
        | None -> XY.make 0 0
        | Some xy -> xy
        in
        let rec find_position y_offset =
            let rec aux = function
                | n_xy :: tl -> if n_xy.x = xy.x && n_xy.y = (xy.y + y_offset)
                                then
                                    find_position (y_offset + 80)
                                else
                                    aux tl
                | [] -> XY.make xy.x (xy.y + y_offset)
            in
            aux location_positions
        in
        let new_pose = find_position 0 in
        location_positions <- new_pose :: location_positions;
        location_ids <- id :: location_ids;
        { loc with xy = Some (new_pose) }
            
end

let simple_layout_internal nta = (new fix_layout_simple nta)#transform nta
