(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Declaration
open Tataf_libta.UppaalSystem.Template

let dbg = Tataf_libcore.Debug.register_debug_module "Globalize"

(*----------------------------------------------------------------------------*)

class collect_ids =
  object (self)
  inherit Tataf_libta.UppaalWalker.unit_walker as super

  (* after walking the complete NTA, 'ids' will contain all identifiers ever *)
  (* occurring in the system *)
  val ids = Hashtbl.create 0;

  (* fill up 'ids' *)
  method! visit_id id =
    Hashtbl.add ids id 0

  (* id is fresh if and only if it does not yet occur in the system, i.e. *)
  (* if it is not in 'ids' -- to be used after complete walk! *)
  method is_fresh id =
    not (Hashtbl.mem ids id)
end

(*----------------------------------------------------------------------------*)

(* 'separator' is the separator between template name and original name *)
(* when constructing fresh identifiers *) 
class globalizer separator = 
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super

  (* all ids occurring in the system *)
  val ids = new collect_ids

  (* list of declarations to become global; will be appended to declarations *)
  (* of NTA at the end *)
  val mutable new_global_decls = []

  (* current template name; used by fresh_id() to create fresh ids *)
  val mutable current_template_name = ""

  (* list of currently active ids introduced by selects; do not transform a *)
  (* local name which has been declared using a select statement; *)
  (* *)
  (* Note: we assume that select statements are the only critical case, *)
  (*       all other places which introduce identifiers, like foreach *)
  (*       statements are left alone: they are subject to a kind of alpha- *)
  (*       renaming, which looks strange, but should not hurt. *)
  (*       E.g. for (x : int[0,1]) ... becomes for (T_x : int[0,1]) ..., *)
  (*       which introduces a new (shadowing) declaration of T_x for the *)
  (*       scope of the loop, which is then doing exactly the same thing *)
  (*       as before. *)
  val mutable current_select_ids = []

  (* the current mapping of local to (fresh) global names *)
  val local2global = Hashtbl.create 0;

  (* return a fresh identifier based on 'id' and 'current_template_name'; *)
  (* abort if our identifier construction strategy yields an identifier *)
  (* which is already in use somewhere (according to 'ids') *)
  method fresh_id id =
    let new_id = (current_template_name ^ separator ^ id) in
    if not (ids#is_fresh new_id) then
      failwith
        (new_id ^ ": fresh identifier exists in system, try --separator maybe");
    Hashtbl.add local2global id new_id;
    new_id

  (* yield the identifier to replace 'id' with -- if 'id' is shadowed by a *)
  (* select statement, or if 'id' is not subject to mapping, leave it alone *)
  (* otherwise use the value from local2global *)
  method! transform_id id =
    if List.mem id current_select_ids then
      id
    else
      try
        Hashtbl.find local2global id
      with Not_found -> id

  method! transform_edge edge =
    (* first, ensure that 'current_select_ids' consists of the identifiers *)
    (* introduced by possible select statements of 'edge' *)
    current_select_ids <- [];
    begin
    let open Edge in
    match edge.select with
    | Some {Select.sl; _}
        -> current_select_ids <- List.map (fun s -> let id,_ = s in id) sl
    | _ -> ()
    end;
    (* then transform regularly, using the overloaded transform_id() *)
    super#transform_edge edge

  method! transform_chanpriority (priority : int) channel =
    (* it's easier to treat channel names here, than in *)
    (* process_template_declaration below *)
    let open Declaration in match channel with
    | DefaultPriority -> channel
    | Channel(id,el) -> let new_id = self#fresh_id id in (* enforce order *)
                        Channel( new_id, self#transform_expressions el )

  method process_template_declaration decl =
    (* the pattern is the same for all three cases: get a fresh identifier *)
    (* (which may fail, if it would construct a duplicate), add the fresh *)
    (* identifier to the global mapping, and continue to transform sub- *)
    (* elements, e.g. function body, with the current mapping *)
    let tt = self#transform_type in
    match decl with
    | Variable(tp,id,init)
        -> (* no need to enforce order; cannot use own name in init *)
           Variable( tt tp, (self#fresh_id id),
                     Tataf_libcore.Util.some_apply self#transform_initializer init )
    | Typedef(tp,id)
        -> (* no need to enforce order; cannot use own name in tp *)
           Typedef( tt tp, (self#fresh_id id) )
    | Function(tp,id,parameters,declarations,stmts)
        -> let new_id = self#fresh_id id in (* enforce order *)
           Function( tt tp, new_id,
                     self#transform_parameters parameters,
                     self#transform_declarations declarations,
                     self#transform_statements stmts) 
    | _ -> (* all others, in particular channel names (see above) *)
           super#transform_declaration decl

  method process_template_declarations =
    (* 'local2global' is cleared for each template *)
    Hashtbl.clear local2global;
    List.map self#process_template_declaration

  method! transform_template {Template. name; parameters; declarations;
                                        locations; init; edges} =
    (* collect the globalised declarations (as obtained by visiting the *)
    (* existing definitions) in 'new_global_decls' and... *)
    let open Name in
    current_template_name <- name.text;
    new_global_decls <- new_global_decls
                          @ (self#process_template_declarations declarations);
    (* ... process as usual; only exception: no local declarations survive *)
    {Template. 
      name         = super#transform_name name;
      parameters   = super#transform_parameters parameters;
      declarations = [];
      locations    = super#transform_locations locations;
      init         = super#transform_init init;
      edges        = super#transform_edges edges; }

  method! transform_nta ({NTA. imports; declarations; templates;
                               instantiation; system; dtd_version } as nta) =
    (* collect identifiers occurring in the NTA *)
    ids#visit_nta nta;
    (* enforce visiting order by the sequence of let-s! *)
    let imp = self#transform_imports      imports in
    let d =   self#transform_declarations declarations in
    (* during transformation of templates, we collect the new global decls *)
    let t =   self#transform_templates    templates in
    let ins = self#transform_declarations instantiation in
    let s =   self#transform_system       system in
    {NTA.imports = imp;
         (* append collected new global decls to global declarations *)
         declarations = d @ new_global_decls;
         templates = t;
         instantiation = ins;
         system = s;
         dtd_version}
end

(*----------------------------------------------------------------------------*)

let globalize_internal separator upxmlSystem =
    (new globalizer separator)#transform upxmlSystem

let globalize separator source outfile =
  let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
  globalize_internal separator upxmlSystem
  |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
  |> Tataf_libcore.ExtString.string_to_file outfile;
  dbg#info ("Note: this transformation is only behaviour preserving if each"
            ^ " template is instantiated at most once.")
;;

