(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Sync
open Tataf_libta.UppaalSystem.Guard
open Tataf_libta.UppaalSystem.Update
open Tataf_libta.UppaalSystem.Edge

let dbg = Tataf_libcore.Debug.register_debug_module "MaxStep"

class max_step_transformer meta step step_max maxsteps nta =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super

  method transform_guard_option guard_o =
    let step_leq_maxstep = Relation(Var(step), Lt, Var(step_max)) in
    match guard_o with
    | None -> Some {Guard. expr = step_leq_maxstep; xy = None }
    | Some guard
          -> Some {guard with Guard.expr =
                      BoolOperation( step_leq_maxstep,
                                     AndAnd, Parenthesis(guard.expr) ) }

  method transform_update_option update_o =
    let incstep = IncPre(Var(step)) in
    match update_o with
    | None -> Some {Update. el = [ incstep ]; xy = None }
    | Some update
          -> Some {update with Update.el = update.el @ [ incstep ]}

  method! transform_edge
    ({Edge. id; source; target; select; guard; sync; update; comment;
     nails; xy; color} as trans) =
    match sync with
    | Some {Sync. marker = Synchronize; _}
        -> trans
    | _ -> {trans with Edge. 
                        guard  = self#transform_guard_option guard;
                        update = self#transform_update_option update }

  method! transform_nta {NTA. imports; declarations; templates;
                              instantiation; system; dtd_version } =
    (* enforce visiting order by the sequence of let-s! *)
    let imp = self#transform_imports imports in
    let d = self#transform_declarations declarations in
    let t = self#transform_templates templates in
    let ins = self#transform_declarations instantiation in
    let s = self#transform_system system
    in
    let open Declaration in
    let open Type in
    let step_prefixes = if meta then [Meta] else [] in
    let step_decl = Variable( Type.unranged_int(step_prefixes), step, None) in
    let step_max_decl = Variable( Type.unranged_int( [Const] ), step_max,
                                  Initializer.make_some_expr( Num(maxsteps) ) )
    in
    {NTA.imports = imp;
         declarations = d @ [ step_decl; step_max_decl ];
         templates = t;
         instantiation = ins;
         system = s;
         dtd_version }

  method go = super#transform nta
end

(*----------------------------------------------------------------------------*)

let max_step_internal meta step step_max maxsteps upxmlSystem =
  (new max_step_transformer meta step step_max maxsteps upxmlSystem)#go
;;

let max_step_query_internal step step_max =
  CTL( AG( BoolOperation( Parenthesis( Relation( Var(step), Lt, Var(step_max))),
                          OrOr, Deadlock )))

(** 
  This is the start function, where the transformation begins.
*)
let max_step meta step step_max maxsteps queryout source outfile =

  dbg#ldebug_endline 1  "--------------------";  
  dbg#ldebug_endline 1  "MaxStep";
  dbg#ldebug_endline 1  "  Add guards and updates to limit nr. of steps. ";
  dbg#ldebug_endline 1 ("  Input: "  ^ source);
  dbg#ldebug_endline 1  "--------------------";
  
  (* parsing *)  
  dbg#ldebug 2 "parsing ...";
  let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
  dbg#ldebug_endline 2 " ok";  

  let step = if step = "" then "step" else step in
  let step_max = if step_max = "" then "STEP_MAX" else step_max
  in
  max_step_internal meta step step_max maxsteps upxmlSystem
  |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
  |> Tataf_libcore.ExtString.string_to_file outfile;

  if queryout <> "" then
    max_step_query_internal step step_max
    |> (new Tataf_libta.UppaalPrint.query_printer)#visit_expression
    |> Tataf_libcore.ExtString.string_to_file queryout
;;

