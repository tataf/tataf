(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open UppaalExpressionEvaluation
open Tataf_libta.SymbolTable
open Tataf_libta.UppaalWalker
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Declaration
open Tataf_libta.UppaalSystem.Instantiation
open Tataf_libta.UppaalSystem.Name
open Tataf_libta.UppaalSystem.NTA
open Tataf_libta.UppaalSystem.System
open Tataf_libta.UppaalSystem.Template
open Tataf_libta.UppaalSystem.Type
open Tataf_libcore.Util

let dbg = Tataf_libcore.Debug.register_debug_module "Simplifier"

(*----------------------------------------------------------------------------*)

class simplifier ?(remove_template_parameters=false) 
                 ~evaluate_exp ~constant_propagation () =
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_with_symboltable 
        as super

    (* Lookup initializer expressions for variable with given id *)
    method lookup_initializer id = Tataf_libta.SymbolTable.get_initializer self#frame id

    (* Check if variable with given id resolves to constant variable in current
     * frame.
     *)
    method is_const_var = Tataf_libta.SymbolTable.is_const self#frame

    val mutable nta_system = None
    method get_system = unsome(nta_system)

    val mutable instantiated_once_tmpls = []

    method private remove_parameters_from_processes ({ NTA.system; _} as nta) =
        let decls = system.declarations in
        let handle_decl (decl : Declaration.t) = match decl with
            | Instantiation{instance_id; instance_parameters;
                             template_id; template_arguments; _}
                when List.mem template_id instantiated_once_tmpls
              -> Instantiation{instance_id; instance_parameters; 
                               template_id; template_arguments=[]}
            | _ -> decl
        in
        let new_system = { system with 
                                  declarations = List.map handle_decl decls; }
        in
        { nta with system = new_system }


    method! transform_nta_safe ( { NTA. system; _ } as nta) =
        nta_system <- Some system;
        if remove_template_parameters then
            super#transform_nta_safe nta
            |> self#remove_parameters_from_processes
        else
            super#transform_nta_safe nta


    method instantiated_at_most_once ({Template.name; _} as tmpl) =
        let sys = (self#get_system) in
        if List.exists 
            (fun {Process.name; _} -> name = tmpl.name.text) sys.processes then
                begin
                    dbg#ldebug_endline 2 
                    ("Template " ^ tmpl.name.text ^ " skipped - Reason"
                     ^ " was instantiated as process");
                    false
                end
        else
            (* Check if a given process instantiates a given template.
             * Then checking if the template was requested more than 
             * once comes down to checking if there are more 
             * than two processes for which this evaluates to true *)
            let instantiates_tmpl {Process.name; _} =
                let declaration_for_proc_tmpl = function
                    | Instantiation{template_id; instance_id; _} 
                      when template_id = tmpl.name.text
                      -> instance_id = name
                    | _ -> false
                in
                List.exists declaration_for_proc_tmpl sys.declarations
            in
            if ((Tataf_libcore.ExtList.count ~predicate:instantiates_tmpl sys.processes) <= 1)
            then begin 
                if remove_template_parameters then
                    instantiated_once_tmpls <- (name.text :: 
                                                instantiated_once_tmpls);

                true
                end
            else false

                

    method get_parameter_values {Template.name; _ } =
        let decl_for_tmpl = function
            | Instantiation{template_id; _} when template_id = name.text
              -> true
            | _ -> false
        in
        let tmp_decl =
        try
            List.find decl_for_tmpl (self#get_system).declarations
        with Not_found -> failwith("Found no matching declaration for this
                          template: " ^ name.text)
        in
        let open Instantiation in
        match tmp_decl with
        | Instantiation{template_arguments; _} -> template_arguments
        | _ -> failwith("Simplifier: "
                        ^ "Template declaration was not an instantiation!")


    method! transform_template_safe ({Template. name; 
                                       parameters; declarations; 
                                       locations; init; edges} as tmpl) =
        begin if (self#instantiated_at_most_once tmpl) then
            let open Type in let open Parameter in
            let msg = ("Template " ^ tmpl.name.text ^ " instantiated at most"
                   ^ " once.")
            in dbg#ldebug_endline 3 msg;
            let parameter_vals = self#get_parameter_values tmpl in

            let add_parameter_to_init param value =
                let p = new Tataf_libta.UppaalPrint.xta4_printer in
                let val_string = p#visit_expression value in
                dbg#ldebug_endline 3 
                ("Adding expression to initializers hashtable: " ^ param.name 
                 ^ ":= " ^ val_string);
                let init = Initializer.make_some_expr value in
                self#add_initializer param.name init
            in
            begin 
                (* Extend current (template) frame with all parameters and 
                 * their values. This enables us to implicitly propagate all
                 * template parameters into invariants and guards appropriatly.
                 * Note that selects open a new scope *)
                List.iter2 add_parameter_to_init parameters parameter_vals;
                let new_tmpl = super#transform_template_safe tmpl in
                if remove_template_parameters then
                    { new_tmpl with parameters = [] }
                else new_tmpl
            end
        else
            (* Template instantiated multiple times - let's not add anything to
             * the frames or simplify anything. *)
            super#transform_template_safe tmpl
        end


    method treat_unary exp =
      let ce = self#compute_expression in
      let eval_safely = uppaal_compute exp in
      match exp with
      | Unary (UnaryMinus, e) -> let cee = ce e in
                                 begin match cee with
                                       | Num n
                                           -> eval_safely Mult n (-1)
                                       | _ -> Unary (UnaryMinus, cee)
                                end
      | Unary (UnaryPlus, e) -> ce e
      | Unary (UnaryNot, e)        -> Unary(UnaryNot, ce e)
      | _ -> self#fail_transform_mismatch "transform_unary" 
                                          "Unary" 

    method treat_int_op exp =
        if (not evaluate_exp) then exp
        else begin
          let ce = self#compute_expression in
          let eval_safely = uppaal_compute exp in
          (* to evaluate subexpressions safely, we need a different 
           * fallback other than the whole expression 'exp' *)
          let eval_safely_fallback e = uppaal_compute e in
          match exp with
          | IntOperation(e1, Mult, e2)
            -> let (ce1, ce2) = (ce e1, ce e2) in
               let e1 = (new Tataf_libta.UppaalPrint.xta4_printer)#visit_expression ce1 in
               let e2 = (new Tataf_libta.UppaalPrint.xta4_printer)#visit_expression ce1 in
               dbg#ldebug_endline 2 (e1 ^ "*" ^ e2);
               begin match (ce1, ce2) with
               | (Num n1, Num n2) -> (eval_safely_fallback
                                     (IntOperation(ce1, Mult, ce2))
                                     Mult n1 n2)
               | _ -> IntOperation (ce1, Mult, ce2)
          end
          | IntOperation(e1, Div, e2)
            -> let (ce1, ce2) = (ce e1, ce e2) in
               begin match (ce1, ce2) with
               | (Num n1, Num n2) -> (eval_safely_fallback
                                     (IntOperation(ce1, Div, ce2))
                                     Div n1 n2)
               | _ -> IntOperation (ce1, Div, ce2)
          end
                                     
            (* Nested Additions: Try out different disjunctive 
             * ways to evaluate the subtrees.
             *)
          | IntOperation(IntOperation(e1, Plus, e2), Plus, e3)
          | IntOperation(e1, Plus, IntOperation(e2, Plus, e3))
            ->  let (ce1, ce2, ce3) = (ce e1, ce e2, ce e3) in
                begin match (ce1, ce2, ce3) with
                | (Num n1, Num n2, _) 
                ->  ce (IntOperation(
                    (eval_safely_fallback 
                    (IntOperation (e1, Plus, e2)) Plus n1 n2), 
                        Plus,ce ce3))
                | (Num n1, _, Num n3)
                  -> ce (IntOperation(
                      (eval_safely_fallback 
                      (IntOperation (e1, Plus, e3)) Plus n1 n3), 
                        Plus, ce ce2))
                | (_, Num n2, Num n3)
                  -> ce (IntOperation(
                      (eval_safely_fallback 
                      (IntOperation (e2, Plus, e3)) Plus n2 n3),
                        Plus,ce ce1))
                |_ -> IntOperation((ce (IntOperation(ce1, Plus, ce2)),
                        Plus, ce ce3))
                end
          | IntOperation (e1, op, e2)
            ->  let (ce1, ce2) = (ce e1, ce e2) in
                begin match (ce1, ce2) with
                    | (Num n1, Num n2) -> eval_safely op n1 n2 
                    (* op is not applied to two numbers *)
                    | _ -> IntOperation(ce ce1, op, ce ce2)
                end
           | _ ->  self#fail_transform_mismatch "transform_int_operation" 
                                               "IntOperation"
             end

    method treat_var id =
      if (not constant_propagation) || (not (self#is_const_var id)) then 
          let msg = "Simplifier: Skipping variable: '" ^ id ^ "' - Reason:" in
          let msg = 
          if constant_propagation then 
              msg ^ "No constant variable."
          else 
              "Constant propagation not requested!"
          in
          dbg#ldebug_endline 3 msg;
          Var (id)
      else begin 
          match self#lookup_initializer id with
          | None -> Var (id)
          | Some init -> let open Initializer in
                           begin match init with
                           | Expression (e) -> self#compute_expression e 
                           | _ -> Var (id)
                           end
           end

    method compute_expression exp =
        let ce = self#compute_expression in
        match exp with
        | Var(id) -> self#treat_var id
        | Num (n) -> Num (n)
        | Bool (b) -> Bool (b)
        | Call (e,args) -> Call(ce e, args)
        | Array (e1,e2) -> Array(ce e1, ce e2)
        | Parenthesis (e) -> Parenthesis(ce e)
        | IncPost (e) -> IncPost(ce e)
        | IncPre (e) -> IncPre(ce e)
        | DecPost (e) -> DecPost(ce e)
        | DecPre (e) -> DecPre(ce e)
        | Assignment (e1,op,e2) -> Assignment (ce e1, op, ce e2)
        | Unary (op,e) -> self#treat_unary exp
        | Relation (e1,op,e2) -> Relation(ce e1, op, ce e2)
        | IntOperation (e1,op,e2) -> self#treat_int_op exp
        | BoolOperation (e1,op,e2) -> BoolOperation(ce e1, op, ce e2)
        | IfExpr (e1,e2,e3) -> IfExpr(ce e1, ce e2, ce e3)
        | Dot (e1,id) -> Dot (ce e1, id)
        | Dashed (e) -> Dashed (ce e)
        | Deadlock -> Deadlock
        | Imply (e1,e2) -> Imply (ce e1, ce e2)
        | Exists (id,typ,e) -> Exists (id, typ, ce e)
        | Forall (id,typ,e) -> Forall (id, typ, ce e)
        | CTL (e) -> CTL(self#transform_CTLexpression e)

    method! transform_expression exp =
        self#compute_expression exp

    method compute_field (id, init) =
        let open Initializer in
        match init with
        | Expression (e) 
          -> (id, Initializer.make_expr (self#compute_expression e))
        | Fields (fl) 
          -> (id, Initializer.make_fields (List.map self#compute_field fl))

    method! transform_declaration decl =
        let tt = self#transform_type in
        let tid = self#transform_id in
        let tparam = self#transform_parameters in
        let tdl = self#transform_declarations in
        let tstmts = self#transform_statements in
        let tel = self#transform_expressions in
        let open Declaration in 
        match decl with
        | Variable (tp, id, init)
            -> begin let (t_tp, t_id, t_init) = let open Initializer in 
                match init with
                    (* Declarations: initializers can always be propagated,
                     * since even for regular variables (non-const) Uppaal
                     * will always use the initialized value here! *)
                | Some Expression (e) -> (tt tp, id,  
                        Initializer.make_some_expr (self#compute_expression e))
                | Some Fields (fl) 
                  -> begin
                      let t_fl = List.map self#compute_field fl in
                      (tt tp, id, Initializer.make_some_fields t_fl)
                    end
                                            
                | None -> (tt tp, id, init)
              in self#add_initializer t_id t_init;
                 self#add_symbol t_id (S_Variable t_tp) 
                                      (Variable (t_tp, t_id, t_init))
               end
        | Function (tp,id,parameters,declarations,stmts)
            -> let t_tp = tt tp in let (t_tp, t_id, t_param, t_dl, t_stmts) = 
             self#scope (FT_Function 
                        (Function (t_tp, id, parameters, declarations, stmts)))
             (fun () ->
                 let t_tp = tt tp in
                 let t_id = tid id in
                 let t_param = tparam parameters in
                 let t_dl = tdl declarations in
                 let t_stmts = tstmts stmts in
                 (t_tp, t_id, t_param, t_dl, t_stmts) 
              )
              in self#add_symbol t_id (S_Variable t_tp) 
                                      (Function (t_tp, t_id, t_param, 
                                                 t_dl, t_stmts))
        | Typedef(tp,id) -> begin  
             let t_tp = tt tp in
             let t_id = tid id in
             self#add_symbol t_id (S_Typedef t_tp) (Typedef (t_tp, t_id))
                            end
        | Instantiation(inst) 
          -> Instantiation(self#transform_instantiation inst)
        | BeforeUpdate(el) -> BeforeUpdate(tel el)
        | AfterUpdate(el) -> AfterUpdate(tel el)
        | ChanPriority(chel) ->
            let priority = ref 0 in
            ChanPriority(List.map (fun channels ->
              priority := !priority + 1;
              List.map (self#transform_chanpriority !priority) channels) 
                        chel ) 
end

(*----------------------------------------------------------------------------*)
