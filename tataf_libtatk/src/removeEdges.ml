(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

let dbg = Tataf_libcore.Debug.register_debug_module "RemoveEdges"

class remove_edges (remove_edges:Edge.t list) = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable

    method! transform_template ({ Template. edges; _ } as temp) =
        { temp with Template.edges = Tataf_libcore.ExtList.listMinus edges remove_edges}
end

let remove_edges_internal upxmlSystem (edges:Edge.t list) =
    (new remove_edges edges)#transform upxmlSystem

let remove_edges_system source outfile (ids:ID.t list) =
    let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
    let lookup = (new Tataf_libta.UppaalWalker.lookup upxmlSystem) in
    (* returns the edge denoted by id, or None if id does not denote an edge *)
    let edge_from_id id = 
        if (String.trim id) = "" then
            failwith ("RemoveEdge Transformer: Do not specify empty id, if you
            do, this may lead to deletion of the whole nta! Stopping.")
        else
            try 
                Some (lookup#edge id)
            with Not_found -> dbg#warn ("Remove Edge - ID " ^ id^ " not in"
                                        ^ " given NTA source file."); None
    in
    (* remove None-s from list l -- used below because edge_from_id may *)
    (* yield None-s when mapping transition IDs to edges *)
    let rec remove_none l = 
        match l with
        | []            -> []
        | None    :: tl -> (remove_none tl)
        | Some hd :: tl -> hd :: (remove_none tl)
    in
    (* get a list of edges denoted by the list of IDs *)
    let edges = remove_none (List.map edge_from_id ids)
    in
    remove_edges_internal upxmlSystem edges
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
;;
