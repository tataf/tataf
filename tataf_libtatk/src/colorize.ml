(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

let dbg = Tataf_libcore.Debug.register_debug_module "Colorize"

class colorize_ppd (edges:Edge.t list) (color:string) = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable

    method! transform_edge ({Edge. id; _} as edge) =
        let new_color = 
            if (List.exists (fun x -> x == edge) edges) 
            then Some color (* ppd edge *)
            else None       (* no ppd edge, remove color*)
        in {edge with Edge.color = new_color}
end

let colorize_internal upxmlSystem (edges:Edge.t list) (color:string) =
    (new colorize_ppd edges color)#transform upxmlSystem

let colorize_system source outfile (ids:ID.t list) (color:string) =
    let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
    let lookup = (new Tataf_libta.UppaalWalker.lookup upxmlSystem) in
    (* returns the edge denoted by id, or None if id does not denote an edge *)
    let edge_from_id id = 
        try 
            Some (lookup#edge id)
        with Not_found -> dbg#warn ("Colorize - ID " ^ id^ " not in"
                                    ^ " given NTA source file."); None
    in
    (* remove None-s from list l -- used below because edge_from_id may *)
    (* yield None-s when mapping transition IDs to edges *)
    let rec remove_none l = 
        match l with
        | []            -> []
        | None    :: tl -> (remove_none tl)
        | Some hd :: tl -> hd :: (remove_none tl)
    in
    (* get a list of edges denoted by the list of IDs *)
    let edges = remove_none (List.map edge_from_id ids)
    in
    colorize_internal upxmlSystem edges color
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
;;
