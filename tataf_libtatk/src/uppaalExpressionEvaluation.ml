(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
This module provides operations, which project Uppaal operators such as
"+", "-" onto the corresponding OCaml Operators. 

This is needed, because the respective OCaml operations have e.g.,
different overflow/underflow behaviour.

@author: Moritz Freidank
*)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

let dbg = Tataf_libcore.Debug.register_debug_module "UppaalExpressionEvaluation"

(* Checks whether (a+b) will over-/ or underflow. 
 * If so, throws an error, else returns the sum. *)
let uppaal_sum (a:int) (b:int) =
    if (a > 0) && (b > max_int -a) then
        (* Overflow error *)
        failwith("Error: Sum "^ string_of_int(a)^" + "^ string_of_int(b)
        ^ " leads to overflow error")
    else
        if (a < 0) && (b < (min_int -a)) then
            (* Underflow error *)
            failwith("Error: Sum "^ string_of_int(a)^" + "^ string_of_int(b)
            ^ (" leads to underflow error"))
        else
            (a+b)

(* Checks whether (a-b) will over-/ or underflow. 
 * If so, throws an error, else returns the difference. *)
let uppaal_minus (a:int) (b:int) =
    uppaal_sum a (-b)

(* Checks whether (a*b) will over-/ or underflow. 
 * If so, throws an error, else returns the difference. *)
let uppaal_mult (a:int) (b:int) =
    let fail s = failwith("Error: Multiplication " ^
    string_of_int(a) ^ " * " ^ string_of_int(b) ^ " leads to "^ s^ " error")
    in
    if a > 0 then
        (* a positive *)
        if b > 0 then
            (* a pos, b pos *)
            if a > (max_int / b) then
                fail "overflow"
            else
                (a*b)
        else
            (* a pos, b nonpos *)
            if b < (min_int /a) then
                fail "underflow"
            else
                (a*b)
    else
        (* a nonpos *)
        if b > 0 then
            (* a nonpos,b pos *)
            if a < (min_int / b) then
                fail "underflow"
            else
                (a*b)
        else
            (* a nonpos, b nonpos *)
            if (a != 0) && (b < (max_int / a)) then
                fail "overflow"
            else
                (a*b)

(* Checks whether (a/b) will over-/ or underflow. 
 * If so, throws an error, else returns the difference. *)
let uppaal_div (a:int) (b:int) =
    if (b = 0) then
        (* Division by zero error *)
        failwith("Error: Division by zero: "^ string_of_int(a) ^ "/0" )
    else
        if ((a = min_int) && (b = -1)) then
        (* Division underflow error *)
            failwith("Error: Division " ^
            string_of_int(a) ^ " / " ^ string_of_int(b) ^ 
            " leads to "^ "underflow error")
        else
            (a/b)

(* Checks whether (a mod b) will over-/ or underflow. 
 * If so, throws an error, else returns the difference. *)
let uppaal_mod (a:int) (b:int) =
    if (b = 0) then
        (* Modulo by zero error *)
        failwith("Error: Modulo by zero: "^ string_of_int(a) ^ " mod 0" )
    else
        (* Modulo underflow error *)
        if ((a = min_int) && (b = -1)) then
            failwith("Error: Modulo " ^
            string_of_int(a) ^ " mod " ^ string_of_int(b) ^ 
            " leads to "^ "underflow error")
        else
            (a mod b)

let uppaal_compute (exp:Expression.t) op (a:int) (b:int) =
    (* Attempt to evaluate: op (n1 n2); if failure occurs, fall back to exp *)
    try
        match op with
        | Plus  -> Num (uppaal_sum a b)
        | Minus -> Num (uppaal_minus a b)
        | Mult  -> Num (uppaal_mult a b)
        | Div   -> Num (uppaal_div a b)
        | Mod   -> Num (uppaal_mod a b)
        | _     -> exp (* we do not support op yet *)
    with Failure(e) -> dbg#ldebug_endline 2 e; exp

(* Checks whether the given expression 'e' assigns some constant value to a
 * clock variable. 
 *) 
let is_constant_assignment e =
    match e with
    | Assignment(Var x, Assign, Num n) -> true
    | _                                -> false

(* Checks whether the given expression 'e' resets a clock 
 * variable. 
 *) 
let is_reset e =
    match e with
    | Assignment(Var x, Assign, Num 0) -> true
    | _                                -> false


(* Checks whether e1 <=> e2 holds *)
(*
let are_equivalent e1 e2 =
    let imp ex1 ex2 = Imply (ex1, ex2) in
    let bicond= BoolOperation (imp e1 e2,
                               AndAnd,
                               imp e2 e1)
    in
    let f = Util.unsome(UppaalWalker.to_some_form 
                        bicond) in
    let smt2_declaration = 
    let smt2_str = PrintForm.smt2_of_form f smt2Declaration in
*)

