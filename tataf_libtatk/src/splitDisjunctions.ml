(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

let dbg = Tataf_libcore.Debug.register_debug_module "SplitDisjunctions"

(* Checks if a given expression has any clock variable 
 * (determined using Symboltable.frame 'frame') 
 * in any of its subtrees.
 *)
let rec has_clock frame exp =
    let icc = has_clock frame in
    let has_clock_var el = List.exists icc el in
    match exp with
    | Var(id) 
      -> Tataf_libta.SymbolTable.is_clock frame id || Tataf_libta.SymbolTable.is_clock_ref frame id
    | Num(_) | Bool(_) | Deadlock | CTL(_) -> false
    | Parenthesis(e) | IncPost(e) | IncPre(e) | DecPost(e) | DecPre(e) 
    | Call(e,_) | Unary(_, e) | Dashed(e) 
    | Exists(_,_,e) | Forall(_,_,e) | Dot(e,_)
      -> icc e
    | Array(e1, e2) | Assignment(e1,_,e2) | Relation(e1,_,e2) 
    | IntOperation(e1,_,e2) | BoolOperation(e1,_,e2) | Imply(e1, e2)
      -> has_clock_var [e1; e2]
    | IfExpr(e1,e2,e3) 
      -> has_clock_var [e1; e2; e3]

let rec is_clock_constraint_disjunction frame exp =
    let icc = has_clock frame in
    match exp with
    | BoolOperation(e1, OrOr, e2) -> icc e1 || icc e2
    | _ -> false

(* Return list of expressions, where all clock disjunctions where flattened one
 * level deep.
 *)
let rec split_clock_disjunctions frame exp =
    let scd = split_clock_disjunctions frame in
    let iccd = is_clock_constraint_disjunction frame in
    match exp with
    | BoolOperation(e1, OrOr, e2) 
        -> begin 
            if iccd exp then List.flatten ([scd e1; scd e2]) 
            else begin
                let rec aux ll rl curr =
                    match ll with
                    | hd :: tl
                        -> begin
                            let disjunct e = BoolOperation(hd, OrOr, e) in
                            aux tl rl (curr @ (List.map disjunct rl))
                        end
                    | [] -> curr
                in aux (scd e1) (scd e2) []
                 end
            end
    | BoolOperation(e1, AndAnd, e2)
        -> begin
            let rec aux ll rl curr =
            match ll with
            | hd :: tl 
                -> begin
                let conjunct e = BoolOperation(hd, AndAnd, e) in
                aux tl rl (curr @ (List.map conjunct rl))
                   end
            | [] -> curr
            in aux (scd e1) (scd e2) []
           end
    | Unary(op, e) 
        -> List.map (fun e -> Unary(op, e)) (scd e)
    | Parenthesis (e) 
        -> List.map (fun e -> Parenthesis (e)) (scd e)
    | _ -> [exp]


class split_disjunctions nta = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_with_symboltable

    val mutable loc_edges = []
    val split_locs  = Hashtbl.create 0; (* location_id -> location_id list *)

    val mutable init_loc = ""

    val mutable lookup = new Tataf_libta.UppaalWalker.lookup nta

    method! transform_template ({Template. name; 
                                   parameters; declarations; 
                                   locations; init; edges} as tmpl) =
          let open Template in let open Tataf_libta.SymbolTable in
          (* reset added edges for the locations collected on other templates) 
           *)
          loc_edges <- [];
          let t_name = self#transform_name name in
          (self#frame_push ~frame_type:(FT_Template {tmpl with name=t_name});
          let t_parameters = self#transform_parameters parameters in
          (* Visit declarations before changing locations or edges, to ensure 
           * we updated our symboltable frame!
           *)
          let t_declarations = self#transform_declarations declarations in
          (* Handle locations: for each location of the template, check if it 
           * has a disjunctive invariant. If it does, split it into multiple new
           * locations to remove the disjunctivity. *)
          let t_init = self#transform_init init in
          init_loc <- t_init;
          let t_locations = List.flatten (List.map (self#handle_loc) locations) 
          in
          (* Handle edges: for each edge of the template, check if it has a
           * disjunctive guard. If it does, split it into multiple new edges to
           * remove the disjunctivity. *)

          (* 'loc_edges' contains a list of edges generated when splitting a
           * location into multiple individual locations in "handle_loc" *)
          let t_edges = (List.flatten (List.map (self#handle_edge) 
                                                (edges @ loc_edges)))
          in
          let current_frame : frame = self#frame in
          let t_tmpl = {Template. name = t_name;
                        parameters = t_parameters;
                        declarations = t_declarations;
                        locations = t_locations;
                        init = t_init;
                        edges = t_edges; }
          in
          (* Update symbol table. *)
          self#frame_pop t_tmpl
          |> self#add_symbol t_name.Name.text
             (S_Template (t_parameters, (current_frame :> Tataf_libta.SymbolTable.t)) ) )

  method handle_loc
    ({Location. id; name; invariant; xy; _;} as loc) =
    let open Location in let open Invariant in let open Name in
    match invariant with
    | None -> [loc] (* if there is no invariant, there is no disjunctivity;
                       simply keep the input location *)
    | Some {expr; _} (* we have a potentially disjunctive expression, 
                        collect new locations if necessary *)
        -> let el = split_clock_disjunctions self#frame expr in
           if (List.length el) = 1 then [loc]
           else if loc.id = init_loc then
                    failwith("Splitting disjunctive invariants in initial"
                             ^ " locations is not supported!")
           else
           let new_locs = 
            (* For each expression in the split expression list, generate a new
             * location that has this expression as invariant. *)
                    let rec aux el locs n =
                        match el with
                        | exp :: tl 
                            -> (* Add suffix to all locations obtained 
                                  by disjunction splitting, 
                                  to avoid name collisions *)
                                let suffix = if n > 0 then "_" 
                                                           ^ string_of_int(n) 
                                            else "" in
                                let new_name = match name with
                                    | None -> Name.make_some("l"^suffix)
                                    | Some {text; _} -> Name.make_some(text 
                                                                       ^ suffix)
                                in
                            aux tl ({loc with 
                                         invariant = (Invariant.make_some exp);
                                         id = id ^ suffix;
                                         name = new_name;
                                       }
                                        :: locs) (n+1)
                        | [] -> locs
                        in aux el [] 0
                    (* For each of the new locations, generate a new set of 
                     * ingoing and outgoing edges (the same as the old location
                     * had) *)
                    in let open Edge in
                    (* Collect ingoing and outgoing edges into old location loc 
                     *)
                    let in_edges = lookup#edges_in loc.id in
                    let out_edges = lookup#edges_out loc.id in
                    (* Two helper functions to assign a new target or source
                     * location to edges *)
                    let assign_new_target_location {Location. id; _;} 
                                                   ({Edge. target; _} as edge) = 
                        {edge with target = id;}
                    in
                    let assign_new_source_location {Location. id; _;} 
                                                   ({Edge. source; _} as edge) = 
                        {edge with source = id;}
                    in
                    (* For each in + outgoing edge of the old location, 
                     * generate a new edge for
                     * each of the new locations *)
                    let get_edges ({Location.id; _;} as loc) =
                        List.map (assign_new_target_location loc) in_edges 
                        @ List.map (assign_new_source_location loc) out_edges
                    in
                    (* Keep track of the generated edges in an internal list
                     * 'loc_edges' - This list will be appended to the edge set
                     * before transforming the edges. *)
                    let rec collect_edges = function
                        | ({Location.id; _} as nloc) :: tl 
                            -> begin if id = loc.id then collect_edges tl
                                     else loc_edges <- loc_edges 
                                                        @ (get_edges nloc);
                                                        collect_edges tl
                                        end
                        | [] -> ()
                    in collect_edges new_locs; new_locs



  method handle_edge 
    ({Edge. source; target; guard; _;} as trans) = 
        let open Edge in let open Guard in
        match guard with
        | None -> [trans] (* If the edge has no guard, there is no disjunctivity
                            => simply keep the old edge *)
        | Some {expr; _}  (* The edge has a potentially disjunctive guard.
                             check if we need to split it.
                          *)
            -> begin
                let el = split_clock_disjunctions self#frame expr in
                (* If splitting yields a single expression, 'expr' is not
                 * disjunctive, simply return the original edge *)
                if (List.length el) = 1 then [trans]
                else
                    (* Splitting yielded a list of expressions => there was at
                     * least one disjunction. Generate a new edge for each
                     * expression in the split expression list 'el'. *)
                    let rec aux el edges =
                        match el with
                        | exp :: tl 
                            -> aux tl ({trans with 
                                              guard = (Guard.make_some exp);}
                                        :: edges)
                        | [] -> edges
                    in aux el []
                   end
end

let split_disjunctions_internal upxmlSystem =
    (new split_disjunctions upxmlSystem)#transform upxmlSystem

let split_disjunctions_system source outfile =
    let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
    split_disjunctions_internal upxmlSystem
    |> FixLayout.simple_layout_internal
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
