(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem


(*----------------------------------------------------------------------------*)

module LocationNameSet = Set.Make(Name)

class collect_location_names =
  object (self)
  inherit Tataf_libta.UppaalWalker.unit_walker as super

  val mutable names = LocationNameSet.empty
  method get_names = names

  method! visit_location {Location. name; _} =
    match name with
    | Some s -> names <- LocationNameSet.add s names
    | None -> ()
end

(*----------------------------------------------------------------------------*)

(*
  transition id generator will generate unique ids for all transitions.
 *)
class location_name_generator =
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super

  val name_collector = new collect_location_names

  val mutable next_id = 0
  method private incr = next_id <- next_id + 1

  (* Check whether 'name' is already taken in the given model *)
  method private name_taken name = 
      LocationNameSet.mem name name_collector#get_names

  method fresh_name =
      let candidate_name = Name.make ("l" ^ string_of_int(next_id)) in
      if self#name_taken candidate_name then
          begin
              (* Increment next_id *)
              self#incr;
              (* Get and check a new candidate name *)
              self#fresh_name
          end
      else
          begin
              (* Increment next_id to have a new name next time *)
              self#incr;
              (* candidate_name is new name, return it *)
              candidate_name
          end

  method! transform_location ({Location. name; _} as loc) =
      let open Location in
      match name with
      | Some na -> loc
      | None -> {loc with name = Some self#fresh_name }

  method! transform_nta nta =
    (* ensure that we start numbering ids/names at 0:
       NOTE: OCaml will sometimes precompute the transformer instance if the
       same method is called repeatedly...resulting in the fact that next_id is 
       not 0 initially for the second call. This is a somewhat dirty fix to that
       problem... *)
    next_id <- 0;
    (* collect names already in use in the given model, *)
    name_collector#visit nta;
    (* equip unnamed locations with fresh, unused names *)
    super#transform_nta nta 
end

(** shortcut for transition_id_generator *)
let generate_location_names_internal = (new location_name_generator)#transform

let generate_location_names filename outfile =
    Tataf_libta.UppaalParser.parse_xta4_file filename 
    |> generate_location_names_internal
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
;;
