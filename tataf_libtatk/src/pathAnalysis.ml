(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libcore.ExtList


class pathAnalysis ~nta =
  object (self)
  
  val lookup = new Tataf_libta.UppaalWalker.lookup nta 
  val pp = new Tataf_libta.UppaalPrint.pretty_printer
  
  val mutable max_depth = 3
  val mutable result : (int * string) list = [] 
  
  method str_location {Location.invariant; _} =
    "invariant: " ^ (pp#option_default "true" pp#visit_invariant invariant) 
  
  method str_transition {Edge. id; guard; sync; update; _} =
    "id: " ^ (pp#option pp#visit_id id) ^ "\n" ^
    "guard: " ^  (pp#option_default "true" pp#visit_guard guard) ^ "\n" ^
    "synchronization: " ^ (pp#option_default "tau" pp#visit_sync sync) ^ "\n" ^
    "assignment: " ^ (pp#option pp#visit_update update)
  
  (**
    Performs a single traversal step.
    
    @param ins Current path, which has to be prepend to the current step. 
    @param trans Current transition.
    @depth current path depth
  *)
  method step (ins : string) (trans : Edge.t) (depth : int) = 
    (*Current step: loc_source -- transition*)
    let source = lookup#location trans.Edge.source in
    let current = 
         (self#str_location source) ^ "\n" ^
         (self#str_transition trans) 
      |> Tataf_libcore.ExtString.prepend_lines (" " ^ (string_of_int depth) ^ " ") 
    in
    let ins = current ^ ins in
    
    (*add to result list*)
    result <- (depth, ins) :: result;
    
    (*traverse further, if the max_depth is not reached*)
    if depth < max_depth then begin
      lookup#edges_in trans.Edge.source 
      |> List.iter (fun tr -> self#step ins tr (depth+1) ) 
    end
  
  (**
    Executes the path analysis.
    
    @param start_id: id of the starting transition
    @param depth: maximum depth to go. 
  *)
  method execute (start_id : ID.t) (depth : int) =
    try begin
      result <- [];
      max_depth <- depth;
      
      (*starting transition*)
      let start_trans = lookup#edge start_id in
      
      (*target location string as init*)
      let ins = lookup#location start_trans.Edge.target
             |> self#str_location
             |> Tataf_libcore.ExtString.prepend_lines " 0 "
      in
      self#step ins start_trans 1;
      
      result
      (*sort result list by depth*)
      |> List.sort (fun (a,_) (b,_) -> compare a b)
      (*to string*)
      |> str_list ~sep:"\n\n" (fun (l, content) -> 
          "Path (l=" ^ (string_of_int l) ^ ") {\n" ^ 
            content ^
          "}" )
    end with Not_found -> 
      raise (Tataf_libcore.CmdLine.ValidationError "Start transition not found!")
end
  

let analysePaths modelFilename template_name startId depth =
  print_endline "--------------------";  
  print_endline "Analysing paths with following starting values:";
  print_endline ("  Model: "  ^ modelFilename);
  print_endline ("  Template: " ^ template_name);
  print_endline ("  StartTransition: " ^ startId);
  print_endline ("  Depth: " ^ (string_of_int depth));
  print_endline "--------------------";
  Tataf_libta.UppaalParser.parse_xta4_file modelFilename           (* parsing *)
  |> fun nta
			-> (new pathAnalysis ~nta)#execute startId depth (* execute analysis *)
  |> print_endline                                     (* output on stdout *)

