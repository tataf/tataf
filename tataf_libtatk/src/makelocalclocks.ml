(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Declaration
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Template
open Tataf_libta.UppaalQeClasses

let dbg = Tataf_libcore.Debug.register_debug_module "MakeLocalClock"

class makelocal_clock (clocks:ID.t list) = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_with_symboltable
    as super

    (* Hashtable: clock -> index of no of uses *)
    val clock_usages = Hashtbl.create 0;



    method private declare_clocks c_ids =
        let declaration_from_id id =
            Variable (Type.Clock, 
                      id ^ "_" ^ string_of_int(Hashtbl.find clock_usages id), 
                      None)
        in
        let update_clock_count c_id =
            let count = try 
                (Hashtbl.find clock_usages c_id) + 1 
                        with Not_found -> 1
            in Hashtbl.replace clock_usages c_id count
        in 
        List.iter update_clock_count c_ids;
        List.map declaration_from_id c_ids



    method private is_globally_declared var_id =
        match Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame var_id with
        | Some {TemplateQualifiedID.template = None; _ } 
        -> true (* NTA *)
        | Some { TemplateQualifiedID.template = Some _; _}
        -> false
        | None (* Function/Blocks/... *)
        -> false

    method private declares_no_clock_of_interest = function
        | Variable (Type.Clock, id, _) 
          -> not (List.exists (fun c_id -> c_id = id) clocks)
        | _ -> true


    method private uses_clock c_id tmpl =
        let rec expr_uses_clock = function
            | Var v -> self#is_globally_declared v
            | Num _ | Bool _ -> false
            | Call (_, args) -> List.mem (Var c_id) args
            | Parenthesis (e) -> expr_uses_clock e
            | IncPost e | IncPre e
            | DecPost e | DecPre e -> expr_uses_clock e
            | Unary (_, e) -> expr_uses_clock e
            | IntOperation (e1, _, e2) | Assignment (e1, _, e2)
            | BoolOperation(e1, _, e2) | Relation (e1, _, e2)
              -> expr_uses_clock e1 || expr_uses_clock e2
            | IfExpr (e1, e2, e3)
              -> expr_uses_clock e1 || expr_uses_clock e2 || expr_uses_clock e3
            | Dot (e, id) -> c_id = id || expr_uses_clock e
            | Deadlock -> false
            | Dashed (e) -> expr_uses_clock (e)
            | Imply (e1, e2) -> expr_uses_clock e1 || expr_uses_clock e2
            | Forall (id, _, e) -> id <> c_id && expr_uses_clock e
            | Exists (id, _, e) -> id <> c_id && expr_uses_clock e
            | Array _ -> false
            | CTL _ -> failwith("CTL expressions unsupported!")
        in let edge_uses_clock {Edge.guard; update; _} = 
            match (guard, update) with
            | (None, None) -> false
            | (Some {Guard.expr; _}, None) 
              -> expr_uses_clock expr
            | (None, Some {Update.el; _})
              -> List.exists expr_uses_clock el
            | (Some {Guard.expr; _}, Some {Update.el; _})
              -> expr_uses_clock expr || List.exists expr_uses_clock el
        in let loc_uses_clock {Location.invariant; _} =
            match invariant with
            | Some {Invariant.expr; _} -> expr_uses_clock expr
            | None -> false
        in let open Initializer
        in let decl_uses_clock = function
            | Variable (_, _, Some Expression(e))
              -> expr_uses_clock e
            (*|  TODO: How to handle functions that use/don't use clock? *)
            | _ -> false
        in 
        List.exists edge_uses_clock tmpl.edges ||
        List.exists loc_uses_clock tmpl.locations ||
        List.exists decl_uses_clock tmpl.declarations




    method! transform_nta_safe 
      ( { NTA. imports; declarations; templates; instantiation; system; 
               dtd_version } as nta) =
        let open NTA in
        let t_imports = self#transform_imports imports in
        (* Fill frame with original declarations *)
        ignore(self#transform_declarations declarations); 
        let t_declarations = 
            List.filter self#declares_no_clock_of_interest declarations
        in
        let t_templates = self#transform_templates templates in
        let t_instantiation = self#transform_declarations instantiation in
        let t_system = self#transform_system system in
        { nta with 
              imports = t_imports;
              declarations = t_declarations;
              templates = t_templates;
              instantiation = t_instantiation;
              system = t_system;
        }

    method! transform_template_dangerous tmpl =
        let new_clocks = List.filter (fun c -> self#uses_clock c tmpl) clocks in
        let new_declarations = self#declare_clocks new_clocks in
        let rec rename_clock_in_expr = function
            | Var v -> begin if (self#is_globally_declared v && 
                                 List.mem v new_clocks) 
                             then
                                 Var (v ^ "_" 
                                      ^ string_of_int(Hashtbl.find clock_usages 
                                                                   v))
                             else
                                 Var v
                       end
            | Parenthesis (e) -> Parenthesis (rename_clock_in_expr e)
            | IncPost e -> IncPost (rename_clock_in_expr e) 
            | IncPre e -> IncPre (rename_clock_in_expr e) 
            | DecPost e -> DecPost (rename_clock_in_expr e) 
            | DecPre e -> DecPre (rename_clock_in_expr e) 
            | Unary (op, e) -> Unary (op, rename_clock_in_expr e)
            | IntOperation (e1, op, e2) 
              -> IntOperation (rename_clock_in_expr e1, op, 
                               rename_clock_in_expr e2)
            | Assignment (e1, op, e2) -> Assignment (rename_clock_in_expr e1,
                                                     op,
                                                     rename_clock_in_expr e2)
            | BoolOperation (e1, op, e2) 
              -> BoolOperation (rename_clock_in_expr e1, op, 
                                rename_clock_in_expr e2)
            | Relation (e1, op, e2) -> Relation (rename_clock_in_expr e1,
                                                 op,
                                                 rename_clock_in_expr e2)
            | IfExpr (e1, e2, e3) -> IfExpr (rename_clock_in_expr e1,
                                             rename_clock_in_expr e2,
                                             rename_clock_in_expr e3)
            | Dot (e, id) 
              -> Dot (rename_clock_in_expr e, id)
            | Deadlock -> Deadlock
            | Dashed (e) -> Dashed (rename_clock_in_expr e)
            | Imply (e1, e2) -> Imply (rename_clock_in_expr e1,
                                       rename_clock_in_expr e2)
            | Array (e1, e2) -> Array (rename_clock_in_expr e1,
                                       rename_clock_in_expr e2)
            | e -> e
        in
        let open Guard in
        let handle_guard = function
            | None -> None
            | Some {expr; xy} -> Some {expr=(rename_clock_in_expr expr); xy}

        in
        let open Update in
        let handle_update = function
            | None -> None
            | Some {el; xy} -> Some {el=(List.map rename_clock_in_expr el); xy}

        in
        let open Edge in
        let rec handle_edges new_edges = function
            | [] -> new_edges
            | e :: tl -> handle_edges 
                         ({e with guard = handle_guard e.guard;
                                  update = handle_update e.update;
            
                          } :: new_edges)
                         tl
        in
        let open Invariant in
        let handle_invariant = function
            | None -> None
            | Some {expr; xy} -> Some {expr=(rename_clock_in_expr expr); xy}

        in
        let open Location in
        let rec handle_locations new_locs = function
            | [] -> new_locs
            | loc :: tl 
              -> begin
            handle_locations 
            ({loc with invariant = handle_invariant loc.invariant} :: new_locs)
            tl
                 end
        in
        {tmpl with declarations = 
            List.filter self#declares_no_clock_of_interest
                        tmpl.declarations @ new_declarations;
                   edges = handle_edges [] tmpl.edges;
                   locations = handle_locations [] tmpl.locations;
        }



end

let makelocal_clock_internal clock_ids =
    (new  makelocal_clock clock_ids)#transform


let makelocal_clock_system source outfile (clock_ids:ID.t list) =
    Tataf_libta.UppaalParser.parse_xta4_file source
    |> makelocal_clock_internal clock_ids
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
;;
