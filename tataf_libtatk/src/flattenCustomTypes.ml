(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.SymbolTable
open Tataf_libta.UppaalTransformer

let dbg = Tataf_libcore.Debug.register_debug_module "FlattenCustomTypes"

class custom_type_transformer remove_typedefs = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_with_symboltable

    (* Transform all Types: Ensure that any user-defined type is replaced by the
     * corresponding native Uppaal Type. *)
    method! transform_type (typ : Type.t) =
        match (Tataf_libta.SymbolTable.resolve_type self#frame typ) with
        | Some base_type -> base_type
        | None -> typ

    method! transform_declarations declarations =
        let tdcl = self#transform_declaration in
        if not remove_typedefs then
            List.map tdcl declarations
        else 
            let open Declaration in 
            let is_no_typedef = function
                | Typedef _ -> false
                | _ -> true
            in
            (* Flatten all custom type definitions *) 
            List.map tdcl declarations
            (* Remove Typedefs from transformed declarations. *)
            |> List.filter is_no_typedef


end

let flatten_custom_types_internal remove_typedefs =
    (new custom_type_transformer remove_typedefs)#transform 

let flatten_custom_types source outfile remove_typedefs = 
    Tataf_libta.UppaalParser.parse_xta4_file source
    |> flatten_custom_types_internal remove_typedefs
    |> Tataf_libta.UppaalPrint.print_xta4
    |> Tataf_libcore.ExtString.string_to_file outfile
