(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Name

let dbg = Tataf_libcore.Debug.register_debug_module "UndeclaredVariables"

class warn_undeclared ?(max_print_chars=10) ?(indent=2) ?(line_width=80) 
                      borrow_frame =
    object (self)
    inherit Tataf_libta.UppaalWalker.unit_walker as super

  val printer = (new Tataf_libta.UppaalPrint.xta4_printer)
  val mutable stack = []

  method private shorten = Tataf_libcore.ExtString.shorten ~length:max_print_chars

  method private push_stack str = stack <- str :: stack

  method private pop_stack = stack <- List.tl stack

  method private is_declared_var v =
      let fr = begin match borrow_frame with
                     | Some f -> f
                     | None -> self#frame
                end
      in Tataf_libcore.Util.not_none (Tataf_libta.SymbolTable.resolve fr v)

  method get_trace =
      List.rev stack
      |> List.filter (fun s -> not (Tataf_libcore.ExtString.empty s))
      |> Tataf_libcore.ExtString.str_list ~sep:" -> " (fun x -> x)
      |> Tataf_libcore.ExtString.wordwrap_and_indent indent line_width

  method private warn id =
      "detected undeclared variable: \"" ^ id ^ "\":\n" ^ self#get_trace
      |> dbg#warn

  method! visit_var = function
      | Var (id) -> if not (self#is_declared_var id) then self#warn id else ()
      | _ -> self#fail_visit_mismatch "visit_var" "Var"

  method! visit_template ({Template.name; _} as tmpl) =
      self#push_stack ("Template: \"" ^ name.text ^ "\"");
      super#visit_template tmpl;
      self#pop_stack;

  method! visit_edge ({Edge.id; guard; sync; update; _} as trans) =
      let e_id = match id with
             | Some i -> "Edge: \""^ i ^ "\""
             | None -> "Edge: Anonymous Edge"
      in self#push_stack e_id;
      super#visit_edge trans;
      self#pop_stack;

  method! visit_sync ({Sync.expr; _} as sync) =
      let exp_string = self#shorten(printer#visit_expression expr) in
      self#push_stack ("Sync: \"" ^ exp_string ^ "\"");
      super#visit_sync sync;
      self#pop_stack;


  method! visit_guard {Guard.expr; _} =
      let exp_string = self#shorten(printer#visit_expression expr) in
      self#push_stack ("Guard: \"" ^ exp_string ^ "\"");
      self#visit_expression expr;
      self#pop_stack;

  method! visit_update {Update.el; _; } =
      let update_string = 
          begin
              List.map (printer#visit_expression) el
              |> Tataf_libcore.ExtList.str_list ~sep:"; " (fun x -> x) 
              |> (^) "\"[ "  
              |> self#shorten
          end
      in self#push_stack ("Update: " ^ update_string ^ "]\"");
      self#visit_expressions el;
      self#pop_stack;

  method! visit_location ({Location.id; _} as loc) =
      self#push_stack ("Location: \"" ^ id ^ "\"");
      super#visit_location loc; self#pop_stack;

  method! visit_invariant {Invariant.expr; _} = 
      let exp_string = self#shorten(printer#visit_expression expr) in
      self#push_stack ("Invariant: \"" ^ exp_string ^ "\"");
      self#visit_expression expr;
      self#pop_stack;


  method! visit_declaration decl =
      self#push_stack "Declaration";
      super#visit_declaration decl;
      self#pop_stack;

  method! visit_nta { NTA.imports; declarations; templates; instantiation;
                          system; _; } =
      self#push_stack "Global Frame";
      self#visit_declarations declarations;
      self#visit_declarations instantiation;
      self#visit_system system;
      self#pop_stack;
      self#visit_templates templates;

end


let check_undeclared_variables_internal ?(indent=2) ?(line_width=80) 
                                        ?(max_print_chars=10)
                                        ?(frame=None) upxmlSystem =
    (new warn_undeclared ~indent ~line_width ~max_print_chars frame)#visit_nta 
    upxmlSystem


let check_undeclared_variables_external source indent 
                                        line_width max_print_chars =
    Tataf_libta.UppaalParser.parse_xta4_file source
    |> check_undeclared_variables_internal
