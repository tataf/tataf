(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Name

class location_count_printer =
    object (self)
    inherit Tataf_libta.UppaalWalker.unit_walker as super

    val mutable location_count = 0

    method get_count =
      location_count

    method! visit_location l =
      location_count <- location_count + 1;
        
end

let print_count_internal upxmlSystem =
    let walker = (new location_count_printer) in
    walker#visit_nta upxmlSystem;
    print_endline (string_of_int walker#get_count)

let print_count_external source =
    let nta = Tataf_libta.UppaalParser.parse_xta4_file source in
    print_count_internal nta
