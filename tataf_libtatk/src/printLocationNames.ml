(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open UppaalSystem
open UppaalSystem.Name
open ExtXml
open ExtList
open String

open UppaalSystem.Expression

let i8min = -128
let i8max = 127
let i16min = -32768
let i16max = 32767

type edge_entry = {
  mutable source : string;
  mutable target : string;
  mutable guard : string;
  mutable guard_bounds : (string*string*string) list;
  mutable channel : string;
  mutable receive : bool;
  mutable send : bool;
  mutable update : string;
}

type location_entry = {
  mutable location : string;
  mutable invariant : string;
  mutable isurgent : bool;
  mutable inv_bounds : (string*string*string) list;
}

let uppalidtoCid_var id =
  "var_" ^ id
let uppalidtoCid_max id =
  "max_" ^ id
let uppalidtoCid_min id =
  "min_" ^ id

let unvar id =
  let n = String.length "var_" in
  let m = String.length id in
  if m > n then
    String.sub id n (m-n)
  else
    id

(* "var_x.currentTime" -> "x" *)
(* "b.currentTime" -> "b.currentTime" *)
(* "var_a" -> "var_a" *)
(* "abcdefg" -> "abcdefg" *)
let cClockIdToUppalId_var id =
  let r1 = Str.regexp_string "var_" in
  let r2 = Str.regexp_string ".currentTime" in
  let a = String.length ".currentTime" in
  let b = String.length id in
  let ret = ref "" in
  if a < b then begin
    if (Str.string_match r1 id 0) && (Str.string_match r2 id (b-a)) then begin
      let idWOprefix = String.sub id 4 ((String.length id)-4) in
      ret := String.sub idWOprefix 0 ((String.length idWOprefix)-a);
    end
  end
  else
    ret := id;
  !ret


let err msg =
  let toprint = (*"§"^*)msg(*^"§"*) in
  (*print_endline toprint;*) toprint


class print_location_names =
  object (self)
  inherit UppaalWalker.string_walker as super

  method print_list delim = function
    | [] -> ()
    | e::l -> print_string e; print_endline delim; self#print_list delim l

  method print_chanlist = function
    | [] -> ()
    | (c,b)::l -> print_endline ("("^c^","^(string_of_bool b)^")"); print_string " "; self#print_chanlist l

  method print_loc_names oc = function
    | [] -> ()
    | {location=loc; _}::[] -> output_string oc loc;
    | {location=loc; _}::l ->
      output_string oc (loc^",\n    ");
      self#print_loc_names oc l

  val mutable currentId = "" 
  val mutable currentInitializer = ""
  val mutable currentBounds = []

  val mutable dec_strs = []
  val mutable chan_decs = []
  val mutable loclist = []
  val mutable edgelist = []
  val mutable init_loc_decl = ""
  val mutable edge_list = []
  val mutable clock_inits = []
  val mutable clock_updates = []

  method print_chans =
    self#print_chanlist chan_decs

  method! visit_id id =
    super#visit_id id;
    id

  method visit_assignOp = function
    | Assign    -> "="
    | AssignPlus    -> err "+="
    | AssignMinus   -> err "-="
    | AssignMult    -> err "*="
    | AssignDiv     -> err "/="
    | AssignMod     -> err "%="
    | AssignOr      -> err "|="
    | AssignAnd     -> err "&="
    | AssignXor     -> err "^="
    | AssignLShift  -> err "<<="
    | AssignRShift  -> err ">>="

  method visit_unary = function
    | UnaryMinus    -> "-"
    | UnaryNot      -> "!"
    | UnaryPlus     -> err "unary +"
  
  method visit_rel = function
    | Lt            -> "<"
    | LtEq          -> "<="
    | Eq            -> "=="
    | NotEq         -> "!="
    | GtEq          -> ">="
    | Gt            -> ">" 
    | Min           -> err"<?"
    | Max           -> err">?" 
  
  method visit_intop = function
    | Plus       -> "+";
    | Minus      -> err "-"
    | Mult       -> err "*"
    | Div        -> err "/"
    | Mod        -> "%";
    | And        -> err "&"
    | Or         -> err "|"
    | Xor        -> err "^"
    | LShift     -> err "<<"
    | RShift     -> err ">>"
  
  method visit_boolop = function
    | AndAnd     -> "&&"
    | OrOr       -> "||"


  method is_unaryminus = function
    | UnaryMinus    -> true
    | UnaryNot      -> false
    | UnaryPlus     -> false

  (* after propagateconstants, initializers are resolved to literals. Especially the init expression of a range is an integer (note: also sth. like int[false, 5] is rejected by the syntax checker). This function evaluates it to an ocaml integer. *)
  method eval_integer expr =
    match expr with
    | Num(n) -> n
    | Unary(op,e) ->
      (*TODO: ersetzen mit (visit_unary op) = "-" - spart die extra-funktion *)
      if self#is_unaryminus op then
        int_of_string ("-"^(string_of_int (self#eval_integer e)))
      else begin
        err "Integer bound or initializer encountered that is not an integer. Try running propagateconstants first.";
        -99999
      end
    | _ ->
         err "Integer bound or initializer encountered that is not an integer. Try running propagateconstants first.";
        -99999

  method unclock id =
    List.hd (Str.split (Str.regexp "\.") id)

  (*from uppaal print*)
  method! visit_expression expr =
(*    super#visit_expression expr;*)
    let paop = self#visit_assignOp in
    let punary = self#visit_unary in
    let pe = self#visit_expression in 
    let prel = self#visit_rel in
    let pintop = self#visit_intop in
    let pbop = self#visit_boolop in
    match expr with
    | Var(id) ->
      if (SymbolTable.is_clock self#frame id) then begin
        ((uppalidtoCid_var id) ^ ".currentTime")
      end
      else
        uppalidtoCid_var id
    | Num(n) -> string_of_int n
    | Bool(b) -> string_of_bool b
    | Call(e,args) -> err "function call?"
    | Array(e1,e2) -> err "array"
    | Parenthesis(e) -> "(" ^ (pe e) ^ ")"
    | IncPost(e) -> err "post++"
    | IncPre(e) -> err "++pre"
    | DecPost(e) -> err "post--"
    | DecPre(e) -> err "--pre"
    | Assignment(e1,op,e2) ->
      if (paop op) = "=" then begin
        let ev1 = pe e1 in
        if SymbolTable.is_clock self#frame (cClockIdToUppalId_var ev1) then begin
          "reset_clock(&" ^ ev1 ^ ", time_unit, floorExponent)"
        end
        else
            "(" ^ ev1 ^ " =  checkBounds(" ^ (pe e2) ^ ", " ^ (uppalidtoCid_min (unvar ev1)) ^ ", " ^ (uppalidtoCid_max (unvar ev1)) ^ "))"
      end
      else
        err "invalid assignment operator"
    | Unary(op,e) ->
      if punary op = "-" then begin
        "(neg(" ^ (pe e) ^ "))"
      end
      else if punary op = "!" then
        "!("^(pe e)^")"
      else
        err "invalid unary operator"
    | Relation(e1,op,e2) ->
      let operator = (prel op) in
      let ev1 = pe e1 in
      let ev2 = pe e2 in
      let clockInvolved = ref false in
      if SymbolTable.is_clock self#frame (cClockIdToUppalId_var ev1) then begin
        self#saveBound(ev1, operator, ev2);
        clockInvolved := true;
      end;
      if SymbolTable.is_clock self#frame (cClockIdToUppalId_var ev2) then begin
        self#saveBound(ev2, operator, ev1);
        clockInvolved := true;
      end;
      if (operator = "==" || operator = "!=") && !clockInvolved then
        "(fabs(" ^ ev1 ^ " - " ^ ev2 ^ " ) < eps)"
      else
        "(" ^ ev1 ^ " " ^ operator ^ " " ^ ev2 ^ ")"
    | IntOperation(e1,op,e2) -> 
      if pintop op = "+" then
        "(add(" ^ (pe e1) ^ ", " ^ (pe e2) ^ "))"
      else if (pintop op) = "%" then
        "(mod(" ^ (pe e1) ^ ", " ^ (pe e2) ^ "))"
      else
        err (pe e1)^"[invINTOP:'"^(pintop op)^"']"^(pe e2)
    | BoolOperation(e1,op,e2) -> "(" ^ (pe e1) ^ " " ^ (pbop op) ^ " " ^ (pe e2) ^ ")"
    | IfExpr(e1,e2,e3) -> err "?:"
    | Dot(e1,id) -> err "."
    | Dashed(e) -> err "'"
    | Deadlock -> err "deadlock"
    | Imply(e1,e2) -> err "imply"
    | Forall(id,tp,e) -> err "forall"
    | Exists(id,tp,e) -> err "exists" 
    | CTL(e) -> err "CTL"
  
  method visit_type_prefix = function
    | Type.Urgent -> ""
    | Type.Broadcast -> "broadcast"
    | Type.Const -> err "const"
    | Type.Meta -> err "meta"

  method visit_type_prefix_list =
    List.fold_left (fun i pe -> i ^ self#visit_type_prefix pe ^ "") ""

  method visit_type_from_declaration tt = 
    super#visit_type tt;
    let prefix = self#visit_type_prefix_list in
    let expr = self#visit_expression in
    let open Expression in
    match tt with
    | Type.Typename(p,id) -> err "typename"
    | Type.Bool(p) ->
      if (SymbolTable.is_clock self#frame currentId) || currentId = "" then "" else begin
      if (currentInitializer = "") then
        currentInitializer <- "false";
        dec_strs <- dec_strs @ ["bool " ^ (uppalidtoCid_var currentId) ^ " = " ^ currentInitializer ^ ";"];
        ""
      end
    | Type.Int(p,e1,e2) ->
      (* after propagateconstants, e1 and e2 are guaranteed to be integers. *)
      if (SymbolTable.is_clock self#frame currentId) || currentId = "" then "" else
      let lb = self#eval_integer e1 in
      let ub = self#eval_integer e2 in
      if (currentInitializer = "") then
        currentInitializer <- (self#makeDefaultIntInit lb ub);
      let tlb = self#makeIntBoundType lb in
      let tub = self#makeIntBoundType ub in
      let t = self#makeIntVarType tt in
(*      let init = self#eval_integer currentInitializer in*)
        dec_strs <- dec_strs @ ["const " ^ tlb ^ " " ^ (uppalidtoCid_min currentId) ^ " = " ^ (string_of_int lb) ^ ";"];
        dec_strs <- dec_strs @ ["const " ^ tub ^ " " ^ (uppalidtoCid_max currentId) ^ " = " ^ (string_of_int ub) ^ ";"];
        dec_strs <- dec_strs @ [t ^ " " ^ (uppalidtoCid_var currentId) ^ " = " ^ currentInitializer ^ ";"];
        ""
    | Type.Chan(p) ->
      if (prefix p) = "broadcast" then (
        chan_decs <- chan_decs @ [(currentId, true)]; ""
      )
      else (
        chan_decs <- chan_decs @ [(currentId, false)]; ""
      )
    | Type.Clock ->
      (* the first walk that fills the symbol table will find a currentId that is "". *)
      if (not (SymbolTable.is_clock self#frame currentId)) || currentId = "" then
          ""
        else begin
          let cid = uppalidtoCid_var currentId in
          dec_strs <- dec_strs @ ["struct clock " ^ cid ^ ";"];
          clock_inits <- clock_inits @ [
            cid ^ ".currentTime = 0.0f";
            "reset_clock(&" ^ cid ^ ", time_unit, floorExponent)";
            cid ^ ".resetTime.seconds = " ^ cid ^ ".resetTime.seconds + initializationDuration"
          ];
        clock_updates <- clock_updates @ ["update_clock(&"^cid^", time_unit, floorExponent);"];
      ""
        end
    | Type.Void -> err "void"
    | Type.Scalar(p,e) -> err "scalar"
    | Type.Struct(p,fields) -> err "struct"
    | Type.Ref(tp) -> err "ref"
    | Type.Range(tp,range) -> "|ERROR PRETTY PRINTING TYPE|" (* assert false *)(*should not be called!*)  

  method visit_type_range = function
    | Type.RangeExpression(e) -> "[" ^ (self#visit_expression e) ^ "]"
    | Type.RangeType(tp) -> err "type range" (*"[" ^ (self#visit_type tp) ^ "]"*)

  (*from uppaal print*)
  method visit_type_range_list =
    List.fold_left (fun i r -> (self#visit_type_range r) ^ i) ""

  method makeIntVarType tp =
    match tp with
    | Type.Int(p,e1,e2) ->
    let lb = self#eval_integer e1 in
    let ub = self#eval_integer e2 in    
      if Type.is_unranged_int tp then
        "int16_t"
      else
	if lb <= i8max && lb >= i8min && ub <= i8max && ub >= i8min then
	  "int8_t"
	else if lb <= i16max && lb >= i16min && ub <= i16max && ub >= i16min then
	  "int16_t"
	else
	  "int32_t"
    | _ -> err "ERROR"

  method makeDefaultIntInit lb ub =
    if lb <= 0 && ub >= 0 then "0"
    else string_of_int lb

  method makeIntBoundType n =
    if n <= i8max && n >= i8min then
      "int8_t"
    else if n <= i16max && n >= i16min then
      "int16_t"
    else (*if n <= i32max && n >= i32min then*)
      "int32_t"
(*    else
      err "INTEGER RANGE KAPUTT"*)

  method! visit_declaration declaration = 
    super#visit_declaration declaration;
    begin let open Declaration in
      match declaration with
      | Variable(tp,id,None) ->
         currentId <- id;
         currentInitializer <- "";
         self#visit_type_from_declaration tp
      | Variable(tp,id,Some init) ->
         currentId <- id;
         currentInitializer <- self#visit_initializer init;
         self#visit_type_from_declaration tp
      | Instantiation(inst) -> err "Instantiation"
      | BeforeUpdate(el) -> err "BeforeUpdate"
      | AfterUpdate(el) -> err "AfterUpdate"
      | Typedef(tp,id) -> err "Typedefs"
      | Function(tp,id,parameters,declarations,statements) -> err "Functions"
      | ChanPriority(chel) -> err "Channel priorities"
    end

  method! visit_initializer i =
    super#visit_initializer i;
    match i with 
    | Initializer.Expression(e) -> string_of_int (self#eval_integer e)
    | Initializer.Fields(fields) -> err "Initializer.Fields"; ""

  method! visit_location
    ({Location. id; name; urgent; committed; xy; color; invariant; comment; exponentialrate} as loc) =
    super#visit_location loc;
    match committed with
    | true -> err "committed location"; ""
    | false ->
      match invariant with
      | None ->
        loclist <- loclist @ [{location=id; invariant="(true)"; isurgent=urgent; inv_bounds=[]}];
        ""
      | Some(inv) ->
        let invStr = self#visit_invariant inv in
        loclist <- loclist @ [{location=id; invariant=invStr; isurgent=urgent; inv_bounds=currentBounds}];
(*	new_edge.guard_bounds <- currentBounds;*)
        currentBounds <- [];
        ""

  method getLocNr str ll =
    let counter = ref 0 in
    match ll with
    | [] -> err "location not found in internal data structure"; -1
    | {location=loc; _}::tl ->
      if loc = str then !counter else (counter := !counter+1; self#getLocNr str tl)

  val mutable currentLocNr = -1

  method! visit_edge
    ({Edge. id; source; target; nails; xy; color;
      select; guard; sync; update; comment; } as edge) =
    super#visit_edge edge;
    let new_edge = {source = source; target = target; guard = "(true)"; guard_bounds = []; channel = ""; receive = false; send = false; update = ""; (*update_checks = []*)} in

    match select with
    | Some(s) -> err "select"; ""
    | None ->

      (match guard with
      | Some(g) ->
	new_edge.guard <- (self#visit_guard g);
	new_edge.guard_bounds <- currentBounds;
	currentBounds <- [];
                 
      | None -> (););

      (match sync with
      | Some(s) ->
        (match s with
        | {Sync.expr; marker; _} ->
          new_edge.channel <- (self#visit_sync s);
          match marker with
          | Sync.Broadcast -> new_edge.send <- true;
          | Sync.Synchronize -> new_edge.receive <- true;
        | _ -> err ""; (););
      | None -> (););
    
      (match update with
      | Some(u) ->
	new_edge.update <- (self#visit_update u);
(*	new_edge.update_checks <- currentCheckList;*)
(*	currentCheckList <- []*)
      | None -> (););
    

    edgelist <- edgelist @ [new_edge];
    ""

  method! visit_sync ({Sync.expr; xy; marker} as sync) =
    super#visit_sync sync;
    self#visit_expression expr
(*    let psync = self#visit_expression expr in
    let pmarker = match marker with
      | Sync.Broadcast -> "!"
      | Sync.Synchronize -> "?"
    in
    "(" ^ pmarker ^ " " ^ psync ^ ")"
*)





























  method saveBound(clock_id, operator, e) =
    (* since we are in a guard, e2 is guaranteed to be evaluated to an integer expression *)
    if operator = ">=" then begin
      currentBounds <- currentBounds @ [(clock_id, "lower", e)]
    end
    else if operator = ">" then begin
      currentBounds <- currentBounds @ [(clock_id, "lower", "("^e^")+FLT_MIN")]
    end
    else if operator = "<=" then begin
       currentBounds <- currentBounds @ [(clock_id, "upper", e)]
    end
    else if operator = "<" then begin
       currentBounds <- currentBounds @ [(clock_id, "upper", "("^e^")-FLT_MIN")]
    end
    else if operator = "==" then begin
      currentBounds <- currentBounds @ [(clock_id, "upper", e)];
      currentBounds <- currentBounds @ [(clock_id, "lower", e)]
    end
    (* != is disallowed in clock constraints. Clock constraints are lower & upper bounds *)
    else if operator = "!=" then begin
      err "!="; ();
    end
    else
      ();

  method! visit_guard ({Guard.expr; xy} as guard) =
    super#visit_guard guard;
(*    currentCheckList <- [];*)
    self#visit_expression expr

  method! visit_update ({Update.el; xy} as assign) = 
    super#visit_update;
    let pe = self#visit_expression in
    match el with
    | [] -> ""
    | e::[] -> (pe e)^";"
    | e::es -> 
      "" ^ (pe e) ^ ";" ^(self#visit_update {assign with Update.el = es})

(*    let pid = id |> option_or_default "" 
                 |> self#visit_id 
                 |> xmlattribute "id" in
    let px, py = ppos xy in
    let pcolor = color |> self#option self#visit_color 
                       |> xmlattribute "color" in
    "  <transition" ^ pid ^ px ^ py ^ pcolor ^ ">\n" ^
      (self#visit_source source) ^
      (self#visit_target target) ^
      (select  |> self#option self#visit_select) ^ 
      (guard   |> self#option self#visit_guard) ^
      (sync    |> self#option self#visit_sync) ^
      (update  |> self#option self#visit_update) ^
v      (comment |> self#option self#visit_comment) ^ 
      (self#visit_nails nails) ^
    "  </transition>\n"
*)

(* 
    if committed then (
      err "committed location"; ""
    )
    else (locations <- locations @ [{location=id, invariant="", outgoing_edges=[], isurgent=false}];)
    ""*)
(*    match invariant with
    | None -> "(1)"
    | Some(inv) -> self#visit_invariant inv
*)

  method extractAttr pref =
    let i = String.index pref '"' + 1 in
    let j = String.rindex pref '"' in
      String.sub pref i (j-i)

  method! visit_init init = 
    super#visit_init init;
    let pref = xmlattribute "ref" init in
      init_loc_decl <- "enum location loc = "^self#extractAttr pref^";"; ""

  method! visit_invariant ({Invariant.expr; xy} as inv) =
    super#visit_invariant inv;
    self#visit_expression expr

(*
    let pid = id |> self#visit_id 
                 |> xmlattribute "id" in
    let px, py = ppos xy in
    let pcolor = color |> self#option self#visit_color
                       |> xmlattribute "color" in
    "  <location" ^ pid ^ px ^ py ^ pcolor ^ ">\n" ^
      (name      |> self#option self#visit_name) ^
      (invariant |> self#option self#visit_invariant) ^
      (comment   |> self#option self#visit_comment) ^
      (if urgent    then "   <urgent/>\n"    else "") ^
      (if committed then "   <committed/>\n" else "") ^
      (exponentialrate |> self#option self#visit_exponentialrate) ^ 
    "  </location>\n"
*)

  method resetMutables () =
    currentLocNr <- -1;
    currentId <- "";
    currentInitializer <- "";
    dec_strs <- [];
    chan_decs <- [];
    loclist <- [];
    init_loc_decl <- "";
    edgelist <- [];
    clock_inits <- [];
    clock_updates <- [];

  method! visit_template
  ({Template. name; parameters; declarations; locations; init; edges} as template) =
    self#resetMutables ();
    let filename = (super#visit_template template)^".c" in
    let oc = open_out (filename) in
    print_endline ("Writing " ^ filename ^ "...");

    output_string oc (self#code_generic 1);
    output_string oc "enum location {\n";
    output_string oc "    ";
    self#print_loc_names oc loclist;
    output_string oc "\n};\n\n";
    output_string oc "// initial location\n";
    output_string oc (init_loc_decl^"\n");
    output_string oc "\n// local variables\n";
    output_string oc (self#strlist_to_str dec_strs ";\n");
    output_string oc (self#code_generic 2);
    output_string oc ("            " ^ self#strlist_to_str clock_inits ";\n            "); 
    output_string oc (self#code_generic 3);

    for i = 0 to (List.length loclist)-1 do
      let l = List.nth loclist i in
      output_string oc ("            case " ^ l.location ^ ":\n");
      output_string oc ("                // Location invariant\n");
      output_string oc ("                if (" ^ l.invariant ^ ")\n");
      output_string oc ("                {\n");
      for j = 0 to (List.length edgelist)-1 do
        let e = List.nth edgelist j in
        if e.source = l.location then begin
          let i_target = self#getLocNr e.target loclist in
          output_string oc ("                    if (edgeTaken == false)\n");
          output_string oc ("                    {\n");
          output_string oc ("                        // Guard on edge && target location invariant\n");
          output_string oc ("                        // # GCS\n");
          output_string oc ("                        if ((" ^ e.guard ^ ") && (" ^ ((List.nth loclist i_target).invariant) ^ "))\n");
          output_string oc ("                        // # GCE\n");
          output_string oc ("                        {\n");
          if e.receive = true then begin
            output_string oc ("                            wait_next_transition(\"" ^ e.channel ^ "\");\n");
            output_string oc ("                            if (signalReceived == true)\n");
            output_string oc ("                            {\n");
            output_string oc ("                                edgeTaken = true;\n");
            output_string oc ("                                signalReceived = false;\n");
            output_string oc ("                                loc = " ^ e.target ^ ";\n");
            output_string oc ("                                " ^ e.update ^ "\n");
            output_string oc ("                                " ^ self#strlist_to_str clock_updates "\n");
            output_string oc ("                            }\n");
            output_string oc ("                            else\n");
            output_string oc ("                            {\n");
            output_string oc ("                                " ^ self#strlist_to_str clock_updates "\n");
            output_string oc ("                            }\n");
          end
          else begin
            output_string oc ("                            edgeTaken = true;\n");
            output_string oc ("                            signalReceived = false;\n");
            output_string oc ("                            loc = " ^ e.target ^ ";\n");
            if e.send then begin
              output_string oc ("                            broadcast(\"" ^ e.channel ^ "\", otherAddr);\n");
            end;
            output_string oc ("                            " ^ e.update ^ "\n");
            output_string oc ("                            " ^ self#strlist_to_str clock_updates "\n");
(*            output_string oc ("                        }\n");*)
          end;
        output_string oc ("                        }\n");
        output_string oc ("                    }\n");
        end;
      done;

      if l.isurgent then begin
        output_string oc ("                    if (edgeTaken == false)\n");
        output_string oc ("                    {\n");
        output_string oc ("                        error(\"bla urgent\");\n");
        output_string oc ("                    }\n");
      end;
        output_string oc ("                }\n");
        output_string oc ("                else\n");
        output_string oc ("                    brokenInvariant(\"" ^ l.location ^ "\");\n");
        output_string oc ("                if (edgeTaken == false)\n");
        output_string oc ("                    wait_next_transition(NULL);\n");
        output_string oc ("                break;\n\n");
    done;
    output_string oc ("        }\n");
    output_string oc ("    }\n");
    output_string oc ("}\n\n\n");

    output_string oc "float next_edge_in() {\n";
    output_string oc "    switch (loc) {\n";
    for i = 0 to (List.length loclist)-1 do
      let l = List.nth loclist i in
      output_string oc ("        case " ^ l.location ^ ": {\n");
      let anyOutgoingEdge = ref false in
      for j = 0 to (List.length edgelist)-1 do
        let anyBound = ref false in
        let e = List.nth edgelist j in
        if e.source = l.location then begin
          anyOutgoingEdge := true;
          output_string oc ("            // edge from " ^ e.source ^ " to " ^ e.target ^ "\n");
          let eb = e.guard_bounds in
          let i_target = self#getLocNr e.target loclist in
          let ib = (List.nth loclist i_target).inv_bounds in
          let nj = "next_" ^ (string_of_int j) in
          let dj = "deadline_" ^ (string_of_int j) in
          
          output_string oc ("            float " ^ nj ^ " =\n");
          output_string oc ("            // lower bounds from guard\n");
          for k = 0 to (List.length eb)-1 do
            anyBound := true;
            let (clid, ul, expr) = List.nth eb k in
            output_string oc ("            fmaxf("^(self#getNextValueFromBound (clid, ul, expr)) ^ ",\n");
          done;
          output_string oc ("            // lower bounds from target location invariant\n");
          for k = 0 to (List.length ib)-1 do
            anyBound := true;
            let (clid, ul, expr) = List.nth ib k in
            if k != (List.length ib)-1 then begin
              output_string oc ("            fmaxf(" ^ (self#getNextValueFromBound (clid, ul, expr)) ^ ",\n");
            end
            else
              output_string oc ("            " ^ self#getNextValueFromBound (clid, ul, expr));
          done;
          for k = 0 to (List.length eb)+(List.length ib)-3 do
            output_string oc ")";
          done;
          if !anyBound then begin
            output_string oc ");\n";
          end
          else 
            output_string oc "            0;\n";

          anyBound := false;
          
          output_string oc ("\n            float " ^ dj ^ " =\n");
          output_string oc ("            // upper bounds from guard\n");
          for k = 0 to (List.length eb)-1 do
            anyBound := true;
            let (clid, ul, expr) = List.nth eb k in
            output_string oc ("            fminf(" ^ (self#getDeadlineValueFromBound (clid, ul, expr)) ^ ",\n");
          done;
          output_string oc ("            // upper bounds from target location invariant\n");
          for k = 0 to (List.length ib)-1 do
            anyBound := true;
            let (clid, ul, expr) = List.nth ib k in
            if k != (List.length ib)-1 then begin
              output_string oc ("            fminf(" ^ (self#getDeadlineValueFromBound (clid, ul, expr)) ^ ",\n");
            end
            else
              output_string oc ("            " ^ self#getDeadlineValueFromBound (clid, ul, expr));
          done;
          for k = 0 to (List.length eb)+(List.length ib)-3 do
            output_string oc ")";
          done;
          if !anyBound then begin
            output_string oc ");\n";
          end
          else 
            output_string oc "            0;\n";

          anyBound := false;
          
          output_string oc ("\n            if (" ^ dj ^ " < " ^ nj ^ ") " ^ nj ^ " = FLT_MAX;\n");
          output_string oc ("            else if (" ^ nj ^ " < 0) {\n");
          output_string oc ("                if (" ^ dj ^ " < 0) " ^ nj ^ " = FLT_MAX;\n");
          output_string oc ("                else " ^ nj ^ " = 0;\n");
          output_string oc ("            }\n\n");
          output_string oc ("            if (" ^ nj ^ " < FLT_MAX) {\n");
          output_string oc ("                // check if integer part of guard is unsatisfied\n");
          output_string oc ("                if (!(" ^ (Str.global_replace (Str.regexp "var_([a-zA-Z1-9_])+\.currentTime") ("(\0 + "^nj^")") e.guard) ^ "))\n");
          output_string oc ("                    " ^ nj ^ " = FLT_MAX;\n");
          output_string oc ("                // check if integer part of target location invariant is unsatisfied\n");
          output_string oc ("                else if (!(" ^ (Str.global_replace (Str.regexp "var_[a-zA-Z1-9_]\.currentTime") ("(\0 + "^nj^")") (List.nth loclist i_target).invariant)^"))\n");
          output_string oc ("                    " ^ nj ^ " = FLT_MAX;\n");
          output_string oc ("            }\n");
        end;
      done;
      if !anyOutgoingEdge then begin
        output_string oc "            float max_next =\n";
        for j = 0 to (List.length edgelist)-1 do
          if j != (List.length edgelist)-1 then begin
            output_string oc ("            fmaxf(next_" ^ (string_of_int j) ^ ",\n");
          end
          else begin
            output_string oc ("            fmaxf(next_" ^ (string_of_int j) ^ ",\n");
            output_string oc ("            // dummy if only one entry\n");
            output_string oc ("            -FLT_MAX)");
            for k = 0 to (List.length edgelist)-2 do
              output_string oc (")");
            done;
          end;
        done;
        output_string oc (";");            
      end
      else begin
        output_string oc ("            float max_next = FLT_MAX;\n");
      end;
      output_string oc ("            if (max_next == FLT_MAX) return 0;\n");
      output_string oc ("            else return max_next;\n        }\n");
    done;
    output_string oc "    }\n";
    output_string oc "}\n";

    output_string oc (self#code_generic 4);
    output_string oc ("    " ^ self#strlist_to_str clock_updates "\n    ");
    output_string oc (self#code_generic 5);
    output_string oc ("    " ^ self#strlist_to_str clock_updates "\n    ");
    output_string oc ("\n}\n");
    ""

  method getNextValueFromBound (clid, ul, expr) =
    if ul = "upper" then
      "-FLT_MAX"
    else if ul = "lower" then
      expr ^ " - " ^ clid
    else
      err ""

  method getDeadlineValueFromBound (clid, ul, expr) =
    if ul = "upper" then
      expr ^ " - " ^ clid
    else if ul = "lower" then
      "FLT_MAX"
    else
      err ""
      

  method strlist_to_str l del =
    match l with
    | [] -> ""
    | hd::tl -> hd^del^(self#strlist_to_str tl del)


  method code_generic n =
    match n with
    | 1 -> 
"#include \"constants.h\"
#include \"sgnlex.h\"
#include \"timemng.h\"
#include \"util.h\"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <float.h>

void wait_next_transition(char *msg);

// Used to initialize the system at a particular time
struct genericTimeval starter;
struct genericTimeval timeLeft;
struct genericTimeval timeNow;

// Various help variables
bool signalReceived = false;
bool edgeTaken = false;
char* homeAddr;
char* otherAddr;

// Operators

// integer bounds check on assignment x := y
int32_t checkBounds(int32_t y, int32_t x_min, int32_t x_max)
{
    if (y > x_max)
        error(\"Integer assignment out of range.\");
    else if (y < x_min)
        error(\"Integer assignment out of range.\");

    return y;
}

// x + y
int32_t add(int32_t x, int32_t y)
{
    if ((y > 0) && (x > INT32_MAX - y)) // x + y would overflow
        return (x + INT32_MAX) + (y - INT32_MIN);
    if ((y < 0) && (x < INT32_MAX - y)) // x + y would underflow
        return (x - INT32_MAX) + (y + INT32_MIN);
    else
        return x + y;
}

// x % y
int32_t mod(int32_t x, int32_t y)
{
    if (y == 0)
        error(\"modulo by zero.\");

    return x % y;
}

// -x
int32_t neg(int32_t x)
{
    if (x == INT32_MIN)
        return INT32_MIN;
    else
        return -x;
}

"

    | 2 ->
"

int main(int argc, char *argv[])
{	
    if (argc != 3)
        error(\"Wrong number of arguments.\");
    homeAddr = argv[1];
    otherAddr = argv[2];

    getCurrentTime(&starter);
    getCurrentTime(&timeNow);

    /* Wait until 10 second interval to initialize. */
    while (starter.seconds % waitToInitialize!=0) {
        getCurrentTime(&starter);
    }

    initCommunication(homeAddr);

    timeLeft.seconds = starter.seconds + initializationDuration
            - timeNow.seconds;
    bool initialized = false;
    
    // Initialize
    while (timeLeft.seconds > 0)
    {
        if (initialized == false)
        {
            /* Initialize clock values to 0, reset the clocks and increase the
            reset time by initializationDuration */
"

    | 3 ->

"            initialized = true;
        }
        getCurrentTime(&timeNow);
        timeLeft.seconds = starter.seconds + initializationDuration
                - timeNow.seconds;
    }

    /* Start. */
    while (1)
    {
        switch (loc)
        {
"

  | 4 ->

"

/* Wait until next transition, either by waiting to receive signal or 
sleeping. */
void wait_next_transition(char *msg)
{
"
(*	updateClock(&counter, time_unit, floorExponent); *)
  
   | 5 ->

"float availableTime = next_edge_in();
    float offsetTime = clockDifference - (timeToArrive + timeToSend);
    float waitToReceive = availableTime - offsetTime;

    if (msg != NULL) {
        int res = receiveSignal(time_unit * waitToReceive, time_unit, msg);
        if (res < 0)
            signalReceived = false;
        else
            signalReceived = true;
    }
    else
        noSignalSleep(availableTime, time_unit);

    // Increase clocks
"
(*	if (updateClock(&counter, time_unit, floorExponent) < 0)
		error(\"Error while updating clock.\n\");"*)

  | _ -> ""



end


let print_external source =
  let nta = UppaalParser.parse_xta4_file source in
  let n = new print_location_names in
  n#visit_nta nta;
  print_endline "Done."; ()
