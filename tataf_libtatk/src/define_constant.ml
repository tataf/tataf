(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open UppaalSystem
open UppaalSystem.Expression

let dbg = Debug.register_debug_module "DefineConstant"

class define_constant (t_name : string) (constant_name : string) 
                      (expr : UppaalSystem.Expression.t) = 
    object (self)
    inherit UppaalTransformerWithSymboltable.transformer_with_symboltable 
            as super

    method get_init = Initializer.make_some_expr expr

    method is_local = ExtString.empty t_name

    method handle_declaration decl = 
        let open Declaration in match decl with
            | Variable(tp,id,init) when id = constant_name 
                -> Variable(tp, id, self#get_init)
            | _ -> super#transform_declaration decl


    method! transform_nta
    ({NTA. imports; declarations; templates; 
           instantiation; system; dtd_version } as nta) =
        if self#is_local then
            super#transform_nta nta
        else
            let open NTA in
            let new_decl = List.map self#handle_declaration declarations in
            { nta with declarations = new_decl; }

    method! transform_template 
    ({Template. name; parameters; declarations; 
                locations; init; edges} as tmpl) =
        let open Name in
        if not self#is_local || name.text <> t_name then
            super#transform_template tmpl
        else
            let open Template in
            let new_decl = List.map self#handle_declaration declarations in
            { tmpl with declarations = new_decl; }
end

let define_constant_internal upxmlSystem (t_name : string) (const_name : string)
                             (expr : UppaalSystem.Expression.t) =
    (new define_constant t_name const_name expr)#transform upxmlSystem

let define_constant_system source outfile (var_name : string) 
                                          (expr_string : string) =
    let upxmlSystem = UppaalParser.parse_xta4_file source in 
    let expr = UppaalParser.parse_expression expr_string in
    let unqualify_safely str = try Util.unqualify2 str with _ -> ("", str) in
    let (t_name, const_name) = unqualify_safely var_name in
    define_constant_internal upxmlSystem t_name const_name expr
    |> (new UppaalPrint.xta4_printer)#visit_nta
    |> ExtString.string_to_file outfile
;;
