(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem

(*----------------------------------------------------------------------------*)

module IDSet = Set.Make(ID)

class collect_location_ids =
  object (self)
  inherit Tataf_libta.UppaalWalker.unit_walker as super

  val mutable ids = IDSet.empty

  method get_ids = ids

  method! visit_location {Location. id; _} =
    ids <- IDSet.add id ids
end

(*----------------------------------------------------------------------------*)

(**
  transition id generator will generate unique ids for all transitions.
 *)
class transition_id_generator =
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super

  val ids = new collect_location_ids
  
  val mutable next_id = 0

  method mk_id n = "t" ^ (string_of_int n)

  method new_id =
    (* increment next_id as long as the candidate ID exists already *)
    let rec fresh_new_id n =
      if IDSet.mem (self#mk_id n) ids#get_ids then
        fresh_new_id (n+1)
      else
        n
    in
    let n = fresh_new_id next_id in
    next_id <- n + 1;
    self#mk_id n
  
  method! transform_edge
    ({Edge. id; _} as trans)= 
    {trans with Edge.id = Some self#new_id }

  method! transform_nta nta =
    ids#visit nta;          (* collect IDs already in use at location, *)
                            (* transition IDs are overwritten! *)
    super#transform_nta nta (* equip transitions with new IDs *)
end

(** shortcut for transition_id_generator *)
let generate_transition_ids_internal =
  (new transition_id_generator)#transform

let generate_transition_ids filename outfile =
    Tataf_libta.UppaalParser.parse_xta4_file filename 
    |> generate_transition_ids_internal
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
;;

