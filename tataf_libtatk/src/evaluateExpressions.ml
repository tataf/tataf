(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open UppaalExpressionEvaluation
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

let dbg = Tataf_libcore.Debug.register_debug_module "EvaluateExpressions"

class evaluate_exp = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable

  method evaluate_safely exp op (n1 : int) (n2 : int) =
      (* Attempt to evaluate: op (n1 n2); if failure occurs, fall back to exp *)
      try
          Num (op n1 n2)
      with _ -> exp


  method! transform_unary exp =
      let eval_safely = self#evaluate_safely exp in
      let te = self#transform_expression in
      match exp with
      | Unary (UnaryMinus, Num n) -> eval_safely uppaal_mult n (-1)
      | Unary (UnaryMinus, e) -> begin match te e with
                                       | Num n -> eval_safely uppaal_mult n (-1)
                                       | _ -> Unary (UnaryMinus, te e)
                                end
      | Unary (UnaryPlus, e) -> e
      | Unary (_) -> exp
      | _ -> self#fail_transform_mismatch "transform_unary" 
                                            "Unary" 


  method! transform_int_operation exp = 
      let eval_safely = self#evaluate_safely exp in
      let te = self#transform_expression in
      match exp with
      | IntOperation(Num n1, Plus, Num n2)  -> eval_safely uppaal_sum n1 n2 
      | IntOperation(Num n1, Minus, Num n2) -> eval_safely uppaal_minus n1 n2 
      | IntOperation (Num n1, Mult, Num n2) -> eval_safely uppaal_mult n1 n2
      | IntOperation (Num n1, Div, Num n2)  -> eval_safely uppaal_div n1 n2 
      | IntOperation (Num n1, Mod, Num n2)  -> eval_safely uppaal_mod n1 n2 
      | IntOperation (e1, op, e2)           -> IntOperation(te e1, op, te e2)
      | _ -> self#fail_transform_mismatch "transform_int_operation" 
                                            "IntOperation" 
end
