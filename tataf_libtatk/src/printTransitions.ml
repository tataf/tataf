(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  This module is a short example, how to print out all Transitions using 
  the Uppaal Walker classes.
*)

(*
open UppaalXmlSystem
open UppaalUtil
open UppaalXmlUtil
*)
open Tataf_libta.UppaalSystem

(* Module specific debugger *)
let dbg = Tataf_libcore.Debug.register_debug_module "PrintTransitions"

(*let pp = new UppaalPrint.pretty_printer (* -- standard syntax *) *)
let pp = new Tataf_libta.UppaalPrint.pnf_printer (* prefix normal form *) 
  
(*----------------------------------------------------------------------------*)
(**
  This class prints out all transitions with its source and target locations.
  
  Note:
  We inherit from [UppaalWalker.string_walker], because we want all methods
  to return a string value. 
*)
class transitions_printer nta =
  object (self)
  inherit Tataf_libta.UppaalWalker.string_walker as super
  
  (* Helper: location lookup class *)
  val mutable loc_lookup = new Tataf_libta.UppaalWalker.lookup nta
  
  (* current string prefix for labels.*)
  val mutable current_prefix = " 1 ";
  
  (*If set, then only the specified template will be visited.*)
  val mutable restrictToTemplate = ""
  
  (* --- Helper methods ------------------------ *)

  method set_restrictedToTemplate tn =
    restrictToTemplate <- tn
    
  (*
  method name_string = function
    | UpXmlNameEmpty -> "<noname>"
    | UpXmlName(nstring,_,_) -> nstring*)
  
  (* --- XTA part (walker)---------------------- *)
  
  method! visit_invariant inv = 
    let prefix = current_prefix ^ "invariant: " in
    prefix ^ (pp#visit_invariant inv) ^ "\n"
  
  method! visit_update assign =
    let prefix = current_prefix ^ "assignment: " in
    prefix ^ (pp#visit_update assign) ^ "\n"
    (*
    match assign with
    | UpEmptyAssign -> prefix ^ "\n" (*default*)
    | _ -> prefix ^ (pp#visit_assign assign) ^ "\n"*)
  
  method! visit_sync sync =
    let prefix = current_prefix ^ "synchronisation: " in
    prefix ^ (pp#visit_sync sync) ^ "\n"
    (*
    match sync with
    | UpEmptySync -> prefix ^ "tau\n" (*default*)
    | _ -> prefix ^ (pp#visit_sync sync) ^ "\n"*)
  
  method! visit_guard guard =
    let prefix = current_prefix ^ "guard: " in
    prefix ^ (pp#visit_guard guard) ^ "\n" 
    (*match guard with
    | UpEmptyGuard -> prefix ^ "true\n" (*default*)
    | _ -> prefix ^ (pp#visit_guard guard) ^ "\n"*)
  
  (* --- XML part (walker)---------------------- *)
  
  method! visit_edge {Edge.id; source; target; _} = 
  (*
  = function
    | UpXmlTransition(id,source,target,labels,_,_,_,_) as trans ->*)
      let trans_id = begin match id with
        | Some idstring -> idstring
        | None -> ""
      end in
      let locstring x = x
        |> loc_lookup#location 
        |> self#visit_location 
      in
      current_prefix <- " 0 ";
      let sloc_string = locstring source in
      current_prefix <- " 1 ";
      let tloc_string = locstring target in
      "Path (l=1) {\n" ^
      " 1 id: " ^ trans_id ^ "\n" ^
       (*(self#visit_labels labels) ^*)
      "OBSOLETE: Either insert code here, or remove the transition printer calss!" ^ 
       tloc_string ^
       sloc_string ^
      "}\n"

  method! visit_template {Template.name; locations; edges; _} = 
  (*
  = function
    | UpXmlTemplate(name,_,_,locs,_,trans) as tmpl ->*)
      (*check if restrictToTemplate*)
      if (restrictToTemplate <> "") && 
         (restrictToTemplate <> name.Name.text (*self#name_string name*)) 
      then 
        "" (* ignore template *)
      else 
        (* Only visit the transitions. *)
        (self#visit_edges edges) ^ "\n"

end
  
(*----------------------------------------------------------------------------*)

let print_all_transitions filename template_name =
  dbg#ldebug_endline 1 "--------------------";  
  dbg#ldebug_endline 1 "Print all Transitions in NTA (Uppaal)";
  dbg#ldebug_endline 1  ("  Input: "  ^ filename);
  dbg#ldebug_endline 1  ("  Template: " ^ template_name);
  dbg#ldebug_endline 1  "--------------------";
  
  (* parsing *)  
  dbg#ldebug 2 "parsing ...";
  let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file filename in 
  dbg#ldebug_endline 2 "ok";  

  (* create and print the output *)
  let tp = new transitions_printer upxmlSystem in
      tp#set_restrictedToTemplate template_name;
  let output_string = tp#visit_nta upxmlSystem in
  print_endline output_string

  
  
