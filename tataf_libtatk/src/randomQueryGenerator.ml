(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(** This module contains code to generate random queries for a 
    given Uppaal (".xml") input file. 

    Supported Queries are:
        - Reachability of Locations
*)

open Tataf_libcore.Util
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "RandomQueryGenerator"

class collect_process_location_map nta =
  object (self)
  inherit [string list] Tataf_libta.UppaalWalker.walker [] ~concat:(@)

  val ht_process_locations = Hashtbl.create 0
  (* process.name -> location.id list *);

  method ht_process_locations = ht_process_locations

  method store_locs {Template.locations; _} process_name  =
  (* get location names for process with process_name which is instantiation 
   * of given template argument. *)
      let open Template in
      let open Name in
      let rec get_location_names locs names =
          match locs with
          | [] -> names
          | {Location.name; _} :: tl
            -> begin
                match name with
                | Some n -> get_location_names tl (n.text :: names)
                | None -> get_location_names tl names
               end
       in 
       let locs = get_location_names locations [] in
       if Tataf_libcore.ExtList.list_empty locs then
           false
        else
           let str_locs = Tataf_libcore.ExtList.str_list ~sep:(", ") (fun a -> a) locs in
           dbg#ldebug_endline 3 ("Locs: " ^ str_locs);
           Hashtbl.add ht_process_locations process_name locs;
           true


  method! visit_instantiation {Instantiation.instance_id; template_id; _} =
      let open NTA in
      let open Name in
      let lookup_template_by_id id =
          List.find (fun {Template.name; _} -> name.text = id) nta.templates
      in
      dbg#ldebug_endline 3 ("Storing locs for process: " ^ instance_id);
      if self#store_locs (lookup_template_by_id template_id) instance_id then
        [instance_id]
      else []

end

class random_query_generator ?(seed = -1) ?(max_locs = 1) nta = object (self)

    (* Collect hashtable: process_name -> location_names *)
    val loc_collector = (new collect_process_location_map nta)

    (* Array of all process names, used to sample processes without 
     * replacement efficiently.
     * (for detailed reference, see documentation for ExtArray module) *)
    val mutable process_array = [||]

    (* list of bool operators used to randomly (with replacements) 
     * sample operators. *)
    val bool_operators = [AndAnd]

    (* Build query for reachability of [no_locs] locations, each from 
     * a distinct process.
     *
     * [ht_process_locations] is a hashtable that maps processes onto their
     * locations *)
    method location_reachability_query ht_process_locations no_locs =
        if (Array.length process_array) = 0 then
            failwith ("RandomQueryGenerator: Could not find any " ^
                      "processes in the given '.xml' sourcefile. "
                     );
        let get_dot_exp () =
            let random_process, new_processes = 
                Tataf_libcore.ExtArray.sample_without_replacement process_array
            in
            process_array <- new_processes;
            dbg#ldebug_endline 2 ("Randomly sampled process: " ^ 
                                  random_process);
            let random_location = 
                let locs = Hashtbl.find ht_process_locations random_process in
                dbg#ldebug_endline 2 ("Locations: " ^ 
                                      string_of_int(List.length locs));
                Tataf_libcore.ExtList.sample locs |> Tataf_libcore.Util.qualify random_process
            in
            dbg#ldebug_endline 2 ("Randomly sampled location: " ^ 
                                  random_location);
            let template, location = unqualify2 random_location in
            Dot (Var template, location)
        in
        let rec construct_and n exp =
            if n = 0 then
                exp
            else begin
                let bool_op = Tataf_libcore.ExtList.sample bool_operators in
                let new_exp = BoolOperation (get_dot_exp (), bool_op, exp) in
                construct_and (n-1) new_exp
            end
        in
        let or_exp = construct_and (no_locs-1) (get_dot_exp ()) in
        CTL(EF(or_exp))


    method generate_queries no_queries =
        let processes = loc_collector#visit_nta nta in
        let processes_str = Tataf_libcore.ExtList.str_list ~sep:(", ") (fun a -> a) processes 
        in dbg#ldebug_endline 2 ("Usable processes: " ^ processes_str);
        let ht_process_locations = loc_collector#ht_process_locations in
        
        if (List.length processes) - 1 < max_locs then
            dbg#ldebug 2 ("Specified number of locations per query larger " ^
                          "than number of processes, but AND-reachability " ^
                          "queries for multiple locations of the same " ^
                          "process are never satisfiable.. Defaulting to:\n" ^
                          "locations per query == number of processes\n");
        let max_locs = min max_locs ((List.length processes) - 1) in
        dbg#ldebug_endline 2 ("Maximal number of locations per query: " ^ 
                              string_of_int(max_locs));

        (* Turn to array which can be subsampled much more efficiently. *)
        let processes = Array.of_list processes in
        let rec queries_add n old_queries =
            process_array <- processes;
            if n = 0 then
                old_queries
            else
                let n_locs = (Random.int max_locs) + 1 in
                let new_query = 
                    self#location_reachability_query ht_process_locations 
                                                     n_locs
                in
                queries_add (n-1) (new_query :: old_queries)
        in queries_add no_queries []

    initializer
     (* Conservatively treat cases where OCaml might precompute the 
      * generator instance when called multiple times successively.. *)
      process_array <- [||]; 
      if seed = -1 then
          Random.self_init ()
      else
          Random.init seed

end

(** 
  This is the main function to generate queries. 
*)
let generate_random_queries ?(seed = -1) ?(no_queries = 1) ?(max_locs = 1) 
                            source outfile =
  let seed_str = if seed <> -1 then string_of_int seed  else "random" in
  dbg#ldebug_endline 1  "--------------------";  
  dbg#ldebug_endline 1  "RandomQueryGenerator";
  dbg#ldebug_endline 1  ("  Generate random queries " ^
                         "for brute-force validity checks. ");
  dbg#ldebug_endline 1 ("  Input: "  ^ source);

  dbg#ldebug_endline 1 ("seed: " ^ seed_str);
  dbg#ldebug_endline 1  "--------------------";


  (* parsing *)  
  let printer = new Tataf_libta.UppaalPrint.query_printer in
  dbg#ldebug 2 "parsing ...";
  let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
  dbg#ldebug_endline 2 " ok";  
  dbg#ldebug_endline 2 "generate list of queries ...";
  let generator = new random_query_generator ~seed:seed ~max_locs upxmlSystem
  in
  (* generating queries *)  
  let queries =
      generator#generate_queries no_queries
  in
  dbg#ldebug 2 "convert queries to strings ...";
  (* printing to file *)  
  let query_str = Tataf_libcore.ExtList.str_list printer#visit_expression queries in
  dbg#ldebug_endline 2 " ok";  
  dbg#ldebug 2 ("writing queries to file: " ^ outfile) ;
  Tataf_libcore.ExtString.string_to_file outfile query_str;
  dbg#ldebug_endline 2 " ok"; 
;;
