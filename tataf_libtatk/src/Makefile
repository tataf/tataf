#
# Copyright (C) 2021 University of Freiburg
#
# This file is part of tataf.
#
# Tataf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tataf is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with tataf.  If not, see <https://www.gnu.org/licenses/>.
#

### version information ###----------------------------------------------------

VERSION_MAJOR=0
VERSION_MINOR=9
VERSION_MAINT=0

VERSION_ML=libtatkVersion.ml

### sources and target ###-----------------------------------------------------

TATAF_SOURCES = \
    $(VERSION_ML)  \
    pathAnalysis.ml \
    printTransitions.ml \
    printLocationCount.ml \
    undeclaredVariables.ml \
    uppaalExpressionEvaluation.ml \
    simplifier.ml \
    addLocationNames.ml \
    addTransitionIds.ml \
    colorize.ml \
    defineConstant.ml \
    evaluateExpressions.ml \
    explicateSystem.ml \
    flattenarrays.ml \
    flattenCustomTypes.ml \
    fixLayout.ml \
    globalize.ml \
    makelocalclocks.ml \
    maxStep.ml \
    removeEdges.ml \
    splitDisjunctions.ml \
    randomQueryGenerator.ml

RESULT=tataf_libtatk

### packs ###------------------------------------------------------------------

PACKS = str xml-light unix \
    tataf_libcore tataf_libta

### build tool or library ###--------------------------------------------------

# TOOL_OR_LIB=[tool|lib]
#
TOOL_OR_LIB=lib

### test suites ###------------------------------------------------------------

# TESTSUITES=      -- no test.sh(1) test cases (no test suite)
# TESTSUITES=.     -- run test.sh(1) in ../tst (one test suite)
# TESTSUITES=a b c -- execute test suites ../tst/a to ../tst/b (multiple)
#
# call as, e.g.,
#
#		make TESTSUITES="general" test
#
# to execute only the test cases of test suite 'general'.
#
TESTSUITES=

### ---------------------------------------------------------------------------

include Makefile.tataf

