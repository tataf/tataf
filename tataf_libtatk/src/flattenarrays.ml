(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.SymbolTable
open UppaalExpressionEvaluation
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Declaration
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Initializer
open Tataf_libta.UppaalSystem.Type
open Tataf_libta.UppaalWalker
open Tataf_libta.UppaalPrint
open Guard
open Tataf_libta.UppaalParser
open Printf
open Tataf_libta.UppaalTransformer
open Edge
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libcore.Util

let dbg = Tataf_libcore.Debug.register_debug_module "FlattenArrays" 

module StringMap = Map.Make (String)

(* Handle consts and store them in a map for lookups of their value *)
class flatten_arrays nta global__frame = 
  object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable
        as super

  (* const_to_val : Map variable_name -> value
   * Gives access to all const variables in the declarations along with their 
   * respective values, as const variables can be used to initialize arrays.
   *)
  val const_to_val = ref StringMap.empty

  (* Constructs a conditional expression to replace array accesses in guards. 
   * arr_name[index_var] is replaced by:
   * (index_var == 0? arr_name_0 : (index_var == 1? arr_name_1 : ....)) *)
  method constructConditional arr_name index_expr =
    let arr_symbol = global__frame#resolve arr_name in
    let open Type in 
    let range = match arr_symbol with
    | Some S_Variable(Range(tp, [RangeExpression(e)])) -> 
            self#get_val e
    | _  -> failwith("Failed to find entry for array "^arr_name^
    " in the symbol table!")
    in let var_equals_n n = 
      Relation((Parenthesis(index_expr), Eq, Num n))
    in let var_from_arrname i =
      Var(arr_name^"_"^string_of_int(i))
    in let singleexpr index final_expr =
      (Parenthesis(IfExpr(var_equals_n (index-1), 
        var_from_arrname (index-1), final_expr)))
    in let rec ifexpressions expr curr total =
      match curr with
      | 0   -> expr
      | _   -> ifexpressions (singleexpr curr expr) (curr-1) total

    in ifexpressions (singleexpr (range-1) (var_from_arrname (range-1)))
                     (range-2) (range-1)

  method! transform_expression expr =
    let te = self#transform_expression in
    let tes = self#transform_expressions in
    let tid = self#transform_id in
    let tt = self#transform_type in
    match expr with
    | Var(id) -> Var(tid id)
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> Call(te e, tes args)
    | Array(e1,e2) -> (let array_name = (match e1 with
            | Var(id) -> id
            | _     -> (failwith("Array format unsupported in 
            flatten_arrays. Expected format: Array(name, index). 
            Failure: received unexpected input instead of name")))
            in
            match e2 with
            | Num n -> Var(array_name^"_"^string_of_int(n))
            | _     -> self#constructConditional array_name e2)
    
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> IncPost(te e)
    | IncPre(e) -> IncPre(te e)
    | DecPost(e) -> DecPost(te e)
    | DecPre(e) -> DecPre(te e)
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> Unary(op,self#transform_expression e)
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> Dot(te e1, tid id)
    | Dashed(e) ->  Dashed(te e)
    | Deadlock -> expr
    | Imply(e1,e2) -> Imply(te e1, te e2)
    | Exists(id,typ,e) -> Exists(tid id, tt typ, te e)
    | Forall(id,typ,e) -> Forall(tid id, tt typ, te e)
    | _          -> failwith("Unsupported expression in 
                flatten arrays 'transform_expression'. 
                If I had to guess: maybe a CTL formula?")


  (* This method exists to avoid code duplication. 
   * Computes the integer value from an integer expression 
   * used to declare arrays and initialize const variables
   *)
  method get_val e =
      match e with
      | Num n -> n
      | Var v -> StringMap.find v !const_to_val
      | IntOperation(e1, op, e2) -> (match op with
                    | Plus  -> uppaal_sum 
                                    (self#get_val e1) (self#get_val e2)
                    | Minus -> uppaal_minus 
                                    (self#get_val e1) (self#get_val e2)
                    | Mult  -> uppaal_mult
                                    (self#get_val e1) (self#get_val e2)
                    | Div   -> uppaal_div  
                                    (self#get_val e1) (self#get_val e2)
                    | Mod   -> uppaal_mod 
                                    (self#get_val e1) (self#get_val e2)
                    | _     -> failwith("FlattenArrays: Unsupported int operation 
                           in get_val "))
      | Unary(UnaryMinus,e1) -> uppaal_mult
                            (-1) (self#get_val e1)
      | Unary(UnaryPlus,e1) -> uppaal_mult
                            1 (self#get_val e1)
      | _   -> failwith("Unsupported expression for array declaration
             in flatten_arrays")



  (* Method to do the actual work. Given an array of type 'arr_tp' with 
   * name 'id', length as given in 'expr' and initializer expression 'init'
   * flatten the array out to a list of individual variables.
   *)
  method handleArrays arr_tp exp id init =
    let range = self#get_val exp in
    let open Initializer in
    let l =
        match init with
        | Some Initializer.Fields l1 -> Some l1
        | None -> None
        | _    -> failwith("Unsupported array initializer
                   for array " ^ id^"!")
    in
    let rec flatten_fields fields =
      match fields with
      | []    -> []
      | (a,b)::tl -> b::(flatten_fields tl)
    in
    (* Get inits from init *)
    let inits = 
      match l with
      | None   -> None
      | Some x -> Some (flatten_fields(Tataf_libcore.Util.unsome(l)))
    in
    let nth_or_none l n =
      match l with
      | None   -> None
      | Some x -> Some (List.nth x (n-1))
    in
    let rec insert_variables l n = 
      if n == 0 then l
      else
        (let open Declaration in
        let decl = Variable(arr_tp, id^"_"^string_of_int(n-1), 
              nth_or_none inits n) in
        insert_variables (decl::l) (n-1)
        )
    in
    insert_variables [] range
    
  method handleVariable tp id init =
    let tt = super#transform_type in
    let tid = super#transform_id in
    let tinit = super#transform_initializer in
    (* Determines if the variable is a const int variable => used for 
     * lookups to determine the range of arrays 
     * (which can be declared using const int variables) *)
    let isconstvar = 
      let open Type in match tp with
      | Int([Const], intMinRange, intMaxRange) -> true
      | _                    -> false
    in
    if isconstvar then
     (* If the variable is a const int variable, store it and its value in a 
      * map to be able to retrieve it later when it appears in an initializer 
      * for an array. *)
      let initValue =
        let open Initializer in
        match Tataf_libcore.Util.unsome(init) with
        | Expression(e) -> self#get_val e
        | _         -> failwith("Unsupported expression used to initialize
                     const variable " ^ id ^".")
      in
      let _ = (const_to_val := StringMap.add id initValue !const_to_val)
      in
      let open Declaration in
      [Variable(tt tp, tid id, some_apply tinit init)]
    else
      (* If this variable is an array, flatten it, otherwise just 
       * return the same variable.
       *)
      let open Type in match tp with
      | Range(arr_tp, [RangeExpression(exp)]) -> self#handleArrays arr_tp 
                                                                exp id init
      | _         -> (let open Declaration in 
              [Variable(tt tp, tid id, some_apply tinit init)])

  (* Variables are handled differently than the remaining declarations:
  * - const variable declarations are stored in reference list [x]
  * - Array declarations are flattened
  *)
  method handle_declaration declaration =
    let open Declaration in match declaration with
    | Variable(tp,id,init) -> self#handleVariable tp id init
    | _            -> [super#transform_declaration declaration]


  method handle_declarations declarations =
    List.flatten(List.map self#handle_declaration declarations)

  method! transform_nta
    {NTA. imports; declarations; templates; instantiation; system; dtd_version } =
    (* enforce visiting order by the sequence of let-s! *)
    let imp = super#transform_imports imports in
    let d = self#handle_declarations declarations in
    let t = super#transform_templates templates in
    let ins = super#transform_declarations instantiation in
    let s = super#transform_system system in
    {NTA.imports = imp; declarations = d; templates = t;
       instantiation = ins; system = s; dtd_version}
end

class flatten_arrays_scoped = 
  object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_with_symboltable as super

  (* Member variables and helper methods *)
  val mutable simplifier = (new Simplifier.simplifier ~evaluate_exp:true
                                                  ~constant_propagation:true ())
  method simplify exp =     simplifier#frame_set self#frame;
                            simplifier#transform_expression exp


  method ith_var_name arr_name i = arr_name ^ "_" ^ (string_of_int(i))
  method ith_var arr_name i = Var (self#ith_var_name arr_name i)

  method var_equals_n var n = Relation((Parenthesis(var), Eq, Num n))
  method base_type typ = Tataf_libcore.Util.unsome (Tataf_libta.SymbolTable.resolve_type self#frame typ)

  method variable_declarations_from_array arr_name tp range_expr init =
      let base_type = Tataf_libcore.Util.unsome (Tataf_libta.SymbolTable.resolve_type self#frame tp) in
      let rec flatten_fields el = function
          | [] -> el
          | (_, b) :: tl -> flatten_fields (b :: el) tl
      in
      let new_init = match init with
          | None -> None
          | Some (Expression e) -> Some [Expression e]
          | Some (Fields f) -> Some (List.rev (flatten_fields [] f))
      in
      let variable_declaration index = 
          let ini = match new_init with
                    | None -> None
                    | Some [Expression e] -> Some (Expression e)
                    | Some el -> Some (List.nth el index)
          in
          Variable (base_type, self#ith_var_name arr_name index, ini)
      in let rec declare_variables vars = function
          | i when (i < 0) -> vars
          | i -> declare_variables ((variable_declaration i) :: vars) (i-1)
      in
      match self#simplify range_expr with
      | Num n -> declare_variables [] (n - 1)
      | _ -> failwith("Failed to compute variable declarations for array "
                      ^ arr_name)


  method construct_conditional arr_name index_var =
      (* Constructs a conditional expression to replace array accesses in 
       * guards. arr_name[index_var] is replaced by:
       * (index_var == 0? arr_name_0 : (index_var == 1? arr_name_1 : ....)) *)
      let arr_typ = match Tataf_libta.SymbolTable.resolve self#frame arr_name with
                    | Some S_Variable(tp) -> tp
                    | _ -> failwith ("Looking up array symbol for " ^ arr_name
                                    ^ " failed!")
      in
      let range = match self#base_type arr_typ with
                  | Range(_, [RangeExpression(Num n)]) -> n
                  | _ -> failwith ("Looking up array size for array '" 
                                    ^ arr_name ^ "' failed!")
      in let single_expression index inner_expression =
          Parenthesis(IfExpr(self#var_equals_n index_var (index - 1), 
                      self#ith_var arr_name (index - 1), inner_expression))
      in let rec if_exprs expr curr = 
          match curr with
          | i when (i = 0) -> expr
          | _ -> if_exprs (single_expression curr expr) (curr -1)
      in if_exprs (single_expression (range - 1) (
                                     self#ith_var arr_name (range - 1)))
                                     (range - 2)

  (*
  method construct_conditional_multidim arr_name index_expr =
      let arr_typ = match SymbolTable.resolve self#frame arr_name with
                    | Some S_Variable(tp) -> tp
                    | _ -> failwith ("Looking up array symbol for " ^ arr_name
                                    ^ " failed!")
      in
      let simplify_rangeexp = function
          | RangeExpression(e) -> self#simplify e
          | _ -> failwith("...")
      in
      let ranges = match self#base_type arr_typ with
                    | Range(_, rel) -> List.map simplify_rangeexp 
                    | _ -> failwith("....")
      in 
      let ite ~i ~t ~e = 
          Parenthesis(IfExpr(i, t, e))
      in Bool(true)
      (* TODO: Construct if-then-else iterating through ranges and always 
       * doing index_var1 = value_uptorange1 && index_var2=const &&
       * index_var3==const etc.etc.*)*)


  method! transform_expression expr =
    let te = self#transform_expression in
    match self#simplify expr with
    | Array(Var(arr_name), Num(i)) -> self#ith_var arr_name i
    | Array(Var(arr_name), e) -> self#construct_conditional arr_name e
    (* Multidimensional arrays *)
    | Array(exp, e) -> te (Array( te exp, e))
    | _ -> super#transform_expression expr


  method! transform_declarations decls =
      let tds = self#transform_declarations in
      let rec handle_declaration decl =
          match decl with
          | Variable (Range(tp, [RangeExpression(e)]), arr_name,  init) 
            -> ignore(super#transform_declaration decl); (* update symbols *)
               self#variable_declarations_from_array arr_name tp e init
          (* multidimensional arrays *)
          (* TODO: update symbols? *)
          | Variable (Range(tp, RangeExpression(e) :: tl), arr_name, init) 
            -> ignore(super#transform_declaration decl); (* update symbols *)
               let arr_tp = Range(tp, tl) in
               tds (self#variable_declarations_from_array arr_name arr_tp e init)
          | _ -> [super#transform_declaration decl]
      in 
      List.map handle_declaration decls |> List.flatten
     
end


let flattenArrays source =
  dbg#ldebug_endline 1 "--------------------";  
  dbg#ldebug_endline 1 "FlattenArrays";
  dbg#ldebug_endline 1 "  Unfold arrays to sequence of variables of the same type. ";
  dbg#ldebug_endline 1  ("  Input: "  ^ source);
  dbg#ldebug_endline 1  "--------------------";
  
  (* parsing *)  
  Tataf_libta.UppaalParser.parse_xta4_file source
  |> (new flatten_arrays_scoped)#transform
