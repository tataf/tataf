(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(* Exception that is thrown if a variable was specified that does not exist in
 * the given input model 
 *)
exception No_Match_Found of string

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

let dbg = Tataf_libcore.Debug.register_debug_module "DefineConstant"

class define_constant (t_name : string) (constant_name : string) 
                      (expr : Tataf_libta.UppaalSystem.Expression.t) = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
        as super

    val mutable found_match = false

    method get_init = Initializer.make_some_expr expr

    method is_local = not (Tataf_libcore.ExtString.empty t_name)

    method handle_declaration decl = 
        let open Declaration in match decl with
            | Variable(tp,id,init) when id = constant_name 
                -> found_match <- true; Variable(tp, id, self#get_init)
            | _ -> super#transform_declaration decl

    method assert_match_and_return nta = 
        if not found_match then
            raise (No_Match_Found ("Failed to find variable with full name: " 
                  ^ t_name ^ "." 
                  ^ constant_name ^ " in the given input model. Check that the "
                  ^ "variable exists and is qualified by the correct template "
                  ^ "name."))
        else nta


    method! transform_nta
    ({NTA. imports; declarations; templates; 
           instantiation; system; dtd_version } as nta) =
        let new_nta = 
            if self#is_local then
                super#transform_nta nta
            else
                let open NTA in
                let new_decl = List.map self#handle_declaration declarations in
                { nta with declarations = new_decl; }
        in self#assert_match_and_return new_nta

    method! transform_template 
    ({Template. name; parameters; declarations; 
                locations; init; edges} as tmpl) =
        let open Name in
        if not self#is_local || name.text <> t_name then
            super#transform_template tmpl
        else
            let open Template in
            let new_decl = List.map self#handle_declaration declarations in
            { tmpl with declarations = new_decl; }
end

let define_constant_internal upxmlSystem (t_name : string) (const_name : string)
                             (expr : Tataf_libta.UppaalSystem.Expression.t) =
    (new define_constant t_name const_name expr)#transform upxmlSystem

let define_constant_system source outfile (var_name : string) 
                                          (expr_string : string) =
    let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
    let expr = Tataf_libta.UppaalParser.parse_expression expr_string in
    let unqualify_safely str = try Tataf_libcore.Util.unqualify2 str with _ -> ("", str) in
    let (t_name, const_name) = unqualify_safely var_name in
    define_constant_internal upxmlSystem t_name const_name expr
    |> (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta
    |> Tataf_libcore.ExtString.string_to_file outfile
;;
