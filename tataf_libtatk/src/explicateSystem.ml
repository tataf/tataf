(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Template
open Tataf_libta.UppaalSystem.Name

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "ExplicateSystem"

let is_used (processes : Process.t list) = 
    let open Process in 
    let open Declaration in 
    let open Instantiation in 
    function
    | Instantiation(inst)
        ->  List.exists (fun p -> p.name = inst.instance_id) processes
    | _ -> false

    
(*----------------------------------------------------------------------------*)

module NoOfCopies = Map.Make(String)

let lookup_no_of_copies ( t : Template.t ) noc =
  try 
    NoOfCopies.find t.name.text noc
  with Not_found -> 0

class n_templates_transformer ?(remove_uninstantiated_templates=false)
                              separator nta noc =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
  as super

  val clone_template = new Tataf_libta.CloneTemplate.clone_template_transformer


  method template_used {Template.name; _} =
    let open Name in
    let t_name = name.text in
    let open NTA in let open System in let open Process in
    let open Declaration in let open Instantiation in
    let rec lookup_process_decl name (declarations:Declaration.t list) = 
        match declarations with
        | [] -> failwith ("Could not find specified process with name " ^
                          name)
        | decl :: tl 
          -> begin 
              match decl with
              | Instantiation(ins) 
                when ins.instance_id = name
                  -> ins
              | _ -> lookup_process_decl name tl
            end
    in
    let rec is_not_instantiated = function
        | [] -> true
        | process :: tl 
          -> if (process.name = t_name) then false
             else
                 try
                     let decl = 
                         lookup_process_decl 
                             process.name 
                             nta.system.declarations 
                     in
                     if decl.template_id = t_name then false
                     else
                        is_not_instantiated tl
                with _ -> is_not_instantiated tl
    in not(is_not_instantiated nta.system.processes)

  method mk_n_copies n t =
    match n with
    | 0 -> []
    | _ -> (self#mk_n_copies (n - 1) t)
             @ [ clone_template#go (separator ^ (string_of_int n)) t ] 

  method! transform_templates ts =
      let tmpls = List.flatten(
      List.map (fun t -> self#mk_n_copies (lookup_no_of_copies t noc) t) ts )
      in if remove_uninstantiated_templates then
          List.filter self#template_used tmpls
      else tmpls

  method go = super#transform nta
end

(*----------------------------------------------------------------------------*)

let compute_lower_upper_bound failmsg global_frame min_expr max_expr =
  let open Simplifier in
  let smp = (new simplifier ~evaluate_exp:true
                            ~constant_propagation:true () )
  in
  smp#frame_set global_frame;
  begin match ( smp#transform_expression min_expr,
                smp#transform_expression max_expr ) with
  | ( Num(lower_bound), Num(upper_bound) )
      -> ( lower_bound, upper_bound )
  | _ -> failwith( failmsg ^
          ": cannot compute min and max value, sorry." )
  end

let lookup_lower_upper_bound failmsg global_frame typ =
  let open Type in
  match Tataf_libta.SymbolTable.resolve_type global_frame typ with
  | Some Int( _, min_expr, max_expr )
      ->  compute_lower_upper_bound failmsg global_frame min_expr max_expr
  | None
      -> failwith( failmsg ^
           ": type not in symbol table.  (Should not happen.)")
  | _ -> failwith( failmsg ^
           ": type not supported (only int[min,max]). Stopping!")

(*----------------------------------------------------------------------------*)

(**
  Transformer
*)
class estransformer ?(remove_uninstantiated_templates=false)
                    separator nta global__frame =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable
     as super

  val mutable decl : Declaration.t list = []
  val mutable templates : Template.t list = []

  val mutable qsubsts : (string * (Expression.t list) * string) list = []

  method query_substitutions = qsubsts

  val mutable noc = NoOfCopies.empty

  method number_of_copies = noc

  (* collect templates, we need them later
   *)
  method! transform_templates t =
    (* get us a copy of the list of templates *)
    templates <- t;
    super#transform_templates t 

  method is_instantiation_of decl name =
    let open Declaration in
    match decl with
    | Instantiation( { Instantiation.instance_id; instance_parameters; 
                       template_id; template_arguments } )
      when (instance_id = name)
        -> true
    | _ -> false

  method is_instantiation name declarations =
    List.exists (fun d -> self#is_instantiation_of d name) declarations

  method lookup_template_by_id id =
    List.find (fun t -> t.name.text = id) templates

  method explicate_process_rec template priority params prefix args =
    let templatename = template.name.text in
    let process_int lower_bound upper_bound tl =
      let l = ref [] in
      for i = lower_bound to upper_bound do
        l := !l @ self#explicate_process_rec
                    template priority tl (prefix ^ separator ^ (string_of_int i))
                    (args @ [ Num(i) ])
      done;
      !l
    in
    match params with
    | hd :: tl
         -> let open Parameter in
            let ( lower_bound, upper_bound ) =
                    lookup_lower_upper_bound
                      (templatename ^ ": " ^ hd.name) global__frame hd.typ
            in
            process_int lower_bound upper_bound tl
    | [] -> let n = (lookup_no_of_copies template noc) + 1 in
            noc <- NoOfCopies.add template.name.text n noc;
            let open Declaration in
            let open Instantiation in
            decl <- decl @ [ Instantiation( {
                               Instantiation.instance_id = prefix;
                                       instance_parameters = [];
                                       template_id = (template.name.text
                                                      ^ separator
                                                      ^ (string_of_int n));
                                       template_arguments = args } ) ];
            qsubsts <- qsubsts @ [ ( template.name.text, args, prefix ) ];
            [ { Process.priority = priority; name = prefix } ]

  method explicate_process templatename priority =
    let open Template in
    let t = List.find (fun t -> let open Name in
                                t.name.text = templatename) templates in
    self#explicate_process_rec t priority t.parameters
                                          (templatename ^ separator) []

  method transform_system_declaration decl =
    let open Declaration in
    let open Instantiation in
    match decl with
    | Instantiation(inst)
        -> if (List.length inst.instance_parameters) <> 0 then
             failwith( inst.instance_id ^
               ": partial instantiation not supported. Stopping!")
           else
             let t_id = inst.template_id in
             let t = self#lookup_template_by_id t_id in
             let n = (lookup_no_of_copies t noc) + 1 in
             noc <- NoOfCopies.add t.name.text n noc;
             Instantiation(
             { Instantiation.instance_id = inst.instance_id;
               instance_parameters = inst.instance_parameters;
               template_id = inst.template_id ^ separator ^ (string_of_int n);
               template_arguments = inst.template_arguments } ) 
    | _ -> decl

  method transform_system_declarations decls =
    let open Declaration in
    let open Instantiation in
      let open NTA in let open System in
      let processes = nta.system.processes in
      let new_decls = List.map self#transform_system_declaration decls in
      if remove_uninstantiated_templates then
        List.filter (fun decl -> is_used processes decl) new_decls
      else new_decls
     

  method! transform_processes processes =
    dbg#ldebug_endline 1 "estransformer.explicate_system\n";
    match processes with
    | [] -> []
    | ({ Process.priority; name } as hd) :: tl
         -> if (self#is_instantiation name decl) then
              hd :: (self#transform_processes tl)
            else
              (self#explicate_process name priority)
                @ (self#transform_processes tl)

  method! transform_system { System.declarations; processes; progresses } =
    dbg#ldebug_endline 1 "estransformer.transform_system\n";
    (* first, transform existing declarations such that each refers to a
       unique template name; noc keeps track of the number of copies needed *)
    decl <- self#transform_system_declarations declarations;
    (* then transform system statement, possibly adding entries to the
       declarations for automatically instantiated processes *)
    let proc = self#transform_processes processes in
    let prog = super#transform_progresses progresses in
    { System.declarations = decl; processes = proc; progresses = prog; }

 
  method go = super#transform nta
end

(*----------------------------------------------------------------------------*)

class var_expr_subst_transformer subst_var by_expr =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super

  method! transform_var var =
    match var with
    | Var(id) when id = subst_var -> by_expr
    | _                           -> super#transform_var var

  method transform_quantifier_expr id expr =
    (* stop substitution if 'id' is hiding 'subst_var' *)
    if (id = subst_var) then
       expr
    else
       self#transform_expression expr

  method! transform_forall = function Forall (id, typ, e)
      -> Forall( id, typ, self#transform_quantifier_expr id e )
  | _ -> self#fail_transform_mismatch "transform_forall" "Forall"

  method! transform_exists = function Exists (id, typ, e)
      -> Exists( id, typ, self#transform_quantifier_expr id e )
  | _ -> self#fail_transform_mismatch "transform_exists" "Exists"

end


(*----------------------------------------------------------------------------*)

class es_AE_transformer global__frame queries =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable
     as super

  method unfold_quantifier lower_bound upper_bound id op triv expr =
    if lower_bound > upper_bound then
      Bool(triv)
    else
      begin
      let ves = new var_expr_subst_transformer id (Num(lower_bound)) in
      let expr_inst = ves#transform_expression expr
      in
      if lower_bound < upper_bound then
        BoolOperation( expr_inst, op,
          self#unfold_quantifier (lower_bound + 1) upper_bound id op triv expr )
      else (* lower_bound = upper_bound *)
        expr_inst
      end

  method mk_forall id typ e = Forall( id, typ, e )

  method mk_exists id typ e = Exists( id, typ, e )

  method transform_quantifier id typ e op triv mk =
    let open Type in
    match Tataf_libta.SymbolTable.resolve_type global__frame typ with
    | Some Int( _, min_expr, max_expr )
        ->  let ( lower_bound, upper_bound ) =
                    compute_lower_upper_bound ("quantified variable: " ^ id)
                      global__frame min_expr max_expr
            in
            Parenthesis(
              self#unfold_quantifier lower_bound upper_bound id op triv
                                     (self#transform_expression e) )
    | None
        -> failwith( "quantified variable: " ^ id ^
             ": type not in symbol table.  (Should not happen.)")
    | _ -> mk (self#transform_id id) (self#transform_type typ)
              (self#transform_expression e)

  method! transform_forall = function Forall (id, typ, e)
      -> self#transform_quantifier id typ e AndAnd true self#mk_forall
  | _ -> self#fail_transform_mismatch "transform_forall" "Forall"

  method! transform_exists = function Exists (id, typ, e)
      -> self#transform_quantifier id typ e OrOr false self#mk_exists
  | _ -> self#fail_transform_mismatch "transform_exists" "Exists"

  method go = super#transform_expressions queries
end

(*----------------------------------------------------------------------------*)

class es_query_transformer global__frame qsubsts queries =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super

  method! transform_dot_expr dot =
    match dot with
    | Dot( Call( Var( tmpl ), args ), id )
        ->  begin try
              let sargs =
                List.map (function arg ->
                            let open Simplifier in
                            let smp = (new simplifier
                                            ~evaluate_exp:true
                                            ~constant_propagation:true ()) in
                            smp#frame_set global__frame;
                            smp#transform_expression arg) args
              in
              let (_,_,proc) = List.find (function (t,a,_)
                                            -> tmpl = t && sargs = a) qsubsts
              in
              Dot( Var(proc), id )
            with Not_found -> super#transform_dot_expr dot
            end
    | _ -> super#transform_dot_expr dot

  method go = super#transform_expressions queries
end

(*----------------------------------------------------------------------------*)

(** 
  Transform the internal representation of a UPPAAL model and queries to an
  explicated one.  List of queries may be empty.

  Returns a pair ( Expression list, NTA ).
*)
let explicate_system_and_queries_internal remove_uninstantiated_templates
                                          separator global_frame
                                          queries upxmlSystem =

  let parameter_simplifier = 
      new Simplifier.simplifier ~remove_template_parameters:true 
                                ~evaluate_exp:true 
                                ~constant_propagation:true () 
  in
  (* transform the system declaration *)
  let est = (new estransformer ~remove_uninstantiated_templates 
                               separator upxmlSystem global_frame) in
  let es_upxmlSystem = est#go in

   

  ( (* transform the queries *)
    let flat_AE_queries = (new es_AE_transformer global_frame queries)#go in
    (new es_query_transformer
      global_frame est#query_substitutions flat_AE_queries)#go,
    (* provide corresponding copies of templates *)
    ((new n_templates_transformer ~remove_uninstantiated_templates 
                                 separator es_upxmlSystem
                                 est#number_of_copies)#go) 
    |> (parameter_simplifier)#transform_nta 
   )
;;

(** 
  Transform the internal representation of a UPPAAL model to an explicated one,
  only NTA, no queries, returns NTA.
*)
let explicate_system_internal ?(remove_uninstantiated_templates = false) 
                              ?(separator = "_") upxmlSystem =
  (* get a symbol table *)
  let walker = (new Tataf_libta.UppaalWalker.unit_walker) in
  walker#visit upxmlSystem;
  let global_frame = Tataf_libcore.Util.unsome walker#global_frame in

  match explicate_system_and_queries_internal remove_uninstantiated_templates
          separator global_frame [] upxmlSystem with
  | ( _, es_upxmlSystem ) -> es_upxmlSystem 
;;
  
(** 
  This is the start function, where the transformation begins.
*)
let explicate_system ?(query = "") ?(queryout = "")
                     ?(remove_uninstantiated_templates = false) 
                     ?(separator = "_")
                     source outfile =
  assert (query ^ queryout = "" || (query <> "" && queryout <> ""));

  dbg#ldebug_endline 1  "--------------------";  
  dbg#ldebug_endline 1  "ExplicateSystem";
  dbg#ldebug_endline 1  "  Unfold templates in system declaration. ";
  dbg#ldebug_endline 1 ("  Input: "  ^ source);
  dbg#ldebug_endline 1  "--------------------";
  
  (* parsing *)  
  dbg#ldebug 2 "parsing ...";
  let upxmlSystem = Tataf_libta.UppaalParser.parse_xta4_file source in 
  dbg#ldebug_endline 2 " ok";  

  (* get a symbol table *)
  let walker = (new Tataf_libta.UppaalWalker.unit_walker) in
  walker#visit upxmlSystem;
  let global_frame = Tataf_libcore.Util.unsome walker#global_frame in

  let queries = 
    if query <> "" then
      let open Queries in
      dbg#ldebug 2 "parsing queries ...";
      let q = Tataf_libta.UppaalParser.parse_queries_file ~symbol_table:global_frame query
      in
      dbg#ldebug_endline 2 " ok";
      q.queries
    else
      []
  in
  let ( es_queries, es_upxmlSystem )
    = explicate_system_and_queries_internal remove_uninstantiated_templates 
                                            separator global_frame
                                            queries upxmlSystem
  in
  (* print transformed NTA *)
  (new Tataf_libta.UppaalPrint.xta4_printer)#visit_nta es_upxmlSystem
  |> Tataf_libcore.ExtString.string_to_file outfile;
  (* print transformed queries, if any *)
  if es_queries <> [] then
    let uqp = (new Tataf_libta.UppaalPrint.query_printer) in
    Tataf_libcore.ExtList.str_list ~sep:"\n\n" uqp#visit_expression es_queries
    |> Tataf_libcore.ExtString.string_to_file queryout
;;

