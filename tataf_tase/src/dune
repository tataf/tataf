;;
;; Copyright (C) 2021 University of Freiburg
;;
;; This file is part of tataf.
;;
;; Tataf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; Tataf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with tataf.  If not, see <https://www.gnu.org/licenses/>.
;;

; stanza to define compilation & linking features of the tool
; generate tool by: $ dune build
;
(executable
 (name tataf_tase)
 (public_name tataf_tase)
 (libraries
  str xml-light unix
  tataf_libcore tataf_libta tataf_libform tataf_libtaqe)
 (modes
  (byte exe))
 (flags -g -w @7@9@40-23 -bin-annot)
 (link_flags -g -custom))

; stanza to generate tool-version-ml file via a make call
; is run automatically when doing: $ dune build
;
(rule
 (targets taseVersion.ml)
 (deps
  (:mk Makefile Makefile.tataf)
  (:in taseVersion.ml.in))
 (action
  (run make -s update_version_info OMIT_OMK=y)))

; stanza to perform tests relying on a make target
; (currently not supported for this tool, but written here for standarization
; of dune files)
;
;(alias
; (name runtest)
; (action
;  (run make -s test DUNE_TST=y OMIT_OMK=y)))

; following two stanzas generate a utop executable by doing:
; $ dune build @tataf_utop
; (currently supported only for libraries, but written here for standarization
; of tools & libraries dune files)
;
;(executable
; (name tataf_utop)
; (modules tataf_utop)
; (link_flags -linkall -custom)
; (modes byte)
; (libraries
;  str xml-light unix
;  tataf_libcore tataf_libta tataf_libform tataf_libtaqe))

;(alias
; (name   tataf_utop)
; (deps   tataf_utop.bc))

