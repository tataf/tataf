(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libcore.CmdLine

(* Seems as if

    validate_empty_or_file_exists "<FLAG>" !<VARIABLE>

   also exists; seems that it checks that the argument to <FLAG> is given and
   that the denoted file does not exist; not used any more because we are
   following the standard UNIX tool convention: do what the user requested, 
   if the given file is there, it's overwritten (of course).
*)

(*----------------------------------------------------------------------------*)
(*- utataf -------------------------------------------------------------------*)
(*----------------------------------------------------------------------------*)

(*----------------------------------------------------------------------------*)
(*- utataf options -----------------------------------------------------------*)

(* Reference of user input; used for validity check *)
let utataf_debug_modules = ref ""
let opt_utataf : optionblock = "",
  "[OPTION]... SUBCOMMAND [...]",
  SCNothing,
  "tataf for Uppaal is a tool capable to work with networks of timed automata.",
  [
   ("","--debuglevel", "LEVEL", Arg.Int (fun debuglevel -> 
      Tataf_libcore.Debug.set_debug_level debuglevel
    ),
    "Sets the verbosity of the debug messages.  Higher is more.  
     Only effective together with --debugmodules.
     ");    
    ("","--debugmodules", "M[,M]",  Arg.String (fun modules ->
      (* Keep track of modules requested on the commandline for 
       * validity check below. *)
      utataf_debug_modules := modules;
      if modules = ""
      then Tataf_libcore.Debug.debugOn := false
      else Tataf_libcore.Debug.debugOn := true;
           Tataf_libcore.Debug.set_debug_modules modules
    ),
    "Output debugging information for specified modules (listed below).  The
     modules must be given as comma seperated list.  Supported modules: "
     ^ Tataf_libcore.Debug.registered_debug_modules ());
   ("-D","--default-dtd","VERSION", Arg.Set_string Tataf_libta.LibtaOptions.dtd_version_string,
    "Parse wrt. DTD version VERSION if the given XTA file does not specify one.
     Accepted values: 1.1 (default), 1.2.");
   ("","--halt-on-warning","", Arg.Set Tataf_libcore.Debug.halt_on_warning,
    "If given, the program will immediately halt if a warning occurs.");
   ("","--nocolor","", Arg.Clear Tataf_libcore.Color.colors_enabled,
    "Disables ANSI escape codes in console output.");    
   ("-v","--verbose","", Arg.Set Tataf_libta.LibtaOptions.verbose,
    "explain what is being done.");
   (*-----------------*)
   ("","--help","", Arg.Unit (fun () -> raise (Arg.Help "command line argument") ),
    "Display the best matching help page.  This flag is always applicable.");
   ("","--version","", Arg.Unit
      (fun () -> print_endline TaseVersion.version_string; 
	 		           print_endline Tataf_libtaqe.LibtaqeVersion.version_string;
	 		           print_endline Tataf_libform.LibformVersion.version_string;
	 		           print_endline Tataf_libta.LibtaVersion.version_string;
			           print_endline Tataf_libcore.LibcoreVersion.version_string;
								 exit 0 ),
    "Display version information and exit.");
  ],
  Subcommands([
    opt_help;
  ]),
  (* "validate_modules_exist" validates that all modules requested by the user 
   * actually exist (are registered) in the Debugging Module. *)
  (fun () ->
    validate_modules_exist 
    "MODULES" !utataf_debug_modules (Tataf_libcore.Debug.registered_debug_modules ());
    let open Tataf_libta.UppaalSystem.DtdVersion in
    match !Tataf_libta.LibtaOptions.dtd_version_string with
    | ""    -> ()
    | "1.1" -> Tataf_libta.LibtaOptions.dtd_version := DTD_1_1 ()
    | "1.2" -> Tataf_libta.LibtaOptions.dtd_version := DTD_1_2 ()
    | _     -> raise_invalid_option "-D"
                 ("'" ^ !Tataf_libta.LibtaOptions.dtd_version_string ^ "': invalid DTD version")
  ),
  [("Output debug information for the workflow of the Uppaal module:",
    "--debuglevel 1 --debugmodules UppaalPrint,UppaalParser ...");
   ("Disable ANSI escape codes:",
    "--nocolor ...");
   ("Display how the help system works:",
    "help");
  ]

