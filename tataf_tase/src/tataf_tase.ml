(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
  
(* main ***********************************************************************)
(******************************************************************************)
let main () =
	Tataf_libcore.CmdLine.help_examples := true;
	try (
		Tataf_libcore.CmdLine.process TaseOptionblocks.opt_utataf
	) with | Arg.Help _ -> ()

(* exception handling *********************************************************)
(******************************************************************************)

let print_exception ex =
  let print_exn s = (output_string stderr (s ^ "\n"); flush stderr)
  in
    match ex with
    | Tataf_libcore.CmdLine.InvalidOption (arg, msg) ->
        ((("Invalid Argument '" ^ (arg ^ ("': " ^ msg))) |> Tataf_libcore.Color.red) |>
           print_exn;
         1)
    | Invalid_argument msg -> ((msg |> Tataf_libcore.Color.red) |> print_exn; 2)
    | Xml.Error(msg,pos) ->
        "Xml.Error:  " ^ Xml.error( msg, pos )
        |> Tataf_libcore.Color.red
        |> print_exn; 10
    | Dtd.Prove_error error -> let open Dtd
        in
          let msg =
            (match error with
             | UnexpectedPCData -> "unexpected PCDATA"
             | UnexpectedTag s -> "unexpected Tag: '" ^ (s ^ "'")
             | UnexpectedAttribute s -> "unexpected Attribute: '" ^ (s ^ "'")
             | InvalidAttributeValue s ->
                 "invalid Attribute value: '" ^ (s ^ "'")
             | RequiredAttribute s -> "Required Attribute: '" ^ (s ^ "'")
             | ChildExpected s -> "child expected: '" ^ (s ^ "'")
             | EmptyExpected -> "empty element expected"
             | DuplicateID s -> "duplicate id: '" ^ (s ^ "'")
             | MissingID s -> "missing id: '" ^ (s ^ "'"))
          in ((("Dtd.Prove_error -- " ^ msg) |> Tataf_libcore.Color.red) |> print_exn; 10)
    | Tataf_libta.UppaalParser.Parse_error (p1, p2, msg, s) -> let open Lexing
        in
          let hs =
            Tataf_libcore.Color.highlight_string Tataf_libcore.Color.red p1.pos_cnum p2.pos_cnum s in
          let line = string_of_int p1.pos_lnum in
          let offset = string_of_int ((p1.pos_cnum - p1.pos_bol) + 1)
          in
            (("UppaalParser.Parse_error in '" ^
                (p1.pos_fname ^
                   ("' at position " ^
                      (line ^
                         ("; " ^
                            (offset ^
                               (" (" ^
                                  ((string_of_int p1.pos_cnum) ^
                                     (")" ^ (" -- " ^ msg))))))))))
               |> Tataf_libcore.Color.red)
              |> (fun x -> ((x ^ ("\n" ^ hs)) |> print_exn; 11))
    | Tataf_libcore.CmdLine.ValidationError msg ->
        ((("ValidationError: " ^ msg) |> Tataf_libcore.Color.red) |> print_exn; 2)
    | Failure msg ->
        ((("Failure: '" ^ (msg ^ "'")) |> Tataf_libcore.Color.red) |> print_exn; 254)
    | (*---------*) e ->
        (* use ignore to avoid warning 21 - function does not return *)
        (* Is this print_backtrace reachable at all?  After raise? *)
        ("An unknown exception was thrown!\n" |> print_exn;
         ignore (raise e);
         Printexc.print_backtrace stderr;
         250)
;;
  
try main (); exit 0 with ex -> print_exception ex |> exit ;;

