(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Printf
open Tataf_libform.Common
open Tataf_libform.Form
open Tataf_libform.FormUtil
open Tataf_libform.PrintForm

open Tataf_libtaqe.DetectPrePostDelay

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libtatk.UppaalExpressionEvaluation
open Tataf_libta.UppaalWalker
open Tataf_libta.UppaalPrint
open Guard
open Tataf_libta.UppaalParser
open Printf
open Tataf_libta.UppaalTransformer
open Edge
open Tataf_libform.FormUtil
open Tataf_libform.PrintForm
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libcore.Util

(*----------------------------------------------------------------------------*)
  (**
    This is the definition of a procedure to perform local checks to detect
    edges violating a given atomic proposition p.
  *)

  class detectNonSupportingEdges nta p =
    object (self)
      inherit [Edge.t list] Tataf_libta.UppaalWalker.walker [] ~concat:(@)
                                                  (*concat must be explicit*)

    val mutable look_up : lookup = (new lookup nta)

    method! visit_edge (edge:Edge.t) =
        (* Updates currently not supported. *)
        let is_none = function None -> true | _ -> false in
        if is_none edge.update then []
        else
            [edge]



    method write_str_to_file filename (str:string) =
      let oc = open_out filename in
      output_string oc str;
      close_out oc;

    method stdout_asStringList program_path =
      let process_output_to_list2 = fun command ->
        let chan = Unix.open_process_in command in
        let res = ref ([] : string list) in
        let rec process_otl_aux () =
          let e = input_line chan in
          res := e::!res;
          process_otl_aux() in
        try process_otl_aux ()
        with End_of_file -> let stat = Unix.close_process_in chan in
        (List.rev !res, stat)
      in let (l,_) = process_output_to_list2 program_path in l

(* *** seems that we have kind of a sketch here, which never worked...
    method call_z3 f =
      let z3_binary =
          (* Check if environment variable with path to z3 binary is set *)
          try Sys.getenv("TATAF_Z3_BINARY") 
          with 
          Not_found -> 
              (* env var with path to z3 binary is not set, attempt to
                 guess based on $0 *)
              (*dbg#info("TATAF_Z3_BINARY environment variable not set, guessing
               * path to z3..."); *)
              let dirname = Filename.dirname Sys.argv.(0) in
              let z3_guess = (if (dirname = ".") then
                                ""
                              else
                                dirname ^ "/") ^ "z3" in
              (*dbg#info( "..., hmm, ..., let's try " ^ z3_guess ); *)
              z3_guess
      in
      let typelookup = (new lookup_typeString nta) in
      let smt2Declaration = typelookup#visit nta in
      let smt2_str = PrintForm.smt2_of_form f smt2Declaration in
      (* debuglevel >= 2: dump Z3 task *)
      (*dbg#ldebug_endline 2 ("Z3 task:\n" ^ smt2_str);*)
      let formula_file_path = Filename.temp_file "formula_" ".smt" in
      let _ = self#write_str_to_file formula_file_path smt2_str in
      let z3_command = z3_binary ^ " -smt2 " ^ formula_file_path in
      let exit_code = Sys.command (z3_command ^ " 1>&2") in
      (* check whether the command terminates normally *)
      (* Note: Currently, tataf will print out 'sat' on failure exit, thus 
       * we explicitly check for the error code and fail if z3 does not work properly *)
      match exit_code with
      |0 -> let result_list = self#stdout_asStringList z3_command in
             let cmp = (fun x -> match x with
              | "unsat" ->  true
              | "sat"   ->  false
              | _       -> (*dbg#info("could prove neither unsat nor sat");*)false)
             in if(List.exists(cmp) result_list) then true else false
      |_ -> failwith("Error occured in call to z3 binary, exit code: " ^
      string_of_int(exit_code))
*** *)
    end

(* Allows colorizing edges in a given edge list with a given color. 
 * Used to mark pre-post delayed edges in an output xml 
 *)
let colorize_ppd nta (edges:Edge.t list) (color:string) = 
    object (self)
    inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable

    method! transform_edge ({Edge. id; _} as edge) =
        let new_color = 
            if (List.exists (fun x -> x == edge) edges) 
            then Some color     (* ppd edge *)
            else None           (* no ppd edge, remove color*)
        in {edge with Edge.color = new_color}
    end
    |> fun transformer -> transformer#transform nta
    

let getPotentiallyNonSupportingEdges source p color = 
  let nta = Tataf_libta.UppaalParser.parse_xta4_file source in
  let walker = (new detectNonSupportingEdges nta "") in
  let potentiallyNonSupportingEdges = walker#visit nta in
  let new_nta = (colorize_ppd nta potentiallyNonSupportingEdges color) in
  match potentiallyNonSupportingEdges with 
      | [] -> (*dbg#info "No Pre-/ Postdelayed Edges found.";*) new_nta
      | _  -> (*dbg#info "\nDetected Pre-/ Postdelayed Edges, generating target
      file.\n";*) new_nta
