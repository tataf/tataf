(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  Module for debugging purposes.
*)

val debugOn : bool ref

val debug_level : int ref
val set_debug_level : int -> unit

val halt_on_warning : bool ref

val lmsg : int -> (unit -> string) -> unit
val msg : string -> unit

(**
  {2 Easy Module debug messages}
*)

class module_debugger :
  string ->
  object
    val debug_name : string

    val mutable print_debug_msg : string -> unit

    method enable : unit
    method disable : unit
    method is_enabled : bool

    method fail : 'a. string -> 'a
    method warn : string -> unit
    method info : string -> unit
    method debug : string -> unit
    method debug_endline : string -> unit
    method ldebug : int -> string -> unit
    method ldebug_endline : int -> string -> unit
  end

val register_debug_module : string -> module_debugger
val registered_debug_modules : unit -> string

val set_debug_module : string -> unit 
val unset_debug_module : string -> unit 
val set_debug_modules : string -> unit 
