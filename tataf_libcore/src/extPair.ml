(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
let first (a,_) = a

let second (_, b) = b

let min (a,b) = min a b

let max (a,b) = max a b

let map ~f (a,b) = (f a, f b)

let sum_int (a,b) = a + b

let sum_float (a,b) = a +. b

let prod_int (a,b) = a * b

let prod_float (a,b) = a *. b

let mapi ~f (a,b) = (f 0 a, f 1 b)

let to_list (a,b) = [a;b]

let of_list l = match l with
    | [a;b] -> (a,b)
    | _ -> failwith ("Attempted conversion of list with more than two elements "
           ^ " to a pair. Pairs must have exactly two elements!")
