(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

(* Modified from source: http://rosettacode.org/wiki/Word_wrap *)
let wordwrap_and_indent indent line_width txt =
  let indent_str = String.make indent ' ' in
  let words = Str.split (Str.regexp "[ \n\r\t]+") txt in
  let buf = Buffer.create 10 in
  Buffer.add_string buf indent_str;
  let _ =
    List.fold_left (fun (width, sep) word ->
      let wlen = String.length word in
      let len = width + wlen + 1 in
      if len > line_width then
      begin
        Buffer.add_char buf '\n';
        Buffer.add_string buf indent_str;
        Buffer.add_string buf word;
        (wlen, " ")
      end else begin
        Buffer.add_string buf sep;
        Buffer.add_string buf word;
        (len, " ")
      end
    ) (0, "") words
  in
  Buffer.contents buf
  
let wordwrap = wordwrap_and_indent 0


(** shortcut function for tedious definitions in List.fold_left *)
let str_collect ?(sep="\n") func i el =
  let pre = if i = "" then "" else i ^ sep in
  pre ^ (func el)
  
(** Returns a string representation of a list. Default separator is newline.
    The to_str function must be given. If the list is a string list, then
    you can use the identity-function. *)
let str_list ?(sep="\n") func =
  List.fold_left (str_collect ~sep:sep func) ""
  

(** 
 Returns a string which, when the value of forcebreak is larger than
 the length of src, is equal to the concatenation of: src, a lineabreak
 and dst. Else, is equal to an overwritten version of dst: such string
 is built by substituting the first characters of dst by the string src
 and, if in terms of lengths dst>src, also the remaining substring of
 dst (as an example of this last option, when src='aaa', dst='wxyz',
 function returns 'aaaz')
 *)
let prepend forcebreak src dst =
  let lens = String.length src in
    if lens > forcebreak 
      then
        src ^ "\n" ^ dst
      else
        src ^ String.sub dst lens (String.length dst - lens)

let prepend_lines pre s = 
     Str.split (Str.regexp "\n") s
  |> List.map (fun s -> pre ^ s)
  |> List.fold_left (fun i e -> i ^ e ^ "\n") ""

let remove_linebreaks s =
  let to_unescape = [
    ("\n", " ");
    ("\r", " ");
    ] in
  List.fold_left
  (fun i (patternIn, patternOut) -> 
    Str.global_replace (Str.regexp_string patternIn) patternOut i)
    s to_unescape
  
  
let string_from_file filename =
  let ic = open_in filename in
  let buf = Buffer.create (in_channel_length ic) in
  try
    while true do
      let line = input_line ic in
      Buffer.add_string buf line;
      Buffer.add_char buf '\n';
    done; assert false
  with End_of_file ->
    Buffer.contents buf
;;
  

let string_to_file filename content =
  if filename = "" || filename = "-" then
		print_endline content (* stdout *)
	else
		let oc = open_out filename in
		Printf.fprintf oc "%s\n" content;
		close_out oc;
		()
  
  
let columns2 ?(col1width=15) a b =
  let len_a = String.length a in
  let c1 = String.make col1width ' ' in
  a ^ (String.sub c1 len_a (col1width - len_a)) ^ " : " ^ b
  
  
let empty s = (String.length s) = 0

let string_of_char c = String.make 1 c

let has_prefix str prefix = 
    let regex = Str.regexp_string prefix in
    try
        Str.string_match regex str 0
    with _ -> false
  
(* Check whether suffix is a suffix of string str *)
let has_suffix ~str ~suffix =
    let regex = Str.regexp_string suffix in
    let pos = (String.length str) - (String.length suffix) in
    try
        Str.string_match regex str pos
    with _ -> false

(* Turn a string of the form 'FRONT_SUFFIX' into 'FRONT' by removing the
 * underscore/dot/... seperator and the suffix
 *)
let remove_suffix str suffix =
    (* Assert that suffix is actually a suffix of str *)
    if not (has_suffix str suffix) then
        raise (Invalid_argument ("extString/remove_suffix called with 'str': "
                    ^ str
                    ^ " and 'suffix':"
                    ^ suffix
                    ^ " ;'suffix' is no suffix of 'str'"))
    else
    let length = (String.length str) - (String.length suffix) -1 in
    String.sub str 0 length

(* Concat str with itself [times] times *)
let string_n_dup ~str ~times =
    let rec append_str collected_str remaining =
        if remaining = 0 then collected_str 
        else append_str (collected_str ^ str) (remaining - 1)
    in append_str str (times - 1)

(* Apply [f] to all chars of [str], and collect results in an array. *)
let map_to_array (f : (char -> 'a)) str =
    let len = String.length str in
    let rec map i arr =
        if i = len then arr
        else 
            let _ = (
                String.get str i
                |> f
                |> Array.set arr i
            ) 
            in map (i + 1) arr
    in map 0 (Array.make len (f (String.get str 0)))

(* Convert [str] to the char array of its characters. *)
let to_char_array str = map_to_array (fun c -> c) str

(* Convert [str] to the char list of its characters. *)
let to_char_list str = Array.to_list (to_char_array str)

let to_string_array str = map_to_array string_of_char str 

let to_string_list str = Array.to_list (to_string_array str)

(* Join chars in char list [cl] to form a new string. *)
let of_char_list (cl : char list) = 
    let rec aux str = function
        | [] -> str
        | c :: tl -> aux (str ^ (String.make 1 c)) tl
    in aux "" cl

(* Join chars in char array [carr] to form a new string. *)
let of_char_array carr = of_char_list (Array.to_list carr)

(* Short-circuit check if [predicate] is satisfied by any char of [str] *)
let exists ~predicate ~str =
    let rec check_el i =
        try
            if predicate (String.get str i) then
                true
            else
                check_el (i + 1)
        with Invalid_argument _ -> false (* whole string checked *)
    in check_el 0
    
(* Short-circuit check if [predicate] is satisfied by all chars of [str] *)
let for_all ~predicate ~str =
    let rec check_el i =
        try
            if not (predicate str.[i]) then
                false
            else
                check_el (i + 1)
        with Invalid_argument _ -> true (* whole string checked *)
    in check_el 0

(* Count number of chars of [str] that satisfy [predicate] *)
let count ~predicate ~str =
    let rec count_el i cnt =
        try
            if predicate (String.get str i) then
                count_el (i + 1) (cnt + 1)
            else
                count_el (i + 1) cnt
        with Invalid_argument _ -> cnt (* whole string checked *)
    in count_el 0 0

(* Return the reversed string for [str] *)
let reverse str =
    let len = String.length str in
    let rec collect i s =
        if i < 0 then
            str
        else
            collect (i - 1) (s ^ (String.make 1 str.[i]))
    in collect len str


(* Returns the smallest i >= [pos] such that f i t.[i], if there is such an i. 
 * By default, pos = 0. *)
let lfindi ?(pos=0) str ~f =
  let n = String.length str in
  let rec loop i =
    if i = n then None
    else if f i str.[i] then Some i
    else loop (i + 1)
  in
  loop pos

 (* Returns the largest i <= [pos] such that f i str.[i], if there is such an i. 
  * By default pos = String.length str - 1. *)
let rfindi ?pos str ~f =
  let rec loop i =
    if i < 0 then None
    else begin
      if f i str.[i] then Some i
      else loop (i - 1)
    end
  in
  let pos =
    match pos with
    | Some pos -> pos
    | None -> String.length str - 1
  in
  loop pos


(* Strip any chars satisfying [drop] from [str]. 
 * Without optional [drop]: strip whitespace chars *)
let strip ?(drop=ExtChar.is_whitespace) t =
  let first_non_drop ~drop str = lfindi str ~f:(fun _ c -> not (drop c)) in
  let last_non_drop ~drop str = rfindi str ~f:(fun _ c -> not (drop c)) in
  let length = String.length t in
  if length = 0 || not (drop t.[0] || drop t.[length - 1])
  then t
  else
    match first_non_drop t ~drop with
    | None -> ""
    | Some first ->
        match last_non_drop t ~drop with
        | None -> assert false
        | Some last -> String.sub t first (last - first + 1)

(* Return new string that contains exactly all chars of [str] that
 * satisfy [pred] *)
let filter ~predicate ~str =
    let length = String.length str in
    let rec filter_el i filtered_str =
        if i = length then filtered_str
        else if predicate (str.[i]) then
            filter_el (i + 1) (filtered_str ^ (String.make 1 str.[i]))
        else filter_el (i + 1) filtered_str
    in filter_el 0 ""

(* Apply [f] to all chars of [str] and return the resulting string. *)
let map ~f ~str =
    let length = String.length str in
    let rec apply_f i new_str =
        if i = length then new_str
        else apply_f (i + 1) (new_str ^ (String.make 1 (f str.[i])))
    in apply_f 0 ""


(* ... f( f( f(init str.[0] ) str.[1] ) str.[2] ) *)
let fold ~f ~init ~str =
    let length = String.length str in
    let rec fold_rec i acc =
        if i = length then acc
        else fold_rec (i + 1) (f acc str.[i])
    in fold_rec 0 init

(* shorten ~str:"longlonglongstring" ~length:4 = "long..." *)
let shorten str ~length =
    if String.length str <= length then
        str
    else
        let sub = String.sub str 0 length in
        sub ^ "..."
