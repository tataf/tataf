(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(** 
  Extended functions for chars.
*)

val is_lowercase : char -> bool
(* [is_lowercase c] returns true if c is a lowercase alphabetic char 
   else false. *)

val is_uppercase : char -> bool
(* [is_uppercase c] returns true if c is an uppercase alphabetic char
   else false. *)

val is_alpha : char -> bool
(* [is_alpha c] returns true if and only if 
   [c] is an (uppercase or lowercase) alphabetic char 
   or [c] is the commonly used seperator "_". *)

val is_digit : char -> bool
(* [is_digit c] returns true if c is a digit '0' - '9'
   else false. *)

val is_alphanum : char -> bool
(* [is_digit c] returns true if c is an alphanumeric char or the commonly used
 * seperator char "_". *)


val is_whitespace : char -> bool
(* [is_whitespace c] returns true if c is a whitespace char 
   (' ', '\r', '\n', '\t') else false. *)

val is_left_paren : char -> bool
(* [is_left_paren c] returns true if c is a left parenthesis
   ('(', '{', '[') else false *)

val is_right_paren : char -> bool
(* [is_right_paren c] returns true if c is a right parenthesis
   (')', '}', ']') else false *)

val get_digit : char -> int option
(* [get_digit c] returns "Some i" if "is_digit c" else None. *)
