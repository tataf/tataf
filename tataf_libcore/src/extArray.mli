(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
val is_empty : 'a array -> bool
(* [is empty arr] returns true iff array [arr] contains zero elements. *)

val filter : predicate:('a -> bool) -> 'a array -> 'a array
(* [filter predicate arr] returns a new array that contains all elements of 
   [arr] that satisfy [predicate] *)

val count : predicate:('a -> bool) -> 'a array -> int
(* [count predicate arr] counts the number of elements [arr] that satisfy 
   [predicate]. *)

val filter_opt : 'a option array -> 'a array
(* [filter_opt arr] filters all "Nones" from array [arr] and replaces 
   all elements "Some x" in [arr] with x. *)

val reverse : 'a array -> 'a array
(* [reverse arr] reverses the order of the elements of array [arr]. *)

val find : predicate:('a -> bool) -> 'a array -> 'a option
(* [find f arr] returns the first element of [arr] that satisfies [predicate] *)

val find_some : f:('a -> 'b option) -> 'a array -> 'a option
(* [find_some f arr] returns the first element e of [arr] so that 
   (f e) evaluates to "Some x". *)

val min : ?cmp:('a -> 'a -> int) -> 'a array -> 'a option
(* [min cmp arr] returns the minimum element of array [arr] according to the
   compare function [cmp]. By default, uses Pervasives.compare. *)

val max : ?cmp:('a -> 'a -> int) -> 'a array -> 'a option
(* [max cmp arr] returns the maximum element of array [arr] according to the
   compare function [cmp]. By default, uses Pervasives.compare. *)

val swap : int -> int -> 'a array -> 'a array
(* [swap i1 i2 arr] returns the array [arr] with values at indices [i1] and [i2]
   swapped. *)

val combine : 'a array -> 'b array -> ('a * 'b) array
(* [combine arr1 arr2] constructs an array of pairs of elements from the arrays 
   [arr1] and [arr2]. Behaves like "List.combine" but for arrays. *)

val sample : 'a array -> 'a 
(* [sample arr] picks an element uniformly at random from array [arr].
 * Note: This operation has runtime costs of O(1) whereas the same operation 
 * has runtime costs of O(n) on lists
 * (but that slower operation is also provided for convenience, 
 * compare documentation for module 'extList'. *)
   
val sample_without_replacement: 'a array -> 'a * 'a array
(* [sample_without_replacement arr] picks an element uniformly at random 
 * (without replacements) from array [arr] and discards it, then returns 
 * a tuple: (sample, arr_after_sampling).
 * Note: This operation has runtime costs of O(1) whereas the same operation 
 * has runtime costs of O(n) on lists
 * (but that slower operation is also provided for convenience, 
 * compare documentation for module 'extList'. *)
