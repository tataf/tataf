(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

(** 
  Extended functions for strings.
*)


val wordwrap_and_indent : int -> int -> string -> string
(** [wordwrap_and_indent indent line_width txt] formats a given text [txt],
  so that a given maximum line width [line_width] is satisfied. Longer lines
  will be word wrapped. The complete block will be idented by [indent].
  
  @return Formatted text with line breaks and leading white spaces.
  @see <http://rosettacode.org/wiki/Word_wrap#OCaml> unmodified source.
*)

val wordwrap : int -> string -> string
(**
  shorthand for [wordwrap_and_indent] with prespecified indent of 0.
*)

val str_collect : ?sep:string -> ('a -> string) ->  string -> 'a -> string
(**
  todo: description
  
  @param sep Specifies the separator.
*)

val str_list : ?sep:string -> ('a -> string) -> 'a list -> string 
(**
  [str_list (?sep) func list] returns the string representation of a given 
  list [list]. Each element of the list is mapped with the specified 
  function [func] into string.
  
  @param sep Specifies the separator between elements. The separator will
    be not printed at the beginning or the end of the list, only between
    elements.
    
  @author Jeremi Dzienian
*)

val prepend : int -> string -> string -> string
(**
  [prepend forcebreak src dst] ...
*)

val prepend_lines : string -> string -> string
(**
  [prepend_lines pre s] prepends each line of the multiline string [s] 
  with the specified prefix [pre].  
*)

val remove_linebreaks : string -> string
(**
  [remove_linebreaks s] removes all line breaks from string [s]. 
*)

(** {2 Strings and Files} *)

val string_from_file : string -> string
(** [string_from_file filename] reads the file [filename] and returns 
  the complete content as string. *)

val string_to_file : string -> string -> unit
(** [string_to_file filename content] outputs the given string [content] into
  the file with name [filename]. *)  
  
  
val columns2 : ?col1width:int -> string -> string -> string
(** [columns2 ?col1width c1 c2] formats the string into two columns. The width
  of the first column is by default 20, and can be given by [col1width].
*)

val empty : string -> bool
(** [empty s] checks if [s] is the empty string (e.g. has length = 0)*)

val string_of_char : char -> string
(** [string_of_char c] converts char [c] into a string with only the char c. *)

val has_prefix : string -> string -> bool
(** [starts_with str prefix] outputs true iff the given string [str] has prefix
  [prefix]
*)

val has_suffix : str:string -> suffix:string -> bool
(** [has_suffix str suffix] outputs true iff the given string [str] has suffix
  [suffix]
*)

val remove_suffix : string -> string -> string
(** [has_suffix str suffix] takes as argument string [str] of the form
  'front_suffix' and the [suffix] to remove from the back (including an
  underscore and returns the string 'front'. Raises 'Invalid_argument' if
  [suffix] is not a suffix of [str].
*)
val string_n_dup :  str:string -> times:int -> string
(** [string_n_dup str times] takes as argument string [str] and int [times] 
 *  and returns a string of the form 'strstrstr...'. Can be used to build longer
 *  suffix strings.
*)

val map_to_array : (char -> 'a) -> string -> 'a array
(* [map_to_array f str] applies function [f] to all chars of [str], 
   collects results in an array and returns it. *)

val to_char_array : string -> char array 
(* [to_char_array str] converts [str] to a char array of its characters. *)

val to_char_list : string -> char list 
(* [to_char_list str] converts [str] to a char list of its characters. *)

val to_string_array : string -> string array
(* [to_char_array str] converts [str] to a string array of strings of 
   its characters. *)

val to_string_list : string -> string list
(* [to_char_array str] converts [str] to a string list of strings of 
   its characters. *)

val of_char_list : char list -> string
(* [of_char_list cl] joins char list [cl] to form and return a string with 
   these characters. *) 

val of_char_array : char array -> string
(* [of_char_array ca] joins char array [ca] to form and return a string with 
   these characters. *) 

val exists : predicate:(char -> bool) -> str:string -> bool
(* [exists predicate str] performs a short-circuit check if [predicate] 
   is satisfied by any char of [str] *)

val for_all : predicate:(char -> bool) -> str:string -> bool
(* [exists predicate str] performs a short-circuit check if [predicate] 
   is satisfied by all chars of [str] *)

val count : predicate:(char -> bool) -> str:string -> int
(* [count predicate str] counts the number of chars of [str] that satisfy the
   given [predicate]. *)

val reverse : string -> string
(* [reverse str] returns a new string which has the chars in [str] in reversed 
   order. *)

val lfindi : ?pos:int -> string -> f:(int -> char -> bool) -> int option
(* [lfindi pos str f] Returns the smallest i >= [pos] such that f i str.[i], 
   if there is such an i. By default, [pos] = 0. *)

val rfindi : ?pos:int -> string -> f:(int -> char -> bool) -> int option
(* [rfindi pos str f] Returns the largest i <= [pos] such that f i str.[i], 
   if there is such an i. By default, [pos] = 0. *)

val strip : ?drop:(char -> bool) -> string -> string
(* [strip drop str] strips any chars satisfying [drop] from [str]. 
   Without optional [drop]: strip whitespace chars *)

val filter : predicate:(char -> bool) -> str:string -> string 
(* [filter predicate str] returns new string that contains exactly 
   all chars of [str] that satisfy [pred] *)

val map : f:(char -> char) -> str:string -> string
(* [map f str] applies [f] to all chars of [str] and returns 
   the resulting string. *)

val shorten : string -> length:int -> string
(* [shorten str length] abbreviates [str] with "..." after [length] chars *)
