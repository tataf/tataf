(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  Allows colorful output on the console. This works only on posix-like
  shells.
*)

val colors_enabled : bool ref
(**
  If this option is set to [false], then all colorization function will 
  have no effect, and returns the input string. Default is [true].
*)

(**
  {2 Colorization functions}
*)


val red : string -> string
(**
  [red msg] returns [msg] in red.
*)

val green : string -> string
(**
  [green msg] returns [msg] in green.
*)

val yellow : string -> string
(**
  [yellow msg] returns [msg] in brown.
*)

val blue : string -> string
(**
  [blue msg] returns [msg] in blue.
*)

val purple : string -> string
(**
  [purple msg] returns [msg] in purple.
*)

val light_blue : string -> string
(**
  [light_blue msg] returns [msg] in light blue.
*)

val gray : string -> string
(**
  [gray msg] returns [msg] in gray.
*)

val white : string -> string
(**
  [white msg] returns [msg] in white.
*)

val highlight : string -> string

(**
  ...
*)
val highlight_string : (string -> string) -> int -> int -> string -> string

