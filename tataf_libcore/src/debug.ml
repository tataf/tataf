(** Printing (conditional) debugging messages. Currently they go to stderr. *)

(* FIXME: mechanics of debug on/off okay? *)

(** This file has been adapted from the Jahob project:
 * http://lara.epfl.ch/w/jahob_system
 *
 *  Andreas Podelski, Thomas Wies: Counterexample-guided focus. POPL 2010:
 *  249-260
 *
 * Karen Zee, Viktor Kuncak, Martin C. Rinard: Full functional verification of
 * linked data structures. PLDI 2008: 349-361
 *) 

(* enable/disable all debug messages -- set e.g. via optionblocks *)
let debugOn = ref false

(* debug level -- set e.g. via optionblocks *)
let debug_level = ref 1

let set_debug_level l = if l >= 0 then debug_level := l

(* raise exception on warnings -- set e.g. via optionblocks *)
let halt_on_warning = ref false

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

(* output channel for debug messages *)
let debug_chan = stderr (*stdout*)

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

(* legacy functions to output debug messages directly, i.e., bypassing
   registered  debug modules
*)
let lmsg l msg =
  if (!debugOn || !debug_level > 0) && l <= !debug_level then
    (output_string debug_chan ("..." ^ msg ()); flush debug_chan)

let msg s = lmsg 0 (fun () -> s)

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

let debug_msg (msg : string) : unit =
  if (!debugOn) then
    output_string debug_chan (Color.purple msg); flush debug_chan

(* Use register_debug_module() (see below) to create new module_debugger
   objects.  register_debug_module() ensures that the new module is accessible
   via the command line options.
*)
class module_debugger (name : string) = 
  object (self)

    val debug_name = name

    val mutable print_debug_msg : string -> unit = ignore

    method enable =
      print_debug_msg <- debug_msg

    method disable =
      print_debug_msg <- ignore

    method is_enabled =
      print_debug_msg == ignore

    method fail : 'a. string -> 'a = 
      fun msg -> raise (Invalid_argument msg)

    method warn msg =
      let cmsg = Color.yellow ("Warning: " ^ msg ^ "\n") in 
      output_string stderr cmsg;
      if !halt_on_warning 
        then self#fail "Halt on Warning!"

    method info msg = 
      let cmsg = Color.light_blue (msg ^ "\n") in
      output_string stderr cmsg; flush stderr

    method debug msg = 
      print_debug_msg msg

    method debug_endline msg = 
      self#debug (msg ^ "\n")

    method ldebug lvl msg = 
      if (lvl <= !debug_level) then
        self#debug msg

    method ldebug_endline lvl msg = 
      (* do not call ldebug with (msg ^ "\n") to avoid unnecessary string
         concat if lvl too low *)
      if (lvl <= !debug_level) then
        self#debug_endline msg

end ;;

let debug_modules = Hashtbl.create 0

let register_debug_module (name : string) : module_debugger = 
  let dbg = (new module_debugger name) in
  try 
    let _ = Hashtbl.find debug_modules name in
    dbg#fail ("Debug.register_debug_module: " ^ name ^ " already registered.")
  with Not_found -> Hashtbl.add debug_modules name dbg; dbg

(* get comma separated, alphabetically sorted list of the names of currently
   registered debug modules -- call late to cover all registrations!
*)
let registered_debug_modules = fun () ->
  let key_list = Hashtbl.fold (fun key value l -> key :: l) debug_modules [] in
  let sorted_key_list = List.sort String.compare key_list in
  List.fold_right (fun elem str -> 
                     match str with
                       | "" -> elem
                       | _  -> (elem ^ ", " ^ str ) ) sorted_key_list ""


let set_debug_module (name : string) : unit = 
  try
    (Hashtbl.find debug_modules name)#enable
  with Not_found -> print_string ("debug module " ^ name ^ " not registered.\n")

let unset_debug_module (name : string) : unit = 
  try
    (Hashtbl.find debug_modules name)#disable
  with Not_found -> print_string ("debug module " ^ name ^ " not registered.\n")

let set_debug_modules (names : string) : unit = 
  List.iter set_debug_module (Util.split_by "," names)

