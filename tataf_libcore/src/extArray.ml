(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
let is_empty = function | [||] -> true | _ -> false

let filter ~(predicate:'a -> bool) arr = 
    Array.to_list arr |> List.filter predicate |> Array.of_list

let count ~(predicate: 'a -> bool) arr =
    Array.to_list arr |> List.filter predicate |> List.length

let filter_opt arr =
    let rec loop new_list = function
        | [] -> new_list
        | hd :: tl 
          -> begin match hd with
          | None -> loop new_list tl
          | Some x -> loop (x :: new_list) tl
             end
    in loop [] (Array.to_list arr) |> Array.of_list


let reverse arr = Array.to_list arr |> List.rev |> Array.of_list

let find ~predicate arr =
    let rec loop = function
        | [] -> None
        | hd :: tl -> if predicate hd then Some hd else loop tl
    in Array.to_list arr |> loop

let find_some ~f arr = find ~predicate:(fun x -> Util.not_none (f x)) arr

let min ?(cmp=compare) arr = Array.to_list arr |> ExtList.min ~cmp

let max ?(cmp=compare) arr = Array.to_list arr |> ExtList.min ~cmp

let swap index1 index2 arr =
    let v1 = Array.get arr index1 in
    let v2 = Array.get arr index2 in
    Array.set arr index1 v2; Array.set arr index2 v1; arr

let combine arr1 arr2 =
    List.combine (Array.to_list arr1) (Array.to_list arr2)
    |> Array.of_list

let sample arr = 
    let len = Array.length arr in
    if len = 0 then
        failwith ("ExtArray: Requested random_choice from empty array..")
    else
       Random.int len |> Array.get arr

let sample_without_replacement arr =
    let len = Array.length arr in
    if len = 0 then
        failwith ("ExtArray: Requested random_choice from empty array..")
    else
        let index = Random.int len in
        let sample = Array.get arr index in
        let new_arr = 
            let head = Array.sub arr 0 index in
            let tail_len = (Array.length arr) - index - 1 in
            begin 
                if tail_len <= 0 then 
                    head 
                else 
                    let tail = Array.sub arr (index + 1) tail_len in
                    Array.concat [head; tail]
            end
        in sample, new_arr
