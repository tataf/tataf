(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(** 
  Module to parse command line arguments as defined by {!Optionblocks} into
  the global variables defined in {!Options}.  In addition to the standard
  Arg module from Ocaml, it adds the the ability to define subcommands and
  provides a better help system.
*)

(** {2 HOWTO} *)

(**
  The command line interface 
          supports subcommands like the subversion client svn(1).  Each
  subcommand can have flags and take positional parameters.  Unlike svn(1),
  subcommands can again have subcommands etc.

  Each subcommand is defined by one optionblock in module {!Optionblocks}.
  An optionblock defines (a) how to parse the given command line by module
  {!CmdLine} and (b) all information to create the help pages as requested by
  the (implicit) flag [--help].

  Usage examples:
  {[tool --help]}
  The tool itself counts as level 0 subcommand, direct flags are
  correspondingly defined by an optionblock, which directly supports the flag
  [--help].
  {[tool dump --help]}
  On level 1, there is, e.g., the subcommand [dump] (which of course supports
  [--help]) and further subcommands like [symboltable]:
  {[tool dump symboltable --help]}
*)

(** {1 Design Principles - Options, Optionblocks, TaTaF, } *)
(**
  When adding a new subcommand, one typically needs to touch the following
  four files:

  {ol
  {- [main/options.mli] - to declare global variables like flags which
      indicate which subcommand has been selected, or file names, etc.}
  {- [main/options.ml] - to define the global variables from
      [main/options.mli] with their respective default values}
  {- [main/optionblocks.ml] - to add optionsblocks which define the
      subcommand name, its parameters, its help text etc., and which fills in
      the global variables in [main/options.mli]}
  {- [TaTaF_.ml] - to use the global variables [main/options.mli] to
      call the corresponding functions.  Note: this is [TaTaF_.ml] (with
      underscore) from which the file [TaTaF.ml] is generated in a
      pre-processing step.}
  }

  In the following, we will discuss them in the order 1. to 4.

  Module {!CmdLine} is called in [TaTaF_.ml] for a list of optionblocks and
  interprets the optionblocks to parse a given command line.  This module
  can typically be viewed as a black box when adding a new subcommand.

  Note: the style of command line processing is strongly inspired by GNU
  [getopt] and the help output by the common GNU tools.  Too see good examples
  for flags and help output, use, e.g., [ls --help].
*)

(** {1 Adding the New Subcommand [reverse] } *)
(**
  Assume we want to add a new (nonsense) subcommand [reverse] (as a subcommand
  of [transform]) which reverses all identifier names in an NTA and query,
  e.g., [id_t] becomes [t_di].  Assume the help text is supposed to read as
  follows:

  {v
USAGE (reverse)

    tool transform reverse [OPTION]... SOURCE

DESCRIPTION (reverse)

    Reverse all identifier names in an NTA and query.

  -c, --clocksonly            Only reverse clock names.
  -n, --num N                 Only reverse the first N identifiers.
  -o, --output FILENAME       Write result to FILENAME (instead of stdout).
  -p, --queryout FILENAME     Filename for the transformed query output (only
                              effective with '-q').
  -q, --query FILENAME        Query-File for the given Uppaal model. If this
                              option is used, an output filename for the
                              transformed query must be specified with '-p'.

POSITIONAL ARGUMENTS (reverse)

   SOURCE                     Required: Specifies the source Uppaal file.  This
                              file must be in XTA 4 (Xml format).


EXAMPLES

    Reverse identifiers only in x.xml and write result to stdout:

        tool transform reverse x.xml

    Reverse identifiers in x.q and x.xml and write result to xr.q and xr.xml: 

        tool transform reverse -q x.q -p xr.q -o xr.xml x.xml
  v}

  That is, SOURCE is required while using a query is optional.  But if a query
  file is given as input, then there must be a filename to output the query to
  (because stdout is reserved for outputting the transformed NTA).
  Giving the additional flag [--clocksonly] should just apply the reversal to
  clock names.

  Note that, in general, flags can be only short, e.g. [-c], or only long,
  e.g. [--clocksonly]; here we decided to offer both forms for all flags.
*)

(** {1 Declaring Global Variables for [reverse] } *)
(**
  For our example, we need seven global variable {b declarations}:

  {ul
  {- [val reverse : bool ref] - should set to true if subcommand [reverse]
        is selected on the command line}
  {- [val clocksonly : bool ref] - should be set to true if only clock names
        should be reversed}
  {- [val reverse_N : int ref] - should give the number of identifiers to be
        reversed or 0}
  {- [val reverse_nta : string ref] - should give the name of the input NTA}
  {- [val reverse_query : string ref] - should give the name of the output
        NTA or be empty; Note: `[-]' (minus) is understood by the main
        printing functions as denoting [stdout]}
  {- [val reverse_nta_out : string ref] - should give the name of the input
        Query file, or be empty}
  {- [val reverse_query_out : string ref] - should give the name of output
        Query file, or be empty}
  }

  Group these declarations together nicely in [options.mli] and provide some
  documentation as needed (see {!Options} for examples).

  Note: some subcommands all use [ppuppaal_out] to store the file name to
  write the transformed NTA to.  This variable was actually introduced for the
  pretty-printing subcommands, so it could be considered bad style to re-use
  it.  Yet there is not clear best practice.
*)

(** {1 Defining Global Variables for [reverse] } *)
(**
  For our example, we correspondingly need seven global variable {b definitions}
  providing proper default values:

  {ul
  {- [let reverse = ref false] - this flag which indicates whether the
      subcommand has been selected {b must} be initialised to [false], they
      are not automatically set to [false] if the subcommand is not selected
      on the command line}
  {- [let clocksonly = ref false]}
  {- [let reverse_N = ref 0]}
  {- [let reverse_nta = ref ""]}
  {- [let reverse_query = ref ""]}
  {- [let reverse_nta_out = ref ""]}
  {- [let reverse_query_out = ref ""]}
  }

  Again, one may want to group these definitions together nicely (see
  {!Options} for examples).
*)

(** {1 Defining the Optionblock for [reverse] } *)
(**
  An [optionblock] structure consists of the following components:

  {ul
  {- the {b key}, i.e., the name of the subcommand (here: ["reverse"])}
  {- the {b usage text}, a plain string}
  {- the {b subcommand enter function}, usually used to set the subcommand
      flag from {!Options} (here: [reverse])}
  {- the {b description}, a plain string}
  {- a list of {b flag specifications} (here: for -c, -o, etc.)}
  {- the {b positional arguments handler} (here: for SOURCE)} 
  {- a {b validation functions}, which may, e.g., check whether all required
      arguments and flags are given and all provided values are legal}
  {- a list of {b examples}, plain strings}
  }

  For our [reverse] subcommand, they could be filled in as follows:

  {ul
  {- {[let opt_reverse : optionblock = "reverse",]}
      defines the {b key}, it is good practice to let all optionblock names
      begin with prefix [opt_],}
  {- {["[OPTION]... SOURCE",]}
      provides the {b usage text} (note that it appears verbatim in the help
      text),}
  {- {[SCSet reverse,]}
      the {b subcommand enter function} sets the global variable [reverse] to
      [true] using the helper function {!CmdLine.SCSet}
      (cf. {!CmdLine.subcommand_enter_func}),}
  {- {["Reverse all identifier names in an NTA and query.",]}
      provides the {b description} (note that it appears verbatim in the help
      text; line-breaks are automatically inserted by {!CmdLine}),}
  {- {[ [("-c","--clocksonly","", Arg.Set clocksonly,
          "Only reverse clock names.");]}
      provides the first {b flag specification}.  A flag specification
      consists of
      {ul
      {- the short name (here: ["-c"]), may be empty; this string is used
        by the optionblock interpreter to identify flags,}
      {- the long name (here: ["--clocksonly"], may be empty,}
      {- a placeholder for a flag values (here: empty); if this is not the
        empty string, the optionblock interpreter expects an argument to the
        flag and passes it to the {b flag function} (see below); the name as
        such is not significant, it is only shown in the help text,}
      {- a {b flag function} which is called for the string following the flag
        if the flag value placeholder is not empty (here: helper function
        [Arg.Set clocksonly] which sets the boolean global variable
        [clocksonly] to [true]}
      {- a {b flag description}, a plain string which is shown in the help
        text} 
      }}
  {- {[ ("-n", "--num", "N", Arg.Set_int reverse_N,
         "Only reverse the first N identifiers."),]}
      here we use helper function {!Arg.Set_int} to set global variable
      [reverse_N] to an integer value,
      note that the default value of the corresponding variable(s) is not
      automatically added to the help text but you can integrate it manually
      into the help text string (cf. {!Optionblocks} for examples, e.g.
      [opt_tool] similarly calls a function to enumerate the available debug
      modules and appends that list to the help text); yet one has to be
      careful: in our case [reverse_N] is a {b variable}, which only initially
      has the default value, as soon as the first [-n] flag is processed, the
      value of [reverse_N] may change, so if the default value should be
      concatenated into the help text, it should better be a constant in
      {!Options},}
  {- {[ ("-o", "--output", "FILENAME", Arg.Set_string reverse_nta_out,
         "Write result to FILENAME (instead of stdout)."),]}
      here we use helper function {!Arg.Set_string} to set global variable
      [reverse_nta_out] to a string value,}
  {- {[ ("-p", "--queryout", "FILENAME", Arg.Set_string reverse_query_out,
         "Filename for the transformed query output (only effective with '-q')."),]}}
  {- {[ ("-q", "--query", "FILENAME", Arg.Set_string reverse_query,
         "Query-File for the given Uppaal model. If this option is used, an
         output filename for the transformed query must be specified with
         '-p'.")]}}
  {- {[ ], ]}
      close the list of flag specifications; the list may be empty,}
  }

  Note that the flags appear in the help text in the order they appear in the
  list here.  It is good practice to order them alphabetically (with possibly
  standard flags like [--help] and [--version] at the very end (cf. GNU
  tools).

  Further note that the flag [--help] is automatically defined for each
  subcommand; one should not add an own flag specification for [--help]. 

  {ul
  {- {[AnonymousParameters([
      ("SOURCE", Arg.Set_string reverse_nta,
       "Required: Specifies the source Uppaal file. This file must be in XTA 4
        (Xml format).");
      ]),]}
    here we define the positional (anonymous) parameters as a list of {b
    positional parameter definitions} (in our case: just one).

    A positional parameter definition consists of:
    {ul
    {- a {b name} which appears in the help text and must not be empty; note
      that the usage text in the help text is {b not} automatically
      constructed from the positional parameter definitions, that is, one is
      supposed to keep them consistent manually,}
    {- a {b parameter function} which is typically used to set the
      corresponding global variable (here: [reverse_nta]}
    {- a {b help text}, which is pretty much standard for common parameter
      functions, so it is good practice to just copy from the other examples
      in {!Optionblocks},)}
    }
    This list {b must not} be empty.
    If we neither have positional parameters nor subcommands (see below), we
    would write
  }
  {- {[ UndefinedParameters,]}
    instead,}
  }

  {ul
  {- {[(fun () -> validate_file_exists "SOURCE" !qeClockReduction_source;
           if reverse_query <> "" && reverse_query_out = ""  then
             raise (InvalidOption("-p","Output filename for the transformed query missing."));
           if reverse_query = "" && reverse_query_out <> ""  then 
             raise (InvalidOption("-q","Query file missing."))
          )]}
      the last but second component is a validation function which is called
      after the optionblock has been completely processed;  it can use helper
      function to check whether files exist, whether both of two dependent
      flags are given (as here), we could also check that [reverse_N] is not
      negative; see {!Optionblocks} for more examples and {!CmdLine} for more
      helper functions,
      
      if we did not want any validation, we would write}
  {- {[ no_validation,]}
      instead}
  }

  {ul
  {- {[ [("Reverse identifiers only in x.xml and write result to stdout:",
    "x.xml");]}
      the last component is a possibly empty list of {b examples}; each
      example consists of a description, which is shown verbatim (with
      automatic linebreak) in the help text, and the parameters which make up
      the example; note that the binary name and the corresponding
      subcommand(s) are automatically prepended,}
  {- {[ ("Reverse identifiers in x.q and x.xml and write result to xr.q and xr.xml:",
    " -q x.q -p xr.q -o xr.xml x.xml") ] ]}}
  }

  Et voila, we are done, this would be the optionblock for our subcommand
  [reverse].
*)

(** {1 Making [reverse] a subcommand of [transform] } *)
(**
  As described above, subcommands can have subcommands.  Our new subcommand
  [reverse] should be a subcommand of [transform] so we need to update the
  optionblock of [transform].

  Intead of positional parameters, it has the component:

  {v Subcommands([ ... ]) v}

  with a list of optionblocks.  So we would just write

  {v Subcommands([ ...; opt_reverse ]) v}

  to make [reverse] an effective subcommand of [transform]; it automatically
  appears in the help text of [transform] then.

  The validation function of [transform]'s optionblock checks whether at least
  one subcommand has been given, so we would write

  {v
  (fun () ->
    if ...
    || !reverse
      then () 
      else raise (InvalidOption("transform","No transformation specified."))
  ),
  v}

  to also consider [reverse] in this check.
*)

(** {1 Calling [reverse] in [TaTaF_.ml] } *)
(**
  In order to execute the function which implements [reverse], we need to call
  it in {!TaTaF.main}.

  There is

  {v let main () = ... v}

  which first uses {!CmdLine} to process the command line (do not touch!)
  followed by many lines of the form

  {v if !Options.reverse then reverse () else (); v}

  which call the function corresponding to the values of the global variables.

  If the function implementing [reverse] is not just a plain function (as in
  this case: it would, e.g., pass the input / output file names to the
  corresponding {!UppaalTransformer} subclass), it is good practice to
  have a dedicated function in [TaTaF_.ml] (here: [reverse()].

  Cf. [TaTaF_.ml] for examples.
*)


(** {2 Optionblocks Definition} *)

type subcommand_enter_func =
    SCUnit of (unit -> unit) 
      (** Calls a paremeterless function. *)
  | SCSet of bool ref 
      (** Set a Boolean reference to [true]. *)
  | SCClear of bool ref 
      (** Set a Boolean reference to [false]. *)
  | SCNothing 
      (** do nothing *)
(** Function that will be executed, when the optionblock is entered. For 
  subcommands this will happen on subcommand changes. For the main 
  optionblock this will not be called (?). *)

type description = string
(** Type for a description (of an optionblock). *)

exception InvalidOption of Arg.key * description
(** Exception may be thrown (intended by validation functions) to specify 
  an error during parsing/validation. The Help system will print the 
  specified [description] and print the help page for the current subcommand.*)

type validation_func = unit -> unit
(** Type of a validation function. If the validation fails, it should 
  throw {!CmdLine.InvalidOption} and specify the reason in the description.*)

type specentry = Arg.key * Arg.spec * Arg.doc
(** Single option definition like in the [Arg] module. *)

type specentry2 = Arg.key * Arg.key * Arg.doc * Arg.spec * Arg.doc

type usage_string = string
(** Type for the usage string. *)

type example = string
(** Tyoe of an example for the help system. *)

type example_entry = description * example
(** Type of an example entry in the help system of an {!CmdLine.optionblock}. *)

type anon_handler =
    Subcommands of optionblock list 
      (** Anonymous argument is interpreted as a subcommand. *)
  | AnonymousParameters of specentry list 
      (** Defines anonymous arguments for this optionblock. *)
  | UndefinedParameters 
      (** Anonymous arguments may not happen. *)
(** Specifies, what should happen on anonymous parameters (undefined in 
  optionblock). *)
  
and optionblock = Arg.key * usage_string * subcommand_enter_func * description 
    * specentry2 list * anon_handler * validation_func * example_entry list
(**
  Type to define the main options (command line arguments) or a subcommand
  for command line parsing.
*)



(** {2 Predefined Validation Functions} *)

val no_validation : validation_func
(** Predefined validation function, that does nothing. *)

val validate_notempty : Arg.key -> string -> unit
(** [validate_notempty key value] ensures that [value] is not the empty
  string. *)

val validate_modules_exist : Arg.key -> string -> string -> unit
(** [validate_modules_exist key requested_modules registered_modules] ensures that all modules in [requested_modules] are also elements of [registered_modules]
 *)

val validate_file_exists : Arg.key -> string -> unit
(** [validate_file_exists key value] ensures that [value] points to an 
  existing file on the file system. *)

val validate_list_lengths : Arg.key -> Arg.key -> string -> string -> unit
(** [validate_file_exists key1 key2 value1 value2] ensures that the strings 
 * [value1] and [value2] are comma-seperated lists of equal length. *)
 

val validate_gteq: Arg.key -> int -> int -> unit
(** [validate_gtq key value threshold] ensures that [value] is greater or
 * equal to a threshold. *)
  
val validate_empty_or_file_exists : Arg.key -> string -> unit
(** [validate_empty_or_file_exists key value] ensures that [value] points
  to an existinf file on the file system, but empty string for [value] is
  allowed. *)

val validate_is_directory : Arg.key -> string -> unit
(** [validate_is_directory key value] ensures, that [value] points to an
  existing directory on the file system. *)

(** This exception is placed here, due to a lack of good place. It should 
    possibly moved into another module. It should be used, whenever an
    Validation fails and tool should stop! *)
exception ValidationError of string
    
    
    
(** {2 Configuration and Processing } *)

val program_cmd : string ref
(** Command to start this program. This will be printed infront of the 
  usage strings by the help system. *)

val help_examples : bool ref
(** Specifies, if examples should be printed out by the help system. Defaults
  to [true].*)

val print_helppage : optionblock -> unit
(** Print the help page of the specified optionblock to stdout. *)

val opt_help : optionblock
(** Definition of the help subcommand (help system). *)

val process : optionblock -> unit
(** 
  [process optionblock] will parse the command line arguments. [optionblock]
  specifies the starting options (which can define multiple subcommands).
*)

val raise_invalid_option : Arg.key -> description -> 'a
(** Throws an {!CmdLine.InvalidOption} exception with given [key] and
  [description]. *)

