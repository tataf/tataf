(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
let is_lowercase c = 'a' <= c && c <= 'z'

let is_uppercase c = 'A' <= c && c <= 'Z'

let is_alpha c = is_lowercase c || is_uppercase c || c = '_'

let is_digit c = '0' <= c && c <= '9'

let is_alphanum c = is_alpha c || is_digit c

(* Is character a whitespace, a tab, a new line CR or LF *)
let is_whitespace (c:char) = (c = ' ') || (c = '\n') || (c = '\r') || (c = '\t')

(* is character a left parenthesis of any type *)
let is_left_paren (c:char) = (c = '(') || (c = '{') || (c = '[')

(* is character a right parenthesis of any type *)
let is_right_paren (c:char) = (c = ')') || (c = '}') || (c = ']')

let get_digit c =
    if not (is_digit c) then None
    else Some (int_of_string (String.make 1 c))
