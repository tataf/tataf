let rec listUnion xs ys = 
  match xs with
    | [] -> ys
    | x :: xs1 -> 
      if not (List.exists (fun y -> y=x) ys) then (listUnion xs1 (x::ys))
      else (listUnion xs1 ys)
	  
let rec range a b =
  if a > b then []
  else a :: range (a + 1) b

let rec range_step a b step =
  if a > b then []
  else a :: range_step (a + step) b step

let listProduct xs ys = 
  List.concat (List.map (fun x -> List.map (fun y -> (x, y)) ys) xs)

let rec listIntersection xs ys =
  match xs with
    | [] -> []
    | a::l -> 
      if (List.exists (fun x -> a=x) ys) then a::(listIntersection l ys)
      else (listIntersection l ys)

let listIsEmpty xs = 
  if List.length xs > 0 then false
  else true

let rec listMinus xs ys =
  match xs with
    | [] -> []
    | x :: xs1 -> 
      if (List.exists (fun y -> x = y) ys ) then listMinus xs1 ys 
      else x::(listMinus xs1 ys)


(******************************************************************************
  Lists to String
 ******************************************************************************)

let identity : 'a. 'a -> 'a = fun x -> x

(** shortcut function for tedious definitions in List.fold_left *)
let str_collect ?(sep="\n") func i el =
  let pre = if i = "" then "" else i ^ sep in
  pre ^ (func el)

(** Returns a string representation of a list. Default separator is newline.
    The to_str function must be given. If the list is a string list, then
    you can use the identity-function. *)
let str_list ?(sep="\n") func =
  List.fold_left (str_collect ~sep:sep func) ""
  
let str_list2 ?(sep="\n") func =
  List.fold_left (fun i el -> i ^ (func el) ^ sep) ""
  
let rec print_list = function
    | [] -> print_endline("")
    | hd :: tl -> print_string(hd); print_list tl

  
(* Added by Moritz -- Computations for numerical (and other) lists.*)
let min ?(cmp=compare) l =
    if listIsEmpty l then 
        None
    else
        let rec loop curr = function
            | [] -> Some curr
            | hd :: tl -> if cmp hd curr < 0 then loop hd tl else loop curr tl
        in loop (List.hd l) l

let max ?(cmp=compare) l =
    if listIsEmpty l then 
        None
    else
        let rec loop curr = function
            | [] -> Some curr
            | hd :: tl -> if cmp hd curr > 0 then loop hd tl else loop curr tl
        in loop (List.hd l) l

let sum_int = List.fold_left (+) 0 

let sum_float = List.fold_left (+.) 0.

let prod_int = List.fold_left (fun a b -> a * b) 1 

let prod_float = List.fold_left (fun a b -> a *. b) 1. 

(* Added by Moritz -- Reimplementations of some functions above *)

(* Constant time complexity instead of linear in "listEmpty" above *)
let list_empty = function | [] -> true | _ -> false

let list_union xs ys = 
    let rec loop new_list = function
        | [] -> new_list
        | hd :: tl -> if not (List.mem hd tl) then loop (hd :: new_list) tl 
                      else loop new_list tl
    in loop [] (xs @ ys)

let list_intersection xs ys = List.filter (fun x -> List.mem x ys) xs

let list_minus xs ys = List.filter (fun x -> not (List.mem x ys)) xs

let count ~predicate l =
    let rec count_el cnt = function
        | [] -> cnt
        | hd :: tl -> begin 
            if predicate hd then 
                count_el (cnt + 1) tl 
            else 
                count_el cnt tl
                      end
    in count_el 0 l

let sample l = Random.int (List.length l) |> List.nth l

let sample_without_replacement l = 
    let sample = sample l in
    sample, list_minus l [sample]
