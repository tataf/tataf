(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
 Color Module to provide colorized output on the console (Posix only).

 All functions return a string.
 *)

(**
 Is the color output enabled? If no, then this methods will have no effect.
 *)
let colors_enabled = ref true

let colored color msg = 
  if !colors_enabled then
    color ^ msg ^ "\027[0m"
  else
    msg

let red =        colored "\027[1;31m"
let green =      colored "\027[1;32m"
let yellow =     colored "\027[33m"
let blue =       colored "\027[34m"
let purple =     colored "\027[35m"
let light_blue = colored "\027[36m"
let gray =       colored "\027[37m"
let white =      colored "\027[1m"


let highlight = colored "\027[7m"

(**
  [highlight f i j s] hightlights the string [s] from position [i] to position
  [j] using the function [f] on the middle part of the string. Good use for
  [f] is one of the Color-funcionts (e.g. [Color.red]).
*)
let highlight_string f i j s =
  if i >= j then s else
  let sub s a b = String.sub s a (b - a) in
  let k = (String.length s) in
  let i = if i < 0 then 0 else i in
  let j = if j > k then k else j in
  let a = sub s 0 i in
  let b = sub s i j in
  let c = sub s j k in 
  a ^ (b |> f |> highlight) ^ c

