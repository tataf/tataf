(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Str
open String
open Sys

(** This exception is placed here, due to a lack of good place. It should 
    possibly moved into another module. It should be used, whenever an
    Validation fails and tataf should stop! *)
exception ValidationError of string

(****************************************************************************
  Types and global variable definitions.
 ******************************************************************************)

type subcommand_enter_func =
  | SCUnit of (unit -> unit)    (* calls the function with unit argument. *)
  | SCSet of bool ref           (* Set the reference to true. *)
  | SCClear of bool ref         (* Set the reference to false. *)
  | SCNothing                  (* Do nothing, switching to a subcommand. *)

type description = string

exception InvalidOption of Arg.key * description

type validation_func = unit -> unit
(* validation_func throws InvalidOption exception, when invalid. *)

let no_validation : validation_func = fun () -> ()

type specentry = Arg.key * Arg.spec * Arg.doc

(*to allow a command a short and a long version*)
type specentry2 = Arg.key * Arg.key * Arg.doc * Arg.spec * Arg.doc

type usage_string = string

type example = string

type example_entry = description * example

type anon_handler =
  | Subcommands of optionblock list
  | AnonymousParameters of specentry list
  | UndefinedParameters

and optionblock = Arg.key
                * usage_string
                * subcommand_enter_func
                * description 
                * specentry2 list 
                * anon_handler
                * validation_func 
                * example_entry list
                
let optionblock_name : optionblock -> Arg.key = function
  | (name,_,_,_,_,_,_,_) -> name
let optionblock_usage : optionblock -> usage_string = function
  | (_,usage,_,_,_,_,_,_) -> usage
let optionblock_enterfunc : optionblock -> subcommand_enter_func = function
  | (_,_,enterfunc,_,_,_,_,_) -> enterfunc
let optionblock_handler : optionblock -> anon_handler = function
  | (_,_,_,_,_,handler,_,_) -> handler
let optionblock_validator : optionblock -> validation_func = function
  | (_,_,_,_,_,_,validator,_) -> validator

let optionblock_speclist : optionblock -> specentry list = function
  | (_,_,_,_,sel,_,_,_) -> 
    List.fold_left 
      (fun i (skey, lkey, argdoc, spec, doc) -> 
        let sspec = if skey = "" then [] else [(skey, spec, doc)] in
        let lspec = if lkey = "" then [] else [(lkey, spec, doc)] in
        i @ sspec @ lspec  
      ) [] sel

let rec find_optionblock name = function
  | [] -> raise Not_found
  | c::cs -> if optionblock_name c = name 
             then c
             else find_optionblock name cs

let current_speclist : specentry list ref = ref []
let current_optionblock : optionblock ref = 
  ref ("","",SCNothing,"",[], UndefinedParameters,no_validation,[])
  
let last_optionblock : optionblock ref = ref !current_optionblock
  (* internal usage only (help system) *)
  
let current_anon_parameters : specentry list ref = ref []

let validate_oblocks = ref ([] : optionblock list)

(****************************************************************************
  Help System
 ******************************************************************************)

let program_cmd = ref Sys.argv.(0)
let help_topic = ref ""
let help_examples = ref true

(*TEST*)
let update_program_cmd (oblock:optionblock) =
  match oblock with
  | (cmdname,_,_,_,_,_,_,_) ->
    let cmdn = if cmdname = "" then "" else " " ^ cmdname in
    program_cmd := !program_cmd ^ cmdn

let usage (oblock:optionblock) =
  match oblock with
  | (_,usage,_,_,_,_,_,_) -> !program_cmd ^ " " ^ usage 

let rec print_helppage (oblock:optionblock) =
  match oblock with
  | (name,_,_,description,specl,handler,_,examples) ->
    let namestr = if name = "" then "" else " (" ^ name ^ ")" in
    let usagestr = usage oblock in
    let options = if specl = [] then "" else  
      "\n\n" ^ (arglist2 specl)
    in
    let handlerstr = match handler with
    | UndefinedParameters -> ""
    | AnonymousParameters(apl) -> 
        if apl = [] then "" else
        "\n\nPOSITIONAL ARGUMENTS" ^ namestr ^ "\n\n" ^ (arglist apl)
    | Subcommands(scl) ->
        if scl = [] then "" else 
        "\n\nSUBCOMMANDS" ^ namestr ^ "\n\n" ^ (subcommandlist scl)
    in
    let examplestr =
      if examples = [] 
      then ""
      else "\n\nEXAMPLES\n\n" ^  (examplelist examples)
    in
    let helpstring = "USAGE" ^ namestr ^ "\n\n" ^ (wwi 4 usagestr) ^ "\n\n"
                   ^ "DESCRIPTION" ^ namestr ^ "\n\n" ^ (wwi 4 description)
                   ^ options
                   ^ handlerstr
                   ^ if !help_examples then examplestr else ""
    in
    print_endline ( helpstring ^ "\n" )
and
  wwi ind txt = ExtString.wordwrap_and_indent ind (80-ind) txt
and
  arglist lst =
    List.fold_left 
    (fun i (arg,_,doc) -> format_argument i "" "" arg doc)
    "" lst
and
  arglist2 lst =
    List.fold_left 
    (fun i (sarg,larg,argdoc,_,doc) -> 
      if sarg = "" 
        then format_argument i "" larg argdoc doc
        else if larg = "" 
          then format_argument i sarg "" argdoc doc
          else format_argument i sarg larg argdoc doc
    ) "" lst
and 
  subcommandlist lst =
    List.fold_left 
    (fun i (arg,_,_,doc,_,_,_,_) -> format_argument i "" "" arg doc)
    "" lst
    
and
  format_argument i sarg larg argdoc doc = 
    let pre = if i = "" then "" else i ^ "\n" in
    let docstr = if doc = "" 
      then wwi 30 "(no helptext found)" 
      else wwi 30  doc 
    in
    let argstr = "  " ^ (if sarg <> "" && larg <> "" then
                           sarg ^ ", " ^ larg
                         else if sarg <> "" && larg = "" then
                           sarg
                         else if sarg = "" && larg <> "" then
                           "    " ^ larg
                         else
                           "")
                      ^ " " ^ argdoc in

    pre ^ (ExtString.prepend 29 argstr docstr)
and 
  examplelist lst =
    List.fold_left 
    (fun i (description,example) ->
       let pre = if i = "" then "" else i ^ "\n\n" in
       pre ^ (wwi 4 description) ^ "\n\n" 
       (* TODO: Revert change below if this is no good *)
           ^ (wwi 8 (!program_cmd ^ " " ^ example))
    ) "" lst


let opt_help : optionblock = "help",
  "[OPTION]... [TOPIC]",
  SCNothing,
  "Without any argument this will print the usage and help page of the program.
   You can get additional help to the subcommands, when you specify the 
   subcommand as argument (TOPIC). Alternatively you can add the '--help'
   argument anywhere, to get the best matching help page.",
  [],
  AnonymousParameters([
    ("TOPIC", Arg.Set_string help_topic,
     "Displays detailed help for the specified subcommand.");  
  ]),
  (fun () -> 
    let help_ob = if !help_topic = "" 
      then !last_optionblock 
        (* help about the main options (previous optionblock) *)
      else 
        match optionblock_handler !last_optionblock with
        | Subcommands(scl) -> 
            begin
              try find_optionblock !help_topic scl
              with Not_found -> 
                raise (Arg.Help ("unkonwn subcommand/topic: '" ^ !help_topic ^ "'."))
            end
          (* help about the specifies topic *)
        | _ -> raise (Arg.Help "unexpected error in help system!")
          (* can only happen, if parent subcommand has no subcommand section,
             and therefore could never be the parent subcommand. *) 
    in
      print_helppage help_ob; 
    exit 0 (* after showing help, exit program. *)
  ),
  [("Display how the help system works (this help page).",
    "help --help");
   ("Display the helppage of the subcommand 'print'.",
    "print --help");
   ("Display the helppage of the subcommand 'print' through the help subcommand",
    "help print.");
  ]

(****************************************************************************
  Functions for evaluation of the command line options.
 ******************************************************************************)

let extract_bad_message msg = 
  let re = regexp ":[ \b]*\\(.+\\)$" in
  try let _ = search_forward re msg 0 in
      matched_group 1 msg
  with Not_found -> "?:{" ^ msg ^ "}"

let extract_first_line msg = 
  let re = regexp "^.*" in
  try let _ = search_forward re msg 0 in
      (matched_string msg) ^ "\n"
  with Not_found -> ""

let validate oblock =
  let validator = optionblock_validator oblock in
  validator () (* raises exception, if invalid *)

let eval_enter_func () =
  match optionblock_enterfunc !current_optionblock with
  | SCUnit(f) -> f ()
  | SCSet(b) -> b := true
  | SCClear(b) -> b := false
  | SCNothing -> ()

let set_current_anon_parameters = function
  | AnonymousParameters(apl) -> current_anon_parameters := apl
  | _ -> current_anon_parameters := []

let set_current_optionblock oblock = 
  validate_oblocks := oblock :: !validate_oblocks;
  last_optionblock := !current_optionblock;
  current_optionblock := oblock;
  current_speclist := optionblock_speclist oblock;
  set_current_anon_parameters (optionblock_handler oblock);
  update_program_cmd oblock;
  (*update_usage ();*)
  eval_enter_func ()

let undefined_msg key =
  print_string ("unknown option: " ^ key ^ "\n")

(* @raise Arg.Bad to reject invalid arguments;
   the general strategy is to try functions like int_of_string() to see if
   that function is able to make an e.g. int out of the given string, and if
   not throws an exception; in the try-with blocks, we catch all exceptions
   raised by the corresp. function according to their specification <= 4.07.1
*)
[@@@warning "-11"]
let rec treat_anon_action value (name,action,_) = 
  match action with
  | Arg.Unit f -> f ()
  | Arg.Bool f -> 
      begin try f (bool_of_string value)
            (* see comments above *)
            with Invalid_argument _ -> 
              raise (Arg.Bad ("Anonymous argument '" ^ name ^ "' must be bool"))
      end
  | Arg.Set r -> r := true
  | Arg.Clear r -> r := false
  | Arg.String f -> f value
  | Arg.Symbol (symb,f) ->
      if List.mem value symb then f value 
      else begin
        raise (Arg.Bad ("Anonymous argument '" ^ name ^ "' must be one of..."))
      end
  | Arg.Set_string r -> r := value
  | Arg.Int f -> 
      begin try f (int_of_string value)
            (* see comments above *)
            with Failure _ -> 
              raise (Arg.Bad ("Anonymous argument '" ^ name ^ "' must be int"))
      end
  | Arg.Set_int r ->
      begin try r := (int_of_string value)
            (* see comments above *)
            with Failure _ -> 
              raise (Arg.Bad ("Anonymous argument '" ^ name ^ "' must be int"))
      end
  | Arg.Float f ->
      begin try f (float_of_string value)
            (* see comments above *)
            with Failure _ -> 
              raise (Arg.Bad ("Anonymous argument '" ^ name ^ "' must be float"))
      end    
  | Arg.Set_float r ->
      begin try r := (float_of_string value)
            (* see comments above *)
            with Failure _ -> 
              raise (Arg.Bad ("Anonymous argument '" ^ name ^ "' must be float"))
      end
  | Arg.Tuple specs -> 
      List.iter (fun spec -> treat_anon_action value (name,spec,"")) specs
  | Arg.Rest f -> 
      let l = Array.length Sys.argv in
      while !Arg.current < l - 1 do
        f Sys.argv.(!Arg.current + 1);
        incr Arg.current;
      done
  (* - the following match-all case is here to assure complete pattern
       matching even if new types are introduced by OCaml (as happened
       in 4.05.1); compileres older than 4.05.1 will generate "Warning 11:
       match case is unused"
     - for a proper solution, one may want to check arg.ml, function
       'parse_and_expand_argv_dynamic_aux' at >= 4.07.1 to implement missing
       pattern(s) *)
  | _ ->
      raise (Arg.Bad ("Anonymous argument type currently not supported"))
[@@@warning "+11"]

let lookup_anon key = 
  match optionblock_handler !current_optionblock with
  | AnonymousParameters(_) ->
      if !current_anon_parameters = []
      then raise (Arg.Bad ("Unexpected option: '" ^ key ^ "'")) 
      else 
        let current_param = List.hd !current_anon_parameters in
        begin
          treat_anon_action key current_param;
          current_anon_parameters := List.tl !current_anon_parameters 
        end
  | UndefinedParameters ->
          raise (Arg.Bad ("Unknown option '" ^ key ^ "'"))
  | Subcommands(scl) -> 
      try set_current_optionblock (find_optionblock key scl) 
      with Not_found -> raise (Arg.Bad ("Unknown subcommand '" ^ key ^ "'"))

let process oblock =
  set_current_optionblock oblock;
  try begin
    Arg.parse_argv_dynamic Sys.argv 
                             current_speclist
                             lookup_anon
                             ""; (*no extra message, when using '--help' *)
    (* validate oblocks *)
    List.fold_right (fun ob _ -> validate ob) !validate_oblocks ()
  end
  with Arg.Bad(msg) ->
       let bad = extract_bad_message msg in
       print_endline bad; failwith(bad)
     | Arg.Help(hmsg) ->
       begin 
         print_helppage !current_optionblock;
         raise (Arg.Help hmsg)
       end
      (*
     | InvalidOption(arg,msg) -> 
       print_string ("Validation failed, option '" ^ arg ^ "': " ^ msg ^ "\n")
      *)

(****************************************************************************
  Helper functions for validation.
 ******************************************************************************)

let raise_invalid_option arg msg =
  raise (InvalidOption(arg,msg))

let validate_notempty arg value =
  if value <> "" then ()
  else raise_invalid_option arg ("required!") 

let validate_file_exists arg value =
  if file_exists value then ()
  else raise_invalid_option arg ("'" ^ value ^ "' is not a valid file!")

let validate_list_lengths arg1 arg2 val1 val2 =
    let raise_error () = raise_invalid_option ( "LENGTHS: "^ arg1 ^ " " ^ arg2)
            ("'" ^ val1 ^ "', " ^ val2 ^ " are either not correctly "
            ^ " seperated or the two lists do not have equal length.")
    in
    try
        let l1 = Util.split_by "," val1 in
        let l2 = Util.split_by "," val2 in
        if (List.length l1) = (List.length l2) then ()
        else raise_error ()
    with _ -> raise_error () (* Splitting one of the lists failed... *)

let validate_modules_exist arg modules registered_modules =
    let modules_list = Str.split(Str.regexp( ",") ) modules in
    let registered_modules_list = Str.split(Str.regexp(", ") )registered_modules
    in
    let exists k l =
        List.fold_left( fun b x -> b || x = k)false l
    in
    let rec first_non_contained l1 l2 =
        match l1 with
        |[] -> []
        |hd::tl -> if (not (exists hd l2)) then [hd]
        else first_non_contained tl l2
    in
    let value = first_non_contained modules_list registered_modules_list 
    in
    if ((List.length(value)) == 0)
    then ()
    else
        raise_invalid_option arg ("'" ^ List.hd(value) ^ "' is no registered Debug module")



let validate_empty_or_file_exists arg value = 
  if value = "" 
    then ()
    else validate_file_exists arg value

let validate_gteq arg (value:int) (threshold:int) =
    if value >= threshold then ()
    else raise_invalid_option arg (string_of_int(value) ^ ": invalid value, should be at least "^string_of_int(threshold))

let validate_is_directory arg value =
  let valid = try is_directory value
              with Sys_error(_) -> false in
  if valid then ()
  else raise_invalid_option arg ("'" ^ value ^ "' is not a directory!")

