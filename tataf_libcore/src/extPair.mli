(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
val first :  'a * 'b -> 'a
(* [first (a,b)] returns first component [a] of the pair [(a,b)] *)

val second : 'a * 'b -> 'b
(* [second (a,b)] returns second component [b] of the pair [(a,b)] *)

val min :  'a * 'a -> 'a
(* [min (a,b)] returns smallest component of the pair [(a,b)] *)

val max :  'a * 'a -> 'a
(* [max (a,b)] returns largest component of the pair [(a,b)] *)

val map : f:('a -> 'b) -> 'a * 'a -> 'b * 'b
(* [map f (a,b)] returns the pair (f [a], f [b]) *)

val sum_int : int * int -> int
(* [sum_int (a,b)] returns a + b *)

val sum_float : float * float -> float
(* [sum_float init (a,b)] returns a +. b*)

val prod_int : int * int -> int
(* [prod_int (a,b)] returns a * b * init *)

val prod_float : float * float -> float
(* [prod_int (a,b)] returns a *. b *. init *)

val mapi : f:(int -> 'a -> 'b) -> 'a * 'a -> 'b * 'b
(* [mapi (a,b)] returns (f 0 a, f 1 b) *)

val to_list : 'a * 'a -> 'a list
(* [to_list (a,b)] returns [a;b] *)

val of_list : 'a list -> 'a * 'a
(* [of_list l] returns (a,b) if l = [a;b], else raises an exception. *)
