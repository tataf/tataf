val listUnion : 'a list -> 'a list -> 'a list
val range : int -> int -> int list
val range_step : int -> int -> int -> int list
val listProduct : 'a list -> 'b list -> ('a * 'b) list
val listIntersection : 'a list -> 'a list -> 'a list
val listIsEmpty : 'a list -> bool
val listMinus : 'a list -> 'a list -> 'a list
val identity : 'a -> 'a
val str_collect : ?sep:string -> ('a -> string) -> string -> 'a -> string
val str_list : ?sep:string -> ('a -> string) -> 'a list -> string
val str_list2 : ?sep:string -> ('a -> string) -> 'a list -> string

val min : ?cmp:('a -> 'a -> int) -> 'a list -> 'a option
(* [min cmp l] returns "Some x" where x is the minimum element of list [l] 
   according to comparison function [cmp] or "None" if list [l] is empty. *)

val max : ?cmp:('a -> 'a -> int) -> 'a list -> 'a option
(* [max cmp l] returns "Some x" where x is the maxmimum element of list [l] 
   according to comparison function [cmp] or "None" if list [l] is empty. *)

val sum_int : int list -> int

val sum_float : float list -> float

val prod_int :  int list -> int

val prod_float : float list -> float

val list_empty : 'a list -> bool
(* [list_empty xs] returns true iff list [xs] contains zero elements. 
   NOTE: Runtime is constant, whereas "listUnion" computes the length of the
   list and has linear runtime. *)

val list_union : 'a list -> 'a list -> 'a list
(* [list_union xs ys] returns a new list that contains all elements of the union 
   of lists [xs] and [ys] without duplicates. *)

val list_intersection : 'a list -> 'a list -> 'a list
(* [list_intersection xs ys] returns a new list that contains all elements of 
   list [xs] that are also element of list [ys]. *)

val list_minus : 'a list -> 'a list -> 'a list
(* [list_minus xs ys] returns a new list that contains all elements of 
   list [xs] that are not element of list [ys]. *)

val count : predicate:('a -> bool) -> 'a list -> int
(* [count predicate l] counts the number of elements of [l] that satisfy the
   given [predicate]. *)

val sample : 'a list -> 'a 
(* [sample l] picks an element uniformly at random (with replacements) 
 * from list [l].
 * Note: This operation has runtime costs of O(n) 
 * whereas the same operation has runtime costs of O(1) on arrays
 * (for reference see documentation of module 'extArray'. *)

val sample_without_replacement: 'a list -> 'a * 'a list
(* [sample_without_replacement l] picks an element uniformly at random 
 * (without replacements) from list [l] and discards it, then returns 
 * a tuple: (sample, l_after_sampling).
 * Note: Avoid in performance-sensitive contexts. 
 * This operation has runtime costs of O(n) 
 * whereas the same operation has runtime costs of O(1) on arrays
 * (for reference see documentation of module 'extArray'. *)
