(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(*=============================================================================
  Module: ExtXml
  -----------------
  Contains helper functions to work with xml files.
  
  Author: Jeremi Dzienian
 =============================================================================*)

(* NOTE: this is a verbatim copy of tataf-libha/src/ExtXml.ml for the sake * 
 *  of keeping tataf-libcore independent of xml-light;                     *
 *  in case of fixes or extensions, please fix the other one, too          *)

open Str
open Xml


let filter_name wanted = List.filter
 (fun e -> match e with
  | Element (name,_,_) -> name = wanted
  | PCData(_) -> false) 


exception Multiple_elements

let exists_element name elements =
  match filter_name name elements with
  | [] -> false
  | _::[] -> true
  | _ -> raise Multiple_elements


let attrib_or_empty element aname =
  try Xml.attrib element aname
  with Xml.No_attribute(_) -> ""

(*
let attrib_or_none ?(f=(fun x->x)) element aname =
  try Some(Xml.attrib element aname |> f)
  with Xml.No_attribute(_) -> None 
*)

exception No_element

let single_element_attrib name aname elements =
  match filter_name name elements with
  | [] -> raise No_element
  | element::[] -> Xml.attrib element aname
  | _ -> raise Multiple_elements 


(**
 * val xmlescape_of_string: string -> string
 *
 * Returns the escaped version of the string, so that it can be written into
 * an xml file.
 *)
let xmlescape_of_string s =
  let to_escape = [
    ("&", "&amp;");
    ("<", "&lt;");
    (">", "&gt;")
    ] in
  List.fold_left
  (fun i (patternIn, patternOut) -> 
    Str.global_replace (Str.regexp_string patternIn) patternOut i)
    s to_escape
    
let xmlunescape_of_string s =
  let to_unescape = [
    ("&amp;", "&");
    ("&lt;", "<");
    ("&gt;", ">")
    ] in
  List.fold_left
  (fun i (patternIn, patternOut) -> 
    Str.global_replace (Str.regexp_string patternIn) patternOut i)
    s to_unescape

let xmlattribute attr_name value = 
  if value = "" 
    then "" 
    else " " ^ attr_name ^ "=\"" ^ (xmlescape_of_string value) ^ "\""


(**
  @deprecated 
*)
let remove_doctype s = 
  try let start = search_forward (regexp "<!DOCTYPE") s 0 in
      let ende = search_forward (regexp ">") s start in
      (string_before s start) ^ (string_after s (ende + 1))
  with Not_found -> s

