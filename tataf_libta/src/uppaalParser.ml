(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  Parser functions for Uppaal XML files (XTA 4), Uppaal-query-files (.q) and
  additional parser functions to only some sub-elements of the Uppaal grammar.
  
  This module contains the XML parser, needed to read xml files in XTA 4 format.
  The parser will perform a DTD-validation on the input XML-file, before the 
  parsing real parsing. If this validation fails, the parser will not run.  
  The parser for the XTA part of the Uppaal grammar is defined in 
  {!UppaalXtaParser}, the lexer in {!UppaalXtaLexer}.  
  
  @author Jeremi Dzienian
*)

open UppaalSystem
open SymbolTable

open Xml
open ExtXml
open Tataf_libcore.Util

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "UppaalParser"

let fail_parse msg = dbg#fail ("ParseError: " ^ msg)

(*----------------------------------------------------------------------------*)
(**
  {1 Exceptions}
*)

(**
  [Parse_error(start_pos, end_pos, message, parsed_string)]
*)
exception Parse_error of Lexing.position * Lexing.position * string * string

(*----------------------------------------------------------------------------*)
(**
  {1 XTA Parsing}
*)

let parseXTA_from_lexbuf ?(is_query=false) parser lexbuf =
  try parser (UppaalXtaLexer.token is_query) lexbuf
  with 
  | Parsing.Parse_error -> 
    (* Unknown parse error; take position from lexbuffer *)
    let open Lexing in
    raise @@ Parse_error (lexbuf.lex_start_p, lexbuf.lex_curr_p, "Syntax Error" , "")
  | UppaalXtaParser.Specific_Parse_error(p1,p2,msg) ->
    (* Known parse error; just rethrow as UppaalParser.Parse_error *)
    raise @@ Parse_error (p1,p2,msg,"")

let parseXTA ?(is_query=false) parser s =
  let lexbuf = Lexing.from_string s in
  try parseXTA_from_lexbuf ~is_query parser lexbuf
  with Parse_error(p1,p2,msg,"") -> 
    raise @@ Parse_error(p1,p2,msg,s)
    (* fill in the content into the exception *)

let parse_expression ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.expression s
  
let parse_invariant ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.invariantXml s

let parse_select ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.selectXml s

let parse_guard ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.guardXml s
  
let parse_sync ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.syncXml s
  
let parse_assignment ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.assignXml s

let parse_parameter ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.parameterXml s

let parse_declarations ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.declarationsXml s

let parse_system ?(symbol_table=new frame None) s =
  UppaalXtaParser.symbol_table := symbol_table;
  parseXTA UppaalXtaParser.systemXml s

let parse_queries_file ?(symbol_table=new frame None) filename =
  UppaalXtaParser.symbol_table := symbol_table;
  let open Lexing in
  let s = Tataf_libcore.ExtString.string_from_file filename in
  let lexbuf = from_string s in
  (*let lexbuf = from_channel (open_in filename) in*)
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
  let parser = UppaalXtaParser.queries in
  try parseXTA_from_lexbuf ~is_query:true parser lexbuf 
  with Parse_error(p1,p2,msg,"") ->
       raise @@ Parse_error(p1,p2,msg,s(*Tataf_libcore.ExtString.string_from_file filename*)) 
       (* Extend exception with some informations. *)

let parse_qeClasses s =
  UppaalXtaParser.symbol_table := new frame None;
  parseXTA UppaalXtaParser.qeClasses s

(*----------------------------------------------------------------------------*)

let strattr element name =
  try Some(Xml.attrib element name)
  with Xml.No_attribute(_) -> None

let xyattr element =
  let x = strattr element "x" |> some_apply int_of_string in
  let y = strattr element "y" |> some_apply int_of_string in
  match x,y with
  | Some x', Some y' -> Some { XY.x = x'; y = y' }
  | None, None -> None
  | _ -> fail_parse "incomplete xy attributes (x or y is missing)."

(*----------------------------------------------------------------------------*)

class xml_parser symbol_table =
  object (self)
  
  val mutable symbol_table : frame = symbol_table

  method symbol_table_update () =
    symbol_table <- !UppaalXtaParser.symbol_table 
    (*DEBUG:*) (*;self#xml_symbol_table_print ()*)

  method new_scope = new xml_parser symbol_table  

  method parse_xml_nail = function
    | Element(_,_,[]) as nail_element ->
      begin
        match xyattr nail_element with
        | Some xy -> Nail.make2 ~xy
        | _ -> fail_parse "nail requires attributes x and y!"
      end
    | _ -> assert false


  method parse_xml_nails elements =
    let nails = filter_name "nail" elements in
    List.map self#parse_xml_nail nails
  
  (*returns (kind:string, data:string, xy:XY.t) *)
  method parse_xml_label2 = function
    | Element(tag_name,_,[PCData(data)]) as label_element ->
      let kind = try Xml.attrib label_element "kind"
        with Xml.No_attribute(_) -> "label requires 'kind' attribute!" in
      let xy = xyattr label_element in
      (kind, data, xy)
    | Element(tag_name,_,[]) as label_element ->
      let kind = try Xml.attrib label_element "kind"
        with Xml.No_attribute(_) -> "label requires 'kind' attribute!" in
      let xy = xyattr label_element in
      (kind, "", xy) 
    | _ -> fail_parse "Unexpected error while trying to parse 'label' element."
  
  method parse_xml_labels2 elements =
    let invariant = ref None in
    let select = ref None in 
    let guard = ref None in 
    let sync = ref None in
    let update = ref None in
    let comment = ref None in
    let exponentialrate = ref None in
    filter_name "label" elements
    |> List.map self#parse_xml_label2
    |> List.iter (fun (kind, data, xy) ->
        match String.lowercase_ascii(kind) with
          | "invariant" ->
             if !invariant = None
             then invariant := parse_invariant ~symbol_table data
              |> some_apply (fun l -> {l with Invariant.xy=xy} )
             else fail_parse "Multiple definitions of 'invariant' label within transition or location!" 
          | "select" -> 
             if !select = None 
             then select := Some (parse_select ~symbol_table data)
              |> some_apply (fun l -> {l with Select.xy=xy} )
             else fail_parse "Multiple definitions of 'select' label within transition or location!"
          | "guard" ->
             if !guard = None
             then guard := parse_guard ~symbol_table data 
              |> some_apply (fun l -> {l with Guard.xy=xy} )
             else fail_parse "Multiple definitions of 'guard' label within transition or location!"
          | "synchronisation" ->
             if !sync = None
             then sync := parse_sync ~symbol_table data
              |> some_apply (fun l -> {l with Sync.xy=xy} )
             else fail_parse "Multiple definitions of 'synchronisation' label within transition or location!"
          | "assignment" ->
             if !update = None
             then update := Some (parse_assignment ~symbol_table data)
              |> some_apply (fun l -> {l with Update.xy=xy} )
             else fail_parse "Multiple definitions of 'assignment' label within transition or location!"
          | "comments" (* not "comment" *) -> 
             if !comment = None
             then comment := Some data
             else fail_parse "Multiple definitions of 'comment' label within transition or location!"
     	  | "exponentialrate" ->
	     if !exponentialrate = None
	     then exponentialrate := Exponentialrate.make_some (float_of_string data)
	      |> some_apply (fun l -> {l with Exponentialrate.xy=xy} )
	     else fail_parse "Multiple definitions of 'exponentialrate' label within transition or location!"
          | _ -> fail_parse ("Unknown label kind '" ^ kind ^ "'")
       );
    (!invariant, !select, !guard, !sync, !update, !comment, !exponentialrate)
  
  method parse_location_labels elements =
    match self#parse_xml_labels2 elements with
    | (_,Some _,_,_,_,_,_) -> fail_parse "Invalid definition of 'select' within location!"
    | (_,_,Some _,_,_,_,_) -> fail_parse "Invalid definition of 'guard' within location!"
    | (_,_,_,Some _,_,_,_) -> fail_parse "Invalid definition of 'synchronisation' within location!"
    | (_,_,_,_,Some _,_,_) -> fail_parse "Invalid definition of 'update' within location!"
    | (invariant,_,_,_,_,comment,exponentialrate) -> (invariant, comment, exponentialrate)
  
  method parse_transition_labels elements =
    match self#parse_xml_labels2 elements with
    | (Some _,_,_,_,_,_,_) -> fail_parse "Invalid definition of 'invariant' within transition!"
    | (_,select,guard,sync,update,comment,_) -> (select,guard,sync,update,comment)
  
  method parse_xml_transition = function
    | Element(_,_,elements) as transition ->
      let nails = self#parse_xml_nails elements in
      let id = strattr transition "id" in
      let xy = xyattr transition in 
      let color = strattr transition "color" in 
      let source = try single_element_attrib "source" "ref" elements
        with No_element -> fail_parse "Transition has no source!"
           | Multiple_elements -> fail_parse "Transition has multiple sources!" in
      let target = try single_element_attrib "target" "ref" elements
        with No_element -> fail_parse "Transition has no target!"
           | Multiple_elements -> fail_parse "Transition has multiple targets!" in
      let select,guard,sync,update,comment = self#parse_transition_labels elements in
      {Edge. id; source; target; select; guard; sync; update; comment;
       nails; xy; color; }
    | _ -> assert false
  
  
  method parse_xml_transitions elements =
    let transitions = filter_name "transition" elements in
    List.map self#parse_xml_transition transitions
  
  
  method parse_xml_name elements =
    match filter_name "name" elements with
    | [] -> None
    | Element(tag_name,_,[PCData(name)])::[] as name_elements ->
      let name_element = List.hd name_elements in
      let xy = xyattr name_element in 
      Some { (Name.make name) with Name.xy }
    | _ -> fail_parse "Multiple 'name'-elements encountered, or incorrect format!"
  
  
  method parse_xml_location = function
    | Element(tag_name,_,elements) as location ->
      let name = self#parse_xml_name elements in
      let urgent = try exists_element "urgent" elements
        with Multiple_elements -> fail_parse "Multiple definitions of 'urgent'!" in
      let committed = try exists_element "committed" elements
        with Multiple_elements -> fail_parse "Multiple definitions of 'committed'!" in
      let xy = xyattr location in 
      let color = strattr location "color" in
      let id = try Xml.attrib location "id"
        with Xml.No_attribute(_) -> fail_parse "location element without required id!" in
      let invariant, comment,exponentialrate = self#parse_location_labels elements in
      {Location. id; name; urgent; committed; xy; color;
       invariant; comment; exponentialrate}
    | _ -> fail_parse "Unexpected error while trying to parse 'location'-element."
  
  
  method parse_xml_locations elements =
    let locations = filter_name "location" elements in
    List.map self#parse_xml_location locations
  
  
  method parse_xml_parameter elements = 
    match filter_name "parameter" elements with
    | [] -> [] (*Parameter.Empty*)
    | Element(tag_name,_,[PCData(parameter)])::[] (*as parameter_elements*) ->
      (* NOTE: The statement below (commented out with this statement) 
       * raises warning 'unused variable parameter_element' and appears to 
       * be deprecated, thus, was commented out.
       *
      let parameter_element = List.hd parameter_elements in
       *)

      (*let xy = xyattr parameter_element in*)
      parse_parameter ~symbol_table parameter
      (*Parameter.Parameters(parse_parameter ~symbol_table parameter)*)
      (*Parameter.Raw({Parameter.data=parameter;xy={XY.x;y}})*)
    | _ -> fail_parse "Multiple 'parameter'-elements encountered, or incorrect format!"
  
  method parse_xml_declaration = function
    | Element(name,_,[PCData(decl)]) -> 
        parse_declarations ~symbol_table decl
        |> fun x -> self#symbol_table_update (); x
    | Element(name,_,[]) -> []
    | _ -> fail_parse "Some error occured while parsing/finding of <declaration>-tag."
  
  method parse_xml_declarations elements =
       filter_name "declaration" elements
    |> List.map self#parse_xml_declaration
    |> List.concat
  
  
  method parse_xml_init elements = 
    match filter_name "init" elements with
    | [] -> fail_parse "No init element defined" 
    | init::[] -> 
      let initref = attrib_or_empty init "ref" in
      initref
    | _ -> fail_parse "Multiple 'init'-elements encountered, or incorrect format!"
  
  
  method parse_xml_template = function
    | Element(tag_name, _, elements) ->
      let name = begin match self#parse_xml_name elements with
        | None -> fail_parse "Template without Name!"
        | Some n -> n
      end in    
      let parameters = self#parse_xml_parameter elements in
      let declarations = self#parse_xml_declarations elements in
      let locations = self#parse_xml_locations elements in
      let init = self#parse_xml_init elements in
      let edges = self#parse_xml_transitions elements in
      { (Template.make "" locations edges init) with Template.
         name; parameters; declarations; 
      }
      (*Template.make ~name ~parameters ~declarations ~locations ~init ~edges*)
    | _ -> fail_parse "Unexpected error while trying to parse template-node. "
  
  
  method parse_xml_templates elements =
    let tmpls = filter_name "template" elements in
    List.map self#parse_xml_template tmpls
  
  
  method parse_xml_imports elements =
    match filter_name "imports" elements with 
    | [] -> None 
    | Element(name,_,[PCData(ips)])::[] -> Some(ips) 
    | _ -> fail_parse "Some error occured while parsing/finding of <import>-tag."
  
  
  method parse_xml_instantiation elements =
    match filter_name "instantiation" elements with
    | [] -> [] (* Instantiation.Empty*)
    | Element(name,_,[PCData(inst)])::[] -> parse_declarations ~symbol_table inst
    | _ -> fail_parse "Some error occured while parsing/finding of <instantiation>-tag."
  
  
  method parse_xml_system elements =
    match filter_name "system" elements with
    | [] -> fail_parse "Missing <system> attribute in <nta>."
    | Element("system",_,[PCData(system)])::[] ->
        parse_system ~symbol_table system
    | _ -> fail_parse "Some error occured while parsing/finding of <system> within <nta>."
  
  
  method parse_xml dtd_version xml =
    match xml with
    | Element(tag, _, elements) when String.lowercase_ascii tag = "nta" ->
      let imports = self#parse_xml_imports elements in
      let declarations = self#parse_xml_declarations elements in
      let templates = self#parse_xml_templates elements in
      let instantiation = self#parse_xml_instantiation elements in 
      let system = self#parse_xml_system elements in  
      {NTA.imports; declarations; templates; instantiation; system; dtd_version}
    | _ -> fail_parse "This is not a valid Uppaal Xml file."
    
end


(*----------------------------------------------------------------------------*)
  
let dtd_string_1_1 = 
"<!ELEMENT nta (imports?, declaration?, template+, instantiation?, system)>
<!ELEMENT imports (#PCDATA)>
<!ELEMENT declaration (#PCDATA)>
<!ELEMENT template (name, parameter?, declaration?, location*, init?, transition*)>
<!ELEMENT name (#PCDATA)>
<!ATTLIST name x   CDATA #IMPLIED
               y   CDATA #IMPLIED>
<!ELEMENT parameter (#PCDATA)>
<!ATTLIST parameter x   CDATA #IMPLIED
                    y   CDATA #IMPLIED>
<!ELEMENT location (name?, label*, urgent?, committed?)>
<!ATTLIST location id ID #REQUIRED
           x  CDATA #IMPLIED
           y  CDATA #IMPLIED
           color CDATA #IMPLIED>
<!ELEMENT init EMPTY>
<!ATTLIST init ref IDREF #IMPLIED>
<!ELEMENT urgent EMPTY>
<!ELEMENT committed EMPTY>
<!ELEMENT transition (source, target, label*, nail*)>
<!ATTLIST transition id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>
<!ELEMENT source EMPTY>
<!ATTLIST source ref IDREF #REQUIRED>
<!ELEMENT target EMPTY>
<!ATTLIST target ref IDREF #REQUIRED>
<!ELEMENT label (#PCDATA)>
<!ATTLIST label kind CDATA #REQUIRED
        x    CDATA #IMPLIED
        y    CDATA #IMPLIED>
<!ELEMENT nail EMPTY>
<!ATTLIST nail x   CDATA #REQUIRED
               y   CDATA #REQUIRED>
<!ELEMENT instantiation (#PCDATA)>
 <!ELEMENT system (#PCDATA)>"

let dtd_string_1_2 =
  "<!ELEMENT nta (imports?, declaration?, template*, lsc*, instantiation?, system, queries?)>
<!ELEMENT imports (#PCDATA)>
<!ELEMENT declaration (#PCDATA)>
<!ELEMENT template (name, parameter?, declaration?, location*, init?, transition*)>
<!ELEMENT name (#PCDATA)>
<!ATTLIST name x   CDATA #IMPLIED
               y   CDATA #IMPLIED>
<!ELEMENT parameter (#PCDATA)>
<!ATTLIST parameter x   CDATA #IMPLIED
                    y   CDATA #IMPLIED>
<!ELEMENT location (name?, label*, urgent?, committed?)>
<!ATTLIST location id ID #REQUIRED
		   x  CDATA #IMPLIED
		   y  CDATA #IMPLIED
		   color CDATA #IMPLIED>
<!ELEMENT init EMPTY>
<!ATTLIST init ref IDREF #IMPLIED>
<!ELEMENT urgent EMPTY>
<!ELEMENT committed EMPTY>
<!ELEMENT transition (source, target, label*, nail*)>
<!ATTLIST transition id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
		     color CDATA #IMPLIED>
<!ELEMENT source EMPTY>
<!ATTLIST source ref IDREF #REQUIRED>
<!ELEMENT target EMPTY>
<!ATTLIST target ref IDREF #REQUIRED>
<!ELEMENT label (#PCDATA)>
<!ATTLIST label	kind CDATA #REQUIRED
		x    CDATA #IMPLIED
		y    CDATA #IMPLIED>
<!ELEMENT nail EMPTY>
<!ATTLIST nail x   CDATA #REQUIRED
               y   CDATA #REQUIRED>
               
<!-- LSC template -->
<!ELEMENT lsc (name, parameter?, type, mode, declaration?, yloccoord+, instance+, prechart?, message*, condition*, update*, cut*, scope*, coregion*, subchart*, loop*, ifthenelse*)>

<!-- type: existential | universal -->
<!ELEMENT type (#PCDATA)>
<!ATTLIST type      x   CDATA #IMPLIED
                    y   CDATA #IMPLIED>
<!-- mode: initial | iterative | invariant -->
<!ELEMENT mode (#PCDATA)>
<!ATTLIST mode      x   CDATA #IMPLIED
                    y   CDATA #IMPLIED>
<!-- role: specification | property -->
<!ELEMENT role (#PCDATA)>
<!ATTLIST role      x   CDATA #IMPLIED
                    y   CDATA #IMPLIED>
<!-- yloccoord: Y coordinate for location on all instances (same loc number => same Y coord) -->
<!ELEMENT yloccoord EMPTY>
<!ATTLIST yloccoord number CDATA #REQUIRED
                    y      CDATA #REQUIRED>


<!-- instance: carries unique ID, label(which can change during run), and temperatures of the locations -->
<!ELEMENT instance (name)>
<!ATTLIST instance id ID #REQUIRED
           x  CDATA #REQUIRED
           y  CDATA #IMPLIED>
<!-- temperature = hot | cold, identified by location_number (or, if location numbers not present, count from 1 on) -->
<!ELEMENT temperature (#PCDATA)>
<!ATTLIST temperature y CDATA #IMPLIED>


<!-- message: refs of src, trg instances; location; label=name -->
<!ELEMENT message (source, target, lsclocation, label)>
<!ATTLIST message    id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>
    <!-- source points to ID of instance line -->
    <!-- target points to ID of instance line -->
<!ELEMENT lsclocation (#PCDATA)>

    <!-- anchor refers to instance ID and optionally carries the location number (y)-->
<!ELEMENT anchor EMPTY>
<!ATTLIST anchor     instanceid IDREF #REQUIRED
                     y CDATA #IMPLIED>

<!-- condition: anchors+ (= {x,y} with y being the same);  temperature; label=expression -->
<!ELEMENT condition (anchor+, lsclocation, temperature, label)>
<!ATTLIST condition  id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- update: anchor; location; label=expression -->
<!ELEMENT update (anchor, lsclocation, label)>
<!ATTLIST update     id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- prechart: spans over ALL instances by default, only need to specify its bottom location -->
<!ELEMENT prechart  (lsclocation)>
<!ATTLIST prechart   id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- the following LSC constructs are deemed optional - may be ignored in the first version of LSC editor -->

<!-- cut: nothing but a set of anchors (=pair {x,y}) -->
<!ELEMENT cut  (anchor+)>
<!ATTLIST cut        id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- scope: upper & lower locations, temperature; reference to the IDs of 2 cuts -->
<!ELEMENT scope (lsclocation, lsclocation, temperature)>
<!ATTLIST scope      id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             cut1ref IDREF #REQUIRED
             cut2ref IDREF #REQUIRED
             color CDATA #IMPLIED>

<!-- coregion: instance (by anchor(s)), locations deduced from anchors -->
<!ELEMENT coregion (anchor, anchor)>
<!ATTLIST coregion   id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- loop: anchors over instances, upper and lower location, and label=expression -->
<!ELEMENT loop (anchor+, lsclocation, lsclocation, label)>
<!ATTLIST loop       id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- subchart: anchors over instances, upper and lower location -->
<!ELEMENT subchart   (anchor+, lsclocation, lsclocation)>
<!ATTLIST subchart   id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>

<!-- ifThenElse: anchors over instances, top/middle/bottom locations and label=expression -->
<!ELEMENT ifthenelse (anchor+, lsclocation, lsclocation, lsclocation, label)>
<!ATTLIST ifthenelse id  ID #IMPLIED
                     x   CDATA #IMPLIED
                     y   CDATA #IMPLIED
             color CDATA #IMPLIED>
               
<!ELEMENT instantiation (#PCDATA)>
<!ELEMENT system (#PCDATA)>
<!ELEMENT queries (query*)>
<!ELEMENT query (formula,comment)>
<!ELEMENT formula (#PCDATA)>
<!ELEMENT comment (#PCDATA)>"

let mk_dtd dtd_version =
  begin match dtd_version with
  | DtdVersion.DTD_1_1 () -> dtd_string_1_1
  | DtdVersion.DTD_1_2 () -> dtd_string_1_2
  end |> Dtd.parse_string |> Dtd.check

let default_dtd = function
    None     -> mk_dtd !LibtaOptions.dtd_version
  | Some dtd -> dtd

let parse_xta4 ?(omit_xta=false) s =
  let p = XmlParser.make() in
  let dtd = ref None in
	(* The default DTD version is defined in LibtaOptions (and can be controlled
	   on the command line using -D. *)
  let dtd_version = ref !LibtaOptions.dtd_version in
  (* The DTD version is optionally given by the DOCTYPE element in the XTA
     file.  XmlParser.resolve() seems to require a function which -- on the
     fly -- maps the DTD path specifier to a proper Dtd object (as declared in
     xml-light).  We provide such a function below which in addition (as a
     side-effect) stores the detected DTD version in dtd/dtd_version.
     If the XTA file does not have a DOCTYPE element, dtd remains None.
     Note that DtdVersion.lookup() fails for unknown DTD paths. *)
  XmlParser.resolve p (fun dtd_path ->
                        dtd_version := DtdVersion.lookup dtd_path;
                        dtd := Some (mk_dtd !dtd_version);
                        Tataf_libcore.Util.unsome !dtd);
  (Obj.magic XmlParser.parse p (XmlParser.SString s) : xml)
  (* Prove extra time with default DTD in case the DTD header is missing.
     If the XTA did not contain a DOCTYPE element, dtd is still None.  Then
     function default_dtd() will provide a proper Dtd object according to the
     default version selected on the command line; otherwise it yields the DTD
     object corresponding to the specification in the XTA file. *)
  |> Dtd.prove (default_dtd !dtd) "nta"
  |> (new xml_parser (new frame None))#parse_xml !dtd_version

let parse_xta4_file ?(omit_xta=false) filename =
  parse_xta4 ~omit_xta:omit_xta (Tataf_libcore.ExtString.string_from_file filename)

