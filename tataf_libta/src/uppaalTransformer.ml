(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  This module contains the transformer base class. It enables to write
  easy transformation for Uppaal models.
*)

open UppaalSystem
open UppaalSystem.Expression
open Tataf_libcore.Util

(*open UppaalXmlSystem
open Util*)

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "UppaalTransform"

(*----------------------------------------------------------------------------*)

class normalizer =
  object (self)
  inherit UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  
  (*
  TODO:
  - assignments not in short form ("x = x + 1" and not "x += 1")
  -comparison of natural numbers shuold always be rhs ("x > 2" and not "x < 2") 
  
  *)
  method! transform_expression = function
    | Parenthesis(e) -> e
    | Assignment(e1,AssignPlus,e2) -> Assignment(e1,Assign,IntOperation(e1,Plus,e2))
    | Assignment(e1,AssignMinus,e2) -> Assignment(e1,Assign,IntOperation(e1,Minus,e2))
    (*todo more*)
    | expr -> expr

  method! transform_statements statements =
    (* filter out empty statements*)
    List.filter (fun s -> match s with 
                      | Statement.Empty -> false 
                      | _ -> true) statements
    |> super#transform_statements

end

(*----------------------------------------------------------------------------*)
module Layout = struct
  class position_translator dx dy =
    object (self)
  inherit UppaalTransformerWithSymboltable.transformer_without_symboltable as super
    
    method! transform_xy {XY.x; y} = { XY.x = x + dx; y = y + dy }
  end

  (*let transform_edge_position_h lookup ({Edge.source; target; nails; _) as edge) =*)

  let translate_edge source target offset edge =
    match source, target with
    | {Location.xy=Some sxy; _}, {Location.xy=Some txy; _} ->
      (*add two new nails at the position of the source and target location*)
      let new_nails = [{Nail.pos=sxy}] @ edge.Edge.nails @ [{Nail.pos=txy}] in
      let ox, oy = if abs(sxy.XY.x - txy.XY.x) > abs(sxy.XY.y - txy.XY.y)
                   then 0, offset 
                   else offset, 0
      in
      { edge with Edge.nails = new_nails }
      |> (new position_translator ox oy)#transform_edge
    | _,_ -> (new position_translator offset offset)#transform_edge edge
  
end 

(*----------------------------------------------------------------------------*)
(**
 Shortcut function, to allow chaining of multiple transformations.
 
 Example 1:
  {[
  nta 
  |> foroeach lst (fun nta lst_element -> 
       nta |> do_transform1
           |> do_transform2
           |> do_transform3
     )
  ]}
  
 Example 2:
  {[
  queries 
  |> foroeach lst (fun query lst_element -> 
       query |> do_transform1
             |> do_transform2
             |> do_transform3
     )
  ]}

 *)
let foreach : 'a 'b. 'a list -> ('b -> 'a -> 'b) -> 'b -> 'b =
  fun lst f nta -> List.fold_left f nta lst
