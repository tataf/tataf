(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

let dtd_version_string = ref ""
let dtd_version = ref (UppaalSystem.DtdVersion.DTD_1_1 ())

let verbose = ref false

let ppuppaal_out = ref "" 
let ppuppaal_onlyxml = ref false
let ppuppaal_xta4_linewidth = ref 0
let ppuppaal_symbol_table = ref false

