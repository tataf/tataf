(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open UppaalSystem
open UppaalSystem.Expression

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "ExampleAnalysis"

(*----------------------------------------------------------------------------*)

(**
  Helper class: Checks if the variable 'varname' is contained within
  an expression. The [bool_walker] is used as super class. That way all
  visit-methods will return [false] by default. We change the return value
  for [visit_expression] to [true], if the wanted variable 'varname' was
  found. Now, if any visit-method will return [true], then the type 
  structure contains this variable.
*)
class expression_checker varname =
  object (self)
  inherit UppaalWalker.bool_walker as super
    
  val varname = varname
  
  method! visit_expression e = (* = function*)
    dbg#ldebug_endline 1 "expr checker: visit expression\n";
    match e with
    | Var(id) -> id = varname
    | e -> super#visit_expression e
end

(**
  Transformer
*)
class mytransformer varname =
  object (self)
  inherit UppaalTransformerWithSymboltable.transformer_without_symboltable 
    as super
  
  val mutable checker = new expression_checker varname
  val varname = varname

  (**
    Helper method [remove_expr e] will return the expression [e], if 
    [e] does not contain the variable 'varname'. Otherwise the result will
    return the true-expression.
    *)  
  method remove_expr (expr : Expression.t) = 
      if checker#visit_expression expr then 
        Bool(true) 
      else
        expr (* keep original *)
  
  (**
    Helper method [concat_expr_in_CNF e1 e2] will concatenate the two 
    expression e1 and e2 with the AND operator. If one of the expressions
    is the true-expression, then simply the other expression is returned
    (no concatenation).
  *)
  method concat_expr_in_CNF (e1 : Expression.t)
                            (e2 : Expression.t) =
    match e1 with 
    | Bool(true) -> e2
    | _ -> begin match e2 with
           | Bool(true) -> e1
           | _ -> BoolOperation(e1, AndAnd, e2)
           end  
  
  (**
    Helper method [remove_expr_in_CNF expr] will remove the variable
    'varname' within all subexpression of the expression [expr], which is
    expected to be in CNF.
  *)
  method remove_expr_in_CNF (expr : Expression.t) =
    match expr with 
    | BoolOperation(o1,AndAnd,o2) -> 
      let ro1 = self#remove_expr_in_CNF o1 in
      let ro2 = self#remove_expr o2 in
      self#concat_expr_in_CNF ro1 ro2
    | e -> self#remove_expr e
  
  
  (** 
    remove the variable 'varname' within the invariant. The invariant is
    expected to be in CNF, so we call the corresponding helper-method. 
  *)
  method! transform_invariant ({Invariant.expr;_} as inv) = 
    { inv with 
      Invariant.expr = match expr with
       | BoolOperation(_,AndAnd,_) -> self#remove_expr_in_CNF expr
       | _ -> self#remove_expr expr 
    }
  
  (**
    Assign expressions (uppaal: update) are not in CNF, but are parsed 
    directly into an expression list. That's why we can simply filter
    this expression list and delete every expression containing 'varname'.
  *)
  (*
  method transform_assign = function
    | UpAssign(el) -> 
      (* filter out 'varname' *)
      let filtered = List.filter 
        (fun e -> not (checker#visit_expression e)) el in
      begin match filtered with
      | [] -> UpEmptyAssign
      | _ -> UpAssign(filtered)
      end
    | e -> super#transform_assign e
  *)                      (* TODO TODO TODO TODO TODO TODO !!!!!!!!!! *)
  
  method! transform_sync ({Sync.expr; _} as sync) =
    { sync with Sync.expr = self#remove_expr_in_CNF expr }
  (*
   = function
    | UpSync(e,marker) ->
      let te = self#remove_expr_in_CNF e in
      UpSync(te,marker)
    | e -> super#transform_sync e*) 

  method! transform_guard ({Guard.expr;_} as guard) =
    { guard with Guard.expr = self#remove_expr_in_CNF expr }
  (*
  = function
    | UpGuard(e) -> 
      let te = self#remove_expr_in_CNF e in
      UpGuard(te)
    | e -> super#transform_guard e*)
      
  method! transform nta =
    dbg#ldebug_endline 1 "Transformer Call\n";
    super#transform nta
end


(*----------------------------------------------------------------------------*)

(** 
  This is the start function, where the transformation begins.
*)
let example_transformation filename =
  dbg#ldebug_endline 1 "--------------------";  
  dbg#ldebug_endline 1 "ExampleTransform Uppaal";
  dbg#ldebug_endline 1 "  This exmplae will delete a clock... TODO ";
  dbg#ldebug_endline 1  ("  Input: "  ^ filename);
  dbg#ldebug_endline 1  "--------------------";
  
  (* parsing *)  
  dbg#ldebug 2 "parsing ...";
  let upxmlSystem = UppaalParser.parse_xta4_file (*~omit_xta=false*) filename in 
  dbg#ldebug_endline 2 "ok";  

  (* do the transformation *)
  let upxmlSystem = (new mytransformer "x")#transform upxmlSystem in

  (* pretty print/output result *)
  let pretty = (new UppaalPrint.pretty_printer)#visit_nta upxmlSystem in
  Tataf_libcore.ExtString.string_to_file !LibtaOptions.ppuppaal_out pretty
;;
