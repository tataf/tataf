(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open UppaalTransformer
open UppaalTransformerWithSymboltable

let dbg = Debug.register_debug_module "TransformerBenchmark"

let benchmark source =
    dbg#info("Launching benchmarking experiment on file " ^ source);
    let profiler = new Profiler.profiler in
    let upxmlSystem = UppaalParser.parse_xta4_file source in 
    let standard_transformer = new UppaalTransformerWithSymboltable.transformer_without_symboltable in
    let symtab_transformer = new transformer_with_symboltable in
    let symtab_nosymtab_transformer = new transformer_without_symboltable in
    let rec aux iter sum_std sum_symtab sum_symtab_nosymtab =
        if iter >= 100 then
            (sum_std, sum_symtab, sum_symtab_nosymtab)
        else
            (dbg#info("Measuring runtime of normal transformer:");
            profiler#start_clock;
            ignore(standard_transformer#transform upxmlSystem);
            let std_perf = profiler#stop_clock in
            dbg#info("Measuring runtime of transformer with symboltable:");
            profiler#start_clock;
            ignore(symtab_transformer#transform upxmlSystem);
            let symtab_perf = profiler#stop_clock in
            dbg#info("Measuring runtime of transformer with symboltable" ^
                     " without symboltable updates:");
            profiler#start_clock;
            ignore(symtab_nosymtab_transformer#transform upxmlSystem);
            let symtabnosymtab_perf = profiler#stop_clock in
            aux (iter + 1) (sum_std +. std_perf) (sum_symtab +. symtab_perf)
                (sum_symtab_nosymtab +. symtabnosymtab_perf))
    in 
    let (sum_std, sum_symtab, sum_symtab_nosymtab) = aux 0 0.0 0.0 0.0 in
    let (avg_std, avg_symtab, avg_symtab_nosymtab) = 
        (sum_std /. 100.0, sum_symtab /. 100.0, sum_symtab_nosymtab /.100.0 ) 
    in
    begin
        dbg#info("Average performance normal transformer: ");
        dbg#info(string_of_float(avg_std)^ " seconds");
        dbg#info("Average performance transformer with symtable: ");
        dbg#info(string_of_float(avg_symtab)^ " seconds");
        dbg#info("Average performance transformer with symtable "
                 ^ "(no symbol table updates performed): ");
        dbg#info(string_of_float(avg_symtab_nosymtab)^ " seconds");
    end
