(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

val dtd_version_string : string ref
val dtd_version : UppaalSystem.DtdVersion.t ref

val verbose : bool ref

val ppuppaal_out : string ref
(** Output filename for the pretty print result.  *)

val ppuppaal_symbol_table : bool ref
(** note: became a dump command, not available with 'print' any more; still
    the dump is implemented with the pretty printer so the name persists *)

val ppuppaal_xta4_linewidth : int ref
(** Add unique ids to the transitions.  *)

val ppuppaal_onlyxml : bool ref
(**
  If [true], the pretty printer will parse only the Xml part of the XTA 
  file, and represent expressions only as strings. If [false], all strings
  will be parsed with the corresponding XTA parser and represented
  accordingly. 
*)

