(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  Parser functions for QE-specific input: equivalence classes of quasi equal
  clocks and corresponding pre/post-delay locations.
*)

module TemplateQualifiedID = struct
  type t =
    { template : UppaalSystem.ID.t option;
      name     : UppaalSystem.ID.t;
    }

  let compare = Pervasives.compare

  let to_str ?(separator = ".") { template; name; _} =
    match template with
    | None     -> name
    | Some id  -> id ^ separator ^ name

  let make      template name = { template = template; name = name}

  let make_some template name = Some { template = template; name = name}
end

module TemplateQualifiedIDSet_ = Set.Make(TemplateQualifiedID)
module TemplateQualifiedIDSet = struct
  include TemplateQualifiedIDSet_
  let mem_some e set =
    match e with
    | None        -> false
    | Some some_e -> mem some_e set
end

