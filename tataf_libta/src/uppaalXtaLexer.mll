(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
{
  open UppaalXtaParser
  
  let create_hashtable init = 
    let tbl = Hashtbl.create (List.length init) in
    List.iter (fun (key,data) -> Hashtbl.add tbl key data) init;
    tbl
    
  let keywords = create_hashtable [
    "const",         T_CONST;
    "select",        T_SELECT;
    "guard",         T_GUARD;
    "sync",          T_SYNC;
    "assign",        T_ASSIGN;
    "process",       T_PROCESS;
    "state",         T_STATE;
    "init",          T_INIT;
    "trans",         T_TRANS;
    "urgent",        T_URGENT;
    "commit",        T_COMMIT;
    "broadcast",     T_BROADCAST;
    "system",        T_SYSTEM;
    "true",          T_TRUE;
    "false",         T_FALSE;
    "and",           T_KW_AND;
    "or",            T_KW_OR;
    "not",           T_KW_NOT;
    "imply",         T_KW_IMPLY;
    "for",           T_FOR;
    "while",         T_WHILE;
    "do",            T_DO;
    "if",            T_IF;
    "else",          T_ELSE;
    "default",       T_DEFAULT;
    "return",        T_RETURN;
    "typedef",       T_TYPEDEF;
    "struct",        T_STRUCT;
    "meta",          T_META;
    "before_update", T_BEFORE;
    "after_update",  T_AFTER;
    "progress",      T_PROGRESS;
    "assert",        T_ASSERT;
    "forall",        T_FORALL;
    "exists",        T_EXISTS;
    "deadlock",      T_DEADLOCK;
    "priority",      T_PRIORITY;
    "bool",          T_BOOL;
    "int",           T_INT;
    "chan",          T_CHAN;
    "clock",         T_CLOCK;
    "void",          T_VOID;
    "scalar",        T_SCALAR;
    "control",       T_CONTROL;
    "control_t",     T_CONTROL_T;
    "inf",           T_INF;
    "sup",           T_SUP;
    "resultset",     T_RESULTSET;
    
    (* added by Jeremi (could not be found in libutap source): *)
    "case",          T_CASE;
  ]
}

let alpha = ['a'-'z' 'A'-'Z' '_']
let num = ['0'-'9']+
let idchr = ['a'-'z' 'A'-'Z' '0'-'9' '_' '$' '#']

rule token is_query = parse
  | '\\'['\t' ' ']*'\n' { Lexing.new_line lexbuf; token is_query lexbuf } (* Use \ as continuation character *)
  | "//"[^'\n']* {token is_query lexbuf } (* ignore (singleline comment) *)

  | [' ' '\t']+ { token is_query lexbuf } (* eat up whitespace *)
  | "/*"    { comment is_query lexbuf.Lexing.lex_start_p lexbuf }

  | '\n'   
  | "\r\n"  { Lexing.new_line lexbuf;
              if is_query then NEWLINE else token is_query lexbuf }

  | '.'     { DOT }
  | ','     { COMMA }
  | ';'     { SEMICOLON }
  | ':'     { COLON }
  | '{'     { LBRACE }
  | '}'     { RBRACE }
  | '['     { LBRACKET }
  | ']'     { RBRACKET }
  | '('     { LPAREN }
  | ')'     { RPAREN }
  | '?'     { QMARK }
  | '''     { APOS }
  | '!'     { T_EXCLAM }

  | "->"    { T_ARROW }
  | "-u->"  { T_UNCONTROL_ARROW }

  | "="     { T_ASSIGNMENT }
  | ":="    { T_ASSIGNMENT } 
  | "+="    { T_ASSPLUS }
  | "-="    {  T_ASSMINUS }
  | "*="    {  T_ASSMULT }
  | "/="    {  T_ASSDIV }
  | "%="    {  T_ASSMOD }
  | "|="    {  T_ASSOR }
  | "&="    {  T_ASSAND }
  | "^="    {  T_ASSXOR }
  | "<<="   {  T_ASSLSHIFT }
  | ">>="   {  T_ASSRSHIFT }
  | "<?"    {  T_MIN }
  | ">?"    {  T_MAX }

  | "+"     {  T_PLUS }
  | "-"     {  T_MINUS }
  | "*"     {  T_MULT }
  | "/"     {  T_DIV }
  | "%"     {  T_MOD }
  | "|"     {  T_OR }
  | "&"     {  T_AND }
  | "^"     {  T_XOR }
  | "<<"    {  T_LSHIFT }
  | ">>"    {  T_RSHIFT }
  | "||"    {  T_BOOL_OR }
  | "&&"    {  T_BOOL_AND }

  | "<="     {  T_LEQ }
  | ">="     {  T_GEQ }
  | "<"      {  T_LT }
  | ">"      {  T_GT }
  | "=="     {  T_EQ }
  | "!="     {  T_NEQ }

  | "++"     {  T_INCREMENT }
  | "--"     {  T_DECREMENT }

  | "A"      {  T_A }
  | "U"      {  T_U }
  | "W"      {  T_W }

  | "A<>"    {  T_AF }
  | "AF"     {  T_AF2 }
  | "A[]"    {  T_AG }
  | "AG"     {  T_AG2 }
  | "E<>"    {  T_EF }
  | "EF"     {  T_EF2 }
  | "E[]"    {  T_EG }
  | "EG"     {  T_EG2 }
  | "-->"    {  T_LEADSTO }

  | alpha idchr* as word {  
      try Hashtbl.find keywords word (* keyword *)
      with Not_found -> 
        let open UppaalXtaParser in
        let open SymbolTable in
        match SymbolTable.resolve !symbol_table word with
        | Some (S_Typedef(_)) -> T_TYPENAME word
        | _ -> T_ID word
    }
  | num as n { T_NAT (int_of_string n) }

  | _ { raise (UppaalXtaParser.Specific_Parse_error 
          (lexbuf.Lexing.lex_start_p, 
           lexbuf.Lexing.lex_curr_p, 
           "Unknown Symbol")) }
  | eof { EOF }

and comment is_query start_pos = parse
  | "*/" { token is_query lexbuf  }
  
  (* Single line comments take precedence over multilines *)
  | "//"[^'\n']* { comment is_query start_pos lexbuf }
  
  | eof { raise (UppaalXtaParser.Specific_Parse_error 
          (start_pos, lexbuf.Lexing.lex_curr_p, "Unclosed Comment"))
    }

  | '\n'
  | "\r\n" { Lexing.new_line lexbuf; comment is_query start_pos lexbuf }

  | _ { comment is_query start_pos lexbuf } (* ignore (multiline comments) *)

