(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  This module defines all pretty printers for Uppaal.
  
  @author Jeremi Dzienian
*)

open UppaalSystem
open UppaalSystem.Expression
(*open UppaalXmlSystem*)
open ExtXml
open Tataf_libcore.ExtList
open String

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "UppaalPrint"



(* helper ********************************************************************)

(* Reminder: do not include this in the interface file! *)
let ppos (pos : XY.t option) =
  match pos with 
  | None -> ("", "")
  | Some p ->
    let px = string_of_int p.XY.x |> xmlattribute "x" in
    let py = string_of_int p.XY.y |> xmlattribute "y" in 
    (px, py)

let option_or_default : 'b. 'b -> 'b option -> 'b =
  fun default opt -> match opt with
  | None -> default
  | Some(s) -> s

(******************************************************************************)

class pretty_printer =
  object (self)
  (*inherit [string] UppaalWalker.walker "" ~concat:(^) as super*)
  inherit UppaalWalker.string_walker as super
  
  method! visit_id id = id
  
  (* --- XTA part ------------------------------ *)
  
  method visit_assignOp = function
    | Assign    -> "="
    | AssignPlus    -> "+="
    | AssignMinus   -> "-="
    | AssignMult    -> "*="
    | AssignDiv     -> "/="
    | AssignMod     -> "%="
    | AssignOr      -> "|="
    | AssignAnd     -> "&="
    | AssignXor     -> "^="
    | AssignLShift  -> "<<="
    | AssignRShift  -> ">>="

  method visit_unary = function
    | UnaryMinus    -> "-"
    | UnaryNot      -> "!"
    | UnaryPlus     -> "+"
  
  method visit_rel = function
    | Lt            -> "<"
    | LtEq          -> "<="
    | Eq            -> "=="
    | NotEq         -> "!="
    | GtEq          -> ">="
    | Gt            -> ">" 
    | Min           -> "<?"
    | Max           -> ">?" 
  
  method visit_intop = function
    | Plus       -> "+"
    | Minus      -> "-"
    | Mult       -> "*"
    | Div        -> "/"
    | Mod        -> "%"
    | And        -> "&"
    | Or         -> "|"
    | Xor        -> "^"
    | LShift     -> "<<"
    | RShift     -> ">>"
  
  method visit_boolop = function
    | AndAnd     -> "&&"
    | OrOr       -> "||"
  
  method! visit_CTLexpression expr =
    let pe = self#visit_expression in 
    match expr with 
    | AF(e)      -> "A<> " ^ (pe e)
    | AG(e)      -> "A[] " ^ (pe e)
    | EF(e)      -> "E<> " ^ (pe e)
    | AGR(e)     -> "A[]* " ^ (pe e)
    | EFR(e)     -> "E<>* " ^ (pe e)
    | EG(e)      -> "E[] " ^ (pe e)
    | AF2(ep,e)  -> "AF {" ^ (pe ep) ^ "} " ^ (pe e)
    | AG2(ep,e)  -> "A[] {" ^ (pe ep) ^ "} " ^ (pe e)
    | EF2(ep,e)  -> "E<> {" ^ (pe ep) ^ "} " ^ (pe e)
    | EG2(ep,e)  -> "E[] {" ^ (pe ep) ^ "} " ^ (pe e)
    | Leadsto(e1,e2) -> (pe e1) ^ " --> " ^ (pe e2)
    | AU(e1,e2)  -> "A " ^ (pe e1) ^ " U " ^ (pe e2)
    | AW(e1,e2)  -> "A " ^ (pe e1) ^ " W " ^ (pe e2)
    | AU2(ep,e1,e2) -> "A[" ^ (pe ep) ^ "] " ^ (pe e1) ^ " U " ^ (pe e2)
    | AW2(ep,e1,e2) -> "A[" ^ (pe ep) ^ "] " ^ (pe e1) ^ " W " ^ (pe e2)
  
  method! visit_expression expr =
    let paop = self#visit_assignOp in
    let punary = self#visit_unary in
    let pe = self#visit_expression in 
    let prel = self#visit_rel in
    let pintop = self#visit_intop in
    let pbop = self#visit_boolop in
    match expr with
    | Var(id) -> id
    | Num(n) -> string_of_int n
    | Bool(b) -> string_of_bool b
    | Call(e,args) -> (pe e) ^ "(" ^ (str_list ~sep:"," pe args) ^ ")"
    | Array(e1,e2) -> (pe e1) ^ "[" ^ (pe e2) ^ "]"
    | Parenthesis(e) -> "(" ^ (pe e) ^ ")"
    | IncPost(e) -> (pe e) ^ "++"
    | IncPre(e) -> "++" ^ (pe e)
    | DecPost(e) -> (pe e) ^ "--"
    | DecPre(e) -> "--" ^ (pe e)
    | Assignment(e1,op,e2) -> (pe e1) ^ " " ^ (paop op) ^ " " ^ (pe e2)
    | Unary(op,e) -> (punary op) ^ (pe e)
    | Relation(e1,op,e2) -> (pe e1) ^ " " ^ (prel op) ^ " " ^ (pe e2)
    | IntOperation(e1,op,e2) -> (pe e1) ^ " " ^ (pintop op) ^ " " ^ (pe e2)
    | BoolOperation(e1,op,e2) -> (pe e1) ^ " " ^ (pbop op) ^ " " ^ (pe e2)
    | IfExpr(e1,e2,e3) -> (pe e1) ^ " ? " ^ (pe e2) ^ " : " ^ (pe e3)
    | Dot(e1,id) -> (pe e1) ^ "." ^ id
    | Dashed(e) -> "'" ^ (pe e)
    | Deadlock -> "deadlock"
    | Imply(e1,e2) -> "!" ^ (pe e1) ^ " || " ^ (pe e2)
    | Forall(id,tp,e) -> 
        "forall ( " ^ (self#visit_id id) ^ " : " ^ (self#visit_type tp) ^ " )" ^ (pe e)
    | Exists(id,tp,e) -> 
        "exists ( " ^ (self#visit_id id) ^ " : " ^ (self#visit_type tp) ^ " )" ^ (pe e)
    | CTL(e) -> self#visit_CTLexpression e
  
  method visit_type_prefix = function
    | Type.Urgent -> "urgent"
    | Type.Broadcast -> "broadcast"
    | Type.Const -> "const"
    | Type.Meta -> "meta"
  
  method visit_type_prefix_list =
    List.fold_left (fun i pe -> i ^ self#visit_type_prefix pe ^ " ") ""
  
  method visit_type_range = function
    | Type.RangeExpression(e) -> "[" ^ (self#visit_expression e) ^ "]"
    | Type.RangeType(tp) -> "[" ^ (self#visit_type tp) ^ "]"

  method visit_type_range_list =
    List.fold_left (fun i r -> (self#visit_type_range r) ^ i) ""
  
  method type_and_range = function 
    | Type.Range(tp,range) -> 
        (self#visit_type tp, self#visit_type_range_list range)
    | Type.Ref(Type.Range(tp,range)) ->
        (self#visit_type (Type.Ref tp), self#visit_type_range_list range)
    | tp -> (self#visit_type tp, "") 
  
  method! visit_type tt = 
    let prefix = self#visit_type_prefix_list in
    let expr = self#visit_expression in
    let open Expression in
    match tt with
    | Type.Typename(p,id) -> (prefix p) ^ id
    | Type.Bool(p) -> (prefix p) ^ "bool"
    | Type.Int(p,e1,e2) ->  
        if Type.is_unranged_int tt
        then (prefix p) ^ "int"
        else (prefix p) ^ "int[" ^ (expr e1) ^ "," ^ (expr e2) ^ "]" 
    | Type.Chan(p) -> (prefix p) ^ "chan" 
    | Type.Clock -> "clock"
    | Type.Void -> "void"
    | Type.Scalar(p,e) -> (prefix p) ^ "scalar[" ^ (expr e) ^ "]"
    | Type.Struct(p,fields) -> 
        let pfields = List.fold_left 
          (fun i (tp, id) -> 
            let tp', range = self#type_and_range tp in
            i ^ tp' ^ " " ^ id ^ range ^ "; " 
          ) "" fields in
        (prefix p) ^ "struct { " ^ pfields ^ "}"
    
    | Type.Ref(tp) -> (self#visit_type tp) ^ "&amp;"
    | Type.Range(tp,range) -> "|ERROR PRETTY PRINTING TYPE|" (* assert false *)(*should not be called!*)

  method visit_singleselect ((id', type') : Select.single) =
    id' ^ " : " ^ (self#visit_type type')
        
  method! visit_invariant {Invariant.expr; xy} =
    self#visit_expression expr
    
  method visit_assign {Update.el; xy} =
    str_list ~sep:"," self#visit_expression el 

  method! visit_sync {Sync.expr; xy; marker} =
    let psync = self#visit_expression expr in
    let pmarker = match marker with
      | Sync.Broadcast -> "!"
      | Sync.Synchronize -> "?"
    in
    psync ^ pmarker

  method! visit_guard {Guard.expr; xy} =
    self#visit_expression expr
    
  method! visit_parameter { Parameter.typ; name } =
    let tp', range = self#type_and_range typ in
    tp' ^ " " ^ (self#visit_id name) ^ range 

  method! visit_chanpriority priority channel =
    let open Declaration in match channel with
    | DefaultPriority -> "default"
    | Channel(id,el) -> 
        let brackets expr = "[" ^ (self#visit_expression expr) ^ "]" in
        id ^ (str_list ~sep:"" brackets el)

  method! visit_initializer = function
    | Initializer.Expression(e) -> self#visit_expression e
    | Initializer.Fields(fields) ->
        let pf = str_list ~sep:", " 
          (fun f -> match f with
            | (None,init) -> self#visit_initializer init
            | (Some id,init) -> id ^ " : " ^ (self#visit_initializer init))
          fields
        in 
      "{ " ^ pf ^ " }"

  method! visit_declaration declaration = 
    let open Declaration in match declaration with
    | Variable(tp,id,None) ->
       let tp', range = self#type_and_range tp in
       tp' ^ " " ^ id ^ range ^ ";"
    | Variable(tp,id,Some init) ->
       let tp', range = self#type_and_range tp in
       tp' ^ " " ^ id ^ range ^ " = " ^ (self#visit_initializer init) ^ ";"
    | Instantiation(instantiation) -> self#visit_instantiation instantiation 
    | BeforeUpdate(el) -> "before_update { " ^ (str_list ~sep:", " self#visit_expression el) ^ " }"
    | AfterUpdate(el) -> "after_update { " ^ (str_list ~sep:", " self#visit_expression el) ^ " }"
    | Typedef(tp,id) -> 
        let tp', range = self#type_and_range tp in
        "typedef " ^ tp' ^ " " ^ (self#visit_id id) ^ range ^ ";" 
    | Function(tp,id,parameters,declarations,statements) ->
        let ptp = self#visit_type tp in
        let pid = self#visit_id id in
        let pparameters = str_list ~sep:", " self#visit_parameter parameters in
        "\n" ^ ptp ^ " " ^ pid ^ " (" ^ pparameters ^ ") {\n" ^
          (str_list ~sep:"\n" self#visit_declaration declarations) ^ "\n" ^
          (str_list ~sep:"\n" self#visit_statement statements) ^ "\n" ^
        "}"
    | ChanPriority(chel) ->
        let priority = ref 0 in
        let innerl = str_list ~sep:", " (self#visit_chanpriority !priority) in
        let outerl = str_list ~sep:" < " (fun ch -> priority := !priority + 1; innerl ch) in
        "chan priority " ^ (outerl chel) ^ ";" 

  method! visit_statement statement = 
    let open Statement in match statement with
    | Empty -> ";"
    | Statement.Expression(e) -> (self#visit_expression e) ^ ";"
    | Block(declarations,statements) -> 
        "{\n" ^ (str_list ~sep:"\n" self#visit_declaration declarations) ^ "\n" ^
                (str_list ~sep:"\n" self#visit_statement statements) ^ "\n}"
    | For(el1,el2,el3,stmt) ->
        let pe = str_list ~sep:", " self#visit_expression in
        "for (" ^ (pe el1) ^ "; " ^ (pe el2) ^ "; " ^ (pe el3) ^ ") " ^
        (self#visit_statement stmt)
    | Foreach(id,tp,stmt) ->
        "for (" ^ (self#visit_id id) ^ " : " ^ (self#visit_type tp) ^ ") " ^
        (self#visit_statement stmt)
    | While(el,stmt) ->
        "while (" ^ (str_list ~sep:", " self#visit_expression el) ^ ") " ^ 
        (self#visit_statement stmt)
    | Do(el,stmt) ->
        "do " ^ (self#visit_statement stmt) ^ 
        " while (" ^ (str_list ~sep:", " self#visit_expression el) ^ ");"
    | If(el,ifstmt,estmt) ->
        let pelse = match estmt with
          | None -> ""
          | Some es -> " else " ^ self#visit_statement es
        in
        "if (" ^ (str_list ~sep:", " self#visit_expression el) ^ ") " ^
        (self#visit_statement ifstmt) ^ pelse
    | Return(expr) -> 
        let pexpr = match expr with
          | None -> ""
          | Some e -> " " ^ self#visit_expression e
        in 
        "return" ^ pexpr ^ ";"
    | _ -> assert false (* should not occur, Break, Continue, Switch and Assert are unused! *)

  method! visit_processes = function
    | [] -> ""
    | {Process.priority; name}::tl -> 
      let prio = ref priority in
      List.fold_left 
        (fun i {Process.priority; name} ->
          let sep = if priority <> !prio then " < " else ", " in
          prio := priority;
          i ^ sep ^ (self#visit_id name)
        ) (self#visit_id name) tl
  
  method! visit_progresses = function
    | [] -> ""
    | pl -> 
      "progress {\n" ^ 
        (str_list2 ~sep:";\n" self#visit_progress pl |> xmlescape_of_string) ^
      "}\n"

  method! visit_instantiation {Instantiation.instance_id; instance_parameters; 
        template_id; template_arguments} =
    let iparams = str_list ~sep:", " self#visit_parameter instance_parameters in
    let iparams = if iparams = "" then " " else "(" ^ iparams ^ ") " in
    let targs = str_list ~sep:", " self#visit_expression template_arguments in
    instance_id ^ iparams ^ "= " ^ template_id ^ "(" ^ targs ^ ");"

  method! visit_queries ?(symbol_table = None) { Queries.queries; nta; } =
    str_list ~sep:"\n\n" self#visit_expression queries
end


(******************************************************************************)

(** Printer for XTA4 format *)
class xta4_printer =
  object (self)
  inherit pretty_printer as super

  val mutable line_width = 0

(* Since this cannot be done using optional constructor arguments in OCAML
 * and moving this into the constructor as non-optional argument would cause 
 * a lot of conflicts, we currently use this setter to enable word-wrapping
 * for the xta4 printer *)
  method set_linewidth lw = line_width <- lw

  method wwrap str = if(line_width < 0) then failwith("Invalid line width"); if (line_width > 0) then Tataf_libcore.ExtString.wordwrap line_width str else str

  method! visit_color color = color
  
  method! visit_invariant {Invariant.expr; xy} =
    let pkind = xmlattribute "kind" "invariant" in
    let px, py = ppos xy in
    let pinv = self#wwrap (self#visit_expression expr |> xmlescape_of_string) in
    "   <label" ^ pkind ^ px ^ py ^ ">" ^ pinv ^ "</label>\n"
        
  method! visit_select {Select.sl; xy} =
    let pkind = xmlattribute "kind" "select" in
    let px, py = ppos xy in
    let passign = str_list ~sep:", " self#visit_singleselect sl 
               |> xmlescape_of_string in
    "   <label" ^ pkind ^ px ^ py ^ ">" ^ passign ^ "</label>\n"
    
    
  method! visit_update {Update.el; xy} =
    let pkind = xmlattribute "kind" "assignment" in
    let px, py = ppos xy in
    let passign = self#wwrap (str_list ~sep:", " self#visit_expression el 
                               |> xmlescape_of_string) in
    "   <label" ^ pkind ^ px ^ py ^ ">" ^ passign ^ "</label>\n"

  method! visit_sync {Sync.expr; xy; marker} =
    let pkind = xmlattribute "kind" "synchronisation" in
    let px, py = ppos xy in
    let psync = self#visit_expression expr |> xmlescape_of_string in
    let pmarker = match marker with
      | Sync.Broadcast -> "!"
      | Sync.Synchronize -> "?"
    in
    "   <label" ^ pkind ^ px ^ py ^ ">" ^ psync ^ pmarker ^ "</label>\n"

  method! visit_guard {Guard.expr; xy} =
    let pkind = xmlattribute "kind" "guard" in
    let px, py = ppos xy in
    let pguard = self#wwrap (self#visit_expression expr |> xmlescape_of_string) in
    "   <label" ^ pkind ^ px ^ py ^ ">" ^ pguard ^ "</label>\n"
  
  method! visit_comment comment =
    let pkind = xmlattribute "kind" "comments" in
    let plabel = xmlescape_of_string comment in
    "   <label" ^ pkind ^ ">" ^ plabel ^ "</label>\n"
  
  method! visit_nail {Nail.pos} =
    let px, py = ppos (Some pos) in
    "   <nail" ^ px ^ py ^ "/>\n"
  
  method! visit_source id =
    let pid = xmlattribute "ref" id in
    "   <source" ^ pid ^ "/>\n"

  method! visit_target id =
    let pid = xmlattribute "ref" id in
    "   <target" ^ pid ^ "/>\n"
  
  method! visit_edge 
    {Edge. id; source; target; nails; xy; color;
      select; guard; sync; update; comment; } =
    let pid = id |> option_or_default "" 
                 |> self#visit_id 
                 |> xmlattribute "id" in
    let px, py = ppos xy in
    let pcolor = color |> self#option self#visit_color 
                       |> xmlattribute "color" in
    "  <transition" ^ pid ^ px ^ py ^ pcolor ^ ">\n" ^
      (self#visit_source source) ^
      (self#visit_target target) ^
      (select  |> self#option self#visit_select) ^ 
      (guard   |> self#option self#visit_guard) ^
      (sync    |> self#option self#visit_sync) ^
      (update  |> self#option self#visit_update) ^
      (comment |> self#option self#visit_comment) ^ 
      (self#visit_nails nails) ^
    "  </transition>\n"
  
  method! visit_name {Name.text; xy} = 
    let px, py = ppos xy in
    let pvalue = xmlescape_of_string text in
    "  <name" ^ px ^ py ^ ">" ^ pvalue ^ "</name>\n"

  method! visit_exponentialrate { Exponentialrate.value; xy } =
    let px, py = ppos xy in
    let pkind = xmlattribute "kind" "exponentialrate" in
    let str = string_of_float value in
    let strvalue = (if str.[(String.length str) - 1] = '.'
		   then (String.sub str 0 ((String.length str) - 1))
                   else str) in
    "   <label" ^ pkind ^ px ^ py ^ ">" ^ strvalue ^ "</label>\n"
     
  method! visit_location 
    {Location. id; name; urgent; committed; xy; color; invariant; comment; exponentialrate} =
    let pid = id |> self#visit_id 
                 |> xmlattribute "id" in
    let px, py = ppos xy in
    let pcolor = color |> self#option self#visit_color
                       |> xmlattribute "color" in
    "  <location" ^ pid ^ px ^ py ^ pcolor ^ ">\n" ^
      (name      |> self#option self#visit_name) ^
      (invariant |> self#option self#visit_invariant) ^
      (comment   |> self#option self#visit_comment) ^
      (if urgent    then "   <urgent/>\n"    else "") ^
      (if committed then "   <committed/>\n" else "") ^
      (exponentialrate |> self#option self#visit_exponentialrate) ^ 
    "  </location>\n"
        
  method! visit_parameters = function
    | [] -> "" (*no parameters*)
    | pl -> "  <parameter>" ^  
            (str_list ~sep:", " self#visit_parameter pl) ^ 
            "</parameter>\n"
    
  method! visit_declarations = function
    | [] -> ""
    | dl ->
      let decls = str_list2 self#visit_declaration dl in
      " <declaration>\n" ^
        (xmlescape_of_string decls) ^
      " </declaration>\n"
    
  method! visit_init init = 
      let pref = xmlattribute "ref" init in
      "  <init" ^ pref ^ "/>\n"
  
  method! visit_template template = 
      " <template>\n" ^
        (super#visit_template template) ^
      " </template>\n"
  
  method! visit_imports imports = 
      " <imports>\n" ^ 
        (xmlescape_of_string imports) ^ "\n" ^
      " </imports>\n"
    
  method visit_xml_instantiation instantiation =
    match instantiation with 
    | [] -> ""
    | _ ->
      " <instantiation>\n" ^
        (super#visit_declarations instantiation) ^ "\n" ^
      " </instantiation>\n" 
  
  method! visit_system { System.declarations; progresses; processes } =
    let decls = List.fold_left 
      (fun i d -> i ^ (self#visit_declaration d) ^ "\n") "" declarations
    in
    " <system>" ^ 
      (xmlescape_of_string decls) ^
      "system " ^ (self#visit_processes processes |> xmlescape_of_string) ^ ";" ^ 
      (self#visit_progresses progresses) ^
    "</system>\n"
  
  method! visit_nta 
      {NTA. imports; declarations; templates; instantiation; system; dtd_version } =
    let dtd_string = DtdVersion.string_of_dtd_version dtd_version in
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" ^
    "<!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.1//EN' '" ^ 
     dtd_string  ^
    "'>\n<nta>\n" ^ 
      self#fl [
        self#option self#visit_imports imports; 
        self#visit_declarations declarations;
        self#visit_templates templates;
        self#visit_xml_instantiation instantiation;
        self#visit_system system ] ^ 
    "</nta>"
  
end

(** shortcut *)
let print_xta4 = (new xta4_printer)#visit

(******************************************************************************)

class query_printer =
  object (self)
  inherit pretty_printer as super
  
  method! visit_expression expr =
    let pe = self#visit_expression in 
    match expr with
    | Unary(UnaryNot,e) -> "not " ^ (pe e)
    | BoolOperation(e1,AndAnd,e2) -> (pe e1) ^ " and " ^ (pe e2)
    | BoolOperation(e1,OrOr,e2) -> (pe e1) ^ " or " ^ (pe e2)
    | Imply(e1,e2) -> (pe e1) ^ " imply " ^ (pe e2)
    | e -> super#visit_expression e 
end 

(** shortcut *)
let print_query = (new query_printer)#visit_expression
let print_queries = (new query_printer)#visit_queries

(******************************************************************************)

class symbol_printer =
  object (pp)
  inherit pretty_printer as super
  
  method visit_symbol symbol = 
    let open SymbolTable in
    let pparam = str_list ~sep:" -> " pp#visit_parameter in 
    let tr tp = 
      let pt, pr = pp#type_and_range tp in
      let pr = if pr = "" then "" else "<range: " ^ pr ^ ">" in
      pt ^ pr
    in 
    match symbol with
    | S_Typedef(t) -> "<typedef> " ^ (pp#visit_type t) 
    | S_Variable(t) -> "<variable> " ^ (tr t)
    | S_Function(rt, param) -> "<function> " ^ (pparam param) ^ " -> " ^ (pp#visit_type rt) 
    | S_Template(param,_) -> "<template> " ^ (pparam param)
end

(******************************************************************************)

class symbol_table_printer =
  object(self)
  inherit UppaalWalker.string_walker as super
  
  val sp = new symbol_printer 
  val mutable indent = 0
  method indents = String.make (indent * 2) ' '
  
  
  (** push frame *)
  method! frame_push ~frame_type =
    super#frame_push ~frame_type;
    (* make type of attribute 'frame' explicit (under the name of
     * current_frame) to avoid the 'ground coercion is not principal' warning.
     *)
    let current_frame : SymbolTable.frame = frame in
    let st_str = SymbolTable.string_of_symbol_table
                   sp (current_frame :> SymbolTable.t) in
    print_endline (self#indents ^ "{{ " ^ st_str);
    indent <- indent + 1
  
  (** pop frame *)
  method! frame_pop : 'a. 'a -> 'a = fun result ->
    indent <- indent - 1;
    print_endline (self#indents ^ "}}");
    super#frame_pop result  
  
  method! add_symbol : 'a. ID.t -> SymbolTable.symbol -> 'a -> 'a =
  let string_of_init id = 
      begin match (self#frame)#get_initializer id with
      | None -> ""
      | Some init -> " = " ^ (sp#visit_initializer init)
      end
  in
    fun id symbol result ->
    begin if String.length id <= 15
      then 
          Tataf_libcore.ExtString.columns2 ~col1width:15 id ((sp#visit_symbol symbol)
                                               ^ (string_of_init id))

      else id ^ " : " ^ (sp#visit_symbol symbol) ^ (string_of_init id)
    end
    |> (^) "| "
    |> (^) self#indents
    |> print_endline; 
    super#add_symbol id symbol result
end

(******************************************************************************)

(** PNF printer (prefix normal form) *)
class pnf_printer =
  object (self)
  inherit pretty_printer as super
  
  (* --- XTA part ------------------------------ *)
  
  method! visit_expression expr =
    let paop = self#visit_assignOp in
    let punary = self#visit_unary in
    let pe = self#visit_expression in 
    let prel = self#visit_rel in
    let pintop = self#visit_intop in
    let pbop = self#visit_boolop in
    match expr with
    | Call(e,args) -> "(" ^ (pe e) ^ " " ^ (str_list pe args) ^ ")"
    | Array(e1,e2) -> (pe e1) ^ "[" ^ (pe e2) ^ "]"
    | Parenthesis(e) -> "(" ^ (pe e) ^ ")"
    | IncPost(e) -> "(++ " ^ (pe e) ^ ")"    (* ATTENTION: looks equal to pre*)
    | IncPre(e) -> "(++ " ^ (pe e) ^ ")"
    | DecPost(e) -> "(-- " ^ (pe e) ^ ")"    (* ATTENTION: looks equal to pre *)
    | DecPre(e) -> "(-- " ^ (pe e) ^ ")"
    | Assignment(e1,op,e2) -> 
      "(" ^ (paop op) ^ " " ^ (pe e1) ^ " " ^ (pe e2) ^ ")"
    | Unary(op,e) -> 
      (* Do not encapsulate unary expressions in additional parenthesis. *)
      let ppe = begin
      match e with 
      | Parenthesis(e2) -> self#visit_expression e2
      | e2 -> self#visit_expression e2
      end in 
      "(" ^ (punary op) ^ " " ^ ppe ^ ")"
    | Relation(e1,op,e2) -> 
      "(" ^ (prel op) ^ " " ^ (pe e1) ^ " " ^ (pe e2) ^ ")"
    | IntOperation(e1,op,e2) -> 
      "(" ^ (pintop op) ^ " " ^ (pe e1) ^ " " ^ (pe e2) ^ ")"
    | BoolOperation(e1,op,e2) -> 
      "(" ^ (pbop op) ^ " " ^ (pe e1) ^ " " ^ (pe e2) ^ ")"
    | IfExpr(e1,e2,e3) -> 
      "(?: " ^ (pe e1) ^ " " ^ (pe e2) ^ " " ^ (pe e3) ^ ")"
    | _ -> super#visit_expression expr


  method! visit_update ({Update.el; xy} as assign) = 
    let pe = self#visit_expression in
    match el with
    | [] -> ""
    | e::[] -> pe e
    | e::es -> 
      "(; " ^ (pe e) ^ " " ^ (self#visit_update {assign with Update.el = es}) ^ ")"


  method! visit_sync {Sync.expr; xy; marker} =
    let psync = self#visit_expression expr in
    let pmarker = match marker with
      | Sync.Broadcast -> "!"
      | Sync.Synchronize -> "?"
    in
    "(" ^ pmarker ^ " " ^ psync ^ ")"
  
end;;
