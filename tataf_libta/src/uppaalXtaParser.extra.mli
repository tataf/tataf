(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**/**)
(**
  During the compilation process, this file will be appended to the regular
  interface file of UppaalXtaParser.  
*)
exception Specific_Parse_error of Lexing.position * Lexing.position * string

(**
  Symbol Table used during parsing. It is used to resolve T_TYPEDEF tokens.
  Set this to the correct starting table, before calling a parsing function.
*)
(*val symbol_table : UppaalSystem.symbol_table ref*)
val symbol_table : SymbolTable.frame ref

(*val resolve : string -> UppaalSystem.Symbol.t*)