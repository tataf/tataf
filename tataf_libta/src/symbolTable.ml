(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open UppaalSystem
open Tataf_libcore.ExtList

(*----------------------------------------------------------------------------*)

type t =
  < frame_type      : frame_type;
    parent          : t option;
    
    resolve         : ID.t -> symbol option;
    get_initializer : ID.t -> Initializer.t option;
    
    fold_left       : 'a. ('a -> ID.t * symbol -> 'a) ->'a -> 'a;
    iter            : (ID.t * symbol -> unit) -> unit;
  >

and symbol =
  | S_Typedef of Type.t
  | S_Variable of Type.t 
  | S_Function of Type.t * Parameter.t list
  | S_Template of Parameter.t list * t

and frame_type =
  | FT_Unspecified 
(* general *)
  | FT_NTA of NTA.t
  | FT_Template of Template.t
(* statements *)
  | FT_Function of Declaration.t
  | FT_Block of Statement.t
  | FT_Foreach of Statement.t
(* expressions *)
  | FT_Forall of Expression.t
  | FT_Exists of Expression.t
(* other *)
  | FT_Edge of Edge.t
  | FT_Instantiation of Instantiation.t


class frame ?(frame_type=FT_Unspecified) parent =
  object (self)
  
  val symbols : (ID.t * symbol) list = []
  method symbolshack = symbols

  val initializers : ((ID.t, Initializer.t option) Hashtbl.t) = Hashtbl.create 0

  val frame_type = frame_type
  method frame_type = frame_type
  
  val parent : frame option = parent
  method parent = parent

  method add_initializer name init =
      Hashtbl.add initializers name init

  method get_initializer name =
      try
          Hashtbl.find initializers name
      with Not_found -> None

(* Method 'add' (seems that) returns a copy of 'symbols' variable
   based on http://rigaux.org/language-study/syntax-across-languages-per-language/OCaml.html
   (search for: object cloning) where it's stated that symbol {< >} returns
   a copy of an object. The copy is a fresh object with the same methods and
   instance variables as the object being copied.
   See https://caml.inria.fr/pub/docs/manual-ocaml/libref/Oo.html

   an OCaml class can have variables which are lists and can be modified
   per request (see page 4/45 from:
   ​http://www.cas.mcmaster.ca/~kahl/CAS706/2010/Pres/OCamlObjects_Presentation.pdf)
   to have this setup working in current 'frame' class, based on same source,
   one should declare 'symbols' as mutable and make 'add' method to return
   unit and not a type: 'a that currently returns, this last thing is
   achieved by changing the last line of the 'add' method to be:
       
        symbols <- (name, symbol) :: symbols
   
   with this change, one can just call elsewhere:

        frame#add id symbol

   and the changes on the 'symbol' list of the given object should be
   performed. *)
  method add name symbol = 
    if List.exists (fun (n, _) -> n  = name) symbols then
        failwith("Symboltable: Duplicate symbol with name " 
                 ^  name ^ " in one scope.")
    else
        {< symbols = (name, symbol) :: symbols >}
  
  method resolve name = 
    let rec lookup = function
      | [] -> None
      | (n, symbol) :: _ when n = name -> Some symbol
      | _ :: tl -> lookup tl
    in 
    lookup symbols

  method iter f = List.iter f symbols

  method fold_left : 'b. ('b -> string * symbol -> 'b) -> 'b -> 'b = 
    fun f accu -> List.fold_left f accu symbols
end


class virtual frame_worker =
  object (self)

  val mutable frame = new frame None
  method frame = frame
  method frame_set new_frame = frame <- new_frame
  
  (* push frame onto symbol table. *)
  method frame_push ~frame_type =
    frame <- new frame ~frame_type (Some frame)

  (* Pop frame from symbol table. *)    
  method frame_pop : 'a. 'a -> 'a = fun result ->
    match frame#parent with
    | Some f -> frame <- f; result
    | None -> failwith "FrameWorker: Trying to pop stack, but no frame left on the stack."    

  (* Opens a new scope (creates a new frame) and executes [f] within this scope.
      After execution the new frame will be popped. *)  
  method scope : 'a. frame_type -> (unit -> 'a) -> 'a = fun frame_type f ->
    self#frame_push ~frame_type;
    let result = f () in
    self#frame_pop result

  (* Adds a new symbol on the current frame. *)
  method add_symbol : 'a. ID.t -> symbol -> 'a -> 'a =
    fun id symbol result ->
    frame <- frame#add id symbol; result

  (* Adds a new initializer expression on the current frame, which can be used
     to look up initializer values (e.g. for constant propagation) *)
  method add_initializer id init =
      frame#add_initializer id init

end

(*----------------------------------------------------------------------------*)

let rec get_initializer frame (name :ID.t) : Initializer.t option =
    match frame#get_initializer name with
    | None -> begin match frame#parent with
                | None -> None
                | Some parent -> get_initializer parent name
              end
    | Some init -> Some init

let rec resolve frame (name : ID.t) : symbol option =
  match frame#resolve name with
  | None -> begin match frame#parent with
              | None        -> None
              | Some parent -> resolve parent name
            end
  | symbol -> symbol

(* provide the "ground type" of typ by resolving typedefs *)
let rec resolve_type frame ( typ : Type.t ) : Type.t option =
  let open Type in
  match typ with
  (* assume typ is representing 'typedef T0 T' *)
  | Typename( pl, id )
      ->  begin
            (* ... then resolve T to get its direct base type T0... *)
            match resolve frame id with
            | Some S_Typedef( typ0 ) 
              -> begin
                  (* ... if T0 is a type: recurse. *)
                  let t = resolve_type frame typ0 in
                  (* make sure to maintain old prefixes (const, etc.) *)
                  match t with
                  | Some (Bool (pl2)) -> Some (Bool(pl @ pl2))
                  | Some Int (pl2, e1, e2) -> Some (Int(pl @ pl2, e1, e2))
                  | Some Chan (pl2) -> Some (Chan(pl @ pl2))
                  | Some Scalar (pl2, e) -> Some (Scalar(pl @ pl2, e))
                  | Some Struct(pl2, fl) -> Some (Struct(pl @ pl2, fl))
                  | _ -> t
                end
            (* ... otherwise T0 is not in symbol table, or not a type *)
            | _ -> None
          end
  | Ref( t )
      ->  begin
            match resolve_type frame t with
            | Some typ0 -> Some (Ref( typ0 ))
            | _ -> None
          end
  | Range( t, rl )
      ->  begin
            match resolve_type frame t with
            | Some typ0 -> Some (Range( typ0, rl ))
            | _ -> None
          end
  | _ -> Some typ


let rec resolve_to_template_qualified frame (name : ID.t) :
  UppaalQeClasses.TemplateQualifiedID.t option =
  let open UppaalQeClasses in
  (* note: #resolve here is not recursive -- it only looks into 'frame' *)
  match frame#resolve name with
  (* this case matches for all names which are _not_ _declared_ in frame (where *)
  (* frame can in particular be an anonymous scope); then we look upwards       *)
  | None -> begin match frame#parent with
              | None        -> None
              | Some parent -> resolve_to_template_qualified parent name
            end
  (* this case matches when we are at the innermost frame where 'name' is     *)
  (* _declared_; if this is an NTA or a Template frame, we can fully qualify  *)
  (* 'name', we have found what we're looking for                             *)
  | _    -> match frame#frame_type with
            | FT_NTA(_)      -> Some { TemplateQualifiedID.
                                         template = None;
                                         name = name; }
            | FT_Template(t) -> Some { TemplateQualifiedID.
                                         template =
                                           Some t.Template.name.Name.text;
                                         name = name; }
            (* this case matches for ids which are _declared_ in anonymous    *)
            (* scopes like functions, or edges' select expression etc.; there *)
            (* is nothing to qualify then, 'name' in the current frame does   *)
            (* not refer to a global or template declaration                  *)
            | _              -> None

let rec resolve_template_qualified
  global_frame { UppaalQeClasses.TemplateQualifiedID.template; name } : symbol option =
    let open UppaalQeClasses in
    match template with
    | None     -> global_frame#resolve name
    | Some id  -> match global_frame#resolve id with
                    | Some ( S_Template( _, local_frame ) ) -> local_frame#resolve name
                    | _                                     -> None

let is_int_ref st (name : ID.t) : bool =
    match resolve st name  with
    | Some  S_Variable(Type.Ref(Type.Int(_))) -> true
    | _ -> false
let is_bool_ref st (name : ID.t) : bool =
    match resolve st name  with
    | Some  S_Variable(Type.Ref(Type.Bool(_))) -> true
    | _ -> false
let is_clock_ref st (name : ID.t) : bool =
    match resolve st name  with
    | Some  S_Variable(Type.Ref(Type.Clock)) -> true
    | _ -> false


let is_clock st (name : ID.t) : bool=
  match resolve st name with
  | Some S_Variable(Type.Clock) -> true
  | _ -> false


let is_bool st (name : ID.t) : bool=
    match resolve st name with
    | Some S_Variable(Type.Bool(_)) -> true
    | _ -> false


let is_int st (name : ID.t) : bool=
    match resolve st name with
    | Some S_Variable(Type.Int(_)) -> true
    | _ -> false


let is_const st (name : ID.t) : bool =
  match resolve st name with
    | Some S_Variable(tp) when Type.is_const tp 
        -> true
    | _ -> false  


(*----------------------------------------------------------------------------*)

let string_of_frame_type : frame_type -> string = function
  | FT_Unspecified -> "unspecified"
  | FT_NTA(_) -> "global"
  | FT_Forall(_) -> "forall expression"
  | FT_Exists(_) -> "exists expression"
  | FT_Block(_) -> "block"
  | FT_Foreach(_) -> "foreach"
  | FT_Function(decl) -> 
      let fname = begin match decl with
        | Declaration.Function(_,id,_,_,_) -> id
        | _ -> "??"
      end in "function '" ^ fname ^ "'" 
  | FT_Template(tmpl) -> 
      let open Template in
      let open Name in
      "template " ^ (tmpl.name.text) ^ "'"
  | FT_Edge(trans) -> 
      let open Edge in
      let tname = begin match trans.id with
        | None -> ""
        | Some x -> " '" ^ x ^ "'"
      end in "transition" ^ tname 
  | FT_Instantiation(inst) ->
      let open Instantiation in
      inst.Instantiation.instance_id


let string_of_symbol_table symbol_printer (st : t) : string = 
  let name = string_of_frame_type st#frame_type in
  let pname = match st#parent with
    | None -> "(none)"
    | Some pframe -> string_of_frame_type pframe#frame_type
  in
  let syms = st#fold_left 
    (fun i (id, symbol) ->
      let pre = if i = "" then "" else "\n" in
      let symbol_str = symbol_printer#visit_symbol symbol in
      pre ^ id ^ " : " ^ symbol_str)
    ""
  in
  name ^ " (parent : " ^ pname ^ ")" ^ syms 

