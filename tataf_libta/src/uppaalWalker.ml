(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(** This modules contains the walker base classes. It enables to write
  easy iteration functions for Uppaal models.
  This is basically the visitor pattern.

  When using walkers, be careful to preserve the visiting mechanism.  For
  example, an overriden visit_edge is responsible for calling visit_guard etc.
  This is important if both visit_edge and visit_guard are overridden, 
  because visit_guard would be skipped during the traversal unless it's called in visit_edge.
  Often, a good way to avoid this is to call super's method. *)

open UppaalSystem
open UppaalSystem.Expression
open Tataf_libcore.Util

open SymbolTable

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "UppaalWalker"


(*----------------------------------------------------------------------------*)

(**
  Traversal order:
Legend: 
{v
  +  This method is called once.
 (+) This method will be called if and only if its the option-type parameter
     is not None, e.g. an edge may not have a guard, thus visit_guard is not
     called for those edges which do not have a guard.
  *  This method is called 0..n times (e.g., a template has a list of edges,
     thus call visit_edge once for each edge).
 
 ... Ellipsis at the end of a line indicate, that further sub callings occur, but
     are not listed directly. (e.g. because of recursion)
v}
Call hierarchy (for NTA):
{v
visit [nta]
+ visit_nta [nta] (new SymbolTable Frame = global_frame)
  + visit_nta_unscoped [nta] 
    + visit_imports [nta's imports] 
    + visit_declarations [nta's declarations] -- global NTA declarations
    + visit_templates [nta.templates]
      + visit_name [template.name]
      + visit_parameters [template parameters]
        * visit_parameter
          + visit_type [type of parameter] ...
          + visit_id [name of parameter]
      + visit_declarations [template declarations]
        + visit_declaration (see below for more detail)
      + visit_locations [template locations]
        + visit_xmlid [location id, not visible in Uppaal GUI]
        + visit_name [location name, displayed in Uppaal GUI]
          + visit_id [text of the name label]
         (+ visit_xy [position of the name-label])
        + visit_invariant [location's invariant]
          + visit_expression [invariant expression] ...
         (+ visit_xy [position of invariant label])
        + visit_comment [location's comment]
        + visit_xy [position of location]
        + visit_color [color of location]
        + visit_exponentialrate [exponential rate of location]
      + visit_init [template.init]
        + visit_xmlid [init]
      + visit_edges [template.edges]
        * visit_edge [for each edge]
         (+ visit_xmlid [id of edge])
          + visit_source [id of source location]
            + visit_xmlid [id of source location]
          + visit_target [id of target location]
            + visit_xmlid [id of target location]
         (+ visit_select [select expression])
            + visit_selectexpressions [select expression]
              * visit_selectexpression [for each select sub-expression]
                + visit_id [id of single select expression]
                + visit_type [type of single select expression] ...
         (+ visit_guard [guard expression])
            + visit_expression [guard expression] ...
           (+ visit_xy [position of guard label])
         (+ visit_sync [synchronisation expression])
            + visit_expression [syncrhonisation expression] ...
           (+ visit_xy [position of syncrhonisation label])
         (+ visit_update [update/assign expression])
            + visit_updateexpressions [update expression]
              * visit_updateexpression [for each update]
                + visit_expression [update sub expression] ...
           (+ visit_xy [position of update label])
         (+ visit_comment [comment of edge])
          * visit_nails [anchor positions for the edge, if not pointing straight from source to target location]
            + visit_nail
              + visit_xy [position of the nail]
         (+ visit_xy [position of the edge's name label])
         (+ visit_color [color of the edge])
    + visit_declarations [nta instantiations] -- global instantiations
      + visit_declaration (see below for more detail)
    + visit_system [nta.system]
      + visit_declarations [system declarations]
      + visit_processes [system processes]
        + visit_process [process]
          + visit_id [process.name]
      + visit_progresses [system progresses]
        + visit_progress [progress]
          + visit_guard [progress guard]
          + visit_measure [progress measure]
            + visit_expression [measure-expression] ...
v}
Details for visit_declaration:
{v
visit_declaration
  | Variable -> 
        + visit_type [variable.typ] ...
        + visit_id [variable.id]
       (+ visit_initializer [variable.init])
          | Initializer.Expression -> 
                + visit_expression [init-expression] ...
          | Initializer.Fields ->
               * for each field:
                 (+ visit_id [id of the initializer field])
                  + visit_initializer [visit initializer of field] ... 
  | Instantiation ->
        + visit_intantiation
          + visit_id [instance.id]
          + visit_parameters [instance.incance_parameters]
            * visit_parameter
              + visit_type [type of parameter] ...
              + visit_id [name of parameter]
          + visit_id [template_id]
          + visit_expressions [instance.template_arguments]
  | BeforeUpdate ->
        + visit_expression [update-expression] ...
  | AfterUpdate ->
        + visit_expression [update-expression] ...
  | Typedef ->
        + visit_type [typedef.type] ...
        + visit_id [typedef.id]
  | Function -> 
        + visit_type [function return type] ...
        + visit_visit_id [function name]
        + visit_parameters [function parameters]
          * visit_parameter
            + visit_type [type of parameter] ...
            + visit_id [name of parameter]
        + visit_declarations [function declarations] (recursion)
        + visit_statements [function statements]
          + visit_statement [statement]
            | Empty -> (do nothing)
            | Expression -> 
                  + visit_expression [statement's expression] ...
            | Block ->
                  + visit_declarations [block declarations]
                    + visit_declaration ...
                  + visit_statements [block statements]
                    + visit_statement ...
            | For -> 
                  + visit_expression [initializer] ...
                  + visit_expression [condition] ...
                  + visit_expression [loop (increment)] ...
                  + visit_statement [loop statement, often 'Block'] ...
            | Foreach ->
                  + visit_id [foreach variable]
                  + visit_type [type of foreach variable] ...
                  + visit_statement [loop statement, often 'Block'] ...
            | While ->
                  + visit_expression [condition] ...
                  + visit_statement [loop statement, often 'Block'] ...
            | Do ->
                  + visit_expression [condition] ...
                  + visit_statement [loop statement, often 'Block'] ...
            | If ->
                  + visit_expression [condition] ...
                  + visit_statement [then-case] ...
                 (+ visit_statement [else-case] ...)
            | Break -> (do nothing)
            | Continue -> (do nothing)
            | Switch -> 
                  + visit_expression [switch expression] ...
                  * for each case:
                    + visit_expression [condition] ...
                    + visit_statements ...
            | Return ->
                 (+ visit_expression [if the return has a returning expression] ...)
            | Assert ->
                  + visit_expression [assert expression] ...
  | ChanPriority -> 
        * visit_chanpriority [priority]
            | DefaultPriority -> (do nothing)
            | Channel ->
                  + visit_id [channel id]
                  + visit_expression [channel expression] (see below for more details)
v}
Details for visit_type
{v
visit_type
  | Typename ->
        + visit_id [name of new type]
  | Bool -> (do nothing)
  | Int -> 
        + visit_expression [for lower bound of ranged int, default: Type.intMinRange] ...
        + visit_expression [for upper bound of ranged int, default: Type.intMaxRange] ...
  | Chan -> (do nothing)
  | Clock -> (do nothing)
  | Void -> (do nothing)
  | Scalar ->
        + visit_expression [scalar's size expression] ...
  | Struct ->
        * for each field:
          + visit_type [type of field] ...
          + visit_id [name of field] ...
  | Ref ->
        + visit_type [type of the reference] ...
  | Range ->
        + visit_type [type of the range/array] ...
        * for each rangedef ( [][][]...):
          | RangeExpression -> visit_expression ...
          | RangeType -> visit_type [if a type is taken as size specifier] ...
v}
Details for visit_expression
{v
visit_expression
  | Var ->
        +  visit_id [name of variable/function]
  | Num -> (do nothing)
  | Bool -> (do nothing)
  | Call -> 
        + visit_expression [expression of the call (eg. function name as Var)] ...
        * visit_expressions [call arguments]
          + visit_expression [call argument] ...
  | Array ->
        + visit_expression [ the expression part of array accessor: arrayaexpr[...] ] ...
        + visit_expression [ the field part of the array accessor: ...[arrayselector] ] ...
  | Parenthesis -> visit_expression ...
  | IncPost -> visit_expression ...
  | IncPre -> visit_expression ...
  | DecPost -> visit_expression ...
  | DecPre -> visit_expression ...
  | Assignment ->
        + visit_expression [lhs of assignment] ...
        + visit_expression [rhs of assignment] ...
  | Unary -> visit_expression ...
  | Relation -> 
        + visit_expression [lhs of relation] ...
        + visit_expression [rhs of relation] ...
  | IntOperation ->
        + visit_expression [lhs of integer operation] ...
        + visit_expression [rhs of integer operation] ...
  | BoolOperation ->
        + visit_expression [lhs of bool operation] ...
        + visit_expression [rhs of bool operation] ...
  | IfExpr ->
        + visit_expression [condition] ...
        + visit_expression [then part] ...
        + visit_expression [else part] ...
  | Dot -> 
        + visit_expression [expression before the dot 'identifier.'] ...
        + visit_id [identifier after the dot '.fieldname']
  | Dashed -> visit_expression ...
  | Deadlock -> (do nothing)
  | Imply -> 
        + visit_expression [lhs of imply expression] ...
        + visit_expression [rhs of imply expression] ...
  | Forall -> ["forall ( ID : Type) Expression"]
        + visit_id 
        + visit_type ...
        + visit_expression  ...
  | Exists -> ["exists (ID : Type) Expression"]
        + visit_id 
        + visit_type ...
        + visit_expression ...
  | CTL ->
        + visit_CTLexpression
          | AF -> visit_expression ...
          | AG -> visit_expression ...
          | EF -> visit_expression ...
          | AGR -> visit_expression ...
          | EFR -> visit_expression ...
          | EG -> visit_expression ...
          | AF2 ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | AG2 ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | EF2 ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | EG2 ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | Leadsto -> 
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | AU ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | AW ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
          | AU2 ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
                + visit_expression [e3] ... 
          | AW2 ->
                + visit_expression [e1] ...
                + visit_expression [e2] ... 
                + visit_expression [e3] ... 
v}
Entry point for nta:
{v
visit
  + visit_nta ...
v}
Entry point for queries:
{v
visit_queries
  + visit_expressions ...
v}
*)
class ['t] walker ?(concat=(fun (a:'t) (b:'t) -> b)) dval = 
  object (self)
  inherit SymbolTable.frame_worker
  val mutable global_frame : SymbolTable.frame option = None 
  method global_frame = global_frame
  
  val mutable default : 't = dval (* identity element of f *) 
  val mutable f : ('t -> 't -> 't) = concat
  method f' a b = f b a
  
  method fl lst = List.fold_right f lst default
  (*method fl lst = List.fold_left f default lst*)
  method iter : 'b. ('b -> 't) -> 'b list -> 't =
    fun tfun lst -> List.fold_left 
      (fun a e -> self#fl [a; (tfun e)]) default lst
  
  method option : 'b. ('b -> 't) -> 'b option -> 't =
    fun f value -> match value with
    | None -> default
    | Some v -> f v 

  method option_default : 'b. 't -> ('b -> 't) -> 'b option -> 't =
    fun def f value -> match value with
    | None -> def
    | Some v -> f v
  
  (* --- XTA part ------------------------------ *)
  
  method visit_CTLexpression ctl = 
    let pe = self#visit_expression in 
    match ctl with
    | AF(e) -> pe e
    | AG(e) -> pe e
    | EF(e) -> pe e
    | AGR(e) -> pe e
    | EFR(e) -> pe e
    | EG(e) -> pe e
    | AF2(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | AG2(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | EF2(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | EG2(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | Leadsto(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | AU(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | AW(e1,e2) ->  self#fl [ pe e1; pe e2 ]
    | AU2(e1,e2,e3) -> self#fl [ pe e1;  pe e2; pe e3; ]
    | AW2(e1,e2,e3) -> self#fl [ pe e1; pe e2; pe e3; ]
  
  method visit_expression expr = 
    match expr with
    | Var (id) -> self#visit_var expr
    | Num (_) -> self#visit_num expr
    | Bool (_) -> self#visit_bool expr
    | Call (e, args) -> self#visit_call expr
    | Array (e1, e2) -> self#visit_array expr
    | Parenthesis (e) -> self#visit_parenthesis expr
    | IncPost (e)     -> self#visit_post_increment expr
    | IncPre (e)      -> self#visit_pre_increment expr
    | DecPost (e)     -> self#visit_post_decrement expr
    | DecPre (e)      -> self#visit_pre_decrement expr
    | Assignment(e1, op, e2) -> self#visit_assignment expr
    | Unary (op, e) -> self#visit_unary_expression expr
    | Relation (e1, op, e2) -> self#visit_relation expr
    | IntOperation (e1, op, e2) -> self#visit_int_op expr
    | BoolOperation (e1, op, e2) -> self#visit_bool_op expr
    | IfExpr (e1, e2, e3) -> self#visit_if_expression expr
    | Dot (e1, id)       -> self#visit_dot_expression expr
    | Dashed (e)        -> self#visit_dashed_expression expr
    | Deadlock          -> self#visit_deadlock expr
    | Imply (e1, e2)    -> self#visit_imply expr
    | Forall (id, typ, e2) -> self#visit_forall expr
    | Exists (id, typ, e2) -> self#visit_exists expr
    | CTL (e) -> self#visit_CTLexpression e

  (* Note: the type check below is necessary because, e.g., Var is neither
     a module (like Parameter in visit_parameter) nor a type whose cases are
     completely covered (like Statement.t in visit_statement).

     IOW: Calling these fine-grained visitor methods with "wrongly" typed
     things is not detected by OCaml typechecking, but is perfectly possible
     technically. *)

  method fail_visit_mismatch fname argtype =
    failwith( "UppaalWalker: '" ^ fname ^ "' called with argument that 
                             does not have type '" ^ argtype ^ "'" )

  method visit_var = function 
    Var (id) -> self#visit_id id
    | _ -> self#fail_visit_mismatch "visit_var" "Var"
  method visit_num = function 
    Num (_) -> default
    | _ -> self#fail_visit_mismatch "visit_num" "Num"
  method visit_bool = function 
    Bool (_) -> default
    | _ -> self#fail_visit_mismatch "visit_bool" "Bool"
  method visit_call = function 
    Call (e, args)->self#fl[self#visit_expression e;self#visit_expressions args]
    | _ -> self#fail_visit_mismatch "visit_call" "Call"
  method visit_array  = function
    Array (e1, e2) -> self#fl [self#visit_expression e1; 
                                 self#visit_expression e2]
    | _ -> self#fail_visit_mismatch "visit_array" "Array"
  method visit_parenthesis = function
    Parenthesis (e) -> self#visit_expression e
    | _ -> self#fail_visit_mismatch "visit_parenthesis" "Parenthesis"

  method visit_post_increment = function
    IncPost (e) -> self#visit_expression e
    | _ -> self#fail_visit_mismatch "visit_post_increment" "IncPost"
  method visit_pre_increment = function
    IncPre (e) -> self#visit_expression e
    | _ -> self#fail_visit_mismatch "visit_pre_increment" "IncPre"
  method visit_post_decrement = function
    DecPost (e) -> self#visit_expression e 
    | _ -> self#fail_visit_mismatch "visit_post_decrement" "DecPost"
  method visit_pre_decrement = function
    DecPre (e) -> self#visit_expression e
    | _ -> self#fail_visit_mismatch "visit_pre_decrement" "DecPre"
  method visit_assignment = function
    Assignment (e1, op, e2) -> self#fl [self#visit_expression e1;
                                            self#visit_expression e2]
    | _ -> self#fail_visit_mismatch "visit_assignment" "Assignment"
  method visit_unary_expression = function
    Unary(op, e) -> self#visit_expression e
    | _ -> self#fail_visit_mismatch "visit_unary_expression" "Unary"
  
  method visit_relation = function
    Relation (e1, op, e2) -> self#fl [self#visit_expression e1;
                                          self#visit_expression e2]
    | _ -> self#fail_visit_mismatch "visit_relation" "Relation"
  method visit_int_op = function
    IntOperation (e1, op, e2) -> self#fl [self#visit_expression e1;
                                              self#visit_expression e2]
    | _ -> self#fail_visit_mismatch "visit_int_op" "IntOperation"
  method visit_bool_op = function
    BoolOperation (e1, op, e2) -> self#fl [self#visit_expression e1;
                                              self#visit_expression e2]
    | _ -> self#fail_visit_mismatch "visit_bool_op" "BoolOperation"

  method visit_if_expression = function
    IfExpr (e1, e2, e3) -> self#fl [self#visit_expression e1;
                                        self#visit_expression e2;
                                        self#visit_expression e3]
    | _ -> self#fail_visit_mismatch "visit_if_expression" "IfExpr"
  method visit_dashed_expression = function
    Dashed (e) -> self#visit_expression e
    | _  -> self#fail_visit_mismatch "visit_dashed_expression" "Dashed"
  method visit_dot_expression = function
    Dot(e1,id) -> self#fl [ self#visit_expression e1; self#visit_id id ]
    | _  -> self#fail_visit_mismatch "visit_dot_expression" "Dot"

  method visit_deadlock = function
    Deadlock -> default
    | _ -> self#fail_visit_mismatch "visit_deadlock" "Deadlock"
  method visit_imply = function
    Imply (e1, e2) -> self#fl [self#visit_expression e1;
                                   self#visit_expression e2]
    | _ -> self#fail_visit_mismatch "visit_imply" "Imply"
  method visit_forall fa = 
    match fa with
        | Forall (id, typ, e1) -> self#fl [self#visit_id id;
                                         self#visit_type typ;
                                         self#scope (FT_Forall fa) 
                                         (fun () -> (self#add_symbol id
                                                      (S_Variable typ) (); 
                                                     self#visit_expression e1;
                                                    )
                                         )
                                         ]
        | _ -> self#fail_visit_mismatch "visit_forall" "Forall"
  method visit_exists ex = 
    match ex with
        | Exists (id, typ, e1) -> self#fl [self#visit_id id;
                                         self#visit_type typ;
                                         self#scope(FT_Exists ex)
                                         (fun () -> (self#add_symbol id
                                                     (S_Variable typ) ();
                                                     self#visit_expression e1;
                                                    )
                                         )
                                        ]
        | _ -> self#fail_visit_mismatch "visit_exists" "Exists"

  method visit_expressions (expressions : Expression.t list ) : 't =
    self#iter self#visit_expression expressions
    
  method visit_type (typ : Type.t) = 
    let pe = self#visit_expression in
    let open Type in match typ with
    | Typename(_,id) -> self#visit_id id 
    | Bool(_) -> default 
    | Int(_,e1,e2) -> self#fl [ pe e1; pe e2 ] 
    | Chan(_) -> default 
    | Clock -> default 
    | Void -> default
    | Scalar(_,e) -> pe e
    | Struct(_,fields) -> 
        self#iter (fun (tp,field) -> 
          self#fl [ self#visit_type tp; self#visit_id field ]) fields 
    | Ref(tp) -> self#visit_type tp
    | Range(tp,rangel) ->
        self#fl [ 
          self#visit_type tp;
          self#iter (fun r -> match r with
            | RangeExpression(e) -> pe e
            | RangeType(tp') -> self#visit_type tp') rangel ]
    
  method visit_initializer = function
    | Initializer.Expression(e) -> self#visit_expression e
    | Initializer.Fields(fl) -> 
        self#iter ( fun (id,init) -> 
          self#fl [ self#option self#visit_id id;
                    self#visit_initializer init ]) fl  
  
  method visit_selectexpression ((id , tp) : Select.single) : 't =
    self#fl [ self#visit_id id;
              self#visit_type tp; ] 
    |> self#add_symbol id (S_Variable tp)
  
  method visit_selectexpressions (selects : Select.single list) : 't =
    self#iter self#visit_selectexpression selects
  
  method visit_select {Select. sl; xy} = 
    self#fl [ self#visit_selectexpressions sl;
              self#option self#visit_xy xy ]
  
  method visit_invariant {Invariant. expr; xy} = 
    self#fl [ self#visit_expression expr;
              self#option self#visit_xy xy ]
  
  method visit_updateexpression = 
    self#visit_expression
  
  method visit_updateexpressions =
    self#iter self#visit_updateexpression 
  
  method visit_update {Update. el; xy} = 
    self#fl [ self#visit_updateexpressions el;
              self#option self#visit_xy xy ]
  
  method visit_sync {Sync. expr; marker; xy; } = 
    self#fl [ self#visit_expression expr;
              self#option self#visit_xy xy ]
  
  method visit_guard {Guard. expr; xy} = 
    self#fl [ self#visit_expression expr;
              self#option self#visit_xy xy ] 
  
  method visit_measure =
    self#visit_expression
  
  method visit_comment (comment : Comment.t) : 't = default
  
  method visit_id (id : ID.t) : 't = default
  
  method visit_xy (xy : XY.t) : 't = default
  
  method visit_color (color : Color.t) : 't = default

  method visit_exponentialrate (exponentialrate : Exponentialrate.t) : 't = default

  method visit_nail {Nail.pos} = 
    self#visit_xy pos
      
  method visit_nails =
    self#iter self#visit_nail
  
  method visit_xmlid (id : ID.t) : 't = default

  method visit_source = self#visit_xmlid
  
  method visit_target = self#visit_xmlid
  
  method visit_edge 
    ({Edge. id; source; target; nails; xy; color;
      select; guard; sync; update; comment} as trans) =
    self#scope (FT_Edge trans) (fun () ->
      self#fl [
        self#option self#visit_xmlid id;
        self#visit_source source;
        self#visit_target target;
        self#option self#visit_select select;
        self#option self#visit_guard guard;
        self#option self#visit_sync sync;
        self#option self#visit_update update;
        self#option self#visit_comment comment;
        (*GUI extras*)
        self#visit_nails nails;
        self#option self#visit_xy xy;
        self#option self#visit_color color ] )

  method visit_edges = 
    self#iter self#visit_edge 
  
  method visit_name : Name.t -> 't = 
    fun {Name. text; xy} -> 
      self#fl [ self#visit_id text;
                self#option self#visit_xy xy; ] 
  
  method visit_location : Location.t -> 't = fun
    {Location. id; name; urgent; committed; xy; color; invariant; comment; exponentialrate} ->
    self#fl [
      self#visit_xmlid id;
      self#option self#visit_name name;
      self#option self#visit_invariant invariant;
      self#option self#visit_comment comment;
      self#option self#visit_xy xy;
      self#option self#visit_color color;
      self#option self#visit_exponentialrate exponentialrate; ]

  method visit_locations =
    self#iter self#visit_location 

  method visit_parameter {Parameter.typ; name} =
    self#fl [ self#visit_type typ;
              self#visit_id name; ]
    |> self#add_symbol name (S_Variable typ)

  method visit_parameters =
    self#iter self#visit_parameter
  
  method visit_statement (statement : Statement.t) =
    let open Statement in match statement with
    | Empty -> default
    | Expression(expr) -> self#visit_expression expr
    | Block(dl,stmts) ->
        self#scope (FT_Block statement) (fun () -> 
          self#fl [ self#visit_declarations dl;
                    self#visit_statements stmts; ] )
    | For(el1,el2,el3,stmt) ->
        self#fl [ self#visit_expressions el1;
                  self#visit_expressions el2;
                  self#visit_expressions el3;
                  self#visit_statement stmt; ]
    | Foreach(id,typ,stmt) -> 
        self#fl [ 
          self#visit_id id; 
          self#visit_type typ; 
          self#scope (FT_Foreach statement) (fun () ->
            self#add_symbol id (S_Variable typ) ();
            self#visit_statement stmt)  
        ]
    | While(el,stmt) ->
        self#fl [ self#visit_expressions el;
                  self#visit_statement stmt; ]
    | Do(el,stmt) ->
        self#fl [ self#visit_expressions el;
                  self#visit_statement stmt; ]
    | If(el,ifstmt,elsestmt) ->
        self#fl [ self#visit_expressions el;
                  self#visit_statement ifstmt;
                  self#option self#visit_statement elsestmt; ]
    | Break -> default
    | Continue -> default
    | Switch(el,cases) ->
        self#fl [ self#visit_expressions el;
                  self#iter 
                    (fun case -> match case with
                      | Case(expr,stmts) -> 
                          self#fl [ self#visit_expression expr;
                                    self#visit_statements stmts; ]
                      | Default(stmts) -> self#visit_statements stmts 
                    ) cases; ]
    | Return(expr) -> self#option self#visit_expression expr
    | Assert(expr) -> self#visit_expression expr

  method visit_statements =
    self#iter self#visit_statement
  
  method visit_chanpriority (priority : int) channel =
    let open Declaration in match channel with
    | DefaultPriority -> default 
      (** if visited, priority contains the default channel priority for the system*)
    | Channel(id,el) ->
        self#fl [ self#visit_id id;
                  self#visit_expressions el; ]
      
  method visit_declaration declaration = 
    let open Declaration in match declaration with
    | Variable(tp,id,init) ->
        self#add_initializer id init;
        self#fl [
          self#visit_type tp;
          self#visit_id id;
          self#option self#visit_initializer init; ]
        |> self#add_symbol id (S_Variable tp)
    | Instantiation(inst) -> self#visit_instantiation inst
    | BeforeUpdate(el) -> self#visit_expressions el
    | AfterUpdate(el) -> self#visit_expressions el
    | Typedef(tp,id) ->
        self#fl [ self#visit_type tp;
                  self#visit_id id ]
        |> self#add_symbol id (S_Typedef tp)
    | Function(tp,id,parameters,declarations,statements) ->
        self#scope (FT_Function declaration)
        (fun () ->
             self#visit_type tp
          |> self#f' (self#visit_id id)
          |> self#f' (self#visit_parameters parameters)
          |> self#f' (self#visit_declarations declarations)
          |> self#f' (self#visit_statements statements)
        ) |> self#add_symbol id (S_Function (tp, parameters))
    | ChanPriority(chel) -> 
        let priority = ref 0 in
        self#iter (fun channels -> 
          priority := !priority + 1;
          self#iter (self#visit_chanpriority !priority) channels
        ) chel

  method visit_declarations = 
    self#iter self#visit_declaration
  
  method visit_init = self#visit_xmlid

  method visit_template 
    ({Template. name; parameters; declarations; locations; init; edges} as tmpl) =
       self#visit_name name
    |> self#f' (
        self#frame_push ~frame_type:(FT_Template tmpl);
         let result = 
            self#visit_parameters parameters
         |> self#f' (self#visit_declarations declarations)
         |> self#f' (self#visit_locations locations)
         |> self#f' (self#visit_init init)
         |> self#f' (self#visit_edges edges)
        in
        (* declare type of current_frame explicitly to avoid warning 
         * 'ground coercion is not principal' during compilation. *)
        let current_frame : frame = self#frame in
        self#frame_pop result
        |> self#add_symbol 
             name.Name.text
             (S_Template (parameters, (current_frame :> SymbolTable.t)) ) )
    
  method visit_templates =
    self#iter self#visit_template 
  
  method visit_instantiation 
    ({Instantiation.instance_id; instance_parameters; 
                   template_id; template_arguments} as instantiation) =
    self#scope (FT_Instantiation instantiation) 
      (fun () ->
           self#visit_id instance_id
        |> self#f' (self#visit_parameters instance_parameters)
        |> self#f' (self#visit_id template_id)
        |> self#f' (self#visit_expressions template_arguments) 
      )
  
  method visit_process {Process.priority; name} =
    self#visit_id name
    
  method visit_processes =
    self#iter self#visit_process
  
  method visit_progress {Progress.guard; measure} =
       self#option self#visit_guard guard
    |> self#f' (self#visit_measure measure)
  
  method visit_progresses = 
    self#iter self#visit_progress
  
  method visit_system { System.declarations; processes; progresses } =
       self#visit_declarations declarations
    |> self#f' (self#visit_processes processes)
    |> self#f' (self#visit_progresses progresses)
    
  method visit_imports imports = 
    default
  
  method visit_nta_unscoped 
    {NTA. imports; declarations; templates; instantiation; system; dtd_version} =
                self#option self#visit_imports imports
    |> self#f' (self#visit_declarations declarations)
    |> self#f' (self#visit_templates templates)
    |> self#f' (self#visit_declarations instantiation)
    |> self#f' (self#visit_system system)
  
    
  method visit_nta 
    ({NTA. imports; declarations; templates; instantiation; system; dtd_version} as nta) =
    self#frame_push ~frame_type:(FT_NTA nta);
    let nta = self#visit_nta_unscoped nta in
      global_frame <- Some self#frame;
      self#frame_pop nta
       
  method visit_queries ?(symbol_table = None) { Queries.queries; nta } = 
    (* note: currently we do not visit the nta, which we obtain as parameter;
             until now there was no good need to visit it *)

    global_frame <- symbol_table; 

    (* in visit_nta(), the attribute 'frame' which points to the current 
       frame is initialized to the top-level frame which is newly created at
       the beginning of visit_nta -- and at the end of visit_nta, the frame
       stack is pushed again leaving 'frame' not pointing to any frame; 

       here, we want to use the given symbol_table, so we have to assign both,
       global_frame and, if some symbol table was given, also 'frame' to point
       to the given symbol table as the current one
     *)
    begin match self#global_frame with
      | Some f -> frame <- f
      | _      -> ()
    end;
    self#visit_expressions queries
    
  method visit = self#visit_nta (** shortcut *)
end

(*----------------------------------------------------------------------------*)

(**
  Decorator Base for Walker
  OBSOLETE: not working attempt of the Decorator pattern. (or not working
  like expected)
  
  A good way could be the observer pattern... The single base would iterate
  over multiple walker-observers, which can react to specific events.
*)
class ['t] walker_decorator blunt default =
  object
  inherit ['t] walker default
  val blunt = (blunt : 't walker)
  
  method! visit_CTLexpression = blunt#visit_CTLexpression
  method! visit_expression = blunt#visit_expression
  method! visit_expressions = blunt#visit_expressions
  method! visit_type = blunt#visit_type
  method! visit_initializer = blunt#visit_initializer
  method! visit_selectexpression = blunt#visit_selectexpression
  method! visit_selectexpressions = blunt#visit_selectexpressions
  method! visit_select = blunt#visit_select
  method! visit_invariant = blunt#visit_invariant
  method! visit_update = blunt#visit_update
  method! visit_sync = blunt#visit_sync
  method! visit_guard = blunt#visit_guard
  method! visit_measure = blunt#visit_measure
  method! visit_comment = blunt#visit_comment
  method! visit_id = blunt#visit_id
  method! visit_xy = blunt#visit_xy
  method! visit_color = blunt#visit_color
  method! visit_nail = blunt#visit_nail
  method! visit_nails = blunt#visit_nails
  method! visit_source = blunt#visit_source
  method! visit_target = blunt#visit_target
  method! visit_edge = blunt#visit_edge
  method! visit_edges = blunt#visit_edges
  method! visit_name = blunt#visit_name
  method! visit_location = blunt#visit_location
  method! visit_locations = blunt#visit_locations
  method! visit_parameter = blunt#visit_parameter
  method! visit_parameters = blunt#visit_parameters
  method! visit_statement = blunt#visit_statement
  method! visit_statements = blunt#visit_statements
  method! visit_chanpriority = blunt#visit_chanpriority
  method! visit_declaration = blunt#visit_declaration
  method! visit_declarations = blunt#visit_declarations
  method! visit_init = blunt#visit_init
  method! visit_template = blunt#visit_template
  method! visit_templates = blunt#visit_templates
  method! visit_instantiation = blunt#visit_instantiation
  method! visit_process = blunt#visit_process
  method! visit_processes = blunt#visit_processes
  method! visit_progress = blunt#visit_progress
  method! visit_progresses = blunt#visit_progresses
  method! visit_system = blunt#visit_system
  method! visit_imports = blunt#visit_imports
  method! visit_nta = blunt#visit_nta
  method! visit_queries = blunt#visit_queries
  method! visit = blunt#visit
end

(*----------------------------------------------------------------------------*)

(**
  {2 Base Walker for common return types.}
 *)

class unit_walker =
  object
  inherit [unit] walker () ~concat:(fun () () -> ())
end

class bool_walker =
  object
  inherit [bool] walker false ~concat:(||)
end

class int_walker = 
  object
  inherit [int] walker 0 ~concat:(+)
end

(**
  TODO: This class should use internally Buffer.t, to build strings.
  Complexity of string concatenation with (^) is quadratic,
  complexity of string concatenation with Buffer is linear!
*)
class string_walker = 
  object
  inherit [string] walker "" ~concat:(^)
end

class ['a] list_walker = 
  object
  inherit ['a list] walker [] ~concat:(@)
end

(*----------------------------------------------------------------------------*)

(**
  {2 Predefined Walker}
*)

(**
   Type to contain the participants of a channel 
*)
module ChannelParticipants = struct
  type t = {
   senders: ID.t list;
   receivers: ID.t list
  }
  
  let list_add list element =
    if (List.mem element list)
    then list
    else element :: list
         
  let add_to_senders {senders; receivers} t =
    {senders = list_add senders t; receivers}

  let add_to_receivers {senders; receivers} t =
    {senders; receivers = list_add receivers t}

  let empty () : t =
    {senders = []; receivers = []}
end
(**
  Lookup class.
  
  This class may be used to lookup some common properties of an nta, which
  are not encoded directly into the data structure.
  
  It gives you access to the following properties:
  -[location id]: 
      returns the {UppaalSystem.Location.t} with the given location [id] 
  -[edge id]: 
      returns the {!UppaalSystem.Edge.t} with the given edge [id]
  -[edges_out id]:
      returns all outgoing edges (list) of the location with the given [id].
  -[edges_in id]:
      returns all incomming edges (list) of the location with the given [id].
      
  -global_frame:
      returns the symbol_table of the global context.
*)
class lookup nta =
  object (self)
  inherit unit_walker as super
  
  val ht_location = Hashtbl.create 0; (* location.id -> location *)
  val ht_edge = Hashtbl.create 0; (* transition.id -> transition *)
  val ht_edges_out = Hashtbl.create 0 (* location.id -> transitions *)
  val ht_edges_in = Hashtbl.create 0  (* location.id -> transitions *)
  val ht_templates = Hashtbl.create 0 (* template.id -> Template *)

  val ht_location_with_name = Hashtbl.create 0; (* location.name -> location *)

  val mutable curr_template_name = "" (* Name of the template being visited *)
  val ht_channels = Hashtbl.create 0 (* channel.id -> channelParticipants.t *)

  method location id = 
    Hashtbl.find ht_location id

  method locations_list =
    Hashtbl.fold (fun id loc l -> (id, loc) :: l) ht_location []

  method edges_list =
    Hashtbl.fold (fun id edge l -> (id, edge) :: l) ht_edge []

  method templates_ht =
    ht_templates

  method location_with_name name =
    Hashtbl.find ht_location_with_name name

  method edge id = 
    Hashtbl.find ht_edge id  
  
  method edges_out id = 
    Hashtbl.find_all ht_edges_out id
    
  method edges_in id = 
    Hashtbl.find_all ht_edges_in id

  method channel_participants id =
    Hashtbl.find ht_channels id
  
  method! visit_location ({Location.id; name; _} as loc) =
    Hashtbl.add ht_location id loc;
    match name with
      | None -> ()
      | Some {Name.text; _} -> Hashtbl.add ht_location_with_name text loc 
    (*CUTOFF*)
  
  method! visit_edge ({Edge.id; source; target; _} as trans) =
    Hashtbl.add ht_edges_out source trans;
    Hashtbl.add ht_edges_in target trans;
    (match id with
     | None -> ()
     | Some tid -> Hashtbl.add ht_edge tid trans);
    super#visit_edge trans

  method! visit_sync {Sync.expr; marker; xy;} =
    let open ChannelParticipants in
    let channel = match expr with
        Var c -> c
      | _ -> "" in (* TODO: no channel arrays supported yet *)
    let curr_participants =
      match (Hashtbl.mem ht_channels channel) with
      | false ->  Hashtbl.add ht_channels channel (empty ()); (empty ())
      | true -> Hashtbl.find ht_channels channel
    in
    match marker with
    | Sync.Broadcast -> Hashtbl.replace ht_channels channel (add_to_senders curr_participants curr_template_name)
    | Sync.Synchronize -> Hashtbl.replace ht_channels channel (add_to_receivers curr_participants curr_template_name)

  method! visit_template ({Template.name; _ } as template) =
    curr_template_name <- name.Name.text;
    Hashtbl.add ht_templates name.Name.text template;
    super#visit_template template
  initializer
    self#visit nta
end

(*----------------------------------------------------------------------------*)

(**
  Example walker: This validates an Uppaal-model and checks, if the specified
  location within the transitions (source and target) are defined within the
  current template-scope.
*)
class simple_validator =
  object (self)
  inherit unit_walker as super

  val mutable location_names : string list = [] 
  
  val mutable valid = true
  
  method add_location_name {Name. text; _} =
     location_names <- text :: location_names

  method exists_location (name : ID.t) =
    List.exists (fun x -> x = name) location_names

  method invalid msg =
    valid <- false;
    dbg#warn ("Invalid: " ^ msg)

  method! visit_edge trans =
    if not (self#exists_location trans.Edge.source) then 
      self#invalid "source location undefined";
    if not (self#exists_location trans.Edge.target) then 
      self#invalid "target location undefined";
    super#visit_edge trans

  method! visit_location loc =
    match loc.Location.name with
    | None -> ()
    | Some name -> self#add_location_name name
    (*super#visit_location loc (*maybe unnecessary*)*)
  
  method! visit_template template =
    (* for each new template, reset location_names *)
    location_names <- [];
    super#visit_template template
      
  method validate nta =
    valid <- true;
    super#visit_nta nta;
    valid

end


(*----------------------------------------------------------------------------*)

module Bounds = struct 
  type t = 
    {  left   : int;
       right  : int;
       top    : int;
       bottom : int;
    }
  let init_bounds = 
    { left=max_int; right=min_int; top=max_int; bottom=min_int }
  let expand bounds1 bounds2 =
    { left   = min bounds1.left   bounds2.left;
      right  = max bounds1.right  bounds2.right;
      top    = min bounds1.top    bounds2.top;
      bottom = max bounds1.bottom bounds2.bottom;
    }
  let left { left; _ } = left
  let right { right; _ } = right
  let top { top; _ } = top
  let bottom { bottom; _ } = bottom
  let height { top; bottom; _ } = bottom - top
  let width {left; right; _} = right - left 
  
  class layout_bounds =
    object (self)
    inherit [t] walker init_bounds ~concat:expand 
    
    method! visit_xy { XY.x; y } =
      { left = x; right = x; top = y; bottom = y; }  
  end 
end

