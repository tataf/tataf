;;
;; Copyright (C) 2021 University of Freiburg
;;
;; This file is part of tataf.
;;
;; Tataf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; Tataf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with tataf.  If not, see <https://www.gnu.org/licenses/>.
;;

; stanza to define compilation & linking features of the library
; generate library by: $ dune build
;
(library
 (public_name tataf_libta)
; (wrapped false)
;   remove ';' on prev. line if wanting to disable type-level module aliasing
;   when compiling this library
 (libraries
  str xml-light unix
  tataf_libcore)
 (modules
  (:standard \ benchmarkTransformers tataf_utop))
 (modes
    byte native)
 (flags -annot -g -w @7@9@40-23 -bin-annot)
 (library_flags -g))

; customised stanza to process mly file within the sources
;
(rule
 (targets uppaalXtaParser.ml uppaalXtaParser.mli)
 (deps
  (:mly uppaalXtaParser.mly)
  (:extra uppaalXtaParser.extra.mli))
 (action
  (progn
   (chdir %{workspace_root}
    (run %{bin:ocamlyacc} %{mly}))
   (system "cat %{extra} >> uppaalXtaParser.mli"))))

; stanza to process mll file within the sources
;
(ocamllex uppaalXtaLexer)

; stanza to generate library-version-ml file via a make call
; is run automatically when doing: $ dune build
;
(rule
 (targets libtaVersion.ml)
 (deps
  (:mk Makefile Makefile.tataf)
  (:in libtaVersion.ml.in))
 (action
  (run make -s update_version_info OMIT_OMK=y)))

;TODO:@gen_doc, should find a way to exclude:
;benchmarkTransformers.ml
;uppaalXtaLexer.ml
;uppaalXtaParser.ml{,i}

; following two stanzas generate a utop executable by doing:
; $ dune build @tataf_utop
;
(executable
 (name tataf_utop)
 (modules tataf_utop)
 (link_flags -linkall -custom)
 (modes byte)
 (libraries utop tataf_libta))

(alias
 (name   tataf_utop)
 (deps   tataf_utop.bc))

