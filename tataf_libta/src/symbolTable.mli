(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
This module defines the symbol table for Uppaal.

Each scope in the Uppaal grammar creates a new {e frame}. A frame has a 
pointer to the parent scope's frame, or is None if this is the frame for the 
global scope. The {e frame_type} of a frame describes the reason, why the scope
was created. A single frame contains a mapping of ids ([UppaalSystem.ID.t]) to 
symbols ([symbol]). A symbol may describe a variable declaration ([S_Variable]),
a typedef ([S_Typedef]), a function declaration ([S_Function]) or a template
declaration ([S_Template]). Each symbol describes the signature of the represented
type. Additionally [S_Template] holds the frame, which was created for the
template. 

We call the composition of one or many frames a [symbol_table]. A symbol table
and a frame has nearly the same signature. If you wish to use a frame in 
the context of a symbol_table it may be necessary to coerce the type. This is done
with the coercion operator:
{[
  let symbol_table = (frame :> SymbolTable.t)  
]} 

{3 Example}
{[
  None
    ^
    |
  +-----+--------------------+ FRAME            \
  | a   | SVariable(Bool)    | (FT_NTA)         |
  | b   | SVariable(Clock)   |                  |
  | c   | STemplate(..., * ) |                  |
  +-----+----------------|---+                  |
       ^                 |                      | SYMBOL TABLE
       | parent          V                      | composition of frames
    +-----+--------------------+ FRAME          | 
    | d   | SVariable(Bool)    | (FT_Template)  |
    | f   | SFunction(...)     |                |
    | b   | SFunction(...)     |                |
    +-----+--------------------+                /
]}

In this example we have three declarations in the global scope:
- A variable of type [bool] with name "a".
- A variable of type [clock] with name "b".
- A template with name "c".

The template "c" contains itself two {e local} declarations: 
- A variable of type [bool] with name "d".
- A function with name "f".
- A function with name "b". This declaration hides the variable declaration in 
  the global scope!

{3 Resolving symbols}

To resolve a name only within a given frame, use the [resolve]-method of this 
frame. This will return None, if no mapping can be found, or Some symbol otherwise:
{[
  frame#resolve name  
]}

Normally you want to resolve a name searching not only within a frame, but the symbol
table. To do this, use the {!section:section_fun_resolve_symbols},
defined directly within this module. Example:
{[
  SymbolTable.resolve frame name
]}

{3 Creating and working with symbol tables }
To create and work with symbol tables you can use the {!frame_worker}-Mixin. This
class implements some helper functions to push/pop frames onto the symbol table
and work with scopes.

The {!UppaalWalker} base class includes this mixin. That means: currently each 
UppaalWalker is constructing a symbol table on traversal!

*)

(*----------------------------------------------------------------------------*)
(* this file has been generated with 'ocamlc -i' to be used by odoc(1) *)
(*----------------------------------------------------------------------------*)
(**
{2 Types}
*)

(** Interface for a symbol table. *)
type t =
    < fold_left : 'a.
                    ('a -> UppaalSystem.ID.t * symbol -> 'a) ->
                    'a -> 'a;
      frame_type : frame_type;
      get_initializer : UppaalSystem.ID.t ->
                        UppaalSystem.Initializer.t option;
      iter : (UppaalSystem.ID.t * symbol -> unit) -> unit;
      parent : t option;
      resolve : UppaalSystem.ID.t -> symbol option >

(** Type for symbols within a frame. *)
and symbol =
    S_Typedef of UppaalSystem.Type.t
    (** Typedef-symbol. The type is the type of the custom type. *)
  | S_Variable of UppaalSystem.Type.t
    (** Variable-symbol. The Type is the type of the variable. *)
  | S_Function of UppaalSystem.Type.t *
      UppaalSystem.Parameter.t list
    (** Function-symbol. First type is return type of the function.
        second parameter defines the types of the function's parameters. *)
  | S_Template of UppaalSystem.Parameter.t list * t
    (** Template-symbol. The first parameter are the types of the template's
        parameters. The second parameter is a reference to the local frame of
        the template.*)

(**
  Type of a frame. Specifies for what scope a frame was created.
*)
and frame_type =
    FT_Unspecified
    (** Used only during initialization. *)
  | FT_NTA of UppaalSystem.NTA.t
    (** Created for the global scope of an NTA. Contains global declarations of 
        variables, functions, templates and the instantiations. *) 
  | FT_Template of UppaalSystem.Template.t
    (** Created for each template. Contains template declarations *)
  | FT_Function of UppaalSystem.Declaration.t
    (** Created for function declarations (global or in template declarations). *)
  | FT_Block of UppaalSystem.Statement.t (** Block statement *)
    (** Created for anonymous statement blocks [\{ ... \}] (possible only in 
        functions). *)
  | FT_Foreach of UppaalSystem.Statement.t
    (** Created for a foreach-statement (possible only in functions). *)
  | FT_Forall of UppaalSystem.Expression.t
    (** Created for forall-expressions, because the [ID] is only valid within
        the forall-scope ([ forall (ID : Type) Expression ]). *)
  | FT_Exists of UppaalSystem.Expression.t
    (** Created for exists-expressions, because the [ID] is only valid within
        the exists-scope ([ exists (ID : Type) Expression ]). *)
  | FT_Edge of UppaalSystem.Edge.t
    (** Created for edges. This is necessary, because the select-expression
        of edges may declare new bindings! *)
  | FT_Instantiation of UppaalSystem.Instantiation.t

(**
  This class is a concrete implementation of the type [SymbolTable.t]. Representing
  only a single frame, this class has additionally methods for frame construction
  ([add name symbol]). If you want to use a frame, when a SymbolTable.t is 
  expected, it may be necessary to coerce the type. (for more info, see the 
  beginning of this module). 
*)
class frame :
  ?frame_type:frame_type ->
  frame option ->
  object ('a)
    val frame_type : frame_type
    val initializers :
      (UppaalSystem.ID.t,
       UppaalSystem.Initializer.t option)
      Hashtbl.t
    val parent : frame option
    val symbols : (UppaalSystem.ID.t * symbol) list
    method add : UppaalSystem.ID.t -> symbol -> 'a
    method add_initializer :
      UppaalSystem.ID.t ->
      UppaalSystem.Initializer.t option -> unit
    method fold_left : ('b -> string * symbol -> 'b) -> 'b -> 'b
    method frame_type : frame_type
    method get_initializer :
      UppaalSystem.ID.t ->
      UppaalSystem.Initializer.t option
    method iter : (UppaalSystem.ID.t * symbol -> unit) -> unit
    method parent : frame option
    method resolve : UppaalSystem.ID.t -> symbol option
    method symbolshack : (UppaalSystem.ID.t * symbol) list
  end

(** 
  Mixin, to create and work with a symbol table. The {!UppaalWalker} are using
  this mixin.
*)
class virtual frame_worker :
  object
    val mutable frame : frame
    method add_initializer :
      UppaalSystem.ID.t ->
      UppaalSystem.Initializer.t option -> unit
    method add_symbol : UppaalSystem.ID.t -> symbol -> 'a -> 'a
    method frame : frame
    method frame_pop : 'a -> 'a
    method frame_push : frame_type:frame_type -> unit
    method frame_set : frame -> unit
    method scope : frame_type -> (unit -> 'a) -> 'a
  end

(**
  {2:section_fun_resolve_symbols Functions to resolve symbols}
  
  These functions operate on a symbol table and will always look up the parent
  frame, if no mapping can be found within the frame. As first parameter you can
  pass symbol tables or frames.
*)

val get_initializer :
  (< get_initializer : UppaalSystem.ID.t ->
                       UppaalSystem.Initializer.t option;
     parent : 'a option; .. >
   as 'a) ->
  UppaalSystem.ID.t ->
  UppaalSystem.Initializer.t option

(** 
  [resolve frame name] resolves the given [name] to Some symbol (if a mapping is
  found), or None. The search will lookup in the parent frames, if no mapping can
  be found within the current frame. If you wish to lookup only within the current
  frame, use the [frame#resolve] method of a frame.
*)
val resolve :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> symbol option
val resolve_type :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.Type.t -> UppaalSystem.Type.t option

(** 
  [resolve_to_template_qualified frame name] can be seen as "fully qualifying"
  a given non-qualified name.  E.g. if the current frame is for template T
  which does not have a symbol y but y is available in global scope, then we
  will obtain (None,y).  If T has a (possibly) overriding y, then we will
  obtain (T,y).  Symbols in frame types other than NTA, i.e. global, or
  Template are not qualifyable, i.e. we will obtain None.
*)
val resolve_to_template_qualified :
  (< frame_type : frame_type; parent : 'a option;
     resolve : UppaalSystem.ID.t -> 'b option; .. >
   as 'a) ->
  UppaalSystem.ID.t ->
  UppaalQeClasses.TemplateQualifiedID.t option

(**
  [resolve_template_qualified global_frame tqid] resolves the given [tqid] to
  Some symbol (if a mapping is found), or None.  If [tqid] does not comprise a
  template component T, its name part is directly looked up in [global_frame],
  otherwise the name part is looked up in the frame of the template denoted by
  T.
  Note that [global_frame] must be the top-most frame, because templates live there,
  as obtained via, e.g., walker#global_frame (after traversing!).
*)
val resolve_template_qualified :
  < resolve : UppaalSystem.ID.t -> symbol option; .. > ->
  UppaalQeClasses.TemplateQualifiedID.t -> symbol option
val is_int_ref :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool
val is_bool_ref :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool
val is_clock_ref :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool

(**
  [is_clock frame name] returns [true], if [name] maps to a variable of type 
  [Clock], otherwise returns [false].
*)
val is_clock :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool

(**
 * [is_bool frame name] returns [true], if [name] maps to a variable of type
 * [Bool], otherwise returns [false].
 *)
val is_bool :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool

(**
 * [is_int frame name] returns [true], if [name] maps to a variable of type
 * [Int], otherwise returns [false].
 *)
val is_int :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool

(**
  [is_const frame name] returns [true], if [name] maps to a constant variable,
  otherwise returns [false]. 
*)
val is_const :
  (< parent : 'a option;
     resolve : UppaalSystem.ID.t -> symbol option; .. >
   as 'a) ->
  UppaalSystem.ID.t -> bool

(**
{2 String Conversion}
*)

val string_of_frame_type : frame_type -> string
val string_of_symbol_table :
  < visit_symbol : symbol -> string; .. > -> t -> string

