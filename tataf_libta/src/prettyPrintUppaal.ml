(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open UppaalSystem

(** 
  Module specific debugger.
*)
let dbg = Tataf_libcore.Debug.register_debug_module "PrettyPrintUppaal"

let pp = (new UppaalPrint.xta4_printer) 

(*----------------------------------------------------------------------------*)
(**
  Pretty print an Uppaal file.
*)
let ppuppaal filename =
  dbg#ldebug_endline 1 "--------------------";  
  dbg#ldebug_endline 1  "Prettyprint Uppaal";
  dbg#ldebug_endline 1  ("  Input: "  ^ filename);
  dbg#ldebug_endline 1  "--------------------";
  
  (* parsing *)  
  dbg#ldebug 2 "parsing ...";
  let omit_xta = !LibtaOptions.ppuppaal_onlyxml in
  let upxmlSystem = UppaalParser.parse_xta4_file ~omit_xta:omit_xta filename in 
  dbg#ldebug_endline 2 "ok";  

  (* pretty printing *)
  let pretty = 
    if !LibtaOptions.ppuppaal_symbol_table 
    then (new UppaalPrint.symbol_table_printer)#visit_nta upxmlSystem
    else (
      (* Since this cannot be done using optional constructor arguments in OCAML
       * and moving this into the constructor as non-optional argument would cause 
       * a lot of conflicts, we currently use this setter to enable word-wrapping
       * for the xta4 printer *)
      pp#set_linewidth !LibtaOptions.ppuppaal_xta4_linewidth;
      pp#visit_nta upxmlSystem )
  in 
  Tataf_libcore.ExtString.string_to_file !LibtaOptions.ppuppaal_out pretty
;;
