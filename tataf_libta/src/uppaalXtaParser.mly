/*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 */
%{
  open UppaalSystem
  open UppaalSystem.Expression
  
  (**
    will be thrown on knwon errors within the grammar
    [Specific_Parse_error(start,end,message)]
  *)
  open Parsing
  open SymbolTable
  open UppaalQeClasses

  exception Specific_Parse_error of Lexing.position * Lexing.position * string
  
  let symbol_table = ref (new frame None)
  
  let add_symbol name symbol = 
    symbol_table := !symbol_table#add name symbol
  
%}

%token NEWLINE
%token <char> CHAR /* single character for comment parsing. */
%token EOF

%token DOT
%token COMMA
%token SEMICOLON
%token COLON
%token LBRACE RBRACE      /* { } */
%token LBRACKET RBRACKET  /* [ ] */
%token LPAREN RPAREN      /* ( ) */
%token QMARK              /* ?   */
%token APOS               /* '   */

/* Assignments: */
%token T_ASSIGNMENT T_ASSPLUS 
%token T_ASSMINUS T_ASSMULT T_ASSDIV T_ASSMOD 
%token T_ASSOR T_ASSAND T_ASSXOR 
%token T_ASSLSHIFT T_ASSRSHIFT

/* Unary operations: */
%token T_EXCLAM

%token T_INCREMENT T_DECREMENT

/* Binary operations: */
%token T_PLUS T_MINUS T_MULT T_DIV  T_INF T_SUP
%token T_MOD T_OR T_AND T_XOR T_LSHIFT T_RSHIFT 
%token T_BOOL_AND T_BOOL_OR 
%token T_KW_AND T_KW_OR T_KW_IMPLY T_KW_NOT

/* Quantifiers */
%token T_FORALL T_EXISTS

/* Relation operations:*/
%token T_LT T_LEQ T_EQ T_NEQ T_GEQ T_GT

/* Special statement keywords: */
%token T_FOR T_WHILE T_DO T_BREAK T_CONTINUE T_SWITCH T_IF T_ELSE 
%token T_CASE T_DEFAULT T_RETURN T_ASSERT
%token T_PRIORITY

/* Variable type declaration keywords: */
%token T_TYPEDEF T_STRUCT 
%token T_CONST T_OLDCONST T_URGENT T_BROADCAST T_TRUE T_FALSE T_META

/* Uppaal keywords */
%token T_SYSTEM T_PROCESS T_STATE T_COMMIT T_INIT T_TRANS T_SELECT
%token T_GUARD T_SYNC T_ASSIGN T_BEFORE T_AFTER T_PROGRESS
%token T_ARROW T_UNCONTROL_ARROW

/* Property tokens */
%token T_A T_U T_W
%token T_DEADLOCK T_EF T_EG T_AF T_AG T_LEADSTO T_RESULTSET
%token T_EF2 T_EG2 T_AF2 T_AG2

/* Control Synthesis */
%token T_CONTROL T_MIN_START T_MAX_START T_CONTROL_T

/* Miscelanious: */
%token T_ERROR 
%token <string> T_ID T_TYPENAME
%token <int> T_NAT

/* Types */
%token T_BOOL T_INT T_CHAN T_CLOCK T_VOID T_SCALAR

/* Syntax switch tokens */
%token T_NEW T_NEW_DECLARATION T_NEW_LOCAL_DECL T_NEW_INST T_NEW_SYSTEM 
%token T_NEW_PARAMETERS T_NEW_INVARIANT T_NEW_GUARD T_NEW_SYNC T_NEW_ASSIGN
%token T_NEW_SELECT
%token T_OLD T_OLD_DECLARATION T_OLD_LOCAL_DECL T_OLD_INST 
%token T_OLD_PARAMETERS T_OLD_INVARIANT T_OLD_GUARD T_OLD_ASSIGN
%token T_PROPERTY T_EXPRESSION 

%token T_MIN T_MAX /* ?? */

%left T_LEADSTO
%right T_AF T_AG T_EF T_EG T_A
%right T_AF2 T_AG2 T_EF2 T_EG2
%left  T_U T_W
%right T_FORALL T_EXISTS
%left T_KW_OR T_KW_IMPLY
%left T_KW_AND
%right T_KW_NOT
%right T_ASSIGNMENT T_ASSPLUS T_ASSMINUS T_ASSMULT T_ASSDIV T_ASSMOD T_ASSAND T_ASSOR T_ASSLSHIFT T_ASSRSHIFT T_ASSXOR
%right QMARK COLON
%left T_BOOL_OR
%left T_BOOL_AND 
%left T_OR
%left T_XOR
%left T_AND
%left T_EQ T_NEQ 
%left T_LT T_LEQ T_GEQ T_GT
%left T_MIN T_MAX
%left T_LSHIFT T_RSHIFT
%left T_MINUS T_PLUS
%left T_MULT T_DIV T_MOD
%right T_EXCLAM T_INCREMENT T_DECREMENT UOPERATOR
%left LPAREN RPAREN LBRACKET RBRACKET DOT APOS

/*
 start rules for XTA parsing
 */

%start expression
%type <UppaalSystem.Expression.t> expression

%type <UppaalSystem.Expression.t list> exprList


/*
 special start rules for Xml parsing 
 */


/* XML <LABEL kind="invariant"> */
%start invariantXml
%type <UppaalSystem.Invariant.t option> invariantXml

/* XML <LABEL kind="select"> */
%start selectXml
%type <UppaalSystem.Select.t> selectXml

/* XML <LABEL kind="guard"> */
%start guardXml
%type <UppaalSystem.Guard.t option> guardXml

/* XML <LABEL kind="synchronisation"> */
%start syncXml
%type <UppaalSystem.Sync.t option> syncXml

/* XML <LABEL kind="assignment> */
%start assignXml
%type <UppaalSystem.Update.t> assignXml

/* XML <parameter> */
%start parameterXml
/*%type <UppaalSystem.Parameter.single list> parameterXml*/
%type <UppaalSystem.Parameter.t list> parameterXml

/* XML <declarations> */
%start declarationsXml
%type <UppaalSystem.Declaration.t list> declarationsXml

%start systemXml
%type <UppaalSystem.System.t> systemXml


%start queries
%type <UppaalSystem.Queries.t> queries

%start query
%type <UppaalSystem.Expression.t> query

%start qeClasses
%type <(UppaalSystem.ID.t * UppaalQeClasses.TemplateQualifiedIDSet.t * UppaalQeClasses.TemplateQualifiedIDSet.t) list> qeClasses

/*----------------------
  temporary helper 
*/
/*%type <UppaalSystem.upType> aType*/

%%

/************************************************
  Custom Parsing Helper 
 ************************************************/

qeClasses:
    qeClass { [$1] }
  | qeClasses COMMA qeClass { $1 @ [$3] }
;

qeClass:
    nonTypeId T_ASSIGNMENT qeClocks qePrePostDelaySet
      { ($1, $3, $4) }
;

qeClocks:
    LBRACE TemplateQualifiedIdList RBRACE { $2 }
;

qePrePostDelaySet:
    /*empty*/ { TemplateQualifiedIDSet.empty }
  | LBRACKET TemplateQualifiedIdList RBRACKET { $2 } 
;

TemplateQualifiedIdList:
    TemplateQualifiedId                               { TemplateQualifiedIDSet.singleton $1 }
  | TemplateQualifiedIdList COMMA TemplateQualifiedId { TemplateQualifiedIDSet.add $3 $1 }
;

TemplateQualifiedId:
    nonTypeId DOT nonTypeId { { TemplateQualifiedID.template = Some ( $1 ); name = $3 } }
  | nonTypeId               { { TemplateQualifiedID.template = None;        name = $1 } }

/************************************************
  XML Parsing 
 ************************************************/
invariantXml: /*USED*/
    /*empty*/ { None }
  | expression { Some (Invariant.make ~xy:None ~expr:$1) }
;

selectXml:
    /*empty*/ 
      { Select.make ~sl:[] }
  | id COLON aType
      { Select.make ~sl:[( $1, $3 )] }
  | selectXml COMMA id COLON aType 
      { { $1 with Select.sl = $1.Select.sl @ [( $3, $5 )] } }

guardXml: /*USED*/
    /*empty*/ { None }
  | expression { Some (Guard.make $1) }
;

syncXml: /*USED*/
    /*empty*/ { None }
  | expression T_EXCLAM { 
      Some (Sync.make ~xy:None
                      ~marker:Sync.Broadcast
                      ~expr:$1) 
    }
  | expression QMARK { 
      Some (Sync.make ~xy:None
                      ~marker:Sync.Synchronize
                      ~expr:$1)
    }
;

assignXml: /*USED*/
    /*empty*/ { Update.make ~xy:None ~el:[] }
  | exprList { Update.make ~xy:None ~el:$1 }
;

parameterXml:
    parameterList { List.rev $1 }
;

declarationsXml: /*USED*/
    declarations { (*List.rev*) $1 } 
;

systemXml: /*USED*/
    xta 
    {
      let declarations, processes, progresses = $1 in
      { System.declarations; processes; progresses } 
      (*System.System (decll, processl, progressl)*) 
    }
;

/************************************************
  Query file parsing
 ************************************************/

queries:
    queryList queryFileEnd { { Queries.empty with Queries.queries = List.rev $1 } }
;

queryList:
    /*empty*/ { [] }
  | query { [$1] }
  | queryList newlines query { $3 :: $1  }
;

query:
    property { $1 }
;

queryFileEnd:
    /*empty*/ {}
  | newlines {}
  | newlines EOF {}
  | EOF {}
;

/*minimum one newline is necessary to seperate properties*/
newlines: /* --> Buffer.t */
    NEWLINE { }
  | newlines NEWLINE { }
;

/************************************************
  ...
 ************************************************/

xta: /*USED*/
    declarations system 
    {
      let processl, progressl = $2 in 
      ( $1, processl, progressl )
    }
;

instantiation: /*NEW --> Instantiation.t */
    nonTypeId optionalInstanceParameterList T_ASSIGNMENT nonTypeId 
      LPAREN argList RPAREN SEMICOLON 
    {
      let open Instantiation in
      { Instantiation.instance_id = $1;
                      instance_parameters = $2;
                      template_id = $4;
                      template_arguments = $6 }
    }
;

optionalInstanceParameterList: /*NEW --> Declaration.parameter list*/
    /*empty*/ { [] }
  | LPAREN RPAREN { [] }
  | LPAREN parameterList RPAREN { List.rev $2 }
;

system: /* USED --> (Process.t list, Progres.t list) */
    sysDecl progress { ($1, $2) }
;

priorityDecl: /*NEW*/
    T_CHAN T_PRIORITY channelList SEMICOLON 
      { Declaration.ChanPriority (List.rev $3) }
/*| T_CHAN T_PRIORITY error SEMICOLON {}*/
;

channelList: /*NEW --> Declaration.chanElement list list - outer REVERSED*/
    chanElement { [[$1]] }
  | channelList COMMA chanElement {
      (* same sublist *)
      match $1 with
      | hl :: tl -> (hl @ [$3]) :: tl
      | _ -> assert false
    }
  | channelList T_LT chanElement {
      (* open new sublist *)
      [$3] :: $1
    }
;

chanElement: /*NEW --> Declaration.chanElement */
    chanExpression { Declaration.Channel(fst $1, snd $1 |> List.rev) }
  | T_DEFAULT { Declaration.DefaultPriority }
;

chanExpression: /*NEW --> (ID.t, Expression.t list REVERSED) */
    nonTypeId { ($1, []) }
  | chanExpression LBRACKET expression RBRACKET { (fst $1, $3 :: snd $1) }
;

sysDecl: /* USED --> Process.t list */
    T_SYSTEM processList SEMICOLON 
    { 
      let current_priority = ref 0 in
      List.map
        ( fun (addend, name) ->
          current_priority := !current_priority + addend;
          Process.make ~priority:(!current_priority) ~name ) 
        $2
    }
;

processList: /* USED --> (priority_addend, proc_id) */
    nonTypeId { [0, $1] }
  | processList COMMA nonTypeId { $1 @ [0, $3] }
  | processList T_LT nonTypeId { $1 @ [1, $3] }
;

progress: /* USED --> Progress.t list */
    /*empty*/ { [] }
  | T_PROGRESS LBRACE progressMeasureList RBRACE { List.rev $3 }
;

progressMeasureList: /*USED*/
    /*empty*/ { [] }
  | progressMeasureList expression COLON expression SEMICOLON 
    { let guard = Some { Guard.default with Guard.expr = $4 } in
      (Progress.make ~guard ~measure:$2) :: $1 } 
  | progressMeasureList expression SEMICOLON 
    { let guard = None in
      (Progress.make ~guard ~measure:$2) :: $1 }
;

declarations:  /* USED --> Declaration.t list*/
    /*empty*/ { [] }
  | declarations functionDecl { $1 @ [$2] }
  | declarations variableDecl { $1 @ $2 }
  | declarations typeDecl { $1 @ $2 }
/*| declarations procDecl {}*//*SEEMS NOT TO BE NECESSARY FOR XML PARSING*/
  | declarations beforeUpdateDecl { $1 @ [$2] }
  | declarations afterUpdateDecl { $1 @ [$2] }
  | declarations instantiation { $1 @ [Declaration.Instantiation $2] }
  | declarations priorityDecl { $1 @ [$2] }
/*| error SEMICOLON { }*/
;  

beforeUpdateDecl: /* --> Declaration.t */
    T_BEFORE LBRACE exprList RBRACE { Declaration.BeforeUpdate $3 }
;

afterUpdateDecl: /* --> Declaration.t */
    T_AFTER LBRACE exprList RBRACE { Declaration.AfterUpdate $3 }
;

functionDecl: /*NEW*/
    aType id optionalParameterList LBRACE blockLocalDeclList statementList RBRACE 
    { Declaration.Function ($1, $2, $3, $5, $6) }
;

optionalParameterList: /*NEW* --> Declaration.parameter list */
    LPAREN RPAREN { [] }
  | LPAREN parameterList RPAREN { List.rev $2 }
/*| LPAREN error RPAREN {}*/
;

parameterList: /*NEW --> Declaration.parameter list (REVERSED ORDER)*/
    parameter { [$1] }
  | parameterList COMMA parameter { $3 :: $1 }
;

parameter: /*NEW --> Declaration.parameter (Type.t * ID.t) */
    aType T_AND nonTypeId arrayDecl 
    {
      match $4 with
      | [] -> { Parameter.typ = Type.Ref $1; name = $3}  (*(Type.Ref $1, $3)*)
      | rl -> { Parameter.typ = Type.Ref (Type.Range ($1, rl)); name = $3 } (*(Type.Ref (Type.Range ($1, rl)), $3)*)
    }
  | aType nonTypeId arrayDecl 
    {  
      match $3 with
      | [] -> { Parameter.typ = $1; name = $2} (*($1, $2)*)
      | rl -> { Parameter.typ = Type.Range ($1, rl); name = $2 } (*(Type.Range ($1, rl), $2)*)
    }
;

variableDecl: /* --> Declaration.t list */
    aType declIdList SEMICOLON 
    { List.map (
      fun (id, ranges, init) -> match ranges with
      | [] -> Declaration.Variable ($1, id, init)
      | rl -> Declaration.Variable (Type.Range ($1, rl), id, init)
      ) (List.rev $2)
    }
;

declIdList: /* --> (ID.t, Type.range list, Initializer.t) list REVERSED */
    declId { [$1] }
  | declIdList COMMA declId { $3 :: $1 }
;

declId: /* --> (id : ID.t, ranges : Type.range list, init : Initializer.t) */
    id arrayDecl varInit { ($1, List.rev $2, $3) }
;

varInit: /* --> Initializer.t */
    /*empty*/ { None }
  | T_ASSIGNMENT aInitializer { Some $2 }
;

aInitializer: 
    expression { Initializer.Expression $1 }
  | LBRACE fieldInitList RBRACE { Initializer.Fields (List.rev $2) }
;

fieldInitList: 
    fieldInit { [$1] }
  | fieldInitList COMMA fieldInit { $3 :: $1 }
;

fieldInit: 
    id COLON aInitializer { (Some $1, $3) }
  | aInitializer { (None, $1) }
;

arrayDecl: /* Type.range list */
    /*empty*/ { [] }
  | LBRACKET expression RBRACKET arrayDecl { (Type.RangeExpression $2) :: $4 }
  | LBRACKET aType RBRACKET arrayDecl { (Type.RangeType $2) :: $4 }
  | LBRACKET error RBRACKET arrayDecl { 
      let start_pos = Parsing.rhs_end_pos 1 in
      let end_pos = Parsing.rhs_start_pos 3 in
      raise (Specific_Parse_error (start_pos, end_pos, "Invalid array content, must be type or expression."))
    }
;

typeDecl: /*NEW --> Declaration.t list */
    T_TYPEDEF aType typeIdList SEMICOLON 
    { let tdecl = List.map (
      fun (id, ranges) -> match ranges with
      | [] -> add_symbol id (S_Typedef $2); Declaration.Typedef ($2, id)
      | rl -> let tp = Type.Range ($2, rl) in
              add_symbol id (S_Typedef tp); Declaration.Typedef (tp, id)
      ) $3 in
      tdecl
    }
/*| T_TYPEDEF error SEMICOLON {}*/
;

typeIdList: /*NEW --> (ID.t, Type.range list) */
    typeId { [$1] }
  | typeIdList COMMA typeId { $1 @ [$3] }
;

typeId: /*NEW --> (ID.t, Type.range list) */
    id arrayDecl { ($1, List.rev $2) } 
;

aType: 
    T_TYPENAME { Type.Typename ([], $1) } 
  | typePrefix T_TYPENAME { Type.Typename ($1, $2) } 
  | T_STRUCT LBRACE fieldDeclList RBRACE { Type.Struct ([], $3) }
  | typePrefix T_STRUCT LBRACE fieldDeclList RBRACE { Type.Struct ($1, $4) }
/*| T_STRUCT LBRACE error RBRACE {} */
/*| typePrefix T_STRUCT LBRACE error RBRACE {} */
  | T_BOOL { Type.Bool [] }
  | typePrefix T_BOOL { Type.Bool $1 }
  | T_INT { Type.unranged_int [] }
  | typePrefix T_INT { Type.unranged_int $1 }
  | T_INT LBRACKET expression COMMA expression RBRACKET 
    { Type.Int ([], $3, $5) }
  | typePrefix T_INT LBRACKET expression COMMA expression RBRACKET 
    { Type.Int ($1, $4, $6) }
  | T_CHAN { Type.Chan [] }
  | typePrefix T_CHAN { Type.Chan $1 }
  | T_CLOCK { Type.Clock }
  | T_VOID { Type.Void }
  | T_SCALAR LBRACKET expression RBRACKET { Type.Scalar ([], $3) }
  | typePrefix T_SCALAR LBRACKET expression RBRACKET { Type.Scalar ($1, $4) }
;

typePrefix:
    T_URGENT { [Type.Urgent] }
  | T_BROADCAST { [Type.Broadcast] }
  | T_URGENT T_BROADCAST { [Type.Urgent; Type.Broadcast] }
  | T_CONST { [Type.Const] }
  | T_META { [Type.Meta] }
;

id: /*USED*/
    nonTypeId { $1 }
  | T_TYPENAME { $1 }
;

nonTypeId: /*USED*/
    T_ID { $1 }
  | T_A { "A" }
  | T_U { "U" }
  | T_W { "W" }
  | T_INF { "inf" }
  | T_SUP { "sup" }
;

fieldDeclList: /*NEW --> (type, id) list */
    fieldDecl { $1 }
  | fieldDeclList fieldDecl { $1 @ $2 }
;

fieldDecl: /*NEW --> (type, id) list */
    aType fieldDeclIdList SEMICOLON
    { List.map (
      fun (id, ranges) -> match ranges with
      | [] -> ($1, id)
      | rl -> (Type.Range ($1, rl), id)
      ) $2
    }
;


fieldDeclIdList: /*NEW* --> (id, range) list REVERSED */
    fieldDeclId { [$1] }
  | fieldDeclIdList COMMA fieldDeclId { $3 :: $1 }
;

fieldDeclId: /*NEW --> (id, range) */
    id arrayDecl { ($1, $2) }
;

/************************************************
  Process Declaration
 ************************************************/

/*
select:
    /* empty * / { ... }
  | T_SELECT selectList SEMICOLON { ... }
;

selectXml:
    selectList { $1 }
;

selectList:
    id COLON uType { ... }
  | selectList COMMA id COLON uType { ... }
;
*/

/************************************************
  Uppaal C Grammar
 ************************************************/

block:
  LBRACE blockLocalDeclList statementList RBRACE { Statement.Block ($2, $3) }
;

blockLocalDeclList: /*NEW --> Declaration.t list */
    /* empty */ { [] }
  | blockLocalDeclList variableDecl { $1 @ $2 }
  | blockLocalDeclList typeDecl { $1 @ $2  }
;

statementList:
    /* empty */ { [] }
  | statementList statement { $1 @ [$2] }
/*| statementList error { ??? }*/
;

statement:
    block { $1 }
  | SEMICOLON { Statement.Empty }
  | expression SEMICOLON { Statement.Expression $1 }
  | T_FOR LPAREN exprList SEMICOLON exprList SEMICOLON exprList RPAREN statement
      { Statement.For ($3, $5, $7, $9) }
  | T_FOR LPAREN T_ID COLON aType RPAREN statement 
      { Statement.Foreach ($3, $5, $7) }
/*| T_FOR LPAREN error RPAREN statement { ??? }*/
  | T_WHILE LPAREN exprList RPAREN statement { Statement.While ($3, $5) }
/*| T_WHILE LPAREN error RPAREN statement { ??? }*/
  | T_DO statement T_WHILE LPAREN exprList RPAREN SEMICOLON { Statement.Do ($5, $2) }
  | T_IF LPAREN exprList RPAREN statement elsePart 
      { Statement.If ($3, $5, $6) }
  | T_BREAK SEMICOLON { Statement.Break }
  | T_CONTINUE SEMICOLON { Statement.Continue }
  | T_SWITCH LPAREN exprList RPAREN LBRACE switchCaseList RBRACE 
      { Statement.Switch ($3, List.rev $6) }
  | T_RETURN expression SEMICOLON { Statement.Return (Some $2) }
  | T_RETURN SEMICOLON { Statement.Return None }
  | T_ASSERT expression SEMICOLON { Statement.Assert $2 }
;

elsePart:
    /* empty */ { None }
  | T_ELSE statement { Some $2 }
;

switchCaseList: /* case list REVERSED */
    switchCase { [$1] }
  | switchCaseList switchCase { $2 :: $1 }
;

switchCase:
    T_CASE expression COLON statementList { Statement.Case ($2, $4) }
/*| T_CASE error COLON statementList*/
  | T_DEFAULT COLON statementList { Statement.Default $3 }
;

exprList: /*USED*/
    expression { [$1] }
  | exprList COMMA expression { $1 @ [$3] }
;

expression: /*USED*/
    T_FALSE   { Bool false }
  | T_TRUE    { Bool true }
  | T_NAT     { Num $1 }
  | nonTypeId { Var $1 }
  | expression LPAREN argList RPAREN { Call ($1, $3) }
  | expression LPAREN error RPAREN { 
      raise (Specific_Parse_error (rhs_start_pos 3, rhs_end_pos 3,
          "Invalid argument in function call (must be expression).")) }
  | expression LBRACKET expression RBRACKET { Array ($1, $3) }
  | expression LBRACKET error RBRACKET {
      raise (Specific_Parse_error (rhs_start_pos 3, rhs_end_pos 3,
          "Invalid expression as array index.")) }
  | LPAREN expression RPAREN { Parenthesis $2 }
  | LPAREN error RPAREN { 
      raise (Specific_Parse_error (rhs_start_pos 2, rhs_end_pos 2,
          "Invalid expression within parenthesis.")) }
  | expression T_INCREMENT { IncPost $1 }
  | T_INCREMENT expression { IncPre $2 }
  | expression T_DECREMENT { DecPost $1 }
  | T_DECREMENT expression { DecPre $2 }
/*
  here below, we have to deal with the common problem of treating unary '-'
  right; e.g. 1 + -2 + 3 has to be parsed as +( 1, +( -(2), 3 )) and not
  as +( 1, -( +( 2, 3 ))); we use the common solution of giving an explicit
  precedence; we use UOPERATOR as precedence since this one is also used in
  the libutap-parser (cf. resources/uppaal/libutap-0.91/src/parser.yy)
*/
  | unaryOp expression %prec UOPERATOR { Unary($1, $2) }

  | expression T_LT expression  { Relation ($1, Lt, $3) }
  | expression T_LEQ expression { Relation ($1, LtEq, $3) }
  | expression T_EQ expression  { Relation ($1, Eq, $3) }
  | expression T_NEQ expression { Relation ($1, NotEq, $3) }
  | expression T_GT expression  { Relation ($1, Gt, $3) }
  | expression T_GEQ expression { Relation ($1, GtEq, $3) }

  | expression T_PLUS expression     { IntOperation ($1, Plus, $3) }
  | expression T_MINUS expression    { IntOperation ($1, Minus, $3) }
  | expression T_MULT expression     { IntOperation ($1, Mult, $3) }
  | expression T_DIV expression      { IntOperation ($1, Div, $3) }
  | expression T_MOD expression      { IntOperation ($1, Mod, $3) }
  | expression T_AND expression      { IntOperation ($1, And, $3) }
  | expression T_OR expression       { IntOperation ($1, Or, $3) }
  | expression T_XOR expression      { IntOperation ($1, Xor, $3) }
  | expression T_LSHIFT expression   { IntOperation ($1, LShift, $3) }
  | expression T_RSHIFT expression   { IntOperation ($1, RShift, $3) }
  | expression T_BOOL_AND expression { BoolOperation ($1, AndAnd, $3) }
  | expression T_BOOL_OR expression  { BoolOperation ($1, OrOr, $3) }
  
  | expression QMARK expression COLON expression { IfExpr ($1, $3, $5) }
  | expression DOT nonTypeId { Dot ($1, $3) }
  
  | expression APOS { Dashed $1 }
  | T_DEADLOCK { Deadlock }
  
  | expression T_KW_IMPLY expression { Imply ($1, $3) } 
  | expression T_KW_AND expression   { BoolOperation ($1, AndAnd, $3) }
  | expression T_KW_OR expression    { BoolOperation ($1, OrOr, $3) }
  | T_KW_NOT expression              { Unary (UnaryNot, $2) }
  | expression T_MIN expression      { Relation ($1, Min, $3) }
  | expression T_MAX expression      { Relation ($1, Max, $3) }

  | T_FORALL LPAREN id COLON aType RPAREN expression %prec T_FORALL 
    { Forall ($3, $5, $7) }
  | T_EXISTS LPAREN id COLON aType RPAREN expression %prec T_EXISTS 
    { Exists ($3, $5, $7) }

  | property   { $1 }
  | assignment { $1 } 
;

/* manually added */
property:
    T_AF expression                 { CTL (AF $2) }
  | T_AG expression                 { CTL (AG $2) }
  | T_EF expression                 { CTL (EF $2) }
  | T_AG T_MULT expression          { CTL (AGR $3) }
  | T_EF T_MULT expression          { CTL (EFR $3) }
  | T_EG expression                 { CTL (EG $2) }
  | T_AF2 pathExpression expression { CTL (AF2 ($2, $3)) }
  | T_AG2 pathExpression expression { CTL (AG2 ($2, $3)) }
  | T_EF2 pathExpression expression { CTL (EF2 ($2, $3)) }
  | T_EG2 pathExpression expression { CTL (EG2 ($2, $3)) }
  | expression T_LEADSTO expression { CTL (Leadsto ($1, $3)) }
  | T_A LBRACKET expression T_U expression RBRACKET 
                                    { CTL (AU ($3, $5)) }
  | T_A LBRACKET expression T_W expression RBRACKET 
                                    { CTL (AW ($3, $5)) }
  | T_A LBRACE expression RBRACE LBRACKET expression T_U expression RBRACKET
                                    { CTL (AU2 ($3, $6, $8)) }
  | T_A LBRACE expression RBRACE LBRACKET expression T_W expression RBRACKET
                                    { CTL (AW2 ($3, $6, $8)) }
;

pathExpression: /*USED*/
    { Bool true } /* TODO: Check, if this is correct! */
  | LBRACE expression RBRACE { $2 }
;

assignment: /*USED*/
    expression T_ASSIGNMENT expression { Assignment ($1, Assign, $3) }
  | expression T_ASSPLUS expression    { Assignment ($1, AssignPlus, $3) }
  | expression T_ASSMINUS expression   { Assignment ($1, AssignMinus, $3) }
  | expression T_ASSDIV expression     { Assignment ($1, AssignDiv, $3) }
  | expression T_ASSMOD expression     { Assignment ($1, AssignMod, $3) }
  | expression T_ASSMULT expression    { Assignment ($1, AssignMult, $3) }
  | expression T_ASSAND expression     { Assignment ($1, AssignAnd, $3) }
  | expression T_ASSOR expression      { Assignment ($1, AssignOr, $3) }
  | expression T_ASSLSHIFT expression  { Assignment ($1, AssignLShift, $3) }
  | expression T_ASSRSHIFT expression  { Assignment ($1, AssignRShift, $3) }
  | expression T_ASSXOR expression     { Assignment ($1, AssignXor, $3) }
;

unaryOp: /*USED*/
    T_MINUS { UnaryMinus }
  | T_EXCLAM { UnaryNot }
  | T_PLUS { UnaryPlus }

argList: /*USED*/
    { [] }
  | expression { [$1] }
  | argList COMMA expression { $1 @ [$3] }
