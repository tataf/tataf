(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
  Extended functions for Xml handling.

{i NOTE: this is a verbatim copy of tataf-libha/src/ExtXml.mli for the sake
of keeping tataf-libcore independent of xml-light;                     
in case of fixes or extensions, please fix the other one, too.} 
*)


val filter_name : string -> Xml.xml list -> Xml.xml list
(** 
  [filter_name wanted] filters a list of [Xml.xml] elements and returns
  a list, containing only the elements with tag-name [wanted].

  @author Jeremi Dzienian 
*)

exception Multiple_elements
(** 
  The exception [Multiple_elements] is thrown, when only a single result
  was expected, but multiple elements came out.
*)

val exists_element : string -> Xml.xml list -> bool
(** 
  Check, if {!filter_name} results in only one element.
  
  @raise Multiple_elements if the result list contains more than one element.
*) 

val attrib_or_empty : Xml.xml -> string -> string
(**
  [attrib_or_empty element attr_name] returns the attribute with name
  [attr_name] from the element [element]. If the specified attribute does
  not exists, then the empty string is returned.   
*)

exception No_element
(**
  The exception [No_element] is thrown, when no element was found, but
  at least one element was excepted.
*)

val single_element_attrib : string -> string -> Xml.xml list -> string
(**
  [single_element name elements] searches in the [elements] list the element
  with tag-name [name] and returns exactly this element.
  
  @raise No_element If no element with given name could be found.
  @raise Multiple_elements If more than one element was found.
*)

val xmlescape_of_string : string -> string
(**
  [xmlescape_of_string s] returns the Xml-escaped version of a string.
  Special characters, like "&", "<" and ">" are represented according to 
  the Xml specification. 
*)

val xmlunescape_of_string : string -> string
(**
  [xmlunescape_of_string s] returns the Xml-unescaped version of a string,
  which contains xml escaped characters. This is the inverse function
  to {!xmlescape_of_string}.
*)

val xmlattribute : string -> string -> string
(**
  [xmlattribute attr_name value] returns the xml representation of an
  attribute. If [value] is the empty string, then the result will be the
  empty string, assuming that empty attributes should not be printed.
*)

val remove_doctype : string -> string
(** [remove_doctype s] removes the xml specific [<!DOCTYPE ...>] declaration,
  so that the Xml module can read the string [s] as Xml. *)

