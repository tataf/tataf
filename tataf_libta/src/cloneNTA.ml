(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open UppaalSystem
open UppaalSystem.Template

(*----------------------------------------------------------------------------*)

class clone_NTA_transformer =
  object (self)

  val mutable suffix = ""

  inherit UppaalTransformerWithSymboltable.transformer_without_symboltable 
  as super

  method! transform_xmlid id =
    id ^ suffix

  method! transform_id id =
    id ^ suffix

  method! transform_name {Name. text; xy} = 
    {Name. text = text ^ suffix;
           xy = Tataf_libcore.Util.some_apply self#transform_xy xy; }

  (* go <suffix> <NTA>
     -- yields a copy of <NTA> (type UppaalSystem.NTA), where all XML IDs,
     identifiers, and names have consistently been suffix'ed with
     <suffix>; used, e.g., by module SimCheck
   *)
  method go name_id_suffix =
    suffix <- name_id_suffix;
    self#transform_nta
end

(*----------------------------------------------------------------------------*)
