(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
Type System for Uppaal.

Within this module all data structures for Uppaal are defined. All other Uppaal
modules will work with this data types.

The types are wrapped within their own modules and often contain additional 
definitions of functions to create, compare or otherwise  work with this type. 

@author Jeremi Dzienian
*)

(**
From Uppaal help file:
The valid identifier names are described by the following regular expression:
[a - zA - Z_]([a - zA - Z0 -9_]) *
*)
module ID = struct
  type t = string
  
  let compare = Pervasives.compare
end

(* ---------------------------------------------------------------------------- *)
(**
{2 XTA Part }
*)

(**
Representation of expressions.
*)
module rec Expression : sig
  type assignOperator =
      Assign
    | AssignPlus
    | AssignMinus
    | AssignMult
    | AssignDiv
    | AssignMod
    | AssignOr
    | AssignAnd
    | AssignXor
    | AssignLShift
    | AssignRShift
  type unaryOperator = UnaryMinus | UnaryNot | UnaryPlus
  type relOperator = Lt | LtEq | Eq | NotEq | GtEq | Gt | Min | Max
  type intOperator =
      Plus
    | Minus
    | Mult
    | Div
    | Mod
    | And
    | Or
    | Xor
    | LShift
    | RShift
  type boolOperator = AndAnd | OrOr
  type t =
      Var of ID.t
    | Num of int
    | Bool of bool
    | Call of t * t list
    | Array of t * t
    | Parenthesis of t
    | IncPost of t
    | IncPre of t
    | DecPost of t
    | DecPre of t
    | Assignment of t * assignOperator * t
    | Unary of unaryOperator * t
    | Relation of t * relOperator * t
    | IntOperation of t * intOperator * t
    | BoolOperation of t * boolOperator * t
    | IfExpr of t * t * t
    | Dot of t * ID.t
    | Dashed of t
    | Deadlock
    | Imply of t * t
    | Forall of ID.t * Type.t * t
    | Exists of ID.t * Type.t * t
    | CTL of ctl
  and ctl =
      AF of t
    | AG of t
    | EF of t
    | AGR of t
    | EFR of t
    | EG of t
    | AF2 of t * t
    | AG2 of t * t
    | EF2 of t * t
    | EG2 of t * t
    | Leadsto of t * t
    | AU of t * t
    | AW of t * t
    | AU2 of t * t * t
    | AW2 of t * t * t

  val turn_relop : relOperator -> relOperator
  val match_variable_relation : ID.t * relOperator * t -> t -> bool

end = struct
  (** Operator used in assignments ({!t.Assignment}). *)
  type assignOperator =
    | Assign        (** =  *)
    | AssignPlus    (** += *)
    | AssignMinus   (** += *)
    | AssignMult    (** *= *)
    | AssignDiv     (** /= *)
    | AssignMod     (** %= *)
    | AssignOr      (** |= *)
    | AssignAnd     (** &= *)
    | AssignXor     (** ^= *)
    | AssignLShift  (** <<= *)
    | AssignRShift  (** >>= *)
  
  (** Operator used in unary expressions ({!t.Unary}). *)
  type unaryOperator =
    | UnaryMinus    (** - *)
    | UnaryNot      (** ! *)
    | UnaryPlus     (** + *)
  
  (** Operator used in relations ({!t.Relation}). *)
  type relOperator =
    | Lt            (** <  *)
    | LtEq          (** <= *)
    | Eq            (** == *)
    | NotEq         (** != *)
    | GtEq          (** >= *)
    | Gt            (** >  *)
    | Min           (** <? *)
    | Max           (** >? *)
  
  (** Operator used in integer operations ({!t.IntOperation}). *)
  type intOperator =
    | Plus       (** + *)
    | Minus      (** - *)
    | Mult       (** * *)
    | Div        (** / *)
    | Mod        (** % *)
    | And        (** & *)
    | Or         (** | *)
    | Xor        (** ^ *)
    | LShift     (** << *)
    | RShift     (** >> *)
  
  (** Operator used in boolean operations ({!t.BoolOperation}). *)
  type boolOperator =
    | AndAnd     (** && *)
    | OrOr       (** || *)
  
  type t =
    | Var of ID.t        (** ID *)
    | Num of int         (** <natural number> *)
    | Bool of bool       (** true|false *)
    | Call of t * t list (** e1(e2,e2,e2...) *)
    | Array of t * t     (** e1\[e2\] *)
    | Parenthesis of t   (** (e) *)
    | IncPost of t       (** e++ *)
    | IncPre of t        (** ++e *)
    | DecPost of t       (** e-- *)
    | DecPre of t        (** --e *)
    | Assignment of t * assignOperator * t  (** e <ASSIGN> e *)
    | Unary of unaryOperator * t            (** <UNARY> e *)
    | Relation of t * relOperator * t       (** e <REL> e *)
    | IntOperation of t * intOperator * t   (** e <INTOP> e *)
    | BoolOperation of t * boolOperator * t (** e <BOOLOP> e *)
    | IfExpr of t * t * t    (** e1 ? e2 : e3 *)
    | Dot of t * ID.t    (** e1 . ID *)
    | Dashed of t        (** 'e *)
    | Deadlock           (** deadlock *)
    | Imply of t * t     (** imply (equivalend to (!e1 || e2) *)
    | Forall of ID.t * Type.t * t (** forall ( id : type) expression *)
    | Exists of ID.t * Type.t * t (** exists ( id : type) expression *)
    | CTL of ctl
  
  and
  ctl =
    | AF of t               (** A<> *)
    | AG of t               (** A[] *)
    | EF of t               (** E<> *)
    | AGR of t              (** A[]* *)
    | EFR of t              (** E<>* *)
    | EG of t               (** E[] *)
    | AF2 of t * t          (** AF *)
    | AG2 of t * t          (** AG *)
    | EF2 of t * t          (** EF *)
    | EG2 of t * t          (** EG *)
    | Leadsto of t * t      (** --> *)
    | AU of t * t           (** A e U e *)
    | AW of t * t           (** A e W e *)
    | AU2 of t * t * t      (** A\[e\] U e *)
    | AW2 of t * t * t      (** A\[e\] W e *)
  
  (* ----------------------------------------------- *)
  
  let turn_relop = function
    | Lt    -> Gt
    | LtEq  -> GtEq
    | Eq    -> Eq
    | NotEq -> NotEq
    | GtEq  -> LtEq
    | Gt    -> Lt
    | Min   -> Max  (* correct? *)
    | Max   -> Min  (* correct? *)
  
  let match_variable_relation (x, rel, expr) match_expr =
    match match_expr with
    | Relation(      Var(x),  rel, expr) -> true
    | Relation(expr, trel,         Var(x)) when trel = turn_relop rel
        -> true
    | _ -> false
  
  let rec match_variable_assignment (x, op, expr) match_expr =
    match match_expr with
    | Assignment(Var(x),op,_expr) when _expr = expr -> true
    | Assignment(Var(x),op,_expr) ->
      (*check, if expression would be true in explicit notation*)
      let rcall e = match_variable_assignment (x, Assign, e) match_expr in
      begin match op with
        | Assign -> false
        | AssignPlus   -> rcall (IntOperation(Var(x), Plus, _expr))
                       || rcall (IntOperation(_expr,  Plus, Var(x)))
        | AssignMinus  -> rcall (IntOperation(Var(x), Minus, _expr))
                       || rcall (IntOperation(_expr,  Minus, Var(x)))
        | AssignMult   -> rcall (IntOperation(Var(x), Mult, _expr))
                       || rcall (IntOperation(_expr,  Mult, Var(x)))
        | AssignDiv    -> rcall (IntOperation(Var(x), Div, _expr))
                       || rcall (IntOperation(_expr,  Div, Var(x)))
        | AssignMod    -> rcall (IntOperation(Var(x), Mod, _expr))
                       || rcall (IntOperation(_expr,  Mod, Var(x)))
        | AssignOr     -> rcall (IntOperation(Var(x), Or, _expr))
                       || rcall (IntOperation(_expr,  Or, Var(x)))
        | AssignAnd    -> rcall (IntOperation(Var(x), And, _expr))
                       || rcall (IntOperation(_expr,  And, Var(x)))
        | AssignXor    -> rcall (IntOperation(Var(x), Xor, _expr))
                       || rcall (IntOperation(_expr,  Xor, Var(x)))
        | AssignLShift -> rcall (IntOperation(Var(x), LShift, _expr))
                       || rcall (IntOperation(_expr,  LShift, Var(x)))
        | AssignRShift -> rcall (IntOperation(Var(x), RShift, _expr))
                       || rcall (IntOperation(_expr,  RShift, Var(x)))
      end
    | _ -> false 
  
  (* TODO! create a function (for requirements) to check whether an expression
     is side-effect free: let is_side_effect_free expr = ... *)
  
end and Type : sig
  type prefix = Urgent | Broadcast | Const | Meta
  type t =
      Typename of prefix list * ID.t
    | Bool of prefix list
    | Int of prefix list * Expression.t * Expression.t
    | Chan of prefix list
    | Clock
    | Void
    | Scalar of prefix list * Expression.t
    | Struct of prefix list * field list
    | Ref of t
    | Range of t * range list
  and field = t * ID.t
  and range = RangeExpression of Expression.t | RangeType of t
  val intMinRange : int
  val intMaxRange : int
  
  val unranged_int : prefix list -> t
  
  val is_unranged_int : t -> bool
  val is_const : t -> bool
  
  val is_clock : t -> bool 

(** Type representation ([int], [bool], [chan], [void], ...). *)
end = struct
  (**
  Some types can be prefixed with the following keywords to specify some extra
  semantics:
  - [const]: Integers, booleans, and arrays over integers and booleans can
  be marked constant.
  - [meta]: Integers, booleans, and arrays over integers and booleans can
  be marked as meta variables.
  - [urgent / broadcast]: Channels can be declared as urgent and / or broadcast
  channels.
  *)
  type prefix =
    | Urgent     (** urgent *)
    | Broadcast  (** broadcast *)
    | Const      (** const *)
    | Meta       (** meta *)
  
  (**
  There are these basic types:
  - {b Boolean } ({!t.Bool }): Variables of type [bool] can have values [false] and
  [true], which are equivalent to the integer values [0] and [1].
  Like in C, any non - zero integer value evaluates to [true] and [0]
  evaluates to [false].
  - {b Integer } ({!t.Int }): {ul
  {- [int]: unbounded Integer. The bounds are automatically set to
  \[{!intMinRange }, {!intMaxRange }\]. }
  {- [int [lower, upper]]: bounded Integer.}
  }
  - {b Channel } ({!t.Chan }): Channels can be declared as [urgent] and / or [broadcast].
  - {b Clock } ({!t.Clock })
  - {b Void } ({!t.Void }): Used primary as return type for functions.
  - {b Scalar } ({!t.Scalar })
  
  These are {i constructed } types:
  - {b Reference } ({!t.Ref }): If a type is prefixed with [ref], then it is a
  reference - type. The subtype will be available as child of this element.
  References are used as function parameters.
  - {b Range / Array } ({!t.Range })
  *)
  type t =
    | Typename of prefix list * ID.t
    | Bool of prefix list  (** (prefix) bool *)
    | Int of prefix list * Expression.t * Expression.t (** (prefix) int *)
    | Chan of prefix list  (** (prefix) chan *)
    | Clock                (** clock *)
    | Void                 (** void *)
    | Scalar of prefix list * Expression.t (** (prefix) scalar *)
    | Struct of prefix list * field list   (** (prefix) struct \{ \} *)
    
    (* -- constructed types - *)
    | Ref of t                 (** & <type> *)
    | Range of t * range list  (** <type>\[e\]\[e\]... *)
  
  and field = t * ID.t
  
  and range =
    | RangeExpression of Expression.t
    | RangeType of t (** may be only \[int\] or \[scalar\]. *)
  
  (** Default lower bound for unbound integers (=-32768) *)
  let intMinRange = -32768
  
  (** Default upper bound for unbound integers (=32767) *)
  let intMaxRange = 32767
  
  let unranged_int prefix =
    let open Expression in
    Int(prefix, Num(intMinRange), Num(intMaxRange))
  
  let is_unranged_int type' =
    let open Expression in match type' with
    | Int(_, Num(min), Num(max)) -> min = intMinRange && max = intMaxRange
    | _ -> false
  
  let rec is_const type' =
    let rec has_const_prefix = function
      | [] -> false
      | Const :: _ -> true
      | _ :: tl -> has_const_prefix tl
    in
    match type' with
    | Typename(pl, _) ->  has_const_prefix pl
    | Bool(pl) -> has_const_prefix pl
    | Int(pl, _, _) -> has_const_prefix pl
    | Chan(pl) -> has_const_prefix pl
    | Clock | Void -> false
    | Scalar(pl, _) -> has_const_prefix pl
    | Struct(pl, _) -> has_const_prefix pl
    | Ref(t) -> is_const t
    | Range(t, _) -> is_const t

  let rec is_clock type' =
    match type' with
    | Clock -> true
    | Ref(t) -> is_clock t
    | Range(t, _) -> is_clock t
    | _ -> false
end

(**
Initializer for variable declarations ({!Declaration.t.Variable }).
*)
module Initializer = struct
  type t =
    | Expression of Expression.t
    | Fields of (ID.t option * t) list 
  
  (**
  TODO: checks, if the initializer matches the given type.
  *)
  (*let match_type (tp : Type.t) = false*)
  
  let make_expr expr = Expression(expr)
  let make_fields fields = Fields(fields)
  
  let make_some_expr expr = Some (make_expr expr)
  let make_some_fields fields = Some (make_fields fields)
end

module Parameter = struct
  type t =
    { typ : Type.t;
      name : ID.t;
      (* xy : XY.t; defined in DTD, but ignored by Uppaal GUI *)
    }
    let make typ name = { typ; name; }
end

(**
Representation of declarations (Variable, Instantiation, BeforeUpdate,
AfterUpdate, Typedef, Function, ...)
*)
module rec Declaration : sig
  type t =
    (*| Const of Type.t ID.t * Initializer ??*)
    | Variable of Type.t * ID.t * Initializer.t option
    | Instantiation of Instantiation.t
    | BeforeUpdate of Expression.t list
    | AfterUpdate of Expression.t list
    | Typedef of Type.t * ID.t
    
    | Function of Type.t * ID.t * Parameter.t list * t list * Statement.t list
    | ChanPriority of chanElement list list
  
  (* and parameter = Type.t * ID.t *)
  and chanElement =
    | DefaultPriority
    | Channel of ID.t * Expression.t list
  
  val is_blockDeclaration : t list -> bool
  val is_function_declaration : t -> bool
end = struct
  type t =
    | Variable of Type.t * ID.t * Initializer.t option
    (** <type> id (= initializer); *)
    | Instantiation of Instantiation.t
    (** id(parameter) = id(args); *)
    | BeforeUpdate of Expression.t list
    (** before_update \{ e, e, e... \}; *)
    | AfterUpdate of Expression.t list
    (** after_update \{ e, e, e...  \}; *)
    | Typedef of Type.t * ID.t
    (** typedef <type> id; *)
    
    | Function of Type.t * ID.t * Parameter.t list *
    t list * Statement.t list
    (* param decl stmt *)
    | ChanPriority of chanElement list list
  (* | ProcessDecl ? | Instantiation ? *)
  
  (* and parameter = Type.t * ID.t *)
  and chanElement =
    | DefaultPriority
    | Channel of ID.t * Expression.t list
  
  (**
  [is_blockDeclaration decl_list] checks if [decl_list] contains only valid
  declarations for a block statement. Block statements may only contain
  variable - and type - declarations.
  *)
  let rec is_blockDeclaration = function
    | [] -> true
    | Variable(_, _, _) :: tl -> is_blockDeclaration tl
    | Typedef(_, _) :: tl -> is_blockDeclaration tl
    | _ -> false

  (**
  [is_function_declaration decl] checks if [decl] declares a function.*)
  let is_function_declaration = function
      | Function (_, _, _, _, _) -> true
      | _ -> false
  
end and Instantiation : sig
  type t =
    { instance_id : ID.t;
      instance_parameters : Parameter.t list; (*Declaration.parameter list;*)
      template_id : ID.t;
      template_arguments : Expression.t list
    }
(* type t = | Empty | Raw of string | Instantiations of single list REMOVE *)
(* this?                                                                   *)
end = struct
  type t =
    { instance_id : ID.t;
      instance_parameters : Parameter.t list; (* Declaration.parameter list;*)
      template_id : ID.t;
      template_arguments : Expression.t list
    }
  (* type t = | Empty | Raw of string | Instantiations of single list REMOVE *)
  (* this?                                                                   *)
end and Statement : sig
  type t =
    | Empty
    | Expression of Expression.t
    | Block of Declaration.t list * t list
    | For of Expression.t list * Expression.t list * Expression.t list * t
    | Foreach of ID.t * Type.t * t
    | While of Expression.t list * t
    | Do of Expression.t list * t
    | If of Expression.t list * t * t option
    | Break      (* reserved, unused *)
    | Continue   (* reserved, unuesd *)
    | Switch of Expression.t list * case list  (*reserved, unused*)
    | Return of Expression.t option
    | Assert of Expression.t (*reserved, unused*)
  
  and case =
    | Case of Expression.t * t list
    | Default of t list
end = struct
  type t =
    | Empty (*EmptyStatement to avoid collisions?*)
    | Expression of Expression.t (*same here?*)
    | Block of Declaration.t list * t list
    | For of Expression.t list * Expression.t list * Expression.t list * t
    (** for (<e>; <e>; <e>) <stmt> *)
    | Foreach of ID.t * Type.t * t
    | While of Expression.t list * t
    (** while ( <e>, ...) <stmt> *)
    | Do of Expression.t list * t
    (** do <stmt> while ( <e>, ...); *)
    | If of Expression.t list * t * t option
    (** if ( <e>, ... ) <stmt>; else stmt;
    - I do not know, why the grammar allows Expression list as condition.
    - If no else - part exists, then the second statement will be [None].
    *)
    | Break      (** break;*)
    | Continue   (** continue; *)
    | Switch of Expression.t list * case list
    (** switch (e, e, ...) \{ <case> \}* *)
    | Return of Expression.t option
    (** return \[?\]; *)
    | Assert of Expression.t    (** assert e; *)
  
  and case =
    | Case of Expression.t * t list
    | Default of t list
end

(* ---------------------------------------------------------------------------- *)
(**
{2 XML Part }
*)

module XY = struct
  type t =
    { x : int; (*string option;*)
      y : int; (*string option;*)
    }
  
  let make x y = { x; y }
  let make_some x y = Some (make x y)
end

module Color = struct
  type t = string
  
  let red     = "#ff0000"
  let gray    = "#999999"
  let cyan    = "#00ffff"
  let magenta = "#ff00ff"
end

module Nail = struct
  type t = { pos : XY.t }
  
  let make ~x ~y = { pos = { XY.x = x; XY.y = y } }
  let make2 ~xy = { pos = xy }
end

module Invariant = struct
  open Expression
  
  type t =
    { expr : Expression.t;
      xy : XY.t option;
    }
  let make ?(xy=None) ~expr = { expr; xy }
  let make_some expr = Some ( make expr )
  let default = { expr = Bool(true); xy = None }
  
  (**
    Checks if the given expression matches the invariant expression. If 
    the invariant is not set (None), the return value is [false].
  *)
  let eq invariant expr1 = match invariant with
    | Some { expr; _} when expr = expr1 -> true
    | _ -> false 
end

module Select = struct
  (** single select expression *)
  type single = ID.t * Type.t
  
  type t =
    { sl : single list;
      xy : XY.t option;
    }
  let make ~sl = { sl; xy = None }
  let default = { sl = []; xy = None }
end

module Guard = struct
  open Expression
  
  type t =
    { expr : Expression.t;
      xy : XY.t option;
    }
  let make expr = { expr; xy=None }
  let make_some expr = Some ( make expr )
  
  let default = { expr = Bool(true); xy = None }
  
  let match_variable_relation relation = function
    | None -> false
    | Some {expr; _} -> Expression.match_variable_relation relation expr

  let eq guard x c = match guard with
    | Some {expr=(Relation(Var(x),GtEq,c)); _} 
    | Some {expr=(Relation(c,LtEq,Var(x))); _} 
    | Some {expr=(Relation(Var(x),Eq,c)); _} 
    | Some {expr=(Relation(c,Eq,Var(x))); _} 
    		-> true 
    | _ -> false
end

module Measure = struct
  type t = Expression.t
end

module Sync = struct
  open Expression
  type syncMarker =
    | Broadcast (* ! Broadcast *)
    | Synchronize (* ? Synchronize *)
  
  type t =
    { expr : Expression.t;
      marker : syncMarker;
      xy : XY.t option;
    }
  
  let make ?(xy=None) ~expr ~marker = { expr; marker; xy }
  let make_broadcast expr = { expr; marker=Broadcast; xy=None }
  let make_synchronize expr = { expr; marker=Synchronize; xy=None }

  let make_some_broadcast expr = Some (make_broadcast expr)
  let make_some_synchronize expr = Some (make_synchronize expr)

  let is_tau : t option -> bool = function
    | None -> true     (*tau*)
    | Some _ -> false

  let is_broadcast :t option -> bool = function
    | Some { marker=Broadcast; _ } -> true
    | Some { marker=Synchronize; _ } -> false
    | None -> false
end

module Update = struct
  type t =
    { el : Expression.t list;
      xy : XY.t option;
    }
  let empty = { el = []; xy = None }

  let make ?(xy=None) ~el = { el; xy }
  let make_some el = Some ( make el )

  let add_expr supd expr =
    match supd with 
    | None     -> Some { empty with el = [expr] }
    | Some upd -> Some { upd   with el = upd.el @ [expr] } 

  (**
    Checks, if the update (list) contains exactly one given expression
    (= is this expression).
  *)
  let is update expr = match update with
    | Some {el=[e]; _} when e = expr -> true
    | _ -> false 

  (**
    Checks, if the update (list) equals the given expression list (el).
  *)
  let eq update el1 = match update with
    | Some {el ;_} when el = el1 -> true
    | _ -> false
end

module Comment = struct
  type t = string
end

module Edge = struct
  type t =
    { id : ID.t option;
      source : ID.t;
      target : ID.t;
      select : Select.t option;
      guard : Guard.t option;
      sync : Sync.t option;
      update : Update.t option;
      comment : Comment.t option;
      (*GUI extras*)
      nails : Nail.t list;
      xy : XY.t option;
      color : Color.t option;
      (* symbol_table : ... *)
    }
    
  let make ~source ~target =
    { id=None; 
      source; 
      target; 
      nails=[]; 
      xy=None; 
      color=None;
      select=None; 
      guard=None; 
      sync=None; 
      update=None; 
      comment=None }

  (**
    Compares two edges, using the following fields in order:
      - id
      - source
      - target
  *)
  let compare e1 e2 =
    let open Pervasives in
    List.fold_left
      (fun i cmp -> if i <> 0 then i else cmp)
      0
      [ compare e1.id     e2.id;
        compare e1.source e2.source;
        compare e1.target e2.target;
        compare e1.select e2.select;
        compare e1.guard  e2.guard;
        compare e1.sync   e2.sync;
        compare e1.update e2.update; ]
end

module Name = struct
  type t =
    { text : ID.t;
      xy : XY.t option;
    }
  let make text = { text; xy = None }
  let make_some text = Some (make text)
  (* let make ~xy ~text = { xy; text } *)
  let compare n1 n2 = Pervasives.compare n1.text n2.text
end

module Exponentialrate = struct
  type t =
      { value: float;
	xy : XY.t option;
      }
  let make value = { value; xy = None }
  let make_some value = Some (make value)		     
end
			   
module Location = struct
  type t =
    { id : ID.t;
      name : Name.t option;
      urgent : bool;
      committed : bool;
      invariant : Invariant.t option;
      comment : Comment.t option;
      (*GUI extras*)
      xy : XY.t option;
      color : Color.t option;
      exponentialrate : Exponentialrate.t option;
    }

  let is_urgent {urgent; _} = urgent

  let make id =
    {
      id; 
      urgent = false; 
      committed = false;
      name = None;
      invariant = None;
      comment = None;
      xy = None;
      color = None;
      exponentialrate = None;
    }
    (*
  let make ~id ~name ~urgent ~committed ~xy ~color : t =
    { id; name; urgent; committed; xy; color;
      invariant=None; comment=None }
    *)
  let compare l1 l2 = Pervasives.compare l1.id l2.id
end

module Init = struct
  type t = ID.t
end

module Template = struct
  type t =
    { name : Name.t;
      parameters : Parameter.t list;
      declarations : Declaration.t list;
      locations : Location.t list;
      init : Init.t;
      edges : Edge.t list;
      (* symbol_table : ... *)
    }
  let make name ~locations ~edges ~init = 
    { name = { Name.text = name; xy = None };
      parameters = [];
      declarations = [];
      locations;
      init;
      edges;
    }
  (*
  let make ~name ~parameters ~declarations ~locations ~init ~edges =
    { name; parameters; declarations; locations; init; edges }
  *)
end

module Imports = struct
  type t = string (*ofiicially not used*)
end

module Process = struct
  type t =
    { priority : int; (* defaults to 0 *)
      name : ID.t;
    }
  let make ?(priority =0) ~name = { priority; name }
end

module Progress = struct
  type t =
    { guard : Guard.t option;
      measure : Measure.t   (* measure *)
    }
  let make ?(guard = None) ~measure = { guard; measure }
end

module System = struct
  type t =
    { declarations : Declaration.t list;
      processes : Process.t list;
      progresses : Progress.t list;
    }
  let max_process_id {processes; _} =
    List.fold_left 
      (fun i {Process.priority;_} -> if priority > i then priority else i)
      0 processes
end

module DtdVersion = struct
  type t = DTD_1_1 of unit | DTD_1_2 of unit
  let lookup dtd_path =
    match dtd_path with 
    | "http://www.it.uu.se/research/group/darts/uppaal/flat-1_1.dtd"
      -> DTD_1_1 ()
    | "http://www.it.uu.se/research/group/darts/uppaal/flat-1_2.dtd"
      -> DTD_1_2 () 
    | _ -> failwith "XML file has incorrect DTD Header"

  let string_of_dtd_version = function
      DTD_1_1 _ -> "http://www.it.uu.se/research/group/darts/uppaal/flat-1_1.dtd"
    | DTD_1_2 _ -> "http://www.it.uu.se/research/group/darts/uppaal/flat-1_2.dtd"
      
end

module NTA = struct
  type t =
    { imports : Imports.t option;
      declarations : Declaration.t list;
      templates : Template.t list;
      instantiation : Declaration.t list; (* is this used in XTA4??? Seems not to be the case! *)
      system : System.t;
      (* symbol_table : ... *)
      dtd_version : DtdVersion.t;
    }
end

(**
Representation of an Uppaal Query - File.
*)
module Queries = struct
  type t =
    { queries : Expression.t list; (* "property" instead of query? *)
      nta : NTA.t option;
      (* line -- int ; could be useful for the verifier *)
    }
  let empty = { queries = []; nta = None }
end

module ClockTemplate = struct
  type t =
    { template : Name.t;
      clock : ID.t;
      qeClass : ID.t;
    }
    
  let make ~name ~clock ~qeClass = 
    { template = { Name.text = name; xy = None };
      clock;
      qeClass;
    }
      
  let compare = Pervasives.compare
end


module SumTokensNumClocks = struct
  type t =
    { sumTokens : Expression.t;
      numClocks : ID.t;
    }
    
  let make ~sumTokens ~numClocks  = 
    { sumTokens;
      numClocks;
    }
      
  let compare = Pervasives.compare
end

(* ---------------------------------------------------------------------- *)

(**
{2 Predefined Sets }
  Some predefined Set-types for the most commonly used Uppaal types.
*)

module IDSet = Set.Make(ID)


module ClockTemplateSet = Set.Make(ClockTemplate)


module SumTokensNumClocksSet = Set.Make(SumTokensNumClocks)

module EdgeSet_ = Set.Make(Edge)
module EdgeSet = struct
  include EdgeSet_
  (* defining the of_list method, because it is introduced in OCaml 4.02.0, but
     we use OCaml 4.01.0 *)
  let of_list = List.fold_left (fun i el -> EdgeSet_.add el i) EdgeSet_.empty
end

module LocationSet = Set.Make(Location)

(* ---------------------------------------------------------------------- *)

(* This module is somehow a relict of olden times when together with Jeremy *)
(* we tried to do query transformation (1) without symbol-table -- the idea *)
(* was to walk first and collect references to those objects which need to  *)
(* be replaced, and later work on exactly those. *)
(* This required a way of storing references, and we reverted to force cast *)
(* object ID into an integer.  There should be more elegant methods. :-/ *)
(* (1): so far only used in QE-transformation *)
(* *)
(* This is variant (a) below. *)
(* *)
(* On the qetrafo-branch, it was changed to variant (b) some time ago, and *)
(* on the trunk kept back becasue we did not fully understand the change.  *)
(* Now we have test cases which show that QE-trafo with (a) actually does  *)
(* not work as expected any more.  It seems not to be wrong, but dupli-    *)
(* cates some sub-expressions now.                                         *)
(* *)
(* So from now on, for the moment, have (b) on the trunk. *)
(* *)
(* Should become obsolete with transformers with symbol table soon... *)
(* *)
module ExpressionReference = struct
  include Expression

  (* used below to make sure that the result of Obj.magic is compared as an   *)
  (* integer, not again "magically" as an object *)
  let force_int ( x : int ) : int = x

  (* compare for physical equality (not structural) -- use compare on integer *)
  (* representation of identities in order to properly support equal (0),     *)
  (* smaller-than (-1), and bigger-than (1), in particular for Set.mem.       *)
(* (a) *)
(*let compare a b = Pervasives.compare (force_int (Obj.magic a)) *)
(*                                     (force_int (Obj.magic b)) *)
(* (b) *)
  let compare = Pervasives.compare
end

module ExpressionReferenceSet = Set.Make(ExpressionReference)


