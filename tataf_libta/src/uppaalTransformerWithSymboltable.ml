(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open SymbolTable
open UppaalSystem
open UppaalSystem.Expression
open Tataf_libcore.Util

class transformer_without_symboltable =
  object (self) 

  (* Cf. UppalWalker.fail_visit_mismatch *)
  method fail_transform_mismatch fname argtype =
    failwith( "UppaalTransformer: '" ^ fname ^ "' called with argument that 
                             does not have type '" ^ argtype ^ "'" )

(******************************************************************************
 * Copy over from old transformer class.
******************************************************************************)
  method transform_CTLexpression ctl = 
    let te = self#transform_expression in
    match ctl with
    | AF(e) -> AF(te e)
    | AG(e) -> AG(te e)
    | EF(e) -> EF(te e)
    | AGR(e) -> AGR(te e)
    | EFR(e) -> EFR(te e)
    | EG(e) -> EG(te e)
    | AF2(e1,e2) -> AF2(te e1, te e2)
    | AG2(e1,e2) -> AG2(te e1, te e2)
    | EF2(e1,e2) -> EF2(te e1, te e2)
    | EG2(e1,e2) -> EG2(te e1, te e2)
    | Leadsto(e1,e2) -> Leadsto(te e1, te e2)
    | AU(e1,e2) -> AU(te e1, te e2)
    | AW(e1,e2) -> AW(te e1, te e2)
    | AU2(e1,e2,e3) -> AU2(te e1, te e2, te e3)
    | AW2(e1,e2,e3) -> AW2(te e1, te e2, te e3)

  method transform_expression expr = 
    match expr with
    | Var(id) -> self#transform_var expr
    | Num(_) -> self#transform_num expr
    | Bool(_) -> self#transform_bool expr
    | Call(e,args) -> self#transform_call expr
    | Array(e1,e2) -> self#transform_array expr
    | Parenthesis(e) -> self#transform_parenthesis expr
    | IncPost(e) -> self#transform_post_increment expr
    | IncPre(e) -> self#transform_pre_increment expr
    | DecPost(e) -> self#transform_post_decrement expr
    | DecPre(e) -> self#transform_pre_decrement expr
    | Assignment(e1,op,e2) -> self#transform_assignment expr
    | Unary(op,e) -> self#transform_unary expr
    | Relation(e1,op,e2) -> self#transform_relation expr
    | IntOperation(e1,op,e2) -> self#transform_int_operation expr
    | BoolOperation(e1,op,e2) -> self#transform_bool_operation expr
    | IfExpr(e1,e2,e3) -> self#transform_if_expr expr
    | Dot(e1,id) -> self#transform_dot_expr expr
    | Dashed(e) -> self#transform_dashed_expr expr
    | Deadlock -> self#transform_deadlock expr
    | Imply(e1,e2) -> self#transform_imply expr
    | Exists(id,typ,e) -> self#transform_exists expr
    | Forall(id,typ,e) -> self#transform_forall expr
    | CTL(e) -> CTL(self#transform_CTLexpression e)

  method transform_var = function Var (id) 
  -> Var(self#transform_id id) 
  | _ -> self#fail_transform_mismatch "transform_var" "Var"

  method transform_num = function Num n 
  -> Num n
  | _ -> self#fail_transform_mismatch "transform_num" "Num"

  method transform_bool = function Bool b 
  -> Bool b
  | _ -> self#fail_transform_mismatch "transform_bool" "Bool"

  method transform_call = function Call(e, args)
  -> Call (self#transform_expression e, self#transform_expressions args)
  | _ -> self#fail_transform_mismatch "transform_call" "Call"
                
  method transform_array = function Array(e1, e2)
  -> Array (self#transform_expression e1, self#transform_expression e2)
  | _ -> self#fail_transform_mismatch "transform_array" "Array"

  method transform_parenthesis = function Parenthesis (e)
  -> Parenthesis (self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_parenthesis" "Parenthesis"

  method transform_post_increment = function IncPost (e)
  -> IncPost (self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_post_increment" "IncPost"

  method transform_pre_increment = function IncPre (e)
  -> IncPre (self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_pre_increment" "IncPre"

  method transform_post_decrement = function DecPost (e)
  -> DecPost (self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_post_decrement" "DecPost"

  method transform_pre_decrement = function DecPre (e)
  -> DecPre (self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_pre_decrement" "DecPre"

  method transform_assignment = function Assignment (e1, op, e2)
  -> Assignment (self#transform_expression e1, op, self#transform_expression e2)
  | _ -> self#fail_transform_mismatch "transform_assignment" "Assignment"

  method transform_unary = function Unary (op, e)
  -> Unary (op, self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_unary" "Unary"

  method transform_relation  = function Relation (e1, op, e2)
  -> Relation (self#transform_expression e1, op, self#transform_expression e2)
  | _ -> self#fail_transform_mismatch "transform_relation" "Relation"

  method transform_int_operation = function IntOperation (e1, op, e2)
  -> IntOperation(self#transform_expression e1,op,self#transform_expression e2)
  | _ -> self#fail_transform_mismatch "transform_int_operation" "IntOperation"

  method transform_bool_operation = function BoolOperation (e1, op, e2)
  -> BoolOperation(self#transform_expression e1,op,self#transform_expression e2)
  | _ -> self#fail_transform_mismatch "transform_bool_operation" "BoolOperation"

  method transform_if_expr = function IfExpr (e1, e2, e3)
  -> let te = self#transform_expression in IfExpr(te e1, te e2, te e3)
  | _ -> self#fail_transform_mismatch "transform_if_expr" "IfExpr"

  method transform_dot_expr = function Dot (e1, id)
  -> Dot (self#transform_expression e1, self#transform_id id)
  | _ -> self#fail_transform_mismatch "transform_dot_expr" "Dot"

  method transform_dashed_expr = function Dashed (e)
  -> Dashed (self#transform_expression e)
  | _ -> self#fail_transform_mismatch "transform_dashed_expr" "Dashed"

  method transform_deadlock = function Deadlock
  -> Deadlock
  | _ -> self#fail_transform_mismatch "transform_deadlock" "Deadlock"

  method transform_imply = function Imply (e1, e2)
  -> Imply (self#transform_expression e1, self#transform_expression e2)
  | _ -> self#fail_transform_mismatch "transform_imply" "Imply"

  method transform_expressions expressions =
    List.map self#transform_expression expressions
  
  method transform_type (typ : Type.t) =
    let tid = self#transform_id in
    let te = self#transform_expression in
    let tt = self#transform_type in
    let open Type in match typ with
    | Typename(pl,id) -> Typename(pl,tid id)
    | Bool(_) -> typ
    | Int(pl,e1,e2) -> Int(pl, te e1, te e2)
    | Chan(_) -> typ
    | Clock -> typ
    | Void -> typ
    | Scalar(pl,e) -> Scalar(pl, te e)
    | Struct(pl,fields) -> 
        Struct(pl, List.map (fun (tp,id) -> (tt tp, tid id)) fields)
    | Ref(tp) -> Ref(tt tp)
    | Range(tp,rangel) -> 
        Range(tt tp, List.map (fun range -> match range with
          | RangeExpression(e) -> RangeExpression(te e)
          | RangeType(tp') -> RangeType(tt tp') ) rangel ) 

  method transform_initializer init = 
    let te = self#transform_expression in
    let tid = self#transform_id in
    let tinit = self#transform_initializer in
    let open Initializer in match init with
    | Expression(e) -> Expression(te e)
    | Fields(fl) -> 
        Fields( List.map (fun (id,init) -> (some_apply tid id, tinit init)) fl)

  method transform_selectexpressions =
    List.map self#transform_selectexpression
    
  method transform_select {Select.sl; xy} =
    {Select.sl = self#transform_selectexpressions sl;
            xy = some_apply self#transform_xy xy }
  
  method transform_invariant {Invariant. expr; xy } = 
    {Invariant.expr = self#transform_expression expr;
               xy = some_apply self#transform_xy xy}
    
  method transform_update {Update. el; xy } = 
    {Update.el = self#transform_expressions el;
            xy = some_apply self#transform_xy xy}

  method transform_sync {Sync. expr; xy; marker} = 
    {Sync.expr = self#transform_expression expr;
          xy = some_apply self#transform_xy xy;
          marker}

  method transform_guard {Guard. expr; xy} = 
    {Guard.expr = self#transform_expression expr;
           xy = some_apply self#transform_xy xy}
  
  method transform_measure =
    self#transform_expression

  (* --- XML part ------------------------------ *)
  
  method transform_comment (comment:Comment.t) = comment
  
  method transform_xmlid (id:ID.t) = id

  method transform_id (id:ID.t) = id
  
  method transform_xy (xy:XY.t) = xy
  
  method transform_color (color:Color.t) = color
  
  method transform_nail {Nail.pos} = 
    {Nail.pos = self#transform_xy pos}
  
  method transform_nails nails = 
    List.map self#transform_nail nails
  
  method transform_source = self#transform_xmlid
  
  method transform_target = self#transform_xmlid

  method transform_edges transitions = 
    List.map self#transform_edge transitions

  method transform_name {Name. text; xy} = 
     {Name. text = text;
           xy = some_apply self#transform_xy xy; }

  method transform_location 
    ({Location. id; name; urgent; committed; invariant; comment; xy; color; exponentialrate} as loc) =
    {loc with Location. 
      id = self#transform_xmlid id;
      name = name |> some_apply self#transform_name;
      urgent;
      committed;
      invariant = some_apply self#transform_invariant invariant;
      comment = some_apply self#transform_comment comment;
      xy = some_apply self#transform_xy xy;
      color = some_apply self#transform_color color;
      exponentialrate; }

  method transform_locations locations =
    List.map self#transform_location locations

  method transform_parameters = 
    List.map self#transform_parameter

  method transform_statements =
    List.map self#transform_statement

  method transform_chanpriority (priority : int) channel =
    let open Declaration in match channel with
    | DefaultPriority -> channel
    | Channel(id,el) -> Channel(self#transform_id id, 
                                self#transform_expressions el)

  method transform_declarations =
    List.map self#transform_declaration

  method transform_init =
    self#transform_xmlid

  method transform_templates templates =
    List.map self#transform_template templates

  method transform_process {Process.priority; name} =
    {Process.priority = priority; 
             name = self#transform_id name }

  method transform_processes =
    List.map self#transform_process
    
  method transform_progress {Progress.guard; measure} =
    {Progress.guard = some_apply self#transform_guard guard;
              measure = self#transform_measure measure }

  method transform_progresses =
    List.map self#transform_progress

  method transform_system { System.declarations; processes; progresses } =
    { System.
        declarations = self#transform_declarations declarations; 
        processes = self#transform_processes processes; 
        progresses = self#transform_progresses progresses; }

  method transform_imports imports = 
    imports

  method transform_queries ({Queries.queries; nta} as q) =
    (* by default, do not transform nta! *)
    { q with Queries.queries = self#transform_expressions queries; }
  
  method transform = self#transform_nta (** shortcut *)

(******************************************************************************
 * OVERWRITABLE METHODS
 *
 * The methods following below this comment can be overwritten by clients
 * without special care for symbol table updates. 
 * The wrapper methods at the end of this file will take care of those updates.
******************************************************************************)

    method transform_forall_safe = 
        function Forall (id, typ, e)
       -> 
       begin
           let t_id = self#transform_id id in
           let t_type = self#transform_type typ in
           let t_exp = self#transform_expression e in
           Forall (t_id, t_type, t_exp)
          end
       | _ -> self#fail_transform_mismatch "transform_forall_safe" "Forall"


    method transform_exists_safe = function Exists (id, typ, e)
       -> begin
           let t_id = self#transform_id id in
           let t_type = self#transform_type typ in
           let t_exp = self#transform_expression e in
           Exists (t_id, t_type, t_exp)
          end
       | _ -> self#fail_transform_mismatch "transform_exists_safe" "Exists"


     method transform_selectexpression_safe ((id, tp) : Select.single) =
        let t_id = self#transform_id id in
        let t_tp = self#transform_type tp in
        (t_id, t_tp)


      method transform_edge_safe
      ({Edge. id; source; target; select; guard; sync; update; comment;
              nails; xy; color } as trans) =
        let open Edge in
        let t_id = some_apply self#transform_xmlid id in
        let t_source = self#transform_source source in
        let t_target = self#transform_target target in
        let t_select = some_apply self#transform_select select in
        let t_guard   = some_apply self#transform_guard guard in
        let t_sync    = some_apply self#transform_sync sync in
        let t_update  = some_apply self#transform_update update in
        let t_comment = some_apply self#transform_comment comment in
        let t_nails   = self#transform_nails nails in
        let t_xy      = some_apply self#transform_xy xy in
        let t_color   = some_apply self#transform_color color in
        { trans with Edge.
          id      = t_id;
          source  = t_source;
          target  = t_target;
          select  = t_select;
          guard   = t_guard;
          sync    = t_sync;
          update  = t_update;
          comment = t_comment;
          nails   = t_nails;
          xy      = t_xy;
          color   = t_color; }

      method transform_parameter_safe {Parameter.typ; name} =
        let t_typ = self#transform_type typ in
        let t_name = self#transform_id name in
        { Parameter.typ = t_typ; 
                    name = t_name; }

      method transform_statement_safe statement =
        let tid = self#transform_id in
        let te = self#transform_expression in
        let tel = self#transform_expressions in
        let tdl = self#transform_declarations in
        let tstmt = self#transform_statement in
        let tstmts = self#transform_statements in
        let tt = self#transform_type in
        let open Statement in match statement with
        | Empty -> statement
        | Expression(e) -> Expression(te e)
        | Block(dl,stmts) -> Block(tdl dl, tstmts stmts)
        | For(e1,e2,e3,stmt) -> For(tel e1, tel e2, tel e3, tstmt stmt)
        | Foreach(id,typ,stmt) -> Foreach(tid id, tt typ, tstmt stmt)
        | While(el,stmt) -> While(tel el, tstmt stmt)
        | Do(el,stmt) -> Do(tel el, tstmt stmt)
        | If(el,stmt1,stmt2) -> If(tel el, tstmt stmt1, some_apply tstmt stmt2)
        | Break -> statement
        | Continue -> statement
        | Switch(el,cases) ->
            Switch(tel el, List.map (fun case -> match case with
              | Case(e,stmts) -> Case(te e, tstmts stmts)
              | Default(stmts) -> Default(tstmts stmts)) cases) 
        | Return(e) -> Return(some_apply te e)
        | Assert(e) -> Assert(te e)

        method transform_declaration_safe declaration =
          let tel = self#transform_expressions in
          let tt = self#transform_type in
          let tid = self#transform_id in
          let tdl = self#transform_declarations in
          let tinit = self#transform_initializer in
          let tparam = self#transform_parameters in
          let tstmts = self#transform_statements in
          let open Declaration in match declaration with
          | Variable(tp,id,init) 
              -> Variable(tt tp, tid id, some_apply tinit init)
          | Instantiation(inst) 
              -> Instantiation(self#transform_instantiation inst)
          | BeforeUpdate(el) -> BeforeUpdate(tel el)
          | AfterUpdate(el) -> AfterUpdate(tel el)
          | Typedef(tp,id) -> Typedef(tt tp, tid id)
          | Function(tp,id,parameters,declarations,stmts) ->
              Function(tt tp, tid id, tparam parameters, 
                         tdl declarations, tstmts stmts) 
          | ChanPriority(chel) ->
              let priority = ref 0 in
              ChanPriority(List.map (fun channels ->
              priority := !priority + 1;
              List.map (self#transform_chanpriority !priority) channels) chel )


     method transform_template_safe {Template. name; 
                                       parameters; declarations; 
                                       locations; init; edges} =
        
          let t_name = self#transform_name name in
          let t_parameters = self#transform_parameters parameters in
          let t_declarations = self#transform_declarations declarations in
          let t_locations = self#transform_locations locations in
          let t_init = self#transform_init init in
          let t_edges = self#transform_edges edges in
          { Template. name = t_name;
                      parameters = t_parameters;
                      declarations = t_declarations;
                      locations = t_locations;
                      init = t_init;
                      edges = t_edges; }


      method transform_instantiation_safe 
      { Instantiation.instance_id; instance_parameters; template_id; 
                       template_arguments } = 
        let open Instantiation in
        let t_iid = self#transform_id instance_id in
        let t_params = self#transform_parameters instance_parameters in
        let t_tid = self#transform_id template_id in
        let t_args = self#transform_expressions template_arguments in
        { Instantiation.instance_id = t_iid;
                        instance_parameters = t_params;
                        template_id = t_tid;
                        template_arguments = t_args; }


      method transform_nta_safe 
      ( { NTA. imports; declarations; templates; instantiation; system; 
               dtd_version } as nta) =
        let open NTA in
        let t_imports = self#transform_imports imports in
        let t_declarations = self#transform_declarations declarations in
        let t_templates = self#transform_templates templates in
        let t_instantiation = self#transform_declarations instantiation in
        let t_system = self#transform_system system in
        { nta with 
              imports = t_imports;
              declarations = t_declarations;
              templates = t_templates;
              instantiation = t_instantiation;
              system = t_system;
        }

(******************************************************************************
 * Republishing.
 *
 *
******************************************************************************)
      method transform_nta = self#transform_nta_safe
      method transform_instantiation = self#transform_instantiation_safe
      method transform_template = self#transform_template_safe
      method transform_declaration = self#transform_declaration_safe
      method transform_statement = self#transform_statement_safe
      method transform_parameter = self#transform_parameter_safe
      method transform_edge = self#transform_edge_safe
      method transform_selectexpression =
          self#transform_selectexpression_safe
      method transform_exists = self#transform_exists_safe
      method transform_forall = self#transform_forall_safe
  
end

(* NOTE: BETA version: Usage discouraged - please wait for a stable 
 * release of this module.
 *)

class transformer_with_symboltable =
  object (self)
  inherit transformer_without_symboltable
  (* maintain symbol table internally *)
  inherit SymbolTable.frame_worker


    (* Specifies if the symboltable frames should always use the *new* transformed
     * names. This MUST be "true" if "Symboltable.resolve_to_template_qualified"
     * is to be used. Setting this to false avoids some extra work and can be
     * done to improve performance very slightly. 
     * Only set to false in client code if you are sure you are doing no harm. *)
    val mutable new_names_in_symtab = true
    method set_new_names_in_symtab new_value = new_names_in_symtab <- new_value

    (* FIXME: tws_ prefix tries to avoid name clashes for the moment *)
    val mutable tws_global_frame : SymbolTable.frame option = None 
    method tws_global_frame = tws_global_frame


(******************************************************************************
 * NON-OVERWRITABLE METHODS
 *
 * Warning:
 * The methods following below this comment can and should not be overwritten 
 * by subclasses. Consider overwriting the corresponding non-private 
 * "wrapped" method above.
 *
 * This warning can be ignored by overwriting the wrapper methods like
 * "transform_forall" directly. This is discouraged, since it requires
 * client-side handling of symbol table updates.
******************************************************************************)

    method private transform_forall_dangerous = 
        function Forall (id, typ, e)
        -> begin 
            let t_id = if new_names_in_symtab then self#transform_id id else id in 
            let t_type = if new_names_in_symtab then self#transform_type typ else typ in
            self#scope (FT_Forall (Forall (t_id, typ, e)))
            (fun () -> 
                self#transform_forall_safe (Forall (id, typ, e))
                |>
                self#add_symbol t_id (S_Variable t_type)
            );
           end
        | _ -> self#fail_transform_mismatch "transform_forall" "Forall"


    method private transform_exists_dangerous = 
        function Exists (id, typ, e)
        -> begin 
            let t_id = if new_names_in_symtab then self#transform_id id else id in 
            let t_type = if new_names_in_symtab then self#transform_type typ else typ in
            self#scope (FT_Exists (Exists (t_id, typ, e)))
            (fun () -> 
                self#transform_exists_safe (Exists (id, typ, e))
                |>
                self#add_symbol t_id (S_Variable t_type)
            );
           end
        | _ -> self#fail_transform_mismatch "transform_exists" "exists"


     method private transform_selectexpression_dangerous 
            ((id, tp) : Select.single) =
        let (t_id, t_tp) = self#transform_selectexpression_safe (id, tp) in
        self#add_symbol t_id (S_Variable t_tp) (t_id, t_tp)


      method private transform_edge_dangerous
      ({Edge. id; source; target; select; guard; sync; update; comment;
              nails; xy; color} as trans) = 
        let open Edge in
        let t_id = if new_names_in_symtab then some_apply self#transform_xmlid id else id in
        self#scope (FT_Edge {trans with id = t_id }) 
        (fun () -> self#transform_edge_safe trans)


      method private transform_parameter_dangerous 
            ({Parameter.typ; name} as param) =
        let open Parameter in
        let t_param = 
            if new_names_in_symtab then self#transform_parameter_safe param 
            else param 
        in
        self#add_symbol t_param.name (S_Variable t_param.typ) t_param


      method private transform_statement_dangerous (statement : Statement.t) =
        let tid = self#transform_id in
        let te = self#transform_expression in
        let tel = self#transform_expressions in
        let tdl = self#transform_declarations in
        let tstmt = self#transform_statement in
        let tstmts = self#transform_statements in
        let tt = self#transform_type in
        let open Statement in match statement with
        | Empty -> statement
        | Expression(e) -> Expression(te e)
        | Block(dl, stmts) -> self#scope( FT_Block statement) (fun () ->
                let t_dl = tdl dl in
                let t_stmts = tstmts stmts in
                Block (t_dl, t_stmts))
        | For(e1,e2,e3,stmt) -> For(tel e1, tel e2, tel e3, tstmt stmt)
        | Foreach(id,typ,stmt) 
          -> let t_id = if new_names_in_symtab then tid id else id in
             let t_typ = if new_names_in_symtab then tt typ else typ in
             self#scope (FT_Foreach statement) 
             (fun () -> self#add_symbol t_id (S_Variable t_typ) ();
              let t_stmt = tstmt stmt in Foreach(t_id, t_typ, t_stmt))
        | While(el,stmt) -> While(tel el, tstmt stmt)
        | Do(el,stmt) -> Do(tel el, tstmt stmt)
        | If(el,stmt1,stmt2) -> If(tel el, tstmt stmt1, some_apply tstmt stmt2)
        | Break -> statement
        | Continue -> statement
        | Switch(el,cases) ->
            Switch(tel el, List.map (fun case -> match case with
              | Case(e,stmts) -> Case(te e, tstmts stmts)
              | Default(stmts) -> Default(tstmts stmts)) cases) 
        | Return(e) -> Return(some_apply te e)
        | Assert(e) -> Assert(te e)


      method private transform_declaration_dangerous declaration =
        let tel = self#transform_expressions in
        let tt = self#transform_type in
        let tid = self#transform_id in
        let tdl = self#transform_declarations in
        let tinit = self#transform_initializer in
        let tparam = self#transform_parameters in
        let tstmts = self#transform_statements in
        let open Declaration in match declaration with
        | Variable(tp,id,init) 
          -> let t_tp   = tt tp in
             let t_id   = tid id in
             let t_init = some_apply tinit init in
             self#add_initializer t_id t_init;
             self#add_symbol t_id (S_Variable t_tp) 
             (Variable (t_tp, t_id, t_init))
        | Instantiation(inst) 
          -> Instantiation(self#transform_instantiation inst)
        | BeforeUpdate(el) -> BeforeUpdate(tel el)
        | AfterUpdate(el) -> AfterUpdate(tel el)
        | Typedef(tp,id) 
          -> let t_tp = tt tp in
             let t_id = tid id in
             self#add_symbol t_id (S_Typedef t_tp) (Typedef (t_tp, t_id))

        | Function(tp,id,parameters,declarations,stmts) 
          -> let t_tp = tt tp in let (t_tp, t_id, t_param, t_dl, t_stmts) = 
             self#scope (FT_Function 
                        (Function (t_tp, id, parameters, declarations, stmts)))
             (fun () ->
                 let t_tp = tt tp in
                 let t_id = tid id in
                 let t_param = tparam parameters in
                 let t_dl = tdl declarations in
                 let t_stmts = tstmts stmts in
                 (t_tp, t_id, t_param, t_dl, t_stmts) 
              )
              in self#add_symbol t_id (S_Variable t_tp) 
                 (Function (t_tp, t_id, t_param, t_dl, t_stmts))
              
        | ChanPriority(chel) ->
            let priority = ref 0 in
            ChanPriority(List.map (fun channels ->
              priority := !priority + 1;
              List.map (self#transform_chanpriority !priority) channels) chel )


      method private transform_template_dangerous ({Template. name; 
                                   parameters; declarations; 
                                   locations; init; edges} as tmpl) =
          let open Template in
          let t_name = if new_names_in_symtab then self#transform_name name
          else name in
          (self#frame_push ~frame_type:(FT_Template {tmpl with name=t_name});
          let current_frame : frame = self#frame in
          let t_tmpl = self#transform_template_safe tmpl in
          self#frame_pop t_tmpl
          |> self#add_symbol t_name.Name.text
             (S_Template (t_tmpl.parameters, (current_frame :> SymbolTable.t))))


      method private transform_instantiation_dangerous
      ({ Instantiation.instance_id; instance_parameters;template_id; 
       template_arguments } as instantiation) = 
        let open Instantiation in
        let t_insid = if new_names_in_symtab then self#transform_id instance_id
        else instance_id in
        self#scope (FT_Instantiation {instantiation with instance_id = t_insid}) 
        (fun () -> self#transform_instantiation_safe instantiation)


      method private transform_nta_dangerous
      ( { NTA. imports; declarations; templates; instantiation; system; 
               dtd_version } as nta) =
        begin
            self#frame_push ~frame_type:(FT_NTA nta);
            let t_nta = self#transform_nta_safe nta in
            (tws_global_frame <- Some self#frame;
            t_nta
            |> self#frame_pop)
        end



(******************************************************************************
 * Republishing.
 *
 *
******************************************************************************)
      method! transform_nta = self#transform_nta_dangerous
      method! transform_instantiation = self#transform_instantiation_dangerous
      method! transform_template = self#transform_template_dangerous
      method! transform_declaration = self#transform_declaration_dangerous
      method! transform_statement = self#transform_statement_dangerous
      method! transform_parameter = self#transform_parameter_dangerous
      method! transform_edge = self#transform_edge_dangerous
      method! transform_selectexpression =
          self#transform_selectexpression_dangerous
      method! transform_exists = self#transform_exists_dangerous
      method! transform_forall = self#transform_forall_dangerous
end

