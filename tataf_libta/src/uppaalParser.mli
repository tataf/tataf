(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(**
Parser functions for Uppaal XML files (XTA 4), Uppaal-query-files (.q) and
additional parser functions to only some sub-elements of the Uppaal grammar.

This module contains the XML parser, needed to read xml files in XTA 4 format.
The parser will perform a DTD-validation on the input XML-file, before the 
parsing real parsing. If this validation fails, the parser will not run.  
The parser for the XTA part of the Uppaal grammar is defined in 
{!UppaalXtaParser}, the lexer in {!UppaalXtaLexer}.  

{2 Parse example}
Parse an XML file:
{[
  let nta : UppaalSystem.NTA.t = parse_xta4_file "my_uppaal_file.xml"
]} 
Parse a query file:
{[
  let queries : UppaalSystem.Queries.t = parse_queries_file "my_qyeries.q"
]}
*)

(**
  Default exception on parse errors.
*)
exception Parse_error of Lexing.position * Lexing.position * string * string


(******************************************************************************)
(**
  {2 XTA Parsing}
  
  Functions to parse strings in XTA 4 format into {!UppaalSystem} Types. These
  are used to parse the raw XML strings into the corresponding Uppaal-types.
*)

(**
  [parse_expression s] parses [s] into the expression-type.
*)
val parse_expression : ?symbol_table:SymbolTable.frame 
                    -> string 
                    -> UppaalSystem.Expression.t

(**
  [parse_invariant s] parses [s] into the invariant-type. 
  
  It is used to parse the [<label kind="invariant">...</label>] 
  elements of locations.
  If the input is empty (or contains only comments), then [None] is
  returned.
*)
val parse_invariant : ?symbol_table:SymbolTable.frame 
                   -> string 
                   -> UppaalSystem.Invariant.t option

(**
  [parse_guard s] parses [s] into the guard-type.
  
  It is used to parse the [<label kind="guard">...</label>] elements 
  of transitions.
*)
val parse_guard : ?symbol_table:SymbolTable.frame 
               -> string 
               -> UppaalSystem.Guard.t option

(**
  [parse_sync s] parses [s] into the synchronization-type.
  
  It is used to parse the [<label kind="synchronisation">...</label>] elements
  of transitions.
*)
val parse_sync : ?symbol_table:SymbolTable.frame 
              -> string 
              -> UppaalSystem.Sync.t option

(**
  [parse_assignment s] parses [s] into the update-type.
  
  It is used to parse the [<label kind="assignment">...</label>] elements
  of transitions.
*)
val parse_assignment : ?symbol_table:SymbolTable.frame 
                    -> string 
                    -> UppaalSystem.Update.t


(**
  [parse_parameter s] parses [s] into the parameter-type.
  
  It is used to parse the [<parameter>] elements within templates.
*)
val parse_parameter : ?symbol_table:SymbolTable.frame 
                      -> string 
                      -> UppaalSystem.Parameter.t list (*single list*)

(**
  [parse_declarations s] parses [s] into the Declarations.t type.
  
  It is used to parse the [<declarations>...</declarations>] elements.
*)
val parse_declarations : ?symbol_table:SymbolTable.frame 
                      -> string 
                      -> UppaalSystem.Declaration.t list

(**
  [parse_system s] parses [s] into the System.t type.
  
  It is used to parse the [<system>...</system>] element.
*)
val parse_system : ?symbol_table:SymbolTable.frame 
                -> string 
                -> UppaalSystem.System.t

(**
  TODO
*)
val parse_queries_file : ?symbol_table:SymbolTable.frame
                -> string 
                -> UppaalSystem.Queries.t

(**
  TODO (QE-specific stuff)
*)

val parse_qeClasses : string -> 
  (UppaalSystem.ID.t * UppaalQeClasses.TemplateQualifiedIDSet.t
                     * UppaalQeClasses.TemplateQualifiedIDSet.t) list

(******************************************************************************)
(**
  {2 XML Parsing}
  
  Use one of these functions to parse Uppaal systems in the XTA4 format into
  {!UppaalSystem} types.
*)

(** 
  [parse_xta ?omit_xta s] parses the string [s] into the {!UppaalSystem.NTA} 
  format.
  
  @param omit_xta Specifies, if the XTA elements of the XML
  file should be parsed into types. If [true], then theses elements are left
  untouched and will be represented as strings (raw); defaults to [false].
*)
val parse_xta4 : ?omit_xta:bool -> string -> UppaalSystem.NTA.t

(**
  [parse_xta4_file ?omit_xta f] parses the file [f] into the {!UppaalSystem.NTA}
  format. [f] must be a valid filename of an Uppaal XTA4 file (XML format). 
  This function will raise an exception, when some IO operations fail.

  @param omit_xta Specifies, if the XTA elements of the XML
  file should be parsed into types. If [true], then theses elements are left
  untouched and will be represented as strings (raw); defaults to [false].
*)
val parse_xta4_file : ?omit_xta:bool -> string -> UppaalSystem.NTA.t
