#! /bin/sh -ex
#
# Copyright (C) 2021 University of Freiburg
#
# This file is part of tataf.
#
# Tataf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Tataf is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with tataf.  If not, see <https://www.gnu.org/licenses/>.

opam switch 4071 || exit 27

eval `opam env --shell=sh`

BINDIR=./bin
LIBDIR=./lib

mkdir -p $BINDIR
mkdir -p $LIBDIR

MK_BINDIR="$BINDIR"
MK_LIBDIR="../../$LIBDIR"

OCAMLPATH="$MK_LIBDIR:$OCAMLPATH"
export OCAMLPATH
OCAMLFIND_INSTFLAGS="-destdir $MK_LIBDIR"
export OCAMLFIND_INSTFLAGS
OPAMCLI=2.0
export OPAMCLI

PATH="`pwd`/$MK_BINDIR:$PATH"

for lib in libcore libuxu libform libta libtatk libtaqe; do
  make -C tataf_$lib/src MK_BINDIR="$MK_BINDIR" libinstall
done

for tool in tatk tase taqe; do
  make -C tataf_$tool/src MK_BINDIR="$MK_BINDIR"
  cp -va tataf_$tool/src/tataf_$tool "$MK_BINDIR"
done

