(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Printf
open Tataf_libform.Common
open Tataf_libform.Form
open Tataf_libform.FormUtil
open Tataf_libform.PrintForm
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libtatk.UppaalExpressionEvaluation
open Tataf_libta.UppaalWalker
open UppaalWalkerToForm
open Tataf_libta.UppaalPrint
open Form
open Guard
open Tataf_libta.UppaalParser
open Printf
open Tataf_libta.UppaalTransformer
open Edge
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libcore.Util

let dbg = Tataf_libcore.Debug.register_debug_module "DetectPrePostDelay" 

(*----------------------------------------------------------------------------*)

(** Walker - sanity checks:

   - require edge IDs 
*)
class require_edge_ids_walker =
  object (self)

  inherit Tataf_libta.UppaalWalker.bool_walker as super

  method! visit_edge {Edge. id; _} =
    match id with
    | None -> false
    | _    -> true

  method go = super#visit
end

(*----------------------------------------------------------------------------*)

class lookup_typeString nta suffix =
  object (self)
      inherit [string list] walker ["(declare-fun d () Real)\n"] ~concat:(@)

  method resolve_type id = 
      let is_int_type = if((Tataf_libta.SymbolTable.is_int self#frame id) || 
                            Tataf_libta.SymbolTable.is_int_ref self#frame id)
                   then true else false in
      let is_bool_type = if((Tataf_libta.SymbolTable.is_bool self#frame id) || 
                             Tataf_libta.SymbolTable.is_bool_ref self#frame id)
                   then true else false in
      let is_real_type = if((Tataf_libta.SymbolTable.is_clock self#frame id) || 
                             Tataf_libta.SymbolTable.is_clock_ref self#frame id)
                   then true else false in
      if (is_int_type) then 
          (* Imprecise hack: all int variables are now declared as Real-types.
           * Will be superseded later, as this makes our analysis lose precision, 
           * however this is sound, as it will never lead to a detection of an 
           * edge that is not really pre-/post delayed. 
           *) 
          ["(declare-fun " ^ id^ "() Real)\n"; 
          "(declare-fun " ^id^"_"^suffix^"() Real)\n"]
      else 
          if(is_bool_type) then
              ["(declare-fun " ^ id^ "() Bool)\n";
               "(declare-fun " ^id^"_"^suffix^"() Bool)\n"]
          else
            if (is_real_type) then
                ["(declare-fun " ^ id^ "() Real)\n"; 
                 "(declare-fun " ^id^"_"^suffix^"() Real)\n"]
            else begin
                dbg#ldebug_endline 2 ("Could not deduce type for variable 
                with id " ^id^  ": Edge not checked.");
                ["(declare-fun " ^ id^ "() Real)\n"]
            end


  method getTypes e =
      match e with
      | Var id         -> self#resolve_type id
      | Num n          -> []
      | Bool b         -> []
      | Call (_,_)     -> failwith "No calling supported."
      | Array (_,_)    -> failwith "No arrays supported." 
      | Parenthesis e  -> self#getTypes e
      | IncPost e      -> self#getTypes e
      | IncPre e       -> self#getTypes e
      | DecPost e      -> self#getTypes e
      | DecPre e       -> self#getTypes e
      | Assignment (e1,_,e2)    -> self#getTypes e1 @self#getTypes e2
      | Unary(UnaryMinus,e)   -> self#getTypes e
      | Unary(UnaryNot,e)     -> self#getTypes e
      | Unary(UnaryPlus,e)    -> self#getTypes e
      | Relation(e1,Lt,e2)    -> self#getTypes e1@ self#getTypes e2; 
      | Relation(e1,LtEq,e2)  -> self#getTypes e1@ self#getTypes e2; 
      | Relation(e1,Eq,e2)    -> self#getTypes e1@ self#getTypes e2; 
      | Relation(e1,NotEq,e2) -> self#getTypes e1@ self#getTypes e2;
      | Relation(e1,GtEq,e2)  -> self#getTypes e1@ self#getTypes e2;
      | Relation(e1,Gt,e2)    -> self#getTypes e1@ self#getTypes e2;
      | Relation(e1,_,e2)     -> failwith "'>?' and '<?' not supported."
      | IntOperation(e1,Plus,e2)    -> self#getTypes e1@ self#getTypes e2;
      | IntOperation(e1,Minus,e2)   -> self#getTypes e1@ self#getTypes e2;
      | IntOperation(e1,Mult,e2)    -> self#getTypes e1@ self#getTypes e2;
      | IntOperation(e1,Div,e2)     -> self#getTypes e1@ self#getTypes e2;
      | BoolOperation(e1,AndAnd,e2) -> self#getTypes e1@ self#getTypes e2;
      | BoolOperation(e1,OrOr,e2)   -> self#getTypes e1@ self#getTypes e2;
      | IfExpr(_,_,_)               -> failwith "No if-expression supported."
      | Dot(_,_)                    -> failwith "Currently dot expressions not 
                                                 supported."
      | _                           -> failwith "Unsupported (sub)expression."

  method! visit_guard (g:Guard.t) = 
      let e = g.expr in
      self#getTypes e
  
      method! visit_update {Update.el; xy} =
      let open Tataf_libta.UppaalSystem.Update in
      (List.flatten (List.map (self#getTypes) el))
      

end



(*----------------------------------------------------------------------------*)
  (**
    This is a definition of a basic procedure to detect pre-/post delay in
    networks of timed automata.
  *)

  class detectPrePostDelay nta prime_suffix =
    object (self)
      inherit [Edge.t list] Tataf_libta.UppaalWalker.walker [] ~concat:(@)

    val mutable edgeLookup : lookup = (new lookup nta)

    method call_z3 f suffix =
          let z3_binary =
              (* Check if environment variable with path to z3 binary is set *)
              try Sys.getenv("TATAF_Z3_BINARY") 
              with 
              Not_found -> 
                  (* env var with path to z3 binary is not set, attempt to
                     guess based on $0 *)
                  dbg#info("TATAF_Z3_BINARY environment variable not set, "
                            ^ "guessing path to z3..."); 
                  let dirname = Filename.dirname Sys.argv.(0) in
                  let z3_guess = (if (dirname = ".") then
                                    ""
                                  else
                                    dirname ^ "/") ^ "z3" in
                  dbg#info( "..., hmm, ..., let's try " ^ z3_guess ); 
                  z3_guess
          in
          let typelookup = (new lookup_typeString nta suffix) in
          let smt2Declaration = typelookup#visit nta in
          let smt2_str = smt2_of_form f smt2Declaration in
          (* debuglevel >= 2: dump Z3 task *)
          dbg#ldebug_endline 2 ("Z3 task:\n" ^ smt2_str);
          let formula_file_path = Filename.temp_file "formula_" ".smt" in
          let _ = Tataf_libcore.ExtString.string_to_file formula_file_path smt2_str in
          let z3_command = z3_binary ^ " -smt2 " ^ formula_file_path in
          let exit_code = Sys.command (z3_command ^ " 1>&2") in
          (* check whether the command terminates normally *)
          (* Note: Currently, tataf will print out 'sat' on failure exit, thus 
           * we explicitly check for the error code and fail if z3 does not 
           * work properly *)
          match exit_code with
          |0 -> let result_list = Tataf_libcore.Util.call_and_parse_output z3_command in
                 let cmp = (fun x -> match x with
                  | "unsat" ->  true
                  | "sat"   ->  false
                  | _       -> dbg#ldebug_endline 2 ("could prove neither 
                                                      unsat nor sat");false)
                 in if(List.exists(cmp) result_list) then true else false
          |_ -> failwith("Error occured in call to z3 binary, exit code: " ^
          string_of_int(exit_code))

    (* Method to check whether any path starting at 'edge' 
     * must be delayed.
     *)
    method enforces_delay (out_edges : Edge.t list) (edge : Edge.t) = 
      (* Edge guard to corresponding form *)
      let guard_form edge = 
          match edge.guard with
          | None -> mk_true
          | _    -> let g = unsome edge.guard 
                    in unsome(to_some_form(g.Guard.expr))
      in
      (* Turn a constant assignment into a corresponding equality 'form' *)
      let assignment_form expr =
          match expr with
          | Assignment(Var x, Assign, Num n) -> if 
              (Tataf_libta.SymbolTable.is_clock (unsome(edgeLookup#global_frame)) x
                        ||
               Tataf_libta.SymbolTable.is_clock_ref (unsome(edgeLookup#global_frame)) x)
                                                then
                  mk_eq(mk_var (x^"_"^prime_suffix), 
                  mk_real(float_of_int(n)))
                                                else
                  mk_eq(mk_var (x), mk_real(float_of_int(n)))
          | _ -> mk_true                                
      in 
      let in_guard = guard_form edge in
      (* Turn all resets of the ingoing edge into a 'form' and keep track of
       * the clocks that are being reset in 'reset_vars'
       *)
      let processResets edge =
          let has_clock_type var =
              let f = unsome(edgeLookup#global_frame) in
              Tataf_libta.SymbolTable.is_clock f var || Tataf_libta.SymbolTable.is_clock_ref f var
          in
          let get_reset_var reset_expr =
              match reset_expr with
              | Assignment(Var x, Assign, Num n) -> if has_clock_type x then
                                                        [x]
                                                    else
                                                        []                
              | _                                -> []
          in
          match edge.update with
          | None      -> (mk_true, []) (* No variables are reset *)
          | Some u    -> (let open Tataf_libta.UppaalSystem.Update in
                         let reset_expressions = u.el in
                         (* Turn all reset expressions into 'form' types *)
                         let reset_form = 
                             List.fold_left 
                                 (fun a b -> mk_and[a; b]) mk_true 
                                 (List.map assignment_form 
                                 reset_expressions)
                         in
                         (* Keep track of all variables that are reset 
                          * (must be renamed later *)
                         let reset_vars = 
                             List.flatten (List.map (fun a -> get_reset_var a) 
                             reset_expressions)
                        in (reset_form, reset_vars)
                        )
      in
      let (reset_form, reset_vars) = processResets edge in
      (* Construct conjunction from outgoing guards, renaming all variables
       * that are reset in the ingoing edge reset. *)
      let outgoingGuardFormula edge =
          let rec rename_vars (form:form) vars =
              let rename_list l =
                  List.map (fun a -> rename_vars a vars) l
              in
              match form with
              | Var v -> if List.mem v vars then (mk_var (v^"_"^prime_suffix))
                         else mk_var v
              | Const (BoolConst true) -> Const (BoolConst true)
              | App(Const Not, [f]) -> 
                      App(Const Not, [rename_vars f vars])
              | App(Const Or,fs) -> 
                      App(Const Or, rename_list fs)
              | App(Const And,fs) -> 
                      App(Const And, rename_list fs)
              | App(Const Impl,[f1;f2]) -> (* infx f1 " --> " f2 *)
                      App(Const Impl, rename_list [f1;f2])
              | App(Const Ite,[f1;f2;f3]) -> 
                      App(Const Ite, rename_list [f1;f2;f3])
              | App(Const Eq,[f1;f2]) -> (* infx f1 " = " f2 *)
                      App(Const Eq, rename_list [f1;f2])
              | Const (k) -> Const (k)
              | App(Const Lt,[f1;f2]) -> 
                      App(Const Lt, rename_list [f1;f2])
              | App(Const LtEq,[f1;f2]) -> (* infx f1 " <= " f2 *)
                      App(Const LtEq, rename_list [f1;f2])
              | App(Const Gt,[f1;f2]) -> 
                      App(Const Gt, rename_list [f1;f2])
              | App(Const GtEq,[f1;f2]) -> 
                      App(Const GtEq, rename_list [f1;f2])
              | App(Const UMinus,[f]) -> 
                      App(Const UMinus, [rename_vars f vars])
              | App(Const Plus,[f1;f2]) -> 
                      App(Const Plus, rename_list [f1;f2])
              | App(Const Minus,[f1;f2]) ->
                      App(Const Minus, rename_list [f1;f2])
              | App(Const Mult,[f1;f2]) -> 
                      App(Const Mult, rename_list [f1;f2])
              | App(Const Div,[f1;f2]) -> 
                      App(Const Div, rename_list [f1;f2])
              | App(Const Mod,[f1;f2]) ->
                      App(Const Mod, rename_list[f1;f2])
              | TypedForm(TypedForm(f1,t1),t2) -> 
                      TypedForm(TypedForm(rename_vars f1 vars, t1), t2)
              | _ -> failwith("UNSUPPORTED EXPRESSION IN 'rename vars'")
          in
          let _ = edgeLookup#visit_edge edge in 
          (* NOTE: This is bad (hacked). I would like to use the local frame 
           * and have it use push/pop to get to the global frame where
           * necessary, but this is not working because the parent of the local
           * frame is not the global frame *)
          let borrowed_frame = unsome(edgeLookup#global_frame) in
          match edge.guard with
          | None      -> mk_true
          | _         -> (let g = unsome edge.guard in
                             rename_vars (unsome (enhance_guard_expression 
                             g.Guard.expr borrowed_frame)) reset_vars
                         )
      in
      let out_guards = List.map (fun x -> outgoingGuardFormula x) out_edges in
      (* Function to build formulae of the kind: g1 \/ g2 (g1, g2 guards) 
       * while handling the corner case of: false \/ false accurately in our
       * setting *)
      let or_guards_safely = (fun a b -> if a == mk_false && b == mk_true 
                                         then mk_true else mk_or[a;b]
                             )
      in
      let out_formula = List.fold_left (or_guards_safely) mk_false out_guards in

      (* Build conjunction of resets, in_guard and all outgoing_guards (with
       * renamed variables, and add the constraint "d >= 0" *)
      let f = mk_and[mk_and[mk_and[in_guard; reset_form]; out_formula]; 
      mk_gteq(mk_var"d", mk_real(0.))] in
      (* Negate the formula above, since we want to prove its validity using 
       * a satisfiability check *)
      let form = mk_not(mk_impl(f, mk_gt(mk_var("d"), mk_real(0.)))) in
      let _ = dbg#ldebug_endline 2 ("Final formula " 
                                    ^ string_of_form form) in
      (* Call z3 and process the result of the call: True = form is unsat
       * => f is valid 
       *)
      let result = self#call_z3 form prime_suffix in
      match result with
      | true -> true
      | _    -> false
      


    method isPredelayed e =
      let ingoingEdges_source = edgeLookup#edges_in e.Edge.source in
      (* If there are no edges entering source, there is predelay *)
      if (List.length ingoingEdges_source = 0) then
          true
      else if (List.exists self#has_unsupported_update ingoingEdges_source)
      then false 
      else
      if (List.exists self#has_unsupported_update ingoingEdges_source)
      then false
      else
      let outgoingEdges_Source = edgeLookup#edges_out e.Edge.source in
      let is_predelayed = 
          List.for_all (self#enforces_delay outgoingEdges_Source) 
                       ingoingEdges_source
      in (dbg#ldebug_endline 2 ("Z3 SAYS PREDELAY: "
                                 ^string_of_bool(is_predelayed)); is_predelayed)
      
    method isPostDelayed e =
        let outgoingEdges_Target = edgeLookup#edges_out e.Edge.target in
        if (List.length outgoingEdges_Target) = 0 then true
        else
        let ingoingEdges_Target = edgeLookup#edges_in e.Edge.target in
        let is_postdelayed =
            List.for_all (self#enforces_delay outgoingEdges_Target) 
                         ingoingEdges_Target
      in (dbg#ldebug_endline 2 ("Z3 SAYS POSTDELAY: "^string_of_bool(is_postdelayed)); is_postdelayed)

    (* These are asserts to ensure, that the edge does not have 
     * unsupported elements (actions = update or select clauses).
     * This serves the purpose to avoid spurious detections, because actions 
     * can turn a ppd edge into a non-ppd edge (e.g. through function applications 
     * on clock variables *)
    method has_unsupported_update (edge:Edge.t) =
        let unsupported_update_ldebug edge lvl msg =
            dbg#ldebug_endline lvl ((unsome edge.id) 
                                    ^ ": Edge not checked, " ^ msg)
        in
        match edge.update with
        | None   -> false
        | Some u -> let open Tataf_libta.UppaalSystem.Update in 
                    if (List.length u.el) = 0 then false (* empty updates 
                                                           are no problem
                                                         *)
                    else if (List.length u.el) = 1 then
                      if is_reset (List.hd u.el) then
                        false
                      else begin
                        unsupported_update_ldebug edge 2
                          "SINGLE UPDATE MUST ASSIGN 0!";
                        true (* only resets supported *)
                      end
                    else begin
                      unsupported_update_ldebug edge 2
                        "MULTIPLE UPDATES NOT SUPPORTED!";
                      true (* multiple updates not supported *)
                    end
                
                (* NOTE: This code is useful, if multiple resets are
        allowed, but currently we only want to support a single reset of a
        single clock! *)(*not (let open UppaalSystem.Update in 
                    List.fold_left (fun a b -> a && b) true 
                    (List.map (fun e -> is_reset e) u.el)
                    )*)

    method uses_urgent_location {Edge.source; target; _} =
        let source_loc =  edgeLookup#location source in
        let target_loc = edgeLookup#location target in
        Location.is_urgent source_loc || Location.is_urgent target_loc


    (* For each edge visited, determine whether it is pre- and post delayed. *)
    (* Look up relevant edges to determine whether the current edge is
       pre-/post delayed.*)
    method! visit_edge (edge:Edge.t) =
      if self#uses_urgent_location edge then begin
        dbg#ldebug_endline 2 ((unsome edge.id) ^ ": USES URGENT LOCATION");
        []
      end
      else if not (self#isPredelayed edge) then begin
        dbg#ldebug_endline 2 ((unsome edge.id) ^ ": NOT PREDELAYED");
        []
      end
      else if not (self#isPostDelayed edge) then begin
        dbg#ldebug_endline 2 ((unsome edge.id) ^ " NOT POSTDELAYED");
        []
      end
      else if (self#has_unsupported_update edge) then
        []
      else begin
        dbg#ldebug_endline 2 ((unsome edge.id) ^ ": PRE/POSTDELAYED");
        [edge]
      end

  end

let getPrePostDelayedEdges_internal nta output prime_suffix = 
  (* check whether all transitions have IDs, before doing any work *)
  if not ((new require_edge_ids_walker)#go nta) then
    failwith("DetectPrePostDelay: all transitions must have XML IDs.\n"
      ^ "          Consider running \"utataf transform addtransitionids\" first.");
  (new detectPrePostDelay nta prime_suffix)#visit nta

let getPrePostDelayedEdges source output prime_suffix = 
  let nta = Tataf_libta.UppaalParser.parse_xta4_file source in
  let prePostDelayedEdges =
        getPrePostDelayedEdges_internal nta output prime_suffix in
  (* Output results of analysis *)
  if prePostDelayedEdges == [] then
    dbg#info "No Pre-/ Postdelayed Edges found."
  else begin
    (* Ensure all transitions actually have ids.. *)
    let is_none = function None -> true | _ -> false in
    if List.exists (fun e -> is_none e.id) prePostDelayedEdges then
        (* Avoid giving generic 'util tried to deoptionify None' message 
         * when some edges do not have id's: complain with style instead!
         * Note that we sanity-checked beforehand -- so if this message
         * is ever triggered, there is a bug or somebody fiddled with
         * sanity checking.
         *)
        failwith( "DetectPrePostDelay: fatal error, "
                  ^ "a pre/post delayed edge has no ID.");
    (* Unsome cannot fail for transition IDs, which we assume to exist. *)
    let id_of_edge e = (unsome(e.id)) in
    let ids_string =
          Tataf_libcore.ExtString.str_list ~sep:"," (id_of_edge) prePostDelayedEdges in

    Tataf_libcore.ExtString.string_to_file output ids_string
  end

