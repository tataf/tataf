(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.SymbolTable
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Declaration
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Statement
open Tataf_libta.UppaalSystem.Update
open Tataf_libcore.Util

let dbg = Tataf_libcore.Debug.register_debug_module "DetectComplexEdgesFunctions" 


class complex_edge_detector =
  object (self)
  inherit [Edge.t list] Tataf_libta.UppaalWalker.walker [] ~concat:(@) as super

  val mutable global_function_decls = []
  val mutable local_function_decls  = []

  method private get_func_id = function
      | Function (typ, id, params, _, statements) -> id
      | _ -> failwith("DetectComplexEdgesFunctions: 'get_func_id' called "
                      ^ "with argument that was not a function..")


  method private get_reset_clocks = function
      | Function (typ, id, params, _, statements)
        -> begin
            let rec only_plain_statements = function
                | [] -> true
                | (For _) :: tl -> false
                | (Foreach _) :: tl -> false
                | (While _)  :: tl -> false
                | (If _) :: tl -> false
                | (Do _) :: tl -> false
                | (Block (_, sl)) :: tl 
                  -> (only_plain_statements sl) && (only_plain_statements tl)
                | _ :: tl -> only_plain_statements tl
            in if not (only_plain_statements statements) then
                []
            else
                let rec get_assignments assignments = function
                    | [] -> assignments
                    | Block (_, sl) :: tl 
                      -> get_assignments (assignments @ (get_assignments [] sl)) tl
                    | Expression (Assignment(Var x, _, _) as e) :: tl 
                      -> get_assignments (e :: assignments) tl
                    | _  -> failwith("DetectComplexEdgesFunctions:"
                                     ^ "Unsupported Assignment ")
                in let assignments = get_assignments [] statements in
                let rec find_reset_clocks no_reset_vars reset_clocks = function
                    | [] -> reset_clocks
                    | Assignment(Var x, Assign, Num 0) :: tl
                      -> if not (List.mem x no_reset_vars) then
                          find_reset_clocks no_reset_vars (x :: reset_clocks) tl
                         else
                          find_reset_clocks no_reset_vars reset_clocks tl
                    | Assignment(Var x, _, _) :: tl
                      -> find_reset_clocks (x :: no_reset_vars) reset_clocks tl
                    | _ :: tl -> find_reset_clocks no_reset_vars reset_clocks tl
                in find_reset_clocks [] [] assignments
           end
      | _ -> failwith("DetectComplexEdgesFunctions: Expected function, "
                      ^ "called with something else..")

      
  method is_reset_clock_fun = function
      | (Function (_)) as fun_decl
        -> ((List.length (self#get_reset_clocks fun_decl) > 0))
      | _ -> false



  method! visit_nta ({NTA.declarations; _ } as nta) =
      global_function_decls <- List.filter self#is_reset_clock_fun declarations;
      self#frame_push ~frame_type:(FT_NTA nta);
      let nta = self#visit_nta_unscoped nta in
      global_frame <- Some self#frame;
      self#frame_pop nta

  method! visit_template ({Template. name; parameters; declarations; 
                                     locations; init; edges} as tmpl) =
      local_function_decls <- List.filter self#is_reset_clock_fun declarations;
       self#visit_name name
    |> self#f' (
        self#frame_push ~frame_type:(FT_Template tmpl);
         let result = 
            self#visit_parameters parameters
         |> self#f' (self#visit_declarations declarations)
         |> self#f' (self#visit_locations locations)
         |> self#f' (self#visit_init init)
         |> self#f' (self#visit_edges edges)
        in
        (* declare type of current_frame explicitly to avoid warning 
         * 'ground coercion is not principal' during compilation. *)
        let current_frame : frame = self#frame in
        self#frame_pop result
        |> self#add_symbol 
             name.Name.text
             (S_Template (parameters, (current_frame :> Tataf_libta.SymbolTable.t)) ) )

  method! visit_edge ({Edge.update; _} as edge) =
      match update with
      | None -> []
      | Some up -> 
        begin
          match up.el with
          | [Call (Var f, args)]
            -> begin
                    dbg#info("Handling function call");
                    if (  (List.exists (fun d -> self#get_func_id d = f) 
                                       local_function_decls)
                       || (List.exists (fun d -> self#get_func_id d = f) 
                                       global_function_decls))

                    then
                        [edge]
                    else []
                end
          | _ -> []
        end
end
