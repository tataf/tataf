(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

let dbg = QeClockReductionCommons.dbg

open Tataf_libta.UppaalQeClasses

open QeClockReductionCommons
open QeClockReductionCommonTransformer
open QeClockReductionSimpleTransformer
open QeClockReductionComplexTransformer
open QeClockReductionQueryTransformer

(**
  {1 QE Clock Reduction}
  
  This module performs the QE Clock Reduction algorithm "K".
*)

(** We validate here whether the qe-clocks input do really exist in the underlying network *)
let validateQEExistence qeClasses nta = 
	let allqevarIds =
    List.fold_left (fun acc ( _ , tqid, _ ) -> 
    							 TemplateQualifiedIDSet.union acc tqid )
                   TemplateQualifiedIDSet.empty
                   qeClasses 
  in
  let allvars = str_list ~sep:"," TemplateQualifiedID.to_str
                                           (TemplateQualifiedIDSet.elements
                                            allqevarIds) 
  in
  dbg#info ("THE QE-CLOCKS: " ^ allvars);
	List.iter (fun (x,y,z) -> dbg#info ("THE ECs: " ^ x)) qeClasses ;
  (* flatten the QE-sets into allqevarIds *) 
  let walker = new Tataf_libta.UppaalWalker.unit_walker in
  let global_frame = walker#visit nta ; Tataf_libcore.Util.unsome walker#global_frame in
  let non_existing_names =
    TemplateQualifiedIDSet.fold
       (fun tqid acc -> 
          match Tataf_libta.SymbolTable.resolve_template_qualified global_frame tqid with
            | None -> TemplateQualifiedIDSet.add tqid acc
            | _    -> acc)
       allqevarIds
       TemplateQualifiedIDSet.empty
  in
  match TemplateQualifiedIDSet.cardinal non_existing_names with
    | 0 -> ()
    | 1 -> let clock = TemplateQualifiedIDSet.choose non_existing_names
                       |> TemplateQualifiedID.to_str
           in
           raise(Tataf_libcore.CmdLine.ValidationError ("Clock " ^ clock ^ " does not exists in NTA"))
    | _ -> let clocks = str_list ~sep:"," TemplateQualifiedID.to_str
                                           (TemplateQualifiedIDSet.elements non_existing_names)
           in 
           raise(Tataf_libcore.CmdLine.ValidationError ("Clocks " ^ clocks ^ " do not exist in
NTA")) 

(*----------------------------------------------------------------------------*)
(**
  {2 This is the module Validator which performs several validations, e.g. no invalid names for qe-clocks}
*)
module Validator = struct
  
  class ntaValidator qeClass =
    object (self)
    inherit Tataf_libta.UppaalWalker.unit_walker
    inherit varGenerator qeClass
    
    val mutable forbiddenVariableNames = IDSet.empty
    val mutable forbiddenTypedefs = IDSet.empty
    
    method raiseValidationError tp id =
      raise (Tataf_libcore.CmdLine.ValidationError 
      ("The name '" ^ qeClassName ^ "' for the equivalence class is invalid, " ^
       "because the generated " ^ tp ^ " '" ^ id ^ " already exists " ^
       "within the given Uppaal model. " ))    
    
    method! visit_expression = function
      | Var(id) when IDSet.mem id forbiddenVariableNames ->
          self#raiseValidationError "variable" id
      | _ -> ()
    
    method! visit_parameter {Parameter.name; _} =
      if IDSet.mem name forbiddenVariableNames 
        then self#raiseValidationError "variable" name
    
    method! visit_declaration = function
      | Declaration.Variable(_,id,_) when IDSet.mem id forbiddenVariableNames ->
          self#raiseValidationError "variable" id
      | Declaration.Typedef(_,id) when IDSet.mem id forbiddenTypedefs -> 
          self#raiseValidationError "typedef" id
      | _ -> ()
    
    initializer
      forbiddenVariableNames <- List.fold_left
        (fun set var -> IDSet.add var set)
        IDSet.empty
        [ _Yrep; _rY; _sY; _Yid; _numClocksY ];
        
      forbiddenTypedefs <- IDSet.singleton _Yids
  end
  
  class qeClassesValidator =
    object (self)
    
    val mutable classNames = IDSet.empty
    val mutable clockNames = TemplateQualifiedIDSet.empty
    
    method raiseInvalidClassName name =
      raise (Tataf_libcore.CmdLine.ValidationError 
      ("Ambiguous definition of equivalence class '" ^ name ^ "'. " ^ 
       "Each equivalence class may only occur once!" ))    
  
    method raiseInvalidClocks clocks = 
      let clockstr = str_list ~sep:", " TemplateQualifiedID.to_str clocks in
      raise (Tataf_libcore.CmdLine.ValidationError 
      ("Invalid equivalence classes. Clocks that appear in more than one " ^
       "class: " ^ clockstr))    
  
    method validate (qeClasses : (ID.t
                                  * TemplateQualifiedIDSet.t
                                  * TemplateQualifiedIDSet.t) list) =
      classNames <- IDSet.empty;
      clockNames <- TemplateQualifiedIDSet.empty;
      List.iter
        (fun (id, clocks, _) ->
          if IDSet.mem id classNames
          then self#raiseInvalidClassName id
          else begin
            classNames <- IDSet.add id classNames;
            (* OBSOLETE? (one clock may appear in only one set) *)
            let dupl_clocks = TemplateQualifiedIDSet.inter clockNames clocks in
            if dupl_clocks <> TemplateQualifiedIDSet.empty
            then self#raiseInvalidClocks (TemplateQualifiedIDSet.elements dupl_clocks)
            else clockNames <- TemplateQualifiedIDSet.union clockNames clocks
          end
        )
        qeClasses
  end
  
  let validateInput qeClasses nta = 
    validateQEExistence qeClasses nta ;
    (* check for duplicate classNames and invalid equivalence classes. *)
    (new qeClassesValidator)#validate qeClasses;
    (* check, if generated variables already defined in the model. *)
    List.iter 
      (fun qeClass -> (new ntaValidator qeClass)#visit nta) 
      qeClasses;
    (* return input nta, to enable chaining of function calls.*)
    nta
end

(**
   Building the set of all tokens. For instance, given the ECs: Y={A0.x,A1.y}, Z={A2.z,A3.w}, 
   we output the set of tokens allEC= {tYA0_x, tYA1_y, tZA2_z, tZA3_w}.
*)

class allTokensSetBuilder (qeClasses : (ID.t
                                  * TemplateQualifiedIDSet.t
                                  * TemplateQualifiedIDSet.t) list)  nta =  
    object (self)
    inherit Tataf_libta.UppaalWalker.unit_walker as super
    
    val lookup = new Tataf_libta.UppaalWalker.lookup nta
  	method lookup              = lookup
  	method location            = lookup#location
  
    val mutable _globalTokensAccumulator = ClockTemplateSet.empty (*stores tuples consisting of the sum of 
  																all tokens wrt. a given equiv. class, and its variable numClocks*)
  																
  	val mutable _globalSimpCountersAccumulator = 
  					ClockTemplateSet.empty (*stores tuples consisting of the sum of 
  																all simple counters wrt. a given equiv. class, and its variable numClocks*)
  	
  	
  	val mutable _setTokensIds = IDSet.empty	 
  																
  	val qeClasses = qeClasses
  	
  	val mutable location_names : string list = [] 

		method get_name_loc id = 
		   match lookup#location id with
          | {Location.name=None;_}        -> id
          | {Location.name=(Some name);_} ->  name.Name.text  
          
	  method! visit_template ({Template.name; locations;_} as template) = 
	  super#visit_template template;
		  List.iter
		  (fun {Location.id; name; _}  ->   
		    location_names <- (template.Template.name.Name.text ^ "." ^ (self#get_name_loc id)) :: location_names) 
		  locations;
	
  	method location_names = location_names

  	method _globalTokensAccumulator 			= _globalTokensAccumulator 	
  	method _globalSimpCountersAccumulator = _globalSimpCountersAccumulator 		
  	method _setTokensIds 									= _setTokensIds
  	
  	val _ChannelsOfComplex: (ExpressionReference.t, ID.t) Hashtbl.t = Hashtbl.create 0; (* contains the channels of any complex edge*) 
  	method _ChannelsOfComplex = _ChannelsOfComplex
  	
  	method private getTokensAllEC =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  											_setTokensIds <- IDSet.add ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock) _setTokensIds)
  										_globalTokensAccumulator;
  	
  	
   (*=======================
     Reset Edge Recognition
   ========================*)

	 method private is_const = function
		| Num(_) -> true
		(*| Var(id) when SymbolTable.is_clock self#frame id -> true*)
		| Var(_) -> true
		| _ -> false 

		(* given guard effectively checks x >= c *)
		method private is_simple_edge_guard x c guard =
       Guard.eq guard x c 

		(* given update updates clock x *)
		method private is_simple_edge_update x update = 
       Update.is update (Assignment(Var(x),Assign,Num(0)))
    
    (* check if target location is urgent of committed*)
  	method private is_target_urgent_committed {Location.urgent; committed; _} =
  	if urgent || committed then false  else true	   
       
  	method private is_simple_reset_edge {Edge.sync; guard; update; source; target; _} =
    match lookup#location source with 
    | {Location.invariant = Some {Invariant.expr; _}; _} ->
      begin match expr with
        | Relation(Var(x),LtEq,c)
        | Relation(c,GtEq,Var(x))
          when self#is_const c  (*check if number of constant*)
          -> Sync.is_tau sync
             && self#is_simple_edge_guard x c guard
             && self#is_simple_edge_update x update
             && self#is_target_urgent_committed (lookup#location target)
        | _ -> false
      end 
    | _ -> false
    
    
    (*Here we collect the channels that appear in complex edges*)    
  	method private check_channel  ({Edge.id; sync; _}) qeClass =
				match sync with 
				| Some { Sync.expr; _} -> Hashtbl.add _ChannelsOfComplex expr  qeClass
				| _ -> ()
    
  method private check_reset_edge edge clocks qeClass =   		
    match edge with 
    | {Edge.id; update = Some {Update.el; _};  source; target; _} as edge ->
       List.iter 
        (fun expr -> match expr with
          (* [x := 0] expected, where x is in X*)
		        | Assignment(Var(x),Assign,Num(0)) 
		            when Tataf_libta.SymbolTable.is_clock self#frame x
		              && TemplateQualifiedIDSet.mem_some
		                   (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)
		                   clocks -> 	 
		          let getTemplateName {Tataf_libta.UppaalQeClasses.TemplateQualifiedID.template; _} = template in
		          let clockAndTemplate = (ClockTemplate.make ~name:(Tataf_libcore.Util.unsome (getTemplateName (Tataf_libcore.Util.unsome 
		                (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)))) ~clock:x ~qeClass:qeClass) in
		                _globalTokensAccumulator  <- 
		                		ClockTemplateSet.add  clockAndTemplate _globalTokensAccumulator;         
              (*edges are not simple, if another reset edge has same source/target location*)
              if (self#is_simple_reset_edge edge = true)  
              	&& (List.length (lookup#edges_out source) = 1)
                && (List.length (lookup#edges_in target)  = 1) 
                then begin 
                  (* We store a tuple, composed of a clock in a simple edge and its respective template *)
		              _globalSimpCountersAccumulator <- 
		                			ClockTemplateSet.add  clockAndTemplate _globalSimpCountersAccumulator
                end
              else (*we assume the edge is complex*)
                	self#check_channel edge qeClass;  
          | _ -> () (* ignore expression in other formats. *)
        ) el 
    | _ -> ()
    
      
  	method! visit_edge edge = 
		  super#visit_edge edge;
		  (*Getting the simple edges of each QEClass*)
    	List.iter (fun (  qeClass  , tqid, qqpd ) -> 
    							 (self#check_reset_edge edge tqid qeClass))
      qeClasses;
      self#getTokensAllEC      
		  
		method! visit nta =
    	super#visit nta;

  	initializer
    	self#visit nta;
  end

(*----------------------------------------------------------------------------*)
(**
  {3 Entry Point}
*)

(** 
  pretty prints an equivalence class 
*)
let print_equivalence_class (lookup : relookup) =
  let id = lookup#qeClassName in
  let clocks = lookup#qeClocks in
  "\nCurrent equivalence class: " ^ 
  id ^ " = {" ^ (str_list ~sep:", " TemplateQualifiedID.to_str
                          (TemplateQualifiedIDSet.elements clocks))
              ^ "}"
  |> dbg#info


(**
  Entry point to perform QE Clock Reduction.
*)
let perform_qeClockReduction modelFilename qeClassesString ppdEdgeIDs 
                                           qFilesIn qFilesOut = 
  dbg#info  "--------------------";  
  dbg#info  "QE Clock Reduction";
  dbg#info ("  Model:    " ^ modelFilename);
  dbg#info ("  qeClocks: " ^ qeClassesString);
  dbg#info  "--------------------";

	
  (*|Validation of QE Clocks|*)
  let qeClasses = Tataf_libta.UppaalParser.parse_qeClasses qeClassesString in  
  let nta_input = 
    modelFilename
    |> Tataf_libta.UppaalParser.parse_xta4_file 
    |> Validator.validateInput qeClasses  
  in
  let nta_input_preprocessed = QeClockReductionRenameClocks.rename_clocks_class  nta_input in
  let listQEClasses = List.map 
  				(fun (a,b,c) ->  a )
  		qeClasses in 
  (*Concatenating the name of the clock with the name of the template. i.e. from T1.x we want to get T1.xT1*)
  let qeClasses_with_tqQEClocks =
  		List.map 
  				(fun (a,b,c) -> (a,
  					(TemplateQualifiedIDSet.fold  
  							(fun ({TemplateQualifiedID.template; name } as temName) set ->  
  								TemplateQualifiedIDSet.add 
  								(TemplateQualifiedID.make template 
  								(name ^ 
  							(Tataf_libcore.Util.unqualify_getting_module (TemplateQualifiedID.to_str temName)))) set) 
  					b 
  					TemplateQualifiedIDSet.empty), c)
  				)
  		qeClasses in 
  (* getting the pre/post delayed edges *)
  (* TODO: Comment in below if ppd_edges should be used
   *
   * if ppdEdgeIDs = "" then ppdEdgeIDs  else string_of_file ppdEdgeIDs;
   *  note that string_of_file (fname : string) function should be created
   *  to read the given file into a string.
   * (it used to exist at tataf_libcore::util.ml but was removed on
   * migrating to 4.02 since its implementation mixed String & Bytes)
  let ppd_id_list = match  ppdEdgeIDs with 
  	| "" -> [] 
    | _  -> List.map String.trim (Util.split_by "," (string_of_file ppdEdgeIDs)) in
  let lookup = (new UppaalWalker.lookup nta_input_preprocessed) in
  let ppd_edges = match ppd_id_list with
  	| [] 	-> []
  	| _ 	-> List.map (lookup#edge) ppd_id_list
    in *)
  (* create for each equivalence class a lookup object *)
  let lookups = List.map
      (fun qeClass -> new relookup qeClass nta_input_preprocessed) 
      qeClasses_with_tqQEClocks
  in 
  let globalTokensSet = new allTokensSetBuilder qeClasses_with_tqQEClocks nta_input_preprocessed 
  in
  let qe_global_clock_expr_lookups = new qe_global_clock_expr_lookup lookups nta_input_preprocessed
  in 
  (* Function to add corresponding query names to nta outputstring. *)
  let add_query_names_to_system_dec q_filenames nta_str =
      if Tataf_libcore.ExtList.list_empty q_filenames then
          nta_str
      else
          let qlist = 
              List.map (fun q -> Tataf_libcore.ExtString.remove_suffix q "q"
                                 |> Filename.basename) 
                        q_filenames 
          in
          let qstring = 
              List.fold_left (fun a b -> a ^ "(" ^ b ^ ")") "" qlist
          in
          let comment_string = "\n//&gt;&gt;" ^ qstring ^ "//&lt;&lt;\n" in
          let pattern = "</system>" in
          let replace_str = comment_string ^ " " ^ pattern in
          let r = Str.regexp_string pattern in
          Str.replace_first r replace_str nta_str
  in
  let glookup = create_global_lookup lookups in
  (*|TRANSFORMATION|*)
  let nta_out =
    nta_input_preprocessed  
    |> add_constructor 
    (*|> insert_labels_for_queries (*enable option for direct insertion of query names into the XML model*)*)
    |> Tataf_libta.UppaalTransformer.foreach lookups (fun nta lookup ->
         print_equivalence_class lookup;
         nta 
         		 |> add_resetter lookup globalTokensSet#_globalTokensAccumulator  
         		 									globalTokensSet#_globalSimpCountersAccumulator 
         		 									listQEClasses
         		 |> add_precursor_follower lookup      		 
        )
    |> Tataf_libta.UppaalTransformer.foreach lookups (fun nta lookup -> 
    			 print_equivalence_class lookup;
           nta 
           		|> add_complex_token_variable lookup  qe_global_clock_expr_lookups 
           		 																			globalTokensSet#_globalTokensAccumulator
           		 																			globalTokensSet#_globalSimpCountersAccumulator
           		|> add_simple_RL_counter_bookkeeping lookup    
         )  	
         		|> block_nonResettingEdges_whichSynchronizeWithComplex	
         																							globalTokensSet#_ChannelsOfComplex	
         																							globalTokensSet#_globalSimpCountersAccumulator
       			|> create_virtual_world glookup globalTokensSet#_globalTokensAccumulator
            |> connect_original_to_virtual_world glookup  qe_global_clock_expr_lookups   
            (*to avoid duplicated tokens in declarations, we insert them outside the foreach loop*)
            |> create_globals_tokens globalTokensSet#_setTokensIds listQEClasses
            |> eliminate_helper_comments
    |> Tataf_libta.UppaalTransformer.foreach lookups (fun nta lookup -> 
    			 print_equivalence_class lookup;
           nta 
           		 |> connect_virtual_to_original_world lookup
           	   |> disconnect_original_to_virtual_world lookup
           	   |> gamma lookup 
           	   |> create_globals lookup  (*all globals but tokens*)
         )        
         			 |> create_priority_over_channels listQEClasses  
    (*|OUTPUT|*)
    |> Tataf_libta.UppaalPrint.print_xta4
    |> add_query_names_to_system_dec qFilesOut
  in
  (*|QUERY HANDLING|*)
  if List.length (qFilesIn) > 0 then begin
    (* Grab the global frame once for all queries. *)
    let symbol_table = glookup#global_frame in
    (* Recursively handle pairs of query input and query output files *)
    let rec handle_qlists qlist_in qlist_out =
        match (qlist_in, qlist_out) with
        | (qFileIn :: tl_in, qFileOut :: tl_out) 
          -> begin 
             qFileIn
            |> Tataf_libta.UppaalParser.parse_queries_file ~symbol_table
            (*|QUERY TRANSFORMATIONS|*)
            |> Tataf_libta.UppaalTransformer.foreach lookups (fun query lookup ->
                query 
                        |> q_gamma lookup glookup nta_input_preprocessed globalTokensSet#_globalTokensAccumulator qe_global_clock_expr_lookups
                        globalTokensSet#location_names
               )
            (*|QUERY OUTPUT|*)
            |> Tataf_libta.UppaalPrint.print_queries
            |> Tataf_libcore.ExtString.string_to_file qFileOut
            end; handle_qlists tl_in tl_out
        | ([], []) -> () (* Done handling the two lists *) 
        | _ -> failwith("QeClockReduction.handle_qlists: list of input query "
        ^ "files and list of output query files appeared to have different "
        ^ "lengths!") 
        (* Error Case.. lists must have had unequal length! *)
    in handle_qlists qFilesIn qFilesOut (* Handle all queries *)
  end;

  print_endline nta_out

















(*----------------------------------------------------------------------------*)
(**
  {2 Other}
*)

(* TODO: muss ne eigene Command-Line Option bekommen; hat mit QE-Trafo *)
(* eigtl. nix mehr zu tun; diese Funktionen hier faerben nur ein       *)

(**
  This transformer will colorize each reset edge. Simple reset edges will
  be "Cyan", complex reset edges "Green". 
*)
let colorize_transitions qeClass nta =
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable
  
  val relookup = new relookup qeClass nta
  
  method! transform_edge ({Edge. id; _} as edge) =
     let new_color = 
       if relookup#in_RE edge 
       then begin
         if relookup#in_CE edge 
          then Some "#00ffff"  (*complex reset edge*)
          else Some "#00ff00"  (*simple reset edge*) 
       end else None         (*no reset edge -> default color*)
     in {edge with Edge.color = new_color}
  end 
  |> fun transformer -> transformer#transform nta


let transformTransitions modelFilename outputfilename =   
  print_endline "--------------------";  
  print_endline "Colorizing Transitions";
  print_endline ("  Model: "  ^ modelFilename);
  print_endline "  Simple Reset Edges:   GREEN";
  print_endline "  Complex Reset Edges:  CYAN";
  print_endline "--------------------";  
  let test_qeClass = 
    "Y = {x,y,z}" 
    |> Tataf_libta.UppaalParser.parse_qeClasses
    |> List.hd
  in
  modelFilename 
  |> Tataf_libta.UppaalParser.parse_xta4_file (*parsing*)
  |> Tataf_libtatk.AddTransitionIds.generate_transition_ids_internal
  |> colorize_transitions test_qeClass
  |> Tataf_libta.UppaalPrint.print_xta4
  |> Tataf_libcore.ExtString.string_to_file outputfilename; (*output to file*)
  print_endline ("done\nFile written to: " ^ outputfilename)
