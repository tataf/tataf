(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Template
open Tataf_libta.UppaalSystem.Name
(* We preprocess an input network in order to differenciate clock names in the whole explicated network*)    
(* Changing from clock to clock_template*) 

(*
  Helper class for create_virtual_world() below -- was once realised by
  inheriting from walker.  To avoid name-clashes with new trafo_with_symtab,
  it is for the moment factored out into into something separate.
*)
class rename_clocks_class_walker =
  object (self)

  inherit Tataf_libta.UppaalWalker.unit_walker as super

  val mutable clocksSet = IDSet.empty
  method clocksSet = clocksSet
  
  (*we fill the set clocksSet at the initialization of this class. Later we use that set in order to 
  make the proper changes on the names of clocks*)
  method! visit_template { Template.name; declarations; edges; locations;_ } = 
		      let open Declaration in
		  		let open Type in
		      List.iter
						(fun decl -> match decl with
						  | Variable(Clock, vid, y) -> clocksSet <- IDSet.add vid clocksSet
						  | _ -> clocksSet <- clocksSet
						) 
				  declarations
end

let rename_clocks_class nta : NTA.t =
  object (self)

  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super 
(*  inherit UppaalWalker.unit_walker as super_walker *)
  
  val mutable clocksSet = IDSet.empty
  
  method private transGuardInvariant expr template_name = 
    let te = self#transGuardInvariant in
     match expr with
    | Relation((Var var),op,(const)) -> 
    					if  (IDSet.mem var clocksSet) then begin
    							Relation((Var (var ^ template_name)),op,(const)) 
    							end
    					else begin
    							Relation((Var (var )),op,(const)) 
    							end
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) -> 
        			Relation(IntOperation((Var (clk1 ^ template_name)),op,(Var (clk2 ^ template_name))),op2,const)
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e template_name)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1 template_name,op,te e2 template_name)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1 template_name,op,te e2 template_name)
    | IntOperation(e1,op,e2) -> IntOperation(te e1 template_name,op,te e2 template_name)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1 template_name,op,te e2 template_name)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1 template_name,te e2 template_name, te e3 template_name)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr
  
  method! transform_template ({ Template.name; declarations; edges; locations;_ } as tmpl) = 
    { tmpl with
        Template.declarations = 
		      (let open Declaration in
		  		let open Type in
		      List.map
						(fun decl -> match decl with
						  | Variable(Clock, vid, y) 
						      -> 	Variable(Clock, (vid ^ name.Name.text ),y) 
						  | _ -> decl
						) 
				  declarations);
        edges = 
								List.map
								(fun ({Edge.update; guard; _} as edge) -> 
									{edge with Edge. 
										 guard = begin match guard with
										 | None -> None
										 | Some g -> Some { g with Guard.
																	        expr = self#transGuardInvariant (g.Guard.expr) ( name.Name.text )}
														end;  
									 update = (match update with		
										 | None -> None
										 | Some u -> Some { u with Update.
										 			el =
										 			List.map
													(fun expr -> match expr with
													| Assignment(Var clk,Assign,Num(0)) 
															->  if  (IDSet.mem clk clocksSet) then 
    																(Assignment(Var (clk ^ name.Name.text),Assign,Num 0))
																	else 
																		Assignment(Var clk,Assign,Num(0)) 
													| _ -> expr ;
													) u.Update.el} )
													}
								) 
								edges; 
				locations =
									List.map
									(fun ({Location.id; invariant; _} as location) -> 
										{location with Location. 
											 invariant = begin match invariant with
											 | None -> None
											 | Some g -> Some { g with Invariant.
																			      expr = self#transGuardInvariant (g.Invariant.expr) ( name.Name.text )}
															end;              
											 }
									) 
									locations  
         };   
            
  initializer
    let rccw = new rename_clocks_class_walker in
    rccw#visit nta;
    clocksSet <- rccw#clocksSet
     			 
end   
  |> fun transformer -> transformer#transform nta

