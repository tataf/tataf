(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

let dbg = QeClockReductionCommons.dbg

open QeClockReductionCommons

(*----------------------------------------------------------------------------*)
let add_loops_to_virtual_world (glookup : glookup) (nta : NTA.t) : NTA.t =
  dbg#info "Create virtual world";
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGeneratorGlobal

  val glookup = glookup
  
  method! transform_edges edges =
    edges
    |> List.map (* copy edges to virtual world *)
       (fun edge ->
        match edge with
        | {Edge.id; source; target; xy; nails; _} as tedge  ->
          [edge; {tedge with Edge.
                  source = self#virtual_id source;
                  target = self#virtual_id source;
                  color  = Some Tataf_libta.UppaalSystem.Color.red; }
         ]
       )
    |> List.concat
    
     end
  |> fun transformer -> transformer#transform nta

    
    
    
    
    
