(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open QeClockReductionCommons
open QeClockReductionSimpleTransformer
open Tataf_libta.UppaalSystem
open Tataf_libta.SymbolTable
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalQeClasses
open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

(** The transformation of queries follows three big steps
Given a query expression in the form: ((CTL Express) (CF Express))
S1.- We explore the whole query expression in order to collect logical clocks and logical locations
     from the clocks and locations appearing in the query expression (CF Express).
S2.- We firstly transform the (CF Express).
S3.- We then add the collected logical variables to (CTL Express), and we conjoint the transformed (CF Express)) from the previous step. 
*)

class q_gamma_transformer relookup (glookup : glookup) (_globalTokensAccumulator: ClockTemplateSet.t) 
			(qe_global_clock_expr_lookups: qe_global_clock_expr_lookup)  (location_names:  (ID.t) list) (nta : NTA.t)=
	
	
	let lookup = (new Tataf_libta.UppaalWalker.lookup nta) in		 
	let mem_tqname_of_clock_name_expressions = 
        qe_global_clock_expr_lookups#mem_tqname_of_clock_name_expressions
  in    		
  let mem_tqname_of_clock_name_expressions_non_qe = 
        qe_global_clock_expr_lookups#mem_tqname_of_clock_name_expressions_non_qe
  in    		
  let clockTemplate clk tmp = (clk ^ (match tmp with 
    								 					 | Var(var) -> var 
    								 					 | _ -> "failed" )) in
  let get_template temp = match temp with
	  	| Var x -> x
	  	| _ -> ""  in  								 					 
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_with_symboltable as super
  inherit varGenerator relookup#qeClass
  
  val relookup = relookup 
  val mutable pretYx = "false"
  val mutable pretYy = "false" 
  val mutable is_locVar = "false"
  val mutable sourceOfSimpleEdge = ""
  val mutable destOfSimpleEdge = ""
  val mutable setOfRLSE = IDSet.empty
  val mutable setOfRLSEplus = IDSet.empty
  val mutable setOflogVars = IDSet.empty
  val mutable _YrepComx = "" (* We need this variable for the complex edges with clocks from several equiv. classes*)
  val mutable _YrepComy = "" (* We need this variable for the complex edges with clocks from several equiv. classes*)
   (*In this variable we form a big expression of all log location that we have collected, 
   e.g. T1l1_log imply () and T1l2_log imply () and ...*)
  val mutable bigExpressForLogLocs =  ExpressionReferenceSet.empty
 
(** Methods for supporting the transformation of queries *) 

   (*We use this method to form an expression consisting of all dest. location and log. locations of source locations,
   where a given qe-clocks is being reset in a simple edge*)
  method getExpressionForLogClocks lst template = 
  	List.fold_left 
  		(fun acc (s,t) ->  
  						BoolOperation(acc,OrOr, Parenthesis(BoolOperation(Var (template ^ "." ^ (self#virtualizeLoc t)),
          								AndAnd,
          								Var (template ^ s ^ "_log")))) 
  		)
  	(Bool false)
  	lst
  
  (*We use this method to form an expression consisting of all dest. locations of simple edges in a given automaton*)
	method getExpressionOfAllRSLPlus lst template =
		 List.fold_left
						(fun acc target->   
						match lookup#location target with 
				 						 	| {Location.name=None;_}        -> Parenthesis(BoolOperation(acc,OrOr, Var ("Missing Name")))   
          						| {Location.name=(Some name);_} ->  
          								Parenthesis(BoolOperation(acc,OrOr, Parenthesis(BoolOperation(Var (relookup#_RY ^ "." ^ relookup#finalnstlocY),
          								AndAnd,
          								Var (template ^ "." ^ (self#virtualizeLoc name.Name.text))))))    
						) 
		 (Bool false)				
		 lst 
	  
		  
	(*We need the id of the location. From the query file we only obtain the name but not the id *)	  
  method getIdOfLocation loc  = 
		  		match lookup#location_with_name loc with
          | {Location.id;_}        -> id
	
	(*Given a source location of a simple edge, we retrieve here the destination location of that edge*)	  	  
	method getDestLocSE temp source =
		List.iter 
			(fun x ->
				 match x with 
				 	| {Edge.id; target; _} ->
				 					match lookup#location target with 
			 						 	| {Location.name=None;_}        -> 
				 						 		if ((get_template temp) = "") then 
				 						 			destOfSimpleEdge <- "Missing Name" (*location has id but no name*)
				 						 		else 	destOfSimpleEdge <- (get_template temp) ^ "." ^ "Missing Name" 
          					| {Location.name=(Some name);_} -> 
          							if ((get_template temp) = "") then 
          								destOfSimpleEdge <- name.Name.text 
          							else 	destOfSimpleEdge <- (get_template temp) ^ "." ^  name.Name.text 
			)
		(lookup#edges_out (self#getIdOfLocation source))

	(*Given a dest. location of a simple edge, we retrieve here the source location of that edge*)	
	method getSourceLocSE temp dest =
		List.iter 
			(fun x ->
				 match x with 
				 	| {Edge.id; source; _} -> 
				 					match lookup#location source with 
				 						 	| {Location.name=None;_}        -> 
				 						 		if ((get_template temp) = "") then 
				 						 			sourceOfSimpleEdge <- "Missing Name" (*location has id but no name*)
				 						 		else 	sourceOfSimpleEdge <- (get_template temp) ^ "." ^ "Missing Name" 
          						| {Location.name=(Some name);_} -> 
          							if ((get_template temp) = "") then 
          								sourceOfSimpleEdge <- name.Name.text 
          							else 	sourceOfSimpleEdge <- (get_template temp) ^ "." ^  name.Name.text 
			)
		(lookup#edges_in (self#getIdOfLocation dest))
		
	method virtualizeLoc loc = loc ^ (Char.escaped char_virtual)
	
	method exists_location name =
		List.exists (fun x -> x = name) location_names
		
(** Here begins the step S1 *)						
	(*Here we explore the whole query in order to collect logical variables of qe clocks and locations of simple/complex edges  *)					
	method private checkForRLSimpleEdges expr =  
    let te = self#checkForRLSimpleEdges in
		   match expr with
		  | Var(var) ->  ()
		  | Num(_) -> ()
		  | Bool(_) -> ()
		  | Call(e,args) -> te e
		  | Array(e1,e2) -> te e1; te e2
		  | Parenthesis(e) -> te e
		  | IncPost(e) -> te e
		  | IncPre(e) -> te e
		  | DecPost(e) -> te e
		  | DecPre(e) -> te e
		  | Assignment(e1,op,e2) -> te e1; te e2
		  | Unary(op,e) -> te e
		  | Relation(Dot(e1,id),op,(Num num)) when 
											 mem_tqname_of_clock_name_expressions (Var (clockTemplate id e1) ) 
											 && (relookup#in_allClocksInSimpleEdges (clockTemplate id e1 ))
											  -> 
											  	 is_locVar <- "true"; 
		    									 setOflogVars <-  IDSet.add ((clockTemplate id e1) ^ "_log") setOflogVars;
		    									 (*All source locs of the simple edges where the clock id appears, are going to be transformed into log. locs*)
		    									 List.iter
  														(fun (s,t) -> setOflogVars <-  IDSet.add ((get_template e1) ^ s ^ "_log") setOflogVars; 
  															(*We add the corresp. expression for the logical location *)
  												 			bigExpressForLogLocs <- ExpressionReferenceSet.add
  												 					(Parenthesis(Imply(Var((get_template e1) ^ s ^ "_log"),
  												 					self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e1)) (get_template e1))))
  												 			bigExpressForLogLocs		
  														) 
  												(relookup#getLocations (clockTemplate id  e1 ));	
													(*Here we insert the logical variables for clocks and log. locations that these variables imply*)							 
													let logClk = if (IDSet.mem ((clockTemplate id  e1 ) ^ "_log") setOflogVars)  
																	then  ((clockTemplate id  e1 ) ^ "_log") else "" in 
															(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
																if  logClk = "" then (*id is not in a simple edge*)
																		bigExpressForLogLocs <- bigExpressForLogLocs
																else  (*id is in a simple edge*)
																		bigExpressForLogLocs <- ExpressionReferenceSet.add
																			(Parenthesis(Imply(Var logClk, 
																			Parenthesis(self#getExpressionForLogClocks 
																						(relookup#getLocations (clockTemplate id  e1 )) (get_template e1)))))
																		 bigExpressForLogLocs 			
    	| Relation(IntOperation(Dot(e1,clk1),op,Dot(e2,clk2)),op2,(Num num)) when 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk1  e1 ))) &&
    								 (relookup#in_allClocksInSimpleEdges (clockTemplate clk1  e1 ))					 
    								 && 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk2  e2 ))) = false ->		
											(*Here we insert the logical variables for clocks and log. locations that these variables imply*)	
    								 					 is_locVar <- "true"; 
															 setOflogVars <-  IDSet.add ((clockTemplate clk1  e1 ) ^ "_log") setOflogVars;	
		    						  (*All source locs of the simple edges where the clock clk1 appears, are going to be transformed into log. locs*)
		    									 List.iter
  														(fun (s,t) -> setOflogVars <-  IDSet.add ((get_template e1) ^ s ^ "_log") setOflogVars; 
  															(*We add the corresp. expression for the logical location *)
  												 			bigExpressForLogLocs <-  ExpressionReferenceSet.add
  												 					(Parenthesis(Imply(Var((get_template e1) ^ s ^ "_log"),
  												 			self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e1)) (get_template e1))))
  												 			 bigExpressForLogLocs		
  														) 
  												 (relookup#getLocations (clockTemplate clk1  e1 ));		
  									  (*Here we insert the logical variables for clocks and log. locations that these variables imply*)							 
													let logClk = if (IDSet.mem ((clockTemplate clk1  e1 ) ^ "_log") setOflogVars)  
																	then  ((clockTemplate clk1  e1 ) ^ "_log") else "" in 
															(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
																if  logClk = "" then (*id is not in a simple edge*)
																		bigExpressForLogLocs <- bigExpressForLogLocs
																else  (*id is in a simple edge*)
																		bigExpressForLogLocs <- ExpressionReferenceSet.add
																			(Parenthesis(Imply(Var logClk, 
																			Parenthesis(self#getExpressionForLogClocks 
																						(relookup#getLocations (clockTemplate clk1  e1 )) (get_template e1)))))
																		 bigExpressForLogLocs 			 			
    	| Relation(IntOperation(Dot(e1,clk1),op,Dot(e2,clk2)),op2,(Num num)) when
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk2  e2 )))  &&
    								 (relookup#in_allClocksInSimpleEdges (clockTemplate clk2  e2 ))	
    								 && 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk1  e1 ))) = false  -> 		
											(*Here we insert the logical variables for clocks and log. locations that these variables imply*)						
    								 					 is_locVar <- "true"; 
															 setOflogVars <- IDSet.add ((clockTemplate clk2  e2 ) ^ "_log") setOflogVars;	
		    							(*All source locs of the simple edges where the clock clk2 appears, are going to be transformed into log. locs*)
		    									 List.iter
  														(fun (s,t) -> setOflogVars <-  IDSet.add ((get_template e2) ^ s ^ "_log") setOflogVars; 
  															(*We add the corresp. expression for the logical location *)
  												 			bigExpressForLogLocs <- ExpressionReferenceSet.add
  												 					(Parenthesis(Imply(Var((get_template e2) ^ s ^ "_log"),
  												 			self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e2)) (get_template e2))))
  												 			bigExpressForLogLocs		
  														) 
  												 (relookup#getLocations (clockTemplate clk2  e2 ));			
  									  (*Here we insert the logical variables for clocks and log. locations that these variables imply*)							 
											let logClk = if (IDSet.mem ((clockTemplate clk2  e2 ) ^ "_log") setOflogVars)  
																	then  ((clockTemplate clk2  e2 ) ^ "_log") else "" in 
															(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
																if  logClk = "" then (*id is not in a simple edge*)
																		bigExpressForLogLocs <- bigExpressForLogLocs
																else  (*id is in a simple edge*)
																		bigExpressForLogLocs <- ExpressionReferenceSet.add
																			(Parenthesis(Imply(Var logClk, 
																			Parenthesis(self#getExpressionForLogClocks 
																						(relookup#getLocations (clockTemplate clk2  e2 )) (get_template e2)))))
																		 bigExpressForLogLocs 			  		
   		 | Relation(IntOperation(Dot(e1,clk1),op,Dot(e2,clk2)),op2,(Num num)) when 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk1  e1 )))   &&
    								 (relookup#in_allClocksInSimpleEdges (clockTemplate clk1  e1 ))	  &&
    								 (relookup#in_allClocksInSimpleEdges (clockTemplate clk2  e2 ))	
    								 && 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk2  e2 ))) -> 	
											(*Here we insert the logical variables for clocks and log. locations that these variables imply*)	
    								 					 is_locVar <- "true"; 
															 setOflogVars <- IDSet.add ((clockTemplate clk1  e1 ) ^ "_log") setOflogVars;
															 setOflogVars <- IDSet.add ((clockTemplate clk2  e2 ) ^ "_log") setOflogVars;						 
											(*All source locs of the simple edges where the clocks clk1 and clk2 appear, 
											are going to be transformed into log. locs*)
													 List.iter
  														(fun (s,t) -> setOflogVars <-  IDSet.add ((get_template e1) ^ s ^ "_log") setOflogVars; 
  															(*We add the corresp. expression for the logical location *)
  												 			bigExpressForLogLocs <- ExpressionReferenceSet.add
  												 					(Parenthesis(Imply(Var((get_template e1) ^ s ^ "_log"),
  												 			self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e1)) (get_template e1))))
  												 			bigExpressForLogLocs		
  														) 
  												 (relookup#getLocations (clockTemplate clk1  e1 ));	
		    									 List.iter
  														(fun (s,t) -> setOflogVars <-  IDSet.add ((get_template e2) ^ s ^ "_log") setOflogVars; 
  															(*We add the corresp. expression for the logical location *)
  												 			bigExpressForLogLocs <- ExpressionReferenceSet.add
  												 					(Parenthesis(Imply(Var((get_template e2) ^ s ^ "_log"),
  												 			self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e2)) (get_template e2))))
  												 			bigExpressForLogLocs		
  														) 
  												 (relookup#getLocations (clockTemplate clk2  e2 ));					 
  										let logClk1 = if (IDSet.mem ((clockTemplate clk1  e1 ) ^ "_log") setOflogVars)  
  													then  ((clockTemplate clk1  e1 ) ^ "_log") else "" in 
  										let logClk2 = if (IDSet.mem ((clockTemplate clk2  e2 ) ^ "_log") setOflogVars)  
  													then  ((clockTemplate clk2  e2 ) ^ "_log") else "" in 		
											(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
											if logClk1 = "" && logClk2 = "" then 
		  						  			bigExpressForLogLocs <- bigExpressForLogLocs
		  						  	else 
		  						  		if logClk1 != "" && logClk2 = "" then
		  						  		 bigExpressForLogLocs <- ExpressionReferenceSet.add
																			(Parenthesis(Imply(Var logClk1, 
																			Parenthesis(self#getExpressionForLogClocks 
																						(relookup#getLocations (clockTemplate clk1  e1 )) (get_template e1)))))
												 bigExpressForLogLocs										
		  						  		else
		  						  			if 	logClk1 = "" && logClk2 != "" then
		  						  				bigExpressForLogLocs <- ExpressionReferenceSet.add
																			(Parenthesis(Imply(Var logClk2, 
																			Parenthesis(self#getExpressionForLogClocks 
																						(relookup#getLocations (clockTemplate clk2  e2 )) (get_template e2)))))
												 			bigExpressForLogLocs		
  									  	else 
  									  			  bigExpressForLogLocs <- ExpressionReferenceSet.add
															(Parenthesis(Imply(Var logClk1, 
															Parenthesis(self#getExpressionForLogClocks 
															(relookup#getLocations (clockTemplate clk1  e1 )) (get_template e1)))))
												 			bigExpressForLogLocs;
												 			bigExpressForLogLocs <- ExpressionReferenceSet.add
																			(Parenthesis(Imply(Var logClk2, 
																			Parenthesis(self#getExpressionForLogClocks 
																						(relookup#getLocations (clockTemplate clk2  e2 )) (get_template e2)))))
												 			bigExpressForLogLocs	
			| Relation(Dot(e1,clk1),op,Dot(e2,clk2))  -> 	
										te (Relation(IntOperation(Dot(e1,clk1),Minus,Dot(e2,clk2)),op,Num 0))											 							
		  | Relation(e1,op,e2) -> te e1; te e2
		  | IntOperation(e1,op,e2) -> te e1;te e2
		  | BoolOperation(e1,op,e2) -> te e1;te e2
		  | IfExpr(e1,e2,e3) -> te e1;te e2;te e3
		  | Dot(e1,id) -> 
		  			if (IDSet.mem ((get_template e1) ^ "." ^ id) relookup#_TemplateSERL) then begin
		    				is_locVar <- "true"; 
		    				setOflogVars <-  IDSet.add ((get_template e1) ^ id ^ "_log") setOflogVars;
		    				bigExpressForLogLocs <- ExpressionReferenceSet.add
							 					(Parenthesis(Imply(Var((get_template e1) ^ id ^ "_log"),
							 			self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e1)) (get_template e1))))
							 			bigExpressForLogLocs		
		    				end
		    	  else 
		    				if (IDSet.mem ((get_template e1) ^ "." ^ id) relookup#_TemplateSERLplus) then begin
				  				is_locVar <- "true"; 
				  				self#getSourceLocSE  (Var "") id; 
				  				setOflogVars <-  IDSet.add ((get_template e1) ^ sourceOfSimpleEdge ^ "_log") setOflogVars;
				  				bigExpressForLogLocs <- ExpressionReferenceSet.add
							 					(Parenthesis(Imply(Var((get_template e1) ^ sourceOfSimpleEdge ^ "_log"),
							 			self#getExpressionOfAllRSLPlus (relookup#getRLTemplateSEplus (get_template e1)) (get_template e1))))
							 			bigExpressForLogLocs		
		    				end
		    				else
		    						() 
		  | Dashed(e) ->  te e
		  | Deadlock -> ()
		  | Imply(e1,e2) -> te e1; te e2
		  | Exists(id,typ,e) -> te e
		  | Forall(id,typ,e) -> te e
		  | CTL(e) -> 
		  let rec ter x =  
		  match x with 
		  	| AF(x) ->  te x
				| AG(x) ->  te x
				| EF(x) ->  te x
				| AGR(x) -> te x 
				| EFR(x) -> te x 
				| EG(x) ->  te x
				| AF2(x1,x2) -> te x1; te x2 
				| AG2(x1,x2) -> te x1; te x2  
				| EF2(x1,x2) -> te x1; te x2   
				| EG2(x1,x2) -> te x1; te x2   
				| Leadsto(x1,x2) -> te x1;te x2   
				| AU(x1,x2) -> te x1;te x2   
				| AW(x1,x2) -> te x1;te x2   
				| AU2(x1,x2,x3) -> te x1; te x2; te x3  
				| AW2(x1,x2,x3) -> te x1;te x2;te x3 
				in ter e	    
	
(** This is the step S2*)		
	(*Here is were the transformation of expression for a query occurs.*)				 							 
	method private transExprInQuery expr = 
    let te = self#transExprInQuery in
     match expr with
    | Var(var) ->  expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> Call(te e,args)
    | Array(e1,e2) -> Array(te e1, te e2)
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> IncPost(te e)
    | IncPre(e) -> IncPre(te e)
    | DecPost(e) -> DecPost(te e)
    | DecPre(e) -> DecPre(te e)
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> Unary(op,te e)
     (*Renaming for non-qe clocks*)
    | Relation(Dot(e1,id),op,(Num num)) when 
											 (mem_tqname_of_clock_name_expressions_non_qe (Var (clockTemplate id  e1 ))) 
											  -> Relation(Var ((get_template e1) ^ "." ^ (clockTemplate id  e1)),op,(Num num)) 
  
    (*Transformation for a qe-clock, e.g. x>=20, where x is a qe-clock*)
    | Relation(Dot(e1,id),op,(Num num)) when 
											 mem_tqname_of_clock_name_expressions (Var (clockTemplate id  e1 ))  
											  -> 
											ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										if clock = (clockTemplate id  e1 ) then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  										)
  										_globalTokensAccumulator;
  							let logClk = if (IDSet.mem ((clockTemplate id  e1 ) ^ "_log") setOflogVars)  
  									then  ((clockTemplate id  e1 ) ^ "_log") else "" in 
  							(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
  								if  logClk = "" then (*id is not in a simple edge*)
  											Parenthesis(BoolOperation(Parenthesis(BoolOperation(Relation(Var(_YrepComx),op,(Num num)),
  																	AndAnd, Var pretYx)),OrOr,
    						 							 Parenthesis(BoolOperation(Relation(Num 0,op,(Num num)),AndAnd, 
    						 							 Unary(UnaryNot,Var pretYx)))))
  								else  (*id is in a simple edge*)
  											Parenthesis(
    						 				BoolOperation(Parenthesis(BoolOperation(Relation(Var(_YrepComx),op,(Num num)),AndAnd,
    						 				Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk)))),OrOr,
    						 							 Parenthesis(BoolOperation(Relation(Num 0,op,(Num num)),AndAnd, 
    						 							 Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk)))))))
    (*Transformation for a qe-clock in a clock difference, e.g. x-y>=20, where only x is a qe-clock*)						 							 
  	| Relation(IntOperation(Dot(e1,clk1),op,Dot(e2,clk2)),op2,(Num num)) when 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk1  e1 ))) && 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk2  e2 ))) = false -> 
											ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
												if clock = (clockTemplate clk1  e1 ) then begin
													pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
													_YrepComx <- qeClass ^ "rep"
													end
												)
  										_globalTokensAccumulator; 
  										let logClk = if (IDSet.mem ((clockTemplate clk1  e1 ) ^ "_log") setOflogVars)  
  													then  ((clockTemplate clk1  e1 ) ^ "_log") else "" in 
											(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
											if  logClk = "" then (*id is not in a simple edge *)
												Parenthesis(BoolOperation(Parenthesis(BoolOperation(
										  	Relation(IntOperation(Var(_YrepComx),Minus,
										  	Var ((get_template e2) ^ "." ^ (clockTemplate clk2  e2))),op2,(Num num)),
										  	AndAnd,
										  	Var pretYx)),			
										  	OrOr,
										  	Parenthesis(BoolOperation(
										  	Relation(Num 0,op2,IntOperation((Num num),Plus,
										  	Var ((get_template e2) ^ "." ^ (clockTemplate clk2  e2)))),AndAnd, 
										  	Unary(UnaryNot, Var pretYx)))))		
										  else (*id is in a simple edge*)	
												Parenthesis(BoolOperation(Parenthesis(BoolOperation(
										  	Relation(IntOperation(Var(_YrepComx),Minus,
										  	Var ((get_template e2) ^ "." ^ (clockTemplate clk2  e2))),op2,(Num num)),AndAnd,
										  	Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk)))),			
										  	OrOr,
										  	Parenthesis(BoolOperation(
										  	Relation(Num 0,op2,IntOperation((Num num),Plus,
										  	Var ((get_template e2) ^ "." ^ (clockTemplate clk2  e2)))),AndAnd, 
										  	Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk)))))))
			(*Transformation for a qe-clock in a clock difference, e.g. x-y>=20, where only y is a qe-clock*)		
	    | Relation(IntOperation(Dot(e1,clk1),op,Dot(e2,clk2)),op2,(Num num)) when
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk2  e2 ))) && 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk1  e1 ))) = false  -> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = (clockTemplate clk2  e2 ) then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  										let logClk = if (IDSet.mem ((clockTemplate clk2  e2 ) ^ "_log") setOflogVars)  
  													then  ((clockTemplate clk2  e2 ) ^ "_log") else "" in 
											(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
											if  logClk = "" then (*id is not in a simple edge *)
										  	Parenthesis(BoolOperation(Parenthesis(BoolOperation(
										  	Relation(IntOperation((Var ((get_template e1) ^ "." ^ (clockTemplate clk1  e1))),
										  	Minus,Var(_YrepComy)),op2,(Num num)),AndAnd,Var pretYy)),	  						  				
										  	OrOr,
										  	Parenthesis(BoolOperation(
										  	Relation(IntOperation((Var ((get_template e1) ^ "." ^ (clockTemplate clk1  e1))),
										  	Minus,Num 0),op2,(Num num)),
										  	AndAnd,Unary(UnaryNot,Var pretYy))))) 		
										  else (*id is in a simple edge*)	
										  	Parenthesis(BoolOperation(Parenthesis(BoolOperation(
										  	Relation(IntOperation((Var ((get_template e1) ^ "." ^ (clockTemplate clk1  e1))),
										  	Minus,Var(_YrepComy)),op2,(Num num)),AndAnd,
										  	Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk)))),	  						  				
										  	OrOr,
										  	Parenthesis(BoolOperation(
										  	Relation(IntOperation((Var ((get_template e1) ^ "." ^ (clockTemplate clk1  e1))),
										  	Minus,Num 0),op2,(Num num)),AndAnd,Unary(UnaryNot,
										  	Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk)))))))
		  	(*Transformation for qe-clocks in a clock difference, e.g. x-y>=20, where x and y are qe-clocks*)				  				 							 
		    | Relation(IntOperation(Dot(e1,clk1),op,Dot(e2,clk2)),op2,(Num num)) when 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk1  e1 ))) && 
											(mem_tqname_of_clock_name_expressions (Var (clockTemplate clk2  e2 ))) -> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = (clockTemplate clk1  e1 ) then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  											);	
  												
  										(if clock = (clockTemplate clk2  e2 ) then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  										let logClk1 = if (IDSet.mem ((clockTemplate clk1  e1 ) ^ "_log") setOflogVars)  
  													then  ((clockTemplate clk1  e1 ) ^ "_log") else "" in 
  										let logClk2 = if (IDSet.mem ((clockTemplate clk2  e2 ) ^ "_log") setOflogVars)  
  													then  ((clockTemplate clk2  e2 ) ^ "_log") else "" in 		
											(*we need to differentiate between a clock in a simple edge and a clock in a complex edge*)	
											if logClk1 = "" && logClk2 = "" then 
		  						  	Parenthesis(BoolOperation(BoolOperation(Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Var(_YrepComy)),op2,(Num num)),AndAnd,Var pretYx ),AndAnd,Var pretYy)),  						  				OrOr,
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(Num 0,op2,IntOperation((Num num),Plus,Var(_YrepComy))),AndAnd, Unary(UnaryNot,Var pretYx )),
		  						  	AndAnd,Var pretYy))),
		  						  	OrOr,
		  						  	BoolOperation(Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Num 0),op2,(Num num)),AndAnd,Var pretYx ),AndAnd, 
		  						  	Unary(UnaryNot,Var pretYy))),		 			
		  						  	OrOr,
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Num 0,Minus,Num 0),op2,(Num num)),AndAnd, Unary(UnaryNot,Var pretYx )),AndAnd, 
		  						  	Unary(UnaryNot,Var pretYy))))))
		  						  	else 
		  						  		if logClk1 != "" && logClk2 = "" then
		  						  		Parenthesis(BoolOperation(BoolOperation(Parenthesis(BoolOperation(BoolOperation(
												Relation(IntOperation(Var(_YrepComx),Minus,Var(_YrepComy)),op2,(Num num)),AndAnd,
												Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1))),AndAnd,Var pretYy)),  						  					
												OrOr,
												Parenthesis(BoolOperation(BoolOperation(
												Relation(Num 0,op2,IntOperation((Num num),Plus,Var(_YrepComy))),AndAnd, 
												Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1)))),AndAnd,Var pretYy))),
												OrOr,
												BoolOperation(Parenthesis(BoolOperation(BoolOperation(
												Relation(IntOperation(Var(_YrepComx),Minus,Num 0),op2,(Num num)),AndAnd,
												Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1))),AndAnd, 
												Unary(UnaryNot,Var pretYy))),		 			
												OrOr,
												Parenthesis(BoolOperation(BoolOperation(
												Relation(IntOperation(Num 0,Minus,Num 0),op2,(Num num)),AndAnd, 
												Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1)))),AndAnd, 
												Unary(UnaryNot,Var pretYy))))))		
		  						  		else
		  						  			if 	logClk1 = "" && logClk2 != "" then
		  						  	Parenthesis(BoolOperation(BoolOperation(Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Var(_YrepComy)),op2,(Num num)),AndAnd,Var pretYx ),AndAnd,
		  						  	Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2)))),OrOr,
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(Num 0,op2,IntOperation((Num num),Plus,Var(_YrepComy))),AndAnd, Unary(UnaryNot,Var pretYx )),AndAnd,
		  						  	Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2))))),
		  						  	OrOr,
		  						  	BoolOperation(Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Num 0),op2,(Num num)),AndAnd,Var pretYx ),AndAnd, 
		  						  	Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2))))),		 			
		  						  	OrOr,
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Num 0,Minus,Num 0),op2,(Num num)),AndAnd, Unary(UnaryNot,Var pretYx )),AndAnd, 
		  						  	Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2))))))))	
  									  	else 
  									  	Parenthesis(
  									  	BoolOperation(BoolOperation(Parenthesis(BoolOperation(BoolOperation(
												Relation(IntOperation(Var(_YrepComx),Minus,Var(_YrepComy)),op2,(Num num)),AndAnd,
												Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1))),AndAnd,
												Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2)))),  						  					
												OrOr,
												Parenthesis(BoolOperation(BoolOperation(
												Relation(Num 0,op2,IntOperation((Num num),Plus,Var(_YrepComy))),AndAnd, 
												Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1)))),AndAnd,
												Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2))))),
												OrOr,
												BoolOperation(Parenthesis(BoolOperation(BoolOperation(
												Relation(IntOperation(Var(_YrepComx),Minus,Num 0),op2,(Num num)),AndAnd,
												Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1))),AndAnd, 
												Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2))))),		 			
												OrOr,
												Parenthesis(BoolOperation(BoolOperation(
												Relation(IntOperation(Num 0,Minus,Num 0),op2,(Num num)),AndAnd, 
												Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYx, OrOr, Var logClk1)))),AndAnd, 
												Unary(UnaryNot,Parenthesis(BoolOperation(Var pretYy, OrOr, Var logClk2))))))))	
		| Relation(Dot(e1,clk1),op,Dot(e2,clk2))  -> 	
										te (Relation(IntOperation(Dot(e1,clk1),Minus,Dot(e2,clk2)),op,Num 0))															
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) ->   		 		    					 		 
    				(*Transformation for a reset location of a simple edge*)
		  			if (IDSet.mem ((get_template e1) ^ "." ^ id) relookup#_TemplateSERL) then begin
		  					self#getDestLocSE e1 id; (*updates the variable destOfSimpleEdge*)
		  					let logLoc = if (IDSet.mem ((get_template e1) ^ id ^ "_log") setOflogVars)  then  
		  													((get_template e1) ^ id ^ "_log") 
		  											 else "" in 				 
		    					Parenthesis(BoolOperation(
		    								Parenthesis(BoolOperation(
		    									Parenthesis(BoolOperation(Var destOfSimpleEdge,OrOr,Var (self#virtualizeLoc destOfSimpleEdge))),
		    										AndAnd,Var logLoc)),
		    											OrOr,BoolOperation(Dot(e1,id),OrOr,Dot(e1,self#virtualizeLoc id))))
		    				end
		    				else  begin
		    				 (*Transformation for a reset successor location of a simple edge*)
				  				if (IDSet.mem ((get_template e1) ^ "." ^ id) relookup#_TemplateSERLplus) then begin
												self#getSourceLocSE  (Var "") id;
												let logLoc = if (IDSet.mem ((get_template e1) ^ sourceOfSimpleEdge ^ "_log") setOflogVars)  then  
													((get_template e1) ^ sourceOfSimpleEdge ^ "_log")
												 else "" in 
														Parenthesis(BoolOperation(
															Parenthesis(BoolOperation(Dot(e1,id),
																OrOr,Dot(e1,self#virtualizeLoc id))),
																		AndAnd,Unary(UnaryNot,(Var logLoc))))
				  							end
		    					else  (*Transformation for the rest of the locations*)
		    					 if (self#exists_location ((get_template e1) ^ "." ^ id)) && 
		    					    (IDSet.mem (get_template e1)  relookup#resettingTemplates)
		    					 then 
		    					 begin
		    					 		Parenthesis(BoolOperation(Dot(e1,id),OrOr,Dot(e1,self#virtualizeLoc id)))  
    							 end
    							 else
    							 		Dot(e1,id)
    						end
    | Dashed(e) ->  Dashed(te e)
    | Deadlock -> Deadlock
    | Imply(e1,e2) -> Imply(te e1, te e2)
    | Exists(id,typ,e) -> Exists(id,typ,te e)
    | Forall(id,typ,e) -> Forall(id,typ,te e)
    | CTL(e) -> 
    let rec ter x =  
    match x with 
    	| AF(x) ->  AF(te x) 
		  | AG(x) ->  AG(te x)
		  | EF(x) ->  EF(te x)
		  | AGR(x) -> AGR(te x) 
		  | EFR(x) -> EFR(te x) 
		  | EG(x) ->  EG(te x)
		  | AF2(x1,x2) -> AF2(te x1, te x2) 
		  | AG2(x1,x2) -> AG2(te x1, te x2)   
		  | EF2(x1,x2) -> EF2(te x1, te x2)   
		  | EG2(x1,x2) -> EG2(te x1, te x2)   
		  | Leadsto(x1,x2) -> Leadsto(te x1,te x2)   
		  | AU(x1,x2) -> AU(te x1,te x2)   
		  | AW(x1,x2) -> AW(te x1,te x2)   
		  | AU2(x1,x2,x3) -> AU2(te x1, te x2, te x3)  
		  | AW2(x1,x2,x3) -> AW2(te x1,te x2,te x3)  
		  in CTL(ter e)

(** This is the step S3*)	
  (*We use this method to form an expression consisting of all logical locations created for the transformed query*)						
	method private addLogicalVariables expr= 
	 self#checkForRLSimpleEdges expr;
	 let te = self#transExprInQuery in
    match expr with
		| CTL(e) -> 
		  let rec ter x =  
		  match x with 
		  	| AF(x) ->  if (is_locVar = "true")  then  begin
										let exprForLogLocs = 
												ExpressionReferenceSet.fold
												(fun x acc ->  (BoolOperation(x,AndAnd,acc)))
												bigExpressForLogLocs
												(Bool true) in
										let exprQuery = 
											 IDSet.fold 
											 (fun x acc ->  Exists (x,  Tataf_libta.UppaalSystem.Type.Int([], Num 0, Num 1), acc))
											 setOflogVars 
											 (te x)	 in
										 	 AF(BoolOperation(exprQuery,AndAnd,exprForLogLocs))
										 	 end
										 else begin 
										 				is_locVar <- "false"; 
										 				AF(te x)
										 			end  
				| AG(x) ->	if (is_locVar = "true")  then  begin
										let exprForLogLocs = 
												ExpressionReferenceSet.fold
												(fun x acc ->  (BoolOperation(x,AndAnd,acc)))
												bigExpressForLogLocs
												(Bool true) in
										let exprQuery = 
											 IDSet.fold 
											 (fun x acc ->  Exists (x,  Tataf_libta.UppaalSystem.Type.Int([], Num 0, Num 1), acc))
											 setOflogVars 
											 (te x)	 in
										 	 AG(BoolOperation(exprQuery,AndAnd,exprForLogLocs))
										 	 end
										 else begin 
										 				is_locVar <- "false"; 
										 				AG(te x)
										 			end 
				| EF(x) ->  if (is_locVar = "true")  then  begin
										let exprForLogLocs = 
												ExpressionReferenceSet.fold
												(fun x acc ->  (BoolOperation(x,AndAnd,acc)))
												bigExpressForLogLocs
												(Bool true) in
										let exprQuery = 
											 IDSet.fold 
											 (fun x acc ->  Exists (x,  Tataf_libta.UppaalSystem.Type.Int([], Num 0, Num 1), acc))
											 setOflogVars 
											 (te x)	 in
										 	 EF(BoolOperation(exprQuery,AndAnd,exprForLogLocs))
										 	 end
										 else begin 
										 				is_locVar <- "false"; 
										 				EF(te x)
										 			end 
				| AGR(x) -> if (is_locVar = "true")  then  begin
										let exprForLogLocs = 
												ExpressionReferenceSet.fold
												(fun x acc ->  (BoolOperation(x,AndAnd,acc)))
												bigExpressForLogLocs
												(Bool true) in
										let exprQuery = 
											 IDSet.fold 
											 (fun x acc ->  Exists (x,  Tataf_libta.UppaalSystem.Type.Int([], Num 0, Num 1), acc))
											 setOflogVars 
											 (te x)	 in
										 	 AGR(BoolOperation(exprQuery,AndAnd,exprForLogLocs))
										 	 end
										 else begin 
										 				is_locVar <- "false"; 
										 				AGR(te x)
										 			end 
				| EFR(x) -> if (is_locVar = "true")  then  begin
										let exprForLogLocs = 
												ExpressionReferenceSet.fold
												(fun x acc ->  (BoolOperation(x,AndAnd,acc)))
												bigExpressForLogLocs
												(Bool true) in
										let exprQuery = 
											 IDSet.fold 
											 (fun x acc ->  Exists (x,  Tataf_libta.UppaalSystem.Type.Int([], Num 0, Num 1), acc))
											 setOflogVars 
											 (te x)	 in
										 	 EFR(BoolOperation(exprQuery,AndAnd,exprForLogLocs))
										 	 end
										 else begin 
										 				is_locVar <- "false"; 
										 				EFR(te x)
										 			end  
				| EG(x) ->  if (is_locVar = "true")  then  begin
										let exprForLogLocs = 
												ExpressionReferenceSet.fold
												(fun x acc ->  (BoolOperation(x,AndAnd,acc)))
												bigExpressForLogLocs
												(Bool true) in
										let exprQuery = 
											 IDSet.fold 
											 (fun x acc ->  Exists (x,  Tataf_libta.UppaalSystem.Type.Int([], Num 0, Num 1), acc))
											 setOflogVars 
											 (te x)	 in
										 	 EG(BoolOperation(exprQuery,AndAnd,exprForLogLocs))
										 	 end
										 else begin 
										 				is_locVar <- "false"; 
										 				EG(te x)
										 			end 
				| AF2(x1,x2) -> AF2(te x1, te x2) 
				| AG2(x1,x2) -> AG2(te x1, te x2)   
				| EF2(x1,x2) -> EF2(te x1, te x2)   
				| EG2(x1,x2) -> EG2(te x1, te x2)   
				| Leadsto(x1,x2) -> Leadsto(te x1,te x2)   
				| AU(x1,x2) -> AU(te x1,te x2)   
				| AW(x1,x2) -> AW(te x1,te x2)   
				| AU2(x1,x2,x3) -> AU2(te x1, te x2, te x3)  
				| AW2(x1,x2,x3) -> AW2(te x1,te x2,te x3)  
				in CTL(ter e)
		| _ -> 	te expr
		
		    
  method! transform_expression expr= 
  	self#addLogicalVariables expr
  	
  initializer
    lookup#visit nta;	
                
end

let q_gamma (relookup : relookup) (glookup : glookup) (nta : NTA.t) (_globalTokensAccumulator: ClockTemplateSet.t)
		(qe_global_clock_expr_lookups: qe_global_clock_expr_lookup) 
		(location_names:  (ID.t) list)
		(queries : Queries.t) =
  dbg#info ("Gamma on Queries for EC: " ^ relookup#className);
  (*|TRANSFORMATION OF QUERIES|*)
  let qe_clock_expr_lookup = new qe_clock_expr_lookup relookup in
  qe_clock_expr_lookup#visit_nta nta; (*construct symbol table*)
  qe_clock_expr_lookup#visit_queries ~symbol_table:qe_clock_expr_lookup#global_frame queries; 
  let queryTransformer  = new q_gamma_transformer relookup glookup 
  _globalTokensAccumulator qe_global_clock_expr_lookups location_names nta in
  queryTransformer#transform_queries queries 
  


