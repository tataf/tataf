(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

open Tataf_libta.UppaalQeClasses

let dbg = Tataf_libcore.Debug.register_debug_module "QeClockReduction"

(*----------------------------------------------------------------------------*)
(**
  {2 Helper}
  {3 Variable Name Generator} 
  Mixin to generate correct variable names for a specified equivalence class.
 *)

class varGeneratorGlobal =
  object (self)
  
  (*for virtual world*)
  val char_virtual = 'v'
  method virtual_id id = id ^ (Char.escaped char_virtual)
  method virtual_name_text name =  name ^ (Char.escaped char_virtual)
  	
  method virtual_name = function
    | None -> None
    | Some name -> Some { name with Name.text = name.Name.text ^ (Char.escaped char_virtual) } 
end


class varGenerator (qeClass : ID.t * TemplateQualifiedIDSet.t * TemplateQualifiedIDSet.t) =
  let className, clocks, ppd = qeClass in
  object (self)
  inherit varGeneratorGlobal

  val _Yrep   = className ^ "rep"
  val _rY     = "r" ^ className
  val _resetY = "reset" ^ className
  val _sY     = "s" ^ className
  
  val _Yids   = className ^ "ids"
  val _Yid    = className ^ "id"
  val ty			= "t" ^ className 
  method _tY clock =
    ty ^ (TemplateQualifiedID.to_str ~separator:"_" clock)
    

  (*for resetter*)
  val _RY         = "R" ^ className
  val _lrY        = "lr" ^ className
  val _nstY       = "nst" ^ className
  val _numClocksY = "numClocks" ^ className
  val _urY = "ur" ^ className
  val _prioY = "prio" ^ className
  val _returnAll = "returnAll"

  val qeClass = qeClass
  val qeClassName = className	
  val qeClocks = clocks
  val qePPD = ppd
  val className =className
  method get_Yrep = _Yrep
  method className = className
  method _RY = _RY
end

(*----------------------------------------------------------------------------*)
(**
  {3 Lookup class}

  Lookup Class for reset edges and locations. It generates following sets:
  -RE (reset edges)
  -SE (simple reset edges)
  -CE (complex reset edges)
  -RL (reset locations - reset edge's sources)
  -RLplus (reset locations - reset edge's targets)
*)

class relookup qeClass nta =
  object (self)
  inherit Tataf_libta.UppaalWalker.unit_walker as super
  inherit varGenerator qeClass

  method qeClass = qeClass
  method qeClassName = qeClassName
  method qeClocks = qeClocks

  (*lookup wrapped*)
  val lookup = new Tataf_libta.UppaalWalker.lookup nta
  method lookup              = lookup
  method location            = lookup#location
  method location_with_name  = lookup#location_with_name
  method edge                = lookup#edge
  method edges_in            = lookup#edges_in
  method edges_out           = lookup#edges_out

	val mutable _allClocksInSimpleEdges = IDSet.empty (* all clocks of simple reset edges *)
  val mutable _RE = EdgeSet.empty (* reset edges *)
  val mutable _SE = EdgeSet.empty (* simple reset edges *)
  val mutable _CE = EdgeSet.empty (* complex reset edges *)
  val mutable _CEmod = EdgeSet.empty (* complex reset edges before being modified*)
  val mutable _RL     = IDSet.empty (* we store the ids of reset locations of any resetting edge (reset edges start here) *)
  val mutable _RLplus = IDSet.empty (* we store the ids of reset location+ of any resetting edge (reset edges end here) *)
  val mutable _RLSE     = IDSet.empty (* we store the ids of reset locations of simple edges (reset edges start here) *)
  val mutable _RLSEplus = IDSet.empty (* we store the ids of reset location+ of simple edges(reset edges end here) *)
  val mutable _allTokens = IDSet.empty (* contains tokens (tYA0_x) of a given equi. class*)
  val mutable _allCounters = IDSet.empty (* contains simple counters (sYA0) of each equi. class*)
  val mutable _allrChannels = IDSet.empty (* contains the synch. channels (rYA0) of each template. for a given equi. class*)
  
  val mutable _TemplateSERL = IDSet.empty   (* contains template.reset_location of simple edges *)
  val mutable _TemplateSERLplus = IDSet.empty   (* contains template.reset_location_plus of simple edges *)
  
  val mutable _RLTemplateSEplus = Hashtbl.create 0  (* contains reset location+ of simple edges and its respective template *)
  val mutable _clkRLRLplus = Hashtbl.create 0 (*contains each clock in a simple edge and both locations of this edge*)
  
  (*we need this variable such that from here, the query transformation can retrieve this name, unknown at start*)
  val mutable finalnstlocY = "" 
   
  method finalnstlocY = finalnstlocY
  method set_finalnstlocY loc = finalnstlocY<-loc
  
  method _RE = _RE
  method _SE = _SE
  method _CE = _CE
  method _CEmod 		= _CEmod
  method _RL 				= _RL
  method _RLplus 		= _RLplus
  method _RLSE 			= _RLSE
  method _RLSEplus 	= _RLSEplus
  method _allTokens = _allTokens
  method _allCounters = _allCounters
  method _allrChannels = _allrChannels
  
  method _TemplateSERL = _TemplateSERL
  method _TemplateSERLplus = _TemplateSERLplus
  
 
  method _RLTemplateSEplus = _RLTemplateSEplus 
  method getRLTemplateSEplus x = Hashtbl.find_all _RLTemplateSEplus x
  
  method _clkRLRLplus = _clkRLRLplus
  method getLocations clk = Hashtbl.find_all _clkRLRLplus clk
   
  method getAllTokens set =
  _allTokens <- IDSet.union  set _allTokens
  
  method getAllCounters set =
  _allCounters <- IDSet.union  set _allCounters
  
  method getAllrChannels set =
  _allrChannels <- IDSet.union  set _allrChannels
  
 
  method set_SE set = _SE <- set
  method add_CEmod edge = 
  	_CEmod <- EdgeSet.add edge _CEmod
  
  val mutable __containsRE = false
  val mutable __containsSimpleRE = false
  (**set of all templates (names), that contain at least one resetting edge.*)
  val mutable resettingTemplates = IDSet.empty 
  method resettingTemplates = resettingTemplates
  
  val mutable simpleClockAndTemplate = ClockTemplateSet.empty 
  method simpleClockAndTemplate = simpleClockAndTemplate
  
  val mutable allClocksAndTemplate = ClockTemplateSet.empty 
  method allClocksAndTemplate = allClocksAndTemplate
  
  method computeSize set key =
  	ClockTemplateSet.fold (fun  ({ClockTemplate.template; clock; qeClass})  sum  ->
  	if key = qeClass then 
  		sum+1
  	else 
  		sum)
  	set
  	0
  
  method findTemplate_ClockTemplateSet set key =
    	ClockTemplateSet.filter (fun  ({ClockTemplate.template; clock; qeClass})  ->
  	key = clock)
  	set

  method in_RE 			 x = EdgeSet.mem x _RE 
  method in_SE 			 x = EdgeSet.mem x _SE
  method in_CE 			 x = EdgeSet.mem x _CE
  method in_CEmod 	 x = EdgeSet.mem x _CEmod
  method in_allClocksInSimpleEdges x = IDSet.mem x _allClocksInSimpleEdges
  method in_RL     	 x = IDSet.mem x _RL
  method in_RLplus 	 x = IDSet.mem x _RLplus
  method in_RLSE     x = IDSet.mem x _RLSE
  method in_RLSEplus x = IDSet.mem x _RLSEplus
  

  val _SETemplate = Hashtbl.create 0
  method _SETemplate = Hashtbl.find _SETemplate  
  method _addSETemplate x = Hashtbl.add _SETemplate x

  
  (*=======================
     Reset Edge Recognition
    ========================*)

 method private is_const = function
  | Num(_) -> true
  (*| Var(id) when SymbolTable.is_clock self#frame id -> true *) (*WHY THIS?*)
  | Var(_) -> true
  | _ -> false 

  (* given guard effectively checks x >= c *)
  method private is_simple_edge_guard x c guard =
    Guard.eq guard x c

  (* given update updates clock x *)
  method private is_simple_edge_update x update = 
     Update.is update (Assignment(Var(x),Assign,Num(0)))

  (* check if target location is urgent of committed*)
  method private is_target_urgent_committed {Location.urgent; committed; _} =
  	if urgent || committed then
  		false 
  	else true	
  	

  method private is_simple_reset_edge {Edge.id; sync; guard; update; source; target; _} =
    match lookup#location source with 
    | {Location.invariant = Some {Invariant.expr; _}; _} -> 
      begin match expr with
        | Relation(Var(x),LtEq,c)
        | Relation(c,GtEq,Var(x))
          when self#is_const c  (*check if number of constant*)
          -> let result = Sync.is_tau sync
                          && self#is_simple_edge_guard x c guard
                          && self#is_simple_edge_update x update  
                          && self#is_target_urgent_committed (lookup#location target) in
             if (result) then begin
               dbg#ldebug_endline 2 ((
                 match id with
                 | Some id -> id
                 | None    -> "anonymous edge") ^ ": treated as simple")
             end;
             result
        | _ -> false
      end 
    | _ -> false
  

  method private check_reset_edge = function
    | {Edge.id; update = Some {Update.el; _};  source; target; _} as edge ->
       List.iter 
        (fun expr -> match expr with
          (* [x := 0] expected, where x is in X*)
          | Assignment(Var(x),Assign,Num(0)) 
              when Tataf_libta.SymbolTable.is_clock self#frame x
                && TemplateQualifiedIDSet.mem_some
                     (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)
                     qeClocks 
                      ->  
              let getTemplateName {Tataf_libta.UppaalQeClasses.TemplateQualifiedID.template; _} = template in
              let clockAndTemplate = (ClockTemplate.make ~name:(Tataf_libcore.Util.unsome (getTemplateName (Tataf_libcore.Util.unsome 
                  (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)))) ~clock:x ~qeClass:qeClassName) in
                  allClocksAndTemplate <- ClockTemplateSet.add  clockAndTemplate allClocksAndTemplate;
              __containsRE <- true;
              _RE          <- EdgeSet.add edge _RE;
              _RL          <- IDSet.add source _RL;
              _RLplus      <- IDSet.add target _RLplus;
              (*edges are not simple, if another edge has same source/target location*)
              if ((self#is_simple_reset_edge edge = true)  
              	&& (List.length (lookup#edges_out source) = 1)
                && (List.length (lookup#edges_in target)  = 1))
                then begin 
                	_allClocksInSimpleEdges <- IDSet.add x _allClocksInSimpleEdges;
                  _SE <- EdgeSet.add edge 	_SE;
                  _RLSE <- IDSet.add source _RLSE;		
                  _RLSEplus <- IDSet.add target _RLSEplus;
                   (*We store in this hashtable the clk in a simple edge and both locations of this edge *)
                  let locName l =  match lookup#location l with
											| {Location.name=None;_}        -> l
											| {Location.name=(Some name);_} ->  name.Name.text  in
                  Hashtbl.add _clkRLRLplus x ((locName source), (locName target));
                  (* We store in the hashtable the template and the target location*)
                  Hashtbl.add _RLTemplateSEplus (Tataf_libcore.Util.unsome (getTemplateName (Tataf_libcore.Util.unsome 
                  (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)))) target;
                  (*We store "template"."source"*)
                  _TemplateSERL <- IDSet.add  ((Tataf_libcore.Util.unsome (getTemplateName (Tataf_libcore.Util.unsome 
                  (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)))) ^ "."^ (locName source)) _TemplateSERL;
                  (*We store "template"."target"*)	
                  _TemplateSERLplus <- IDSet.add  ((Tataf_libcore.Util.unsome (getTemplateName (Tataf_libcore.Util.unsome 
                  (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame x)))) ^ "."^ (locName target)) _TemplateSERLplus;
                  (* We store a tuple, composed of a simple clock and its respective template *)
                  simpleClockAndTemplate <- ClockTemplateSet.add  clockAndTemplate simpleClockAndTemplate
                end
                else begin
                  dbg#info " adding complex edge to CE";
                  _CE <- EdgeSet.add edge _CE;
                end 
          | _ -> () (* ignore expression in other formats. *)
        ) el 
    | _ -> ()



 
      
  (*======================
    General Walker Methods
    =======================*)

  method! visit_edge edge = 
    super#visit_edge edge;
    self#check_reset_edge edge

  method! visit_template ({Template.name; edges;_} as template) =
    (*We store all edges and their respective template in a hashtable. Later when we will use the template name to
  	create the counters sYA0 for simple edges. Here we have the initial iteration over edges within a template. *)
    List.iter
    (fun {Edge.source; target; _} ->   
      self#_addSETemplate (source ^ "_" ^ target) name)
    edges;
    __containsRE <- false;
    super#visit_template template;
    (if __containsRE then
      (*This template has resetting edges*)
      resettingTemplates <- IDSet.add name.Name.text resettingTemplates);

  method! visit nta =
    super#visit nta;

  initializer
    self#visit nta;
    
    let pp_templates set = 
      str_list ~sep:", " 
      (fun ({ClockTemplate.template; clock;_}) ->  template.Name.text)
      (ClockTemplateSet.elements set) in  
    let pp_ids set = 
      str_list ~sep:", " 
        (fun id -> 
          match lookup#location id with
          | {Location.name=None;_}        -> id
          | {Location.name=(Some name);_} -> id ^ "(" ^ name.Name.text ^ ")"
        )
        (IDSet.elements set) in
    let pp_edges set = 
      str_list ~sep:", " 
        (fun {Edge.id; source; target; _} -> 
          match id with
            | None     -> source ^ "-->" ^ target
            | Some id' -> source ^ "--(" ^ id' ^ ")-->" ^ target)
        (EdgeSet.elements set)
    in 
    "Generated lookup sets for class " ^ qeClassName |> dbg#info;
    "RE_" ^     qeClassName ^ " = { " ^ (pp_edges _RE) ^ " }\n" ^
    "SE_" ^     qeClassName ^ " = { " ^ (pp_edges _SE) ^ " }\n" ^
    "CE_" ^     qeClassName ^ " = { " ^ (pp_edges _CE) ^ " }\n" ^ 
    "RL-_" ^     qeClassName ^ " = { " ^ (pp_ids _RL) ^ " }\n" ^
    "RL+_" ^ qeClassName ^ " = { " ^ (pp_ids _RLplus) ^ " }\n" ^
    "RL- in SE_" ^     qeClassName ^ " = { " ^ (pp_ids _RLSE) ^ " }\n" ^
    "RL+ in SE_" ^ qeClassName ^ " = { " ^ (pp_ids _RLSEplus) ^ " }\n" ^
    "Templates with simple edges wrt. " ^ qeClassName ^ " = { " ^ (pp_templates simpleClockAndTemplate) ^ " }"
    (* ^ "PDD_" ^    qeClassName ^ " = { " ^ (pp_ids qePPD) ^ "}" *)
    |> prepend_lines "  "
    |> dbg#info
end

(*----------------------------------------------------------------------------*)

class qe_clock_expr_lookup relookup =
  object (self)
  inherit Tataf_libta.UppaalWalker.unit_walker as super

val mutable clock_name_expressions =  ExpressionReferenceSet.empty
val mutable clock_names_set = TemplateQualifiedIDSet.empty


method  clock_names_set = clock_names_set
method  clock_name_expressions = clock_name_expressions
  (* map each expression reference occurring in clock_name_expressions
     to the corresponding template-qualified clock name. ONLY QE CLOCKS. *)
  val tqname_of_clock_name_expressions :
    (ExpressionReference.t, TemplateQualifiedID.t) Hashtbl.t = Hashtbl.create 0;
  method tqname_of_clock_name_expressions = tqname_of_clock_name_expressions
  method find_tqname_of_clock_name_expressions =
    Hashtbl.find tqname_of_clock_name_expressions
    
  method mem_tqname_of_clock_name_expressions =
  Hashtbl.mem tqname_of_clock_name_expressions
    
    
  method! visit_queries ?symbol_table x = 
    (* note: we assume that visit_queries() should only yield expressions in
       the queries, not ones from the automaton.
    
       Yet as visit_queries() requires a symbol_table, visit_nta() will often
       been applied before -- this fills up clock_name_expressions and
       tqname_of_clock_name_expressions with expressions in the automaton
       thus we clear these two collections first.
     *)
    

    super#visit_queries ?symbol_table x

		method! visit_expression expr =  
			match expr with
		  | Var(var) when TemplateQualifiedIDSet.mem_some
		                     (Tataf_libta.SymbolTable.resolve_to_template_qualified
		                        self#frame var)
		                     relookup#qeClocks
		             -> 
		             		clock_name_expressions <- ExpressionReferenceSet.add expr clock_name_expressions;
		             		let xxx =   
		                  (Tataf_libcore.Util.unsome
		                    (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame var))  in
		                        clock_names_set <- TemplateQualifiedIDSet.add xxx clock_names_set;
		                Hashtbl.add tqname_of_clock_name_expressions expr
		                  (* unsome is safe because of the when-clause above *)
		                  (Tataf_libcore.Util.unsome
		                    (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame var))		                                       
		  | _ -> super#visit_expression expr                 



 end

(*----------------------------------------------------------------------------*)

class qe_global_clock_expr_lookup  relookups nta =
  object (self)
  inherit Tataf_libta.UppaalWalker.unit_walker as super
   
  (* map each expression reference occurring in clock_name_expressions
     to the corresponding template-qualified clock name. ONLY QE CLOCKS.*)
  val tqname_of_clock_name_expressions :
    (ExpressionReference.t, TemplateQualifiedID.t) Hashtbl.t = Hashtbl.create 0;
  method tqname_of_clock_name_expressions = tqname_of_clock_name_expressions
  method find_tqname_of_clock_name_expressions =
    Hashtbl.find tqname_of_clock_name_expressions
  method mem_tqname_of_clock_name_expressions =
  	Hashtbl.mem tqname_of_clock_name_expressions  
 
  (* map each expression reference occurring in clock_name_expressions
     to the corresponding template-qualified clock name. ONLY NON-QE CLOCKS.*)
  val tqname_of_clock_name_expressions_non_qe :
    (ExpressionReference.t, TemplateQualifiedID.t) Hashtbl.t = Hashtbl.create 0;
  method tqname_of_clock_name_expressions_non_qe = tqname_of_clock_name_expressions_non_qe
  method find_tqname_of_clock_name_expressions_non_qe =
    Hashtbl.find tqname_of_clock_name_expressions_non_qe
  method mem_tqname_of_clock_name_expressions_non_qe =
  	Hashtbl.mem tqname_of_clock_name_expressions_non_qe  
  
  val mutable allClocks = TemplateQualifiedIDSet.empty
  
  method allClocks = allClocks
  
  method private get_global_expressions =
		List.iter
		(fun relookup  ->
				allClocks<- TemplateQualifiedIDSet.union allClocks relookup#qeClocks)  
		relookups ;

		
  method! visit_expression expr = 
    match expr with
    | Var(var) when TemplateQualifiedIDSet.mem_some
                       (Tataf_libta.SymbolTable.resolve_to_template_qualified
                          self#frame var)
                       allClocks
               -> 
                  Hashtbl.add tqname_of_clock_name_expressions expr
                    (* unsome is safe because of the when-clause above *)
                    (Tataf_libcore.Util.unsome
                      (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame var))
    | Var(var) when  (TemplateQualifiedIDSet.mem_some
		                     (Tataf_libta.SymbolTable.resolve_to_template_qualified
		                        self#frame var)
		                     allClocks) = false 
		                    && (Tataf_libta.SymbolTable.is_clock self#frame var) ->
		              Hashtbl.add tqname_of_clock_name_expressions_non_qe expr
                    (* unsome is safe because of the when-clause above *)
                    (Tataf_libcore.Util.unsome
                      (Tataf_libta.SymbolTable.resolve_to_template_qualified self#frame var))                 
    | _        -> super#visit_expression expr

  method! visit nta =
   	super#visit nta;

  initializer
  	self#get_global_expressions; 
  	self#visit nta;
    	                
end

(*----------------------------------------------------------------------------*)
(**
  global lookup info (qe class independent)
*)
class glookup =
  object (self)
    
  val mutable relookups = []
  method add_relookup (lookup : relookup) =
    relookups <- lookup :: relookups

  method global_frame : Tataf_libta.SymbolTable.frame =
    (*simply output the global frame of the first lookup*)
    (List.hd relookups)#global_frame |> Tataf_libcore.Util.unsome

  method edges_in id =
    List.fold_left
      (fun i lookup -> (lookup#edges_in id) :: i)
      []
      relookups
  
  method in_RLplus x = 
    List.exists 
      (fun lookup -> lookup#in_RLplus x) 
      relookups    

  
  method in_RL x = 
    List.exists 
      (fun lookup -> lookup#in_RL x) 
      relookups    

  method in_CE x = 
    List.exists 
      (fun lookup -> lookup#in_CE x) 
      relookups

  method resettingTemplates = 
    List.fold_left 
      (fun i lookup -> IDSet.union i lookup#resettingTemplates) 
      IDSet.empty 
      relookups
      
  method locIn_RLSE x = 
    List.exists 
      (fun lookup -> lookup#in_RLSE x) 
      relookups 
  
  method locIn_RLSEplus x = 
    List.exists 
      (fun lookup -> lookup#in_RLSEplus x) 
      relookups    
      
end
let create_global_lookup lookups =
  let gl = new glookup in
  List.iter
    (fun lookup -> gl#add_relookup lookup)
    lookups;
  gl
    
