(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalSystem.Comment

open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

let dbg = QeClockReductionCommons.dbg 

open QeClockReductionCommons


(**
  {1 Add token variable for complex resetting edges.}
  add [txY] to guards.
 -rename x/Yrep | x in Y for non-simple edges (complex ones and other non-resetting edges)
 -transform invariants. 
*)
let add_complex_token_variable  (relookup : relookup) 
				(qe_global_clock_expr_lookups: qe_global_clock_expr_lookup)
				(_globalTokensAccumulator: ClockTemplateSet.t) 	
				(_globalSimpCountersAccumulator: ClockTemplateSet.t) 
				(nta : NTA.t) : NTA.t = 
  dbg#info ("Add complex token variable ");
  (*|TRANSFORMER|*)

 
  let mem_tqname_of_clock_name_expressions = 
        qe_global_clock_expr_lookups#mem_tqname_of_clock_name_expressions
  in      

  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass

  val relookup = relookup
  val mutable accumulator = (Num 0) 
  val mutable partialAccumulator = (Num 0)
  val mutable localTokens = IDSet.empty
  val mutable pretYx = "false"
  val mutable pretYy = "false"
  val mutable lastQEClass = ""
  val mutable iterator = 0
  val mutable _YrepComx = "" (* We need this variable for the complex edges with clocks from several equiv. classes*)
  val mutable _YrepComy = "" (* We need this variable for the complex edges with clocks from several equiv. classes*)
  val mutable num_edges = 0  (* Holds the number of edges to be splitted in later (outside of qeClockReduction, namely, 
  													 by the option utataf transform disjunctionsplitting) *)
  
	val mutable isComplexEdge = false
    
(*-----------------------------------------------------------------------------------------------------------------*)  
  (*Case x operator const, where x is qe clock. Example: x>=20.*) 
  method private transGuardOneQEClock expr = 
    let te = self#transGuardOneQEClock in
     match expr with
    | Relation((Var var),op,(const)) when mem_tqname_of_clock_name_expressions (Var var)  -> 
  							ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										if clock = var then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  										)
  										_globalTokensAccumulator;
    						 Parenthesis(BoolOperation(Parenthesis(BoolOperation(Relation(Var(_YrepComx),op,const),AndAnd,Var pretYx)),OrOr,
    						 							 Parenthesis(BoolOperation(Relation(Num 0,op,const),AndAnd, Unary(UnaryNot,Var pretYx)))))    							  	
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr
    
(*-------------------------------------------------------------------------------------------------------------------------------*)
(*Case x operator y operator const, where x is only qe clock. Example: x-y>=20.*)   
  method private transGuardLeftQEClock expr = 
    let te = self#transGuardLeftQEClock in
     match expr with					 							 
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) when mem_tqname_of_clock_name_expressions (Var clk1) &&
     (mem_tqname_of_clock_name_expressions (Var clk2)) = false-> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = clk1 then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  										Parenthesis(
  										BoolOperation(
		  						  	Parenthesis(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,(Var clk2)),op2,const),AndAnd,Var pretYx)),
		  						  	OrOr,
		  						  	Parenthesis(BoolOperation(
		  						  	Relation(Num 0,op2,IntOperation(const,Plus,(Var clk2))),AndAnd, Unary(UnaryNot, Var pretYx)))))
			  							  	
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr 
    
(*-------------------------------------------------------------------------------------------------------------------------------*)  
(*Case x operator y operator const, where y is qe clock and x not. Example: x-y>=20.*) 	
  method private transGuardRightQEClock expr = 
    let te = self#transGuardRightQEClock in
     match expr with					 							 
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const)
     when mem_tqname_of_clock_name_expressions (Var clk2) && (mem_tqname_of_clock_name_expressions (Var clk1)) = false -> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = clk2 then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  										Parenthesis(
  										BoolOperation(
		  						  	Parenthesis(BoolOperation(
		  						  	Relation(IntOperation((Var clk1),Minus,Var(_YrepComy)),op2,const),AndAnd,Var pretYy)),
		  						  	OrOr,
		  						  	Parenthesis(BoolOperation(
		  						  	Relation(IntOperation((Var clk1),Minus,Num 0),op2,const),AndAnd,Unary(UnaryNot, Var pretYy)))))
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr   

(*-------------------------------------------------------------------------------------------------------------------------------*)        
(*Case x operator y operator const, where x and y are qe clocks. Example: x-y>=20.*) 	
  method private transGuardTwoQEClocks expr = 
    let te = self#transGuardTwoQEClocks in
     match expr with					 							 
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) when mem_tqname_of_clock_name_expressions (Var clk1)
    	&& mem_tqname_of_clock_name_expressions (Var clk2) -> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = clk1 then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  											);	
  												
  										(if clock = clk2 then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  										Parenthesis(
  										BoolOperation(
  										BoolOperation(
  										(*1*)
  										BoolOperation(
  										Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Var(_YrepComy)),op2,const),AndAnd,Var pretYx ),AndAnd,Var pretYy)),
  										OrOr,
  										(*2*)
  										Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(Num 0,op2,IntOperation(const,Plus,Var(_YrepComy))),AndAnd, Unary(UnaryNot,Var pretYx )),AndAnd,Var pretYy))),
  										OrOr,
  										(*3*)
  										Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Num 0),op2,const),AndAnd,Var pretYx ),AndAnd, 
		  						  	Unary(UnaryNot,Var pretYy)))),	
		  						  	OrOr,
		  						  	(*4*)
    						  		Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Num 0,Minus,Num 0),op2,const),AndAnd, Unary(UnaryNot,Var pretYx )),AndAnd, 
		  						  	Unary(UnaryNot,Var pretYy)))))	 	
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr       
 
     			
  method private getExpressionInEdge ({Edge.id; source; target; guard; update; _}) =
    	match guard with
        | None -> (Bool false)
        | Some g -> g.Guard.expr
          
  method private candidate_expr expr =
    let te = self#candidate_expr in
		   match expr with 
		     	| Relation((Var var),op,(const)) when mem_tqname_of_clock_name_expressions (Var var)  -> 
		     			num_edges <- 0;
    					expr
    			| Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) 
    				when mem_tqname_of_clock_name_expressions (Var clk1) &&
								(mem_tqname_of_clock_name_expressions (Var clk2)) = false ->
						 num_edges <- 1;
							expr 		 
      		| Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const)
						when mem_tqname_of_clock_name_expressions (Var clk2) && 
								(mem_tqname_of_clock_name_expressions (Var clk1)) = false -> 
							num_edges <- 2;
  						expr
  				| Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) 
  					when mem_tqname_of_clock_name_expressions (Var clk1) && 
  							 mem_tqname_of_clock_name_expressions (Var clk2) -> 
  					  num_edges <- 3;
  						expr 
					| Var(id) ->  						Var(id)
					| Num(num) -> 						Num(num) 
					| Bool(b) -> 							Bool(b)
					| Call(e,args) -> 				Call(te e,args)
					| Array(e1,e2) -> 				Array(te e1, te e2)
					| Parenthesis(e) -> 			Parenthesis(te e)
					| IncPost(e) -> 					IncPost(te e)
					| IncPre(e) -> 						IncPre(te e)
					| DecPost(e) -> 					DecPost(te e)
					| DecPre(e) -> 						DecPre(te e)
					| Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
					| Unary(op,e) -> 					Unary(op,te e) 
					| Relation(e1,op,e2) -> 	Relation(te e1,op,te e2)
					| IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
					| BoolOperation(e1,op,e2) -> 	BoolOperation(te e1,op,te e2)
					| IfExpr(e1,e2,e3) -> 				IfExpr(te e1,te e2,te e3)
					| Dot(e1,id) -> 							Dot(te e1,id)
					| Dashed(e) ->  							Dashed(te e)
					| Deadlock -> 							Deadlock
					| Imply(e1,e2) -> 					Imply(te e1, te e2)
					| Exists(id,typ,e) -> 			Exists(id,typ, te e)
					| Forall(id,typ,e) -> 			Forall(id,typ, te e)
					| CTL(e) -> expr
					
		method! transform_edges edges =				
    (* partition on reset edges*)
    let edges_withguards, other_edges = 
      List.partition (fun e -> match (relookup#in_SE e) = false with
      | true -> true
      | false -> false) edges 
    in
    edges_withguards
    |> List.map
      (fun ({Edge.id; source; target; guard; update; comment; _} as edge) ->
          num_edges <- 0;
          isComplexEdge <-false;
      		ignore(self#candidate_expr (self#getExpressionInEdge edge));
    			if (num_edges) = 0 then  begin 
						 (match update with		
													 | None -> ()
													 | Some u -> 
													 			List.iter
																(fun expr -> match expr with
																	| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																			-> isComplexEdge <- true 
																	| _ -> ()
																) u.Update.el ) ; 	
    					let new_edge_0 = 
    					{edge with Edge.
								 guard = begin match guard with
								 | None -> None
								 | Some g -> Some { g with Guard.
                              expr = self#transGuardOneQEClock g.Guard.expr}
								        end; 
								 comment = if isComplexEdge then begin
								 					Some "transfComplex"; 
								 			end		
								 		else comment;													
								 update = (match update with		
											 | None -> None
											 | Some u -> Some { u with Update.
											 			el =
											 			List.map
														(fun expr -> match expr with
														| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																-> 
																 ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
																		 if clock = clk then begin
																				pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
																		 		end	
																		) 
																		_globalTokensAccumulator;  
																		localTokens <- IDSet.add pretYx localTokens; 
																		relookup#getAllTokens localTokens;
																		(Assignment(Var pretYx,Assign,Num 0))
														| _ -> expr ;
														) u.Update.el
											 									} )                    
								 } in	
								 		[new_edge_0;];
								   end
      			else if (num_edges) = 1 then begin
      						(match update with		
													 | None -> ()
													 | Some u -> 
													 			List.iter
																(fun expr -> match expr with
																	| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																			-> isComplexEdge <- true 
																	| _ -> ()
																) u.Update.el ) ; 
      						let new_edge_1 =
      						{edge with Edge.
									 guard = begin match guard with
									 | None -> None
									 | Some g -> Some { g with Guard.
										               expr = 		self#transGuardLeftQEClock g.Guard.expr}     
										      end;
									  comment = if isComplexEdge then begin
								 					Some "transfComplex"; 
								 							end		
								 					else comment;		       
									  update = (match update with		
											 | None -> None
											 | Some u -> Some { u with Update.
											 			el =
											 			List.map
														(fun expr -> match expr with
														| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																-> 
																 ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
																		 if clock = clk then begin
																				pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
																		 		end	
																		)
																		_globalTokensAccumulator;  
																		localTokens <- IDSet.add pretYx localTokens; 
																		relookup#getAllTokens localTokens;
																		(Assignment(Var pretYx,Assign,Num 0))
														| _ -> expr ;
														) u.Update.el
											 									} )                     
										 } in  
										 [new_edge_1;]
						                end
      						else if (num_edges) = 2 then begin 
      							 (match update with		
													 | None -> ()
													 | Some u -> 
													 			List.iter
																(fun expr -> match expr with
																	| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																			-> isComplexEdge <- true 
																	| _ -> ()
																) u.Update.el ) ; 
      								let new_edge_2 =
      								  {edge with Edge.
												 guard = begin match guard with
												 | None -> None
												 | Some g -> Some { g with Guard.
																              expr = self#transGuardRightQEClock g.Guard.expr} 
																end;  
				              comment = if isComplexEdge then begin
								 					Some "transfComplex"; 
								 							end		
								 					else comment;	       					
											update = (match update with		
											 | None -> None
											 | Some u -> Some { u with Update.
											 			el =
											 			List.map
														(fun expr -> match expr with
														| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																-> 
																 ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
																		 if clock = clk then begin
																				pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
																		 		end	
																		)
																		_globalTokensAccumulator; 
																		localTokens <- IDSet.add pretYx localTokens; 
																		relookup#getAllTokens localTokens;
																		(Assignment(Var pretYx,Assign,Num 0))
														| _ -> expr ;
														) u.Update.el
											 									} ) 				            
													 } in
													[new_edge_2;] 
												      end
												else  	begin	 
													let new_edge_3 =
													 (match update with		
															 | None -> ()
															 | Some u -> 
															 			List.iter
																		(fun expr -> match expr with
																			| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																					-> isComplexEdge <- true 
																			| _ -> ()
																		) u.Update.el ) ; 
      										 {edge with Edge.
														 guard = begin match guard with
														 | None -> None
														 | Some g -> Some { g with Guard.expr = self#transGuardTwoQEClocks g.Guard.expr}
																		end; 
														comment = if isComplexEdge then begin
										 					Some "transfComplex"; 
										 							end		
										 					else comment;	    				
														update = (match update with		
														 | None -> None
														 | Some u -> Some { u with Update.
														 			el =
														 			List.map
																	(fun expr -> match expr with
																	| Assignment(Var clk,Assign,Num(0)) when mem_tqname_of_clock_name_expressions (Var clk) 
																			-> 
																			 ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
																					 if clock = clk then begin
																							pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
																					 		end	
																					)
																					_globalTokensAccumulator;  
																					localTokens <- IDSet.add pretYx localTokens; 
																					relookup#getAllTokens localTokens;
																					(Assignment(Var pretYx,Assign,Num 0))
																	| _ -> expr ;
																	) u.Update.el
														 									} ) 				             
															 }   in
															 			[new_edge_3;]
																      end 		      
      )
    (* flatten list *)
    |> List.concat
    (* include non-simple edges again *)
    |> (@) other_edges
    (* CUTOFF (no superclass call necessary) *)
					 
		(**This disjunction not needed any longer. However, we will keep it temporarily. *)						 
		(*This function creates the disjunction (vnstY || sYA2 + sYA1 + sYA0 + 0 == 0) which is conjoined in the guard of
		a complex edge. This disjunction forbids complex edge of Y, to go first than the simple edges of Y.
		If all simple edges of Y have been taken, then vnstY is true. If there are no simple edges, then the sum
		of all simple counters (sYAi) is zero.*) 
    method private sumTokensOfOneEC ({Edge.id; source; target; update
    ; guard; color; _})=
    let open Update in
    let update = Tataf_libcore.Util.unsome(update) in
    	 accumulator<-(Num 0) ;  
    	List.map
    	(fun expr -> match expr with
		  	| Assignment(Var clk,Assign,Num(0)) 
										when mem_tqname_of_clock_name_expressions (Var clk) 
										-> 
										ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass} as ct1)   ->
												 if ct1.ClockTemplate.clock = clk then begin 
												  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass} as ct2)   ->
												  			if ct1.ClockTemplate.qeClass = ct2.ClockTemplate.qeClass  
												  			then begin
												  				partialAccumulator<-
												  				IntOperation(Var ("s" ^ ct2.ClockTemplate.qeClass ^ 
												  					ct2.ClockTemplate.template.Name.text), Plus, partialAccumulator);
												  					iterator<-iterator+1; 
												  					if iterator = relookup#computeSize _globalSimpCountersAccumulator ct2.ClockTemplate.qeClass  
												  					then (
																					accumulator<- 
																					Parenthesis(
																					BoolOperation(
																					Parenthesis(BoolOperation(Var ("vnst" ^ ct2.ClockTemplate.qeClass ),
																					OrOr,
                              						(Relation(Parenthesis(partialAccumulator),Eq,
                              						Num 0)))),AndAnd,
                              						if accumulator = (Num 0) then Bool true else accumulator
                              						));
                              						iterator<- 0 ;
                              						partialAccumulator <-(Num 0);
												  					)
												  			end		
												  	)
												  	_globalSimpCountersAccumulator;
												 end;	
												)
												_globalTokensAccumulator	; 
												Assignment(Var clk,Assign,Num(0))
				| _ -> expr
    	)
        update.el
	
	
	method private transInvariant expr = 
    let te = self#transInvariant in
     match expr with
    (*Case x operator const, where x is qe clock. Example: x>=20.*) 
    | Relation((Var var),op,(const)) when mem_tqname_of_clock_name_expressions (Var var)  -> 
  							ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										if clock = var then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  										)
  										_globalTokensAccumulator;
    						 Parenthesis(BoolOperation(Parenthesis(BoolOperation(Relation(Var(_YrepComx),op,const),AndAnd,Var pretYx)),OrOr,
    						 							 Parenthesis(BoolOperation(Relation(Num 0,op,const),AndAnd, Unary(UnaryNot,Var pretYx)))))  
    (*Case x operator y operator const, where x is qe clock and y not. Example: x-y>=20.*) 						 							 
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) when mem_tqname_of_clock_name_expressions (Var clk1) &&
     (mem_tqname_of_clock_name_expressions (Var clk2)) = false-> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = clk1 then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  											);	
  												
  										(if clock = clk2 then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  							let _tYx = if pretYx = "false" then Bool true else Var pretYx 
  							in
  							let _tYy = if pretYy = "false" then Bool true else Var pretYy 
  							in 	 			
        				Parenthesis(
    						  BoolOperation(
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,(Var clk2)),op2,const),AndAnd,_tYx),AndAnd,_tYy)),
		  						  	OrOr, 
    						  		Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Num 0,Minus,(Var clk2)),op2,const),AndAnd, Unary(UnaryNot,_tYx)),AndAnd, _tYy))
    						  									)
    						  	)			
    (*Case x operator y operator const, where y is qe clock and x not. Example: x-y>=20.*) 	
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const)
     when mem_tqname_of_clock_name_expressions (Var clk2) && (mem_tqname_of_clock_name_expressions (Var clk1)) = false -> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = clk1 then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  											);	
  												
  										(if clock = clk2 then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  							let _tYx = if pretYx = "false" then Bool true else Var pretYx 
  							in
  							let _tYy = if pretYy = "false" then Bool true else Var pretYy 
  							in 	 			
									Parenthesis(
    						  BoolOperation(
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation((Var clk1),Minus,Var(_YrepComy)),op2,const),AndAnd,_tYx),AndAnd,_tYy)),
		  						  	OrOr, 
    						  		Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation((Var clk1),Minus,Num 0),op2,const),AndAnd, _tYx),AndAnd,Unary(UnaryNot,_tYy)))
    						  									)
    						  	)	
    | Relation(IntOperation((Var clk1),op,(Var clk2)),op2,const) when mem_tqname_of_clock_name_expressions (Var clk1)
    	&& mem_tqname_of_clock_name_expressions (Var clk2) -> 
        				ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})   ->
  										(if clock = clk1 then begin
  											pretYx <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComx <- qeClass ^ "rep"
  											end
  											);	
  												
  										(if clock = clk2 then begin
  											pretYy <- ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock);
  											_YrepComy <- qeClass ^ "rep"
  											end
  											);	
  										)
  										_globalTokensAccumulator	;
  							let _tYx = if pretYx = "false" then Bool true else Var pretYx 
  							in
  							let _tYy = if pretYy = "false" then Bool true else Var pretYy 
  							in 	 	
    						  Parenthesis(
    						  BoolOperation(
    						  	BoolOperation(
		  						  	BoolOperation(
		  						  	Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Var(_YrepComy)),op2,const),AndAnd,_tYx),AndAnd,_tYy)),
		  						  	OrOr, 
    						  		Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Num 0,Minus,Var(_YrepComy)),op2,const),AndAnd, Unary(UnaryNot,_tYx)),AndAnd,_tYy))
    						  									),
    						  		OrOr,				
    						  		Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Var(_YrepComx),Minus,Num 0),op2,const),AndAnd,_tYx),AndAnd, Unary(UnaryNot,_tYy)))			
    						  								),
    						  		 OrOr,
    						  		Parenthesis(BoolOperation(BoolOperation(
		  						  	Relation(IntOperation(Num 0,Minus,Num 0),op2,const),AndAnd, Unary(UnaryNot,_tYx)),AndAnd, 
		  						  	Unary(UnaryNot,_tYy)))									
    						  							)	
    						  	)								  							  	
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr
    
    	
	(** HERE we change the invariant of any location in the automaton*)		 
  method! transform_invariant ({Invariant. expr; xy } as inv) = 
  	let new_expr = 
		  self#transInvariant expr
    in
    super#transform_invariant { inv with Invariant. expr = new_expr}  
    
    end
  |> fun transformer -> transformer#transform nta
    

(**
   Creation of the virtual world.
*)

(*
  Helper class for create_virtual_world() below -- was once realised by
  inheriting from walker.  To avoid name-clashes with new trafo_with_symtab,
  it is for the moment factored out into into something separate.
*)
class create_virtual_world_walker =
  object (self)

  inherit Tataf_libta.UppaalWalker.unit_walker as super

  val mutable committedLocations = IDSet.empty (*stores all commited locations of the undelying automaton*)
  method committedLocations = committedLocations

  method! visit_location ({Location.id; committed;_}) =
  	if committed = true then  
  		committedLocations <- IDSet.add id committedLocations;
end

let create_virtual_world (glookup : glookup) (_globalTokensAccumulator: ClockTemplateSet.t) (nta : NTA.t) : NTA.t =
  dbg#info "Create virtual world";
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
(*  inherit UppaalWalker.unit_walker as super_walker *)
  inherit varGeneratorGlobal

  val glookup = glookup
  val mutable postrans = new Tataf_libta.UppaalTransformer.Layout.position_translator 0 0
  val mutable disjAccumulator = (Bool false) (*disjunction of tokens of all participanting ECs in an automaton*)
  val _globalTokensAccumulator 				= _globalTokensAccumulator
  val mutable committedLocations = IDSet.empty (*stores all commited locations of the undelying automaton*)
  val mutable setOfECs = IDSet.empty;
  val mutable currentTemplate = "";
  
  method private disjoinTokensOfParticipatingECs set =
    disjAccumulator <- (Bool false);
    setOfECs <- IDSet.empty;
    

    (*Given a template, we filter the elements where that template is mentioned. The idea is to get later
    the ECs associated to that template.*)
    let filteredSet =
    		ClockTemplateSet.filter (fun ({ClockTemplate.template; clock; qeClass}) ->
     		template.Name.text = currentTemplate )
    		set
    in
    
    ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})  -> 
    	setOfECs <- IDSet.add qeClass setOfECs) filteredSet;
    	
  	IDSet.iter (fun  x  ->
  			ClockTemplateSet.iter (fun ({ClockTemplate.template; clock; qeClass}) ->
  				if (x = qeClass) then
  					disjAccumulator<-BoolOperation(Var ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock), OrOr, disjAccumulator))
  			set	)
		setOfECs

  
  method! transform_locations locations = 
  	locations
  	    |> List.map (* copy locations to virtual world. Here is for simple and complex edges *)
       (fun ({Location.id;_} as loc)  -> 
         match (postrans#transform_location loc) with
         | {Location.id; name; xy; committed; _} as tloc ->
           [loc; {tloc with Location.
                  id        = self#virtual_id id;
                  name      = self#virtual_name name; 
                  urgent    = not committed;
                  committed = committed;
                  color     = Some Tataf_libta.UppaalSystem.Color.gray;} 
         ] 
       )
    |> List.concat
    

                              			
  method! transform_edges edges =
  	(*building the disjunc. of all tokens for all ECs that are used in a given timed automaton*)
    self#disjoinTokensOfParticipatingECs 	_globalTokensAccumulator; 	 
    edges
    |> List.map (* copy edges to virtual world *)
       (fun edge ->
        match (postrans#transform_edge edge) with
        | {Edge.id; source; target; guard; xy; nails; sync; comment; _} as tedge  ->
          [edge; {tedge with Edge.
									guard = 
									(if IDSet.mem source committedLocations then (*to edges whose origin location is committed, we do not add
																															 the disjunction of all tokens*)
										guard 
									else  
										begin match guard with
															| None -> Guard.make_some (Parenthesis(disjAccumulator))
															| Some g -> Some {g with Guard.
																expr = BoolOperation(g.Guard.expr,AndAnd,Parenthesis(disjAccumulator))}
										end);									
                  id     = some_apply self#virtual_id id;
                  source = self#virtual_id source;
                  target = self#virtual_id target;
                  comment = if (comment = Some "transfComplex" || comment =  Some "transfSimple") then None else comment;
                  color  = Some Tataf_libta.UppaalSystem.Color.gray; }
         ]
       )
    |> List.concat

  method! transform_template ({Template.name; _} as template) =
    (*transform only, if template has resetting edges*)
    if IDSet.mem name.Name.text glookup#resettingTemplates then
    begin 
    	currentTemplate <- name.Name.text;
      postrans <-
        template
        |> (new Tataf_libta.UppaalWalker.Bounds.layout_bounds)#visit_template
        |> Tataf_libta.UppaalWalker.Bounds.height
        |> (+) 80
        |> new Tataf_libta.UppaalTransformer.Layout.position_translator (-30)
      ;
      super#transform_template template
    end else
      template 
  
  initializer
    let cvww = new create_virtual_world_walker in
    cvww#visit nta;
    committedLocations <- cvww#committedLocations

  end
  |> fun transformer -> transformer#transform nta

(**
	Eliminate comments which are helpers for our transformation of simple an complex edges.
*)  

let eliminate_helper_comments (nta : NTA.t) : NTA.t =
  dbg#info "Create virtual world";
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  
  val mutable postrans = new Tataf_libta.UppaalTransformer.Layout.position_translator 0 0
    
  method! transform_edges edges =
  edges
  |> List.map (* copy edges to virtual world *)
     (fun edge ->
      match (postrans#transform_edge edge) with
      | {Edge.id; source; target; guard; xy; nails; sync; comment; _} as tedge  ->
        [{tedge with Edge.
								comment = if (comment = Some "transfComplex" || comment =  Some "transfSimple") then None else comment;}
       ]
     )
  |> List.concat
 
   end
  |> fun transformer -> transformer#transform nta 
  
  
  (**
  Connect Worlds. 
  Generate connections to the virtual world by pointing the complex edges 
  into their corresponding target edge in the virtual world. Additionally 
  backward edges are added.
*)
let connect_original_to_virtual_world (glookup : glookup) (qe_global_clock_expr_lookups: qe_global_clock_expr_lookup) 
		(nta : NTA.t) : NTA.t =
  dbg#info ("Connecting real to virtual world");
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGeneratorGlobal
  
  val glookup = glookup
  
  
  (*Changing the target to the virtual world for a complex edge*)
  method! transform_edge ({Edge.source; target; update; guard; comment;_} as edge) =
    if  (comment = Some "transfComplex" || comment =  Some "transfSimple")
    then begin 
    			 { edge with Edge.
            target = self#virtual_id target;
            color  = Some Tataf_libta.UppaalSystem.Color.magenta;
            comment = None;
            } end
    else begin
    				edge (* no changes *)
				end

  end
  |> fun transformer -> transformer#transform nta
  
  
  (**
  Disconnect Worlds. Newly added edges to RL of simple edges, i.e. edges with rY! or rYA01?, must not
  be connected to virtual world. In the function connect_original_to_virtual_world we connect every resetting edges,
  even the already mentioned ones, and we cannot filter them in the mentioned function because of the parameter
  glookup, we need relookup to get the variable _rY.
   
*)
let disconnect_original_to_virtual_world (relookup : relookup) (nta : NTA.t) : NTA.t =
  dbg#info ("Disconnecting real to virtual world for loops in RL of simple edges");
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass
  
  val relookup = relookup
  
  (*Disconnecting edges from virtual in order to make them loops in real world*)
  method! transform_edge ({Edge.source; target; sync; comment; _} as edge) =
    if  comment  = (Some "loop")
    then { edge with Edge.
            target = source;
            comment = None;
            }
    else edge (* no changes *)


  end
  |> fun transformer -> transformer#transform nta  
  
  
(**
  Connect virtual to real world.
*)
let connect_virtual_to_original_world (relookup : relookup) (nta : NTA.t) : NTA.t = 
   
  (*|TRANSFORMER|*)
  dbg#info ("Connecting virtual to real world");
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass
  
  val relookup = relookup
  val mutable backEdges = []
  
    
  method! transform_location ({Location.id;_} as location) =
    (*we assume that glookup does not contain information about the 
      virtual world locations and edges!*)
    begin match relookup#edges_in id with
      | [] -> () (*no incomming edge for this location, therefore no backward
                   edge must be generated -> ignore*)
      | _ -> 
        let backEdgesY =
        let vid = self#virtual_id id in
        [{ (Edge.make vid id) with Edge.
      				guard		=	None;
		  				sync 		= Sync.make_some_synchronize (Var _resetY);
		          color 	= Some Tataf_libta.UppaalSystem.Color.cyan
      	}]
				 in 
				(backEdges <- backEdges @ backEdgesY)
    end;
    super#transform_location location
    
    

  method! transform_template ({Template.name; _} as template) =
    (*only, if template has resetting edges (and therefore a virtual world) *)
    if IDSet.mem name.Name.text relookup#resettingTemplates then
    begin 
      (*reset back-edges list*) 
      backEdges <- [];
      (*add generated back edges after transforming the template*)
      let ttemplate = super#transform_template template in
      {ttemplate with Template.
          edges = ttemplate.Template.edges @ backEdges }
    end else
      template
  end
  |> fun transformer -> transformer#transform nta
    
 
(**
  We transform here those edges which are not complex but synchronize with complex edges of a given equivalence class.
  We conjoin in the guards of those edges the clause vnstY. *)

let block_nonResettingEdges_whichSynchronizeWithComplex 
	(_ChannelsOfComplex) 
	(_globalSimpCountersAccumulator: ClockTemplateSet.t) 
	(nta : NTA.t) : NTA.t = 
  dbg#info ("Blocking non-resetting edges which syn. with complex edges");
  (*|TRANSFORMER|*)

  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super 
  
  val mutable sum_counters_tokens_one_EC = Bool true    (*sum for counters sY and tokens of one EC*) 
  
  method private sumCountersTokensOfOneEC set classQE =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})  ->
  	if qeClass = classQE then begin
  		sum_counters_tokens_one_EC<- 
  															 BoolOperation(
																 Unary(UnaryNot, Parenthesis(
																 Relation(IntOperation(Var ("s" ^ qeClass ^ template.Name.text),
																 Plus,Var ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock)),Eq,Num 2))), 
																 AndAnd, 
																 sum_counters_tokens_one_EC)
																 
  		end
  	else begin
  		sum_counters_tokens_one_EC<-sum_counters_tokens_one_EC	end )
		set
 
  method! transform_edge ({Edge.id; guard; sync; _} as edge) =
  let newedge = 
  	{edge with Edge.guard = 
  		begin match guard with 
  		| None -> None
  		| Some g ->  
  					(match sync with 
						| Some { Sync.expr; _} -> 	
  							let qeClassesLst =  Hashtbl.find_all _ChannelsOfComplex expr
  							in
  								if (List.length qeClassesLst) = 0 (*this edge has no interesting channel, leave guard untouched*) then 
  										guard
  								else begin 		
											let vars = 
												(List.fold_left 
												(fun acc x ->
												sum_counters_tokens_one_EC <- Bool true;
												self#sumCountersTokensOfOneEC _globalSimpCountersAccumulator x;
												BoolOperation(acc,AndAnd,sum_counters_tokens_one_EC))
												(Bool true)
												qeClassesLst);	
											in 	
											Some { g with Guard.expr = BoolOperation(g.Guard.expr,AndAnd,vars)}
									end		
  					|	None -> guard)
  		end	}		
    in
    	   super#transform_edge newedge
    end
  |> fun transformer -> transformer#transform nta    
  
  
  
  
