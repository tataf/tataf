(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression

open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

let dbg = QeClockReductionCommons.dbg

open QeClockReductionCommons
open Tataf_libta.UppaalQeClasses


(*----------------------------------------------------------------------------*)

(**
  Adds an constructor template and the extends each template with new initial
  locations.
*)
let add_constructor (nta : NTA.t) : NTA.t = 
  dbg#info ("Adding Construtor template..");
  (*TRANSFORMER*)
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super

  val lookup = new Tataf_libta.UppaalWalker.lookup nta

  val _CTR    = "CTR" (* name of global "constructor" channel variable *)
  val _CTRT   = "Constructor" (*template name of construtor*)

  method! transform_template 
    ({Template.name; locations; init; edges; _} as template) =
    (*translator to position the init location*)
    let leftbound =
      template
      |> (new Tataf_libta.UppaalWalker.Bounds.layout_bounds)#visit_template
      |> Tataf_libta.UppaalWalker.Bounds.left
    in
    (*Add new init location and edge to the template*)
    let init_name = _CTR ^ "_" ^ name.Name.text in
    let loc_new_init = 
      { (Location.make init_name) with Location.
         name  = Name.make_some init_name;
         xy    = lookup#location init 
                 |> fun {Location.xy; _} ->
                    begin
                    match xy with
                    | Some {XY.y;_} -> Some {XY.x = leftbound - 50; 
                                                y = y - 50; }
                    | None -> None
                    end
      }
    in
    let init_edge =
      { (Edge.make init_name init) with Edge.
        sync   = Sync.make_some_synchronize (Var _CTR);
      }
    in
    super#transform_template { template with Template.
      locations = loc_new_init :: locations;
      edges     = init_edge :: edges;
      init      = init_name;
    }

  method createConstructorTemplate =
    let l0 = _CTR ^ "_pre" in
    let l1 = _CTR ^ "_post" in
    let loc0 = 
      { (Location.make l0) with Location.
        name      = Name.make_some l0;
        xy        = XY.make_some 0 0;
        committed = true;
      } 
    in
    let loc1 = 
      { (Location.make l1) with Location.
        name      = Name.make_some l1;
        xy        = XY.make_some 200 0;
      } 
    in
    let edge =
      { (Edge.make l0 l1) with Edge.
        sync   = Sync.make_some_broadcast (Var _CTR);
      }
    in
    { Template.name         = Name.make _CTRT;
               parameters   = [];
               declarations = [];
               locations    = [ loc0; loc1; ];
               init         = l0;
               edges        = [ edge ];  
    }

  method! transform_templates templates =
    super#transform_templates templates @ [self#createConstructorTemplate]
    
  method! transform_system ({System.processes; _} as system) =
    (* add new template to system initialization *)
    let max = System.max_process_id system in
    super#transform_system { system with System.
      processes = processes @ [Process.make ~priority:max ~name:_CTRT ]
    }

  method! transform_nta ({NTA.declarations; _} as nta) =
    (*TODO: ensure, that these variables are not used, yet!*)
    let open Declaration in
    let open Type in
    let new_declarations =
      (* add constructor channel *)
      declarations @ [ Variable(Chan [Broadcast], _CTR, None) ]
    in
    dbg#info (" adding Variable '" ^ _CTR ^ "' in system declarations.");
    super#transform_nta { nta with NTA.declarations = new_declarations } 
  end
  |> fun transformer -> transformer#transform nta
;;

(*----------------------------------------------------------------------------*)
(**
  1.- Remove clocks contained in equivalence class in declarations. 
  2.- Add tokens sYA0, sYA1, etc. 
*)
let create_globals (relookup : relookup) (nta : NTA.t) : NTA.t = 
  dbg#info ("Create Globals");
  (*|TRANSFORMER|*)
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass
  
  val relookup = relookup
  

  method! transform_nta ({NTA.declarations; _} as nta) =
    (*TODO: ensure, that these variables are not used, yet!*)
    let open Declaration in
    let open Type in
    let new_declarations = declarations 
      @ [Variable(Chan [Urgent;Broadcast],_urY,  None);
      	 Variable(Chan [],     _rY, None);
         Variable(Chan [Broadcast],     _resetY, None);
         Variable(Bool [],     _prioY,Initializer.make_some_expr (Bool false));  
         Variable(unranged_int [Const], _numClocksY,
           Initializer.make_some_expr
             (Num(Tataf_libta.UppaalQeClasses.TemplateQualifiedIDSet.cardinal qeClocks)) );]
		  @ 
		      List.map 
		        (fun vid -> Variable(Bool [], vid, Initializer.make_some_expr (Bool false)) ) 
		        (IDSet.elements relookup#_allCounters)
		        
		  @ 
		      List.map 
		        (fun vid -> Variable(Chan [], vid,  None)) 
		        (IDSet.elements relookup#_allrChannels)
		  in
    dbg#info (" adding Variable '" ^ _urY         ^ "' in system declarations.");
    dbg#info (" adding Variable '" ^ _rY         ^ "' in system declarations.");
    dbg#info (" adding Variable '" ^ _resetY     ^ "' in system declarations.");
    dbg#info (" adding Variable '" ^ _numClocksY ^ "' in system declarations.");
    super#transform_nta { nta with NTA.declarations = new_declarations } 
  end
  |> fun transformer -> transformer#transform nta
  

(*----------------------------------------------------------------------------*)
(**
  1.- We collect the channels for every equivalence class and later we create a priority over them
*)
let create_priority_over_channels  (listQEClasses : (ID.t) list) (nta : NTA.t) : NTA.t = 
  dbg#info ("Create Priority over Channels");
  (*|TRANSFORMER|*)
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super 
   
   
  method! transform_nta ({NTA.declarations; _} as nta) =
    (*TODO: ensure, that these variables are not used, yet!*)
    let open Declaration in
    let open Type in 
    let priority = ref 0 in
    let lstOfresetChans = 
    	List.map (fun x -> Channel(("reset" ^ x), [])) listQEClasses in
    let lstOfrYChannels = 
    	List.map (fun x -> [Channel(("r" ^ x), []);Channel(("ur" ^ x), [])]) listQEClasses in
    let new_declarations = declarations 
    	@ [ChanPriority(List.map 
    			(fun channels -> priority := !priority + 1;
          List.map (super#transform_chanpriority !priority) channels) 
          ( [lstOfresetChans @ [DefaultPriority]] @ lstOfrYChannels) );]   
		  in
    super#transform_nta { nta with NTA.declarations = new_declarations } 
  end
  |> fun transformer -> transformer#transform nta
  
(*----------------------------------------------------------------------------*)

(**
  1.- We add all tokens from all ECs.
*)
let create_globals_tokens (_setTokensIds: IDSet.t) (listQEClasses : (ID.t) list) (nta : NTA.t) : NTA.t = 
  dbg#info ("Create Globals");
  (*|TRANSFORMER|*)
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super

  method! transform_nta ({NTA.declarations; _} as nta) =
    let open Declaration in
    let open Type in
    let new_declarations = declarations 
      @ 
		      List.map 
		        (fun vid -> Variable(Bool [], vid, Initializer.make_some_expr (Bool true)) ) 
		        (IDSet.elements _setTokensIds)
		in        
    dbg#info (" adding tokens all EC in system declarations.");
    super#transform_nta { nta with NTA.declarations = new_declarations } 
  end
  |> fun transformer -> transformer#transform nta

(*----------------------------------------------------------------------------*)
(**
  add resetter template
*)
let add_resetter (relookup : relookup) 
								 (_globalTokensAccumulator: ClockTemplateSet.t) 
								 (_globalSimpCountersAccumulator: ClockTemplateSet.t)
								 (listQEClasses : (ID.t) list)
								 (nta : NTA.t)
                  : NTA.t =
  dbg#info ("Add Resetter");
  (*|TRANSFORMER|*)
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass


  val relookup = relookup
  val mutable lstlocations = []
  val mutable lstedges = []
  val mutable sum_tokens_one_EC 	= (Num 0) (*sum for tokens tY of one EC*)
  val mutable sum_counters_one_EC =  (Num 0)   (*sum for counters sY of one EC*) 
  val mutable sum_counters_tokens_one_EC = Bool false    (*sum for counters sY and tokens of one EC*) 
  val mutable disj_tokens_one_EC 	= (Bool false) (*disjunction of tokens of one EC*)
  val mutable conj_tokens_one_EC 	= (Bool true) (*conjunction of tokens of one EC*)
  val mutable sum_tokens_all_ECs 	= (Num 0) (*sum for tokens of other ECs different from the current one *)
  val mutable iterator = 0
  val mutable lstExpression = []
  val mutable lastQEClass = ""
  val mutable localTokens= IDSet.empty
  val mutable allTokensReset =  []
  val mutable prioGuard = Bool true


  method createResetter =
  
    let open Type in
    let open Declaration in
    self#sumCountersTokensOfOneEC _globalSimpCountersAccumulator; (*building the sum of all counters and tokens for one given EC*)
        												(*building the conjunction of tokens for one given EC which exclusively belong to complex edges*)
    self#conjoinTokensOfOneEC 	(ClockTemplateSet.diff relookup#allClocksAndTemplate _globalSimpCountersAccumulator); 	
    self#disjoinTokensOfOneEC 	_globalTokensAccumulator; 			(*building the disjunc. of all tokens for one given EC*)
    self#sumTokensOfOneEC 			relookup#allClocksAndTemplate; 	(*building the sum of all tokens for one given EC*)
    self#sumTokensOfAllEC 			_globalTokensAccumulator listQEClasses;	 (*building the sum of all tokens of all EC*)
    self#resetAllTokensOfOneEC 	relookup#allClocksAndTemplate;	(*building the reset of all tokens of a given EC*)
    List.iter (fun a -> sum_tokens_all_ECs <-BoolOperation(sum_tokens_all_ECs,AndAnd,a)) lstExpression ;
    let numOfRecords = ClockTemplateSet.cardinal relookup#simpleClockAndTemplate 
    in
    (*Initial location of resetter*)
    let loc_lrY = 
      { (Location.make _lrY) with Location.
        name   = Name.make_some _lrY;
        xy     = XY.make_some (-440) (-144);
      } 
    in  
    (*nst location of resetter*)
    let loc_nstY = 
      { (Location.make (_nstY ^ (string_of_int numOfRecords))) with Location.
        name   = Name.make_some (_nstY ^ (string_of_int numOfRecords));
        xy     = XY.make_some (-60 +((numOfRecords-1)*90)) (-200);  
        urgent = true;
      } 
    in
     relookup#set_finalnstlocY (_nstY ^ (string_of_int numOfRecords));
    (*timelock location of resetter*)
    let loc_timelock= 
      { (Location.make ("timelock" ^ qeClassName)) with Location.
        name   = Name.make_some ("timelock" ^ qeClassName);
        xy     = XY.make_some (-60 +((numOfRecords-1)*90)) (-350);  
        urgent = true;
      } 
    in
    (*nst locations created for simple edges in resetter*)
    ignore (ClockTemplateSet.fold (fun  ({ClockTemplate.template; clock; qeClass})  i->
    		localTokens<- IDSet.add (_sY ^ template.Name.text) localTokens;
		    localTokens<- IDSet.add (ty ^ template.Name.text ^ "_" ^ clock) localTokens; 
    		lstlocations <- lstlocations @ [{ (Location.make (_nstY ^ (string_of_int i))) with Location.
    			name   = Name.make_some (_nstY ^ (string_of_int i));
		      xy     = XY.make_some (-160 +(i*100)) (-144); 
		      committed = true;
		      color     = Some Tataf_libta.UppaalSystem.Color.gray; 
		    }];
		    if i ==0 then begin 
		    	lstedges <-  lstedges @ [{ (Edge.make _lrY (_nstY ^(string_of_int i))) with Edge.
		    		update = Some { Update.el = [(Assignment(Var _prioY,Assign,Num 1)); ];
		      											xy 	= XY.make_some (-400 +(i*100)) (-143) };									 
						sync   = Some { Sync.expr  	= Var _rY;
      											 marker =	Sync.Synchronize;
      											 xy 		= XY.make_some (-430) (-130)};};				 
				  {(Edge.make (_nstY ^(string_of_int (i))) (_nstY ^(string_of_int (i+1)))) with Edge.
		  	  guard  = Some { Guard.expr = Var (_sY ^ template.Name.text);
        				 			   			xy     = XY.make_some (-140 +(i*100)) (-145) };
		      sync   = Some { Sync.expr   = Var (_rY^ template.Name.text); 
		                           marker = Sync.Broadcast; 
		                           xy     = XY.make_some (-140 +(i*100)) (-160) };};                    
		      {(Edge.make (_nstY ^(string_of_int (i))) (_nstY ^(string_of_int (i+1)))) with Edge.
		      guard  = Some { Guard.expr = BoolOperation((Unary(UnaryNot,(Var (_sY ^ template.Name.text))),
		      															OrOr, Parenthesis(BoolOperation(((Var (_sY ^ template.Name.text)),
		      															AndAnd, Unary(UnaryNot,(Var(ty ^ template.Name.text ^ "_" ^ clock))))))));
        				 			   			xy     = XY.make_some (-140 +(i*100)) (-85) };
		                           nails  = [ Nail.make (-60 +((i-1)*100)) (-90); Nail.make (-60 +(i*90)) (-90);];}
		                           ];
		    i+1; (*increment value for ClockTemplateSet.fold*)                      
		    end                       
		    else
		    if i == (relookup#computeSize relookup#simpleClockAndTemplate qeClass)-1
		      then begin 
		    lstedges <-  lstedges @ [{ (Edge.make (_nstY ^(string_of_int i)) (_nstY ^(string_of_int (i+1)))) with Edge.
		  	  guard  = Some { Guard.expr = Var (_sY ^ template.Name.text);
        				 			   			xy     = XY.make_some (-140 +(i*100)) (-145) };
		      sync   = Some { Sync.expr   = Var (_rY^ template.Name.text); 
		                           marker = Sync.Broadcast; 
		                           xy     = XY.make_some (-140 +(i*100)) (-160) }; };                
		      {(Edge.make (_nstY ^(string_of_int i)) (_nstY ^(string_of_int (i+1)))) with Edge.
		      guard  = Some { Guard.expr = BoolOperation((Unary(UnaryNot,(Var (_sY ^ template.Name.text))),
		      															OrOr, Parenthesis(BoolOperation(((Var (_sY ^ template.Name.text)),
		      															AndAnd, Unary(UnaryNot,(Var(ty ^ template.Name.text ^ "_" ^ clock))))))));
        				 			   				xy     = XY.make_some (-140 +(i*100)) (-85 +(i*16)) };
		                           nails  = [ Nail.make (-60 +((i-1)*100)) (-90); Nail.make (-60 +(i*90)) (-90);];}
		                           ];           
		    i+1; (*increment value for ClockTemplateSet.fold*)
		    end
		    else begin 
		    lstedges <-  lstedges @ [{ (Edge.make (_nstY ^(string_of_int i)) (_nstY ^(string_of_int (i+1)))) with Edge.
		  	  guard  = Some { Guard.expr = Var (_sY ^ template.Name.text);
        				 			   			xy     = XY.make_some (-140 +(i*100)) (-145) };
		      sync   = Some { Sync.expr   = Var (_rY^ template.Name.text); 
		                           marker = Sync.Broadcast; 
		                           xy     = XY.make_some (-140 +(i*100)) (-160) };};
		      {(Edge.make (_nstY ^(string_of_int i)) (_nstY ^(string_of_int (i+1)))) with Edge.
		      guard  = Some { Guard.expr = BoolOperation((Unary(UnaryNot,(Var (_sY ^ template.Name.text))),
		      															OrOr, Parenthesis(BoolOperation(((Var (_sY ^ template.Name.text)),
		      															AndAnd, Unary(UnaryNot,(Var(ty ^ template.Name.text ^ "_" ^ clock))))))));
        				 			   				xy     = XY.make_some (-140 +(i*100)) (-85 +(i*16)) };
		                           nails  = [ Nail.make (-60 +((i-1)*100)) (-90); Nail.make (-60 +(i*90)) (-90);];}
		                           ];           
		    i+1; (*increment value for ClockTemplateSet.fold*)
		    end
    ) 
    relookup#simpleClockAndTemplate
    0); (*initial value for ClockTemplateSet.fold*)
    let rec sublist lst =
    	match lst with
    	| [] -> []
    	| hd :: tl  ->  
    		if hd == className then
    				tl
    	  else 
    	  		sublist tl
    	in
    (*Getting the expression that we will use for priorities of transitions*)
  		List.iter 
  		(fun e ->  prioGuard <- BoolOperation(prioGuard,AndAnd,Relation(Var ("prio" ^ e),Eq,Num 0)) 
  				)
  		(sublist listQEClasses);
    (*This edge connects the nst location to the initial location of the resetter*)
    let edge1 =
      { (Edge.make (_nstY ^ (string_of_int numOfRecords)) _lrY) with Edge.
      	update = Some { Update.el = allTokensReset @ [(Assignment(Var _Yrep,Assign,Num 0));
      																							 (Assignment(Var _prioY,Assign,Num 0));];
      									       xy = XY.make_some (-370) (-218) 
      								};																						 
				guard  = Some { Guard.expr = BoolOperation(BoolOperation(Unary(UnaryNot,(Parenthesis(disj_tokens_one_EC))),
		    																	AndAnd, if sum_tokens_all_ECs = Num 0 then Bool true else sum_tokens_all_ECs),
		    																	AndAnd,
		    																	prioGuard);
        				 			   			xy     = XY.make_some (-430) (-200) }; 
        sync   = Some { Sync.expr  	= Var _resetY;
      											 marker =	Sync.Broadcast;
      											 xy 		= XY.make_some (-430) (-218)};				 			   			
        nails  = [ Nail.make (-440) (-200);];
      }
    in
    (*This is an edge from initial location to nst in resetter*)
		let edge2 =
				  { (Edge.make _lrY (_nstY ^ (string_of_int numOfRecords))) with Edge.
				update = Some { Update.el = [(Assignment(Var _prioY,Assign,Num 1));];
													     xy = XY.make_some (-350) (-245)};
				guard  = Some { Guard.expr =  Unary(UnaryNot,(Parenthesis(if disj_tokens_one_EC = Bool false then Bool true else disj_tokens_one_EC)));
        				 			   			xy     = XY.make_some (-400) (-260) };
        sync   = Some { Sync.expr  	= Var _urY;
      											 marker =	Sync.Broadcast;
      											 xy 		= XY.make_some (-400) (-245)};
        nails  = [ Nail.make (-440) (-230); Nail.make (-100) (-230); ];
      }
    in
    (*This edge connects the nst location to the timelock location of the resetter*)
    let edge4 =
      { (Edge.make (_nstY ^ (string_of_int numOfRecords)) ("timelock" ^ qeClassName)) with Edge.
        update = Some { Update.el = [(Assignment(Var _prioY,Assign,Num 0)); ];
				    											xy 	= XY.make_some (-400 + 100) (-300) };						
				guard  = Some { Guard.expr =  
		    												 BoolOperation(
		    																	Parenthesis(
		    																			BoolOperation(Parenthesis(sum_counters_tokens_one_EC),
		    																			AndAnd,
		    																			conj_tokens_one_EC)),
		    																	AndAnd, 
		    																	 prioGuard);
        				 			   			xy     = XY.make_some (-200) (-300) }; 
      }
    in
    { Template.name = Name.make _RY;
    					 declarations = [];
               parameters   = [];
               locations    =  [ loc_lrY; loc_nstY; loc_timelock;] @ lstlocations;
               init         = _lrY;
               edges        = [ edge1; edge2; edge4;] @ lstedges;  
    }
  
  method private sumTokensOfAllEC set listQEClasses =
  	List.iter 
  	(fun cls -> 
  			ClockTemplateSet.iter
  				(fun  ({ClockTemplate.template; clock; qeClass}) ->
  					if qeClass != className then
  							(if qeClass = cls then
  								(
  									sum_tokens_all_ECs<-
  											IntOperation(Var ("t" ^ qeClass ^ template.Name.text ^ "_" ^ clock), Plus, 
  	  			  					if sum_tokens_all_ECs = Bool true then Num 0 else sum_tokens_all_ECs);
  	  			  			iterator<-iterator+1; 		
  	  			  			if iterator = (relookup#computeSize _globalTokensAccumulator qeClass) then 
  	  			  				(
												lstExpression<- lstExpression @ [
												Parenthesis(BoolOperation((Relation(Parenthesis(sum_tokens_all_ECs),Eq,Num 0)),OrOr, 
												Relation(Parenthesis(sum_tokens_all_ECs),Eq,(Var ("numClocks" ^ qeClass)))))]	;	
												iterator<- 0 ;
												sum_tokens_all_ECs<- (Bool true); 
												lastQEClass<- qeClass;
  	  								)		
  								)
  							) 
  				)
  			set
  	)
  	listQEClasses
  
  method private sumCountersTokensOfOneEC set =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})  ->
  	if qeClass = relookup#qeClassName then begin
  		sum_counters_one_EC				<- IntOperation(Var (_sY ^ template.Name.text),Plus,sum_counters_one_EC);
  		sum_counters_tokens_one_EC<- 
  															 BoolOperation(
																 Parenthesis(
																 Relation(IntOperation(Var (_sY ^ template.Name.text),
																 Plus,Var (ty ^ template.Name.text ^ "_" ^ clock)),Eq,Num 2)), 
																 OrOr, 
																 sum_counters_tokens_one_EC)
																 
  		end
  	else begin
  		sum_counters_tokens_one_EC<-sum_counters_tokens_one_EC	end )
		set
    
  method private sumTokensOfOneEC set =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock;_})  ->
  	sum_tokens_one_EC<-IntOperation(Var (ty ^ template.Name.text ^ "_" ^ clock), Plus, sum_tokens_one_EC))
		set
	
	method private conjoinTokensOfOneEC set =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})  ->
  	if qeClass = relookup#qeClassName then
  		conj_tokens_one_EC<-BoolOperation(Unary(UnaryNot,Var (ty ^ template.Name.text ^ "_" ^ clock)), AndAnd, conj_tokens_one_EC)
  	else 
  		conj_tokens_one_EC<-conj_tokens_one_EC)
		set	
	
	
	method private disjoinTokensOfOneEC set =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock; qeClass})  ->
  	if qeClass = relookup#qeClassName then
  		disj_tokens_one_EC<-BoolOperation(Var (ty ^ template.Name.text ^ "_" ^ clock), OrOr, disj_tokens_one_EC)
  	else 
  		disj_tokens_one_EC<-disj_tokens_one_EC)
		set	
	
	method private resetAllTokensOfOneEC set =
  	ClockTemplateSet.iter (fun  ({ClockTemplate.template; clock;_})  ->
  	allTokensReset<-allTokensReset @ [(Assignment(Var (ty ^ template.Name.text ^ "_" ^ clock),Assign,Num 1));])
		set	
    
  method! transform_templates templates =
    templates @ [self#createResetter]
    
  method! transform_system ({System.processes; _} as system) =
    (* add new template to system initialization *)
    let max = System.max_process_id system in
    { system with System.
      processes = processes @ [Process.make ~priority:max ~name: (" \n"^ _RY)]
    }

    
  end
  |> fun transformer -> transformer#transform nta
  
  
let insert_labels_for_queries (nta : NTA.t) : NTA.t = 
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super 

  method! transform_system ({System.processes; _} as system) =
    (* add new template to system initialization *)
    let max = System.max_process_id system in
    { system with System.
      processes = processes @ [Process.make ~priority:max ~name: 
      "\n //>>(AGnotDeadlock)(AGnotTimelock_1EC)(AGnotTimelock_2EC)(AGnotTimelock_5EC)//<<" ]
    }

    
  end
  |> fun transformer -> transformer#transform nta


  
