(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalQeClasses

open Tataf_libcore.ExtList
open Tataf_libcore.ExtString
open Tataf_libcore.Util

let dbg = QeClockReductionCommons.dbg

open QeClockReductionCommons


   
   
(**
  -remove clocks contained in equivalence class in declarations.
  -add new variable declaration: Yrep
*)    

class gamma_transformer (relookup : relookup)=    
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass


  method remove_qeClockDeclarations template_qualifier declarations = 
    (* Filter out all clocks from equivalence class. We get a new list without the elements being matched.*)
    let open Declaration in
    let open Type in
    List.filter
      (fun decl -> match decl with
        | Variable(Clock, vid, _) when TemplateQualifiedIDSet.mem
            ( TemplateQualifiedID.make template_qualifier vid )
            qeClocks
            -> dbg#info (" deleting clock declaration '"
                        ^ vid ^ "' from "
                        ^ (safe_unsome "[global]" template_qualifier)
                        ^ " declarations.");
               false
        | _ -> true
      ) 
      declarations

  method! transform_template ({ Template.name; declarations; _ } as tmpl) = 
    { ( super#transform_template tmpl ) with
        Template.declarations =
          self#remove_qeClockDeclarations
            ( Some name.Name.text ) declarations }

  method! transform_nta ({NTA.declarations; _} as nta) =
    (*TODO: ensure, that these variables are not used, yet!*)
    let open Declaration in
    let open Type in
    let new_declarations = 
      self#remove_qeClockDeclarations None declarations
      (* add new variable to declarations - Yrep is always global *)
      |> (@) [Variable(Clock,              _Yrep,   None);]
    in
    dbg#info (" adding Variable '" ^ _Yrep       ^ "' in system declarations.");
    super#transform_nta { nta with NTA.declarations = new_declarations } 

  method! transform nta =
    let tnta = super#transform nta in
    tnta
end

 
let gamma (relookup : relookup) (nta : NTA.t) : NTA.t = 
  dbg#info ("Gamma");
  (*|TRANSFORMER|*)
  let qe_clock_expr_lookup = new qe_clock_expr_lookup relookup in
  qe_clock_expr_lookup#visit_nta nta;
  (new gamma_transformer relookup )#transform nta
 
(*----------------------------------------------------------------------------*)
(**
  -add precursor edge
  -add follower edge

*)
let add_precursor_follower (relookup : relookup)	(nta : NTA.t) : NTA.t = 
  dbg#info ("Add precursor/follower/selfloop edges");
  (*|TRANSFORMER|*)
  let qe_clock_expr_lookup = new qe_clock_expr_lookup relookup in
  qe_clock_expr_lookup#visit_nta nta;
  let find_tqname_of_clock_name_expressions = 
        qe_clock_expr_lookup#find_tqname_of_clock_name_expressions
  in
  let mem_tqname_of_clock_name_expressions = 
        qe_clock_expr_lookup#mem_tqname_of_clock_name_expressions
  in
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass


  val relookup = relookup
  val mutable localTokens = IDSet.empty
  val mutable templateName = "" 
  val mutable _rYTemplate = "r"
  val mutable chanTemplateSet = IDSet.empty


  method! transform_edges edges =
    (* partition on reset edges*)
    let simple_edges, other_edges = 
      List.partition (fun e -> match relookup#in_SE e with
      | true -> true
      | false -> false) edges 
    in
    (* for each simple reset edge, we insert instead two new edges + the transformed simple edge *)
    simple_edges
    |> List.map
      (fun ({Edge.id; source; target; guard; update; _} as edge) ->
        match update with
        | Some {Update.el = [Assignment(var,Assign,num)] ; _} 
             when mem_tqname_of_clock_name_expressions var
            ->
            let _tYx = self#_tY (find_tqname_of_clock_name_expressions var)
            in
            let getTemplateFromRecord set =
            	ClockTemplateSet.iter (fun ({ClockTemplate.template; clock;_}) ->
            	templateName<-template.Name.text;
            	_rYTemplate <- _rY ^ templateName;
            	chanTemplateSet <- IDSet.add _rYTemplate chanTemplateSet;
            	relookup#getAllrChannels chanTemplateSet
            	) 
            	set
            in	
            (match  var with
            	| Var(x) -> getTemplateFromRecord (relookup#findTemplate_ClockTemplateSet relookup#simpleClockAndTemplate x)
            	| _ -> getTemplateFromRecord (relookup#findTemplate_ClockTemplateSet relookup#simpleClockAndTemplate "None")
            );
            localTokens <- IDSet.add _tYx localTokens;
            relookup#getAllTokens localTokens;
            
              [              
               (* create edges *)
               {(Edge.make ~source ~target) with Edge.
                  color = Some Color.magenta;
                  guard = begin match guard with
                          | None -> Guard.make_some (Var _tYx)
                          | Some g -> 
                          Some { g with Guard.
                              expr = self#transGuardInvariant g.Guard.expr
                                           }
                          end;
                  comment = (Some "transfSimple");        
                  sync   = Sync.make_some_broadcast (Var _resetY);
                  update = Update.make_some [(Assignment(Var _tYx,Assign,Num 0));
                  (Assignment(Var (_sY ^ templateName),Assign,Num 0))];
               }; 
                {(Edge.make ~source ~target) with Edge.
                  color = Some Color.magenta;
                  guard = begin match guard with
                          | None -> Guard.make_some (Var _tYx) 
                          | Some g -> 
                          Some { g with Guard.
                              expr = self#transGuardInvariant g.Guard.expr
                                           }
                          end;
                  comment = (Some "transfSimple");        
                  sync   = Sync.make_some_synchronize (Var _resetY); 
                  update = Update.make_some [(Assignment(Var _tYx,Assign,Num 0));
                  (Assignment(Var (_sY ^ templateName),Assign,Num 0))];
               }
               |> Tataf_libta.UppaalTransformer.Layout.translate_edge
                      (relookup#location source) (relookup#location target) 40; 
               ]
        (* else case (pattern match x:=0 failed): should never happen! *)
        | _ -> [edge] 
      )
    (* flatten list *)
    |> List.concat
    (* include non-simple edges again *)
    |> (@) other_edges
    (* CUTOFF (no superclass call necessary) *)

    
  method private transGuardInvariant expr = 
    let te = self#transGuardInvariant in
     match expr with
    | Relation(clk,op,const) when mem_tqname_of_clock_name_expressions clk   -> 
   							let _tYx = self#_tY (find_tqname_of_clock_name_expressions clk)
        				in
    						 Parenthesis(BoolOperation(Parenthesis(BoolOperation(Relation(Var(_Yrep),op,const),AndAnd, Var _tYx)),OrOr,
    						 							 Parenthesis(BoolOperation(Relation(Num 0,op,const),AndAnd, Unary(UnaryNot,Var _tYx)))))
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)
    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr  
    
    method private transGuardInvariantNegated expr = 
    let te = self#transGuardInvariantNegated in
     match expr with
    | Relation(clk,op,const) when mem_tqname_of_clock_name_expressions clk   -> 
   							let _tYx = self#_tY (find_tqname_of_clock_name_expressions clk)
        				in
    						 Parenthesis(BoolOperation(Relation(Var(_Yrep),Lt,const),AndAnd,Var _tYx))							 
    | Var(id) -> expr
    | Num(_) -> expr
    | Bool(_) -> expr
    | Call(e,args) -> expr
    | Array(e1,e2) -> expr
    | Parenthesis(e) -> Parenthesis(te e)
    | IncPost(e) -> expr
    | IncPre(e) -> expr
    | DecPost(e) -> expr
    | DecPre(e) -> expr
    | Assignment(e1,op,e2) -> Assignment(te e1,op,te e2)
    | Unary(op,e) -> expr
    | Relation(e1,op,e2) -> Relation(te e1,op,te e2)
    | IntOperation(e1,op,e2) -> IntOperation(te e1,op,te e2)
    | BoolOperation(e1,op,e2) -> BoolOperation(te e1,op,te e2)
    | IfExpr(e1,e2,e3) -> IfExpr(te e1,te e2,te e3)

    | Dot(e1,id) -> expr
    | Dashed(e) ->  expr
    | Deadlock -> expr
    | Imply(e1,e2) -> expr
    | Exists(id,typ,e) -> expr
    | Forall(id,typ,e) -> expr
    | CTL(e) -> expr  
 
  end
  |> fun transformer -> transformer#transform nta

(*----------------------------------------------------------------------------*)
(*
  Helper class for add_simple_RL_counter_bookkeeping() below -- was once
  realised by inheriting from walker.  To avoid name-clashes with new
  trafo_with_symtab, it is for the moment factored out into into something
  separate.
*)
class add_simple_RL_counter_bookkeeping_walker (relookup : relookup) (nta : NTA.t) = 
  object (self)

  inherit Tataf_libta.UppaalWalker.unit_walker as super_walker
  
  (*We need to revisit the template since the newly added constructor edges can be pointing to a RL of a SE.
    If we do not perform this revisit then template "None" is consider for those edges, and
    we add to those edges the update sYNone:=1 instead of, for instance, sYA0:=1, where the initial location
    of A0 is a RL of a SE. *)
  method! visit_template ({Template.name; edges;_}) =
    List.iter
    (fun ({Edge.source; target; _}) ->   
    relookup#_addSETemplate (source ^ "_" ^ target) name)
    edges;
    
end

(**
  3 Add RL Counter Bookkeeping.
  a.- For each template (automaton) A we create a boolean variable sYA.
  b.- We add [sYA0:=1] to updates of incoming edges of reset locations.
*)
let add_simple_RL_counter_bookkeeping (relookup : relookup) (nta : NTA.t) : NTA.t = 
  dbg#info ("Add RL counter bookkeeping");
  (*|TRANSFORMER|*)
  object (self)
  inherit Tataf_libta.UppaalTransformerWithSymboltable.transformer_without_symboltable as super
  inherit varGenerator relookup#qeClass

  val relookup = relookup
  val mutable localSimpleCounters = IDSet.empty

  initializer
    (new add_simple_RL_counter_bookkeeping_walker relookup nta)#visit nta;
    
  method! transform_edge ({Edge.update; source; target; comment; _} as edge) =
  	 	let templateName = 
  				try relookup#_SETemplate (source ^ "_" ^ target)
   							with Not_found -> 
   									Name.make "None"
			in	
			(*Here we insert the variable sYA in the case that a RL is the initial location*)
			(match (relookup#in_RLSE source) with
			| true -> localSimpleCounters <- IDSet.add (_sY ^ templateName.Name.text) localSimpleCounters;
		    					relookup#getAllCounters localSimpleCounters;
		  | _    -> localSimpleCounters <- localSimpleCounters; 			
		  );		
		  let new_update =
		  (*Here we insert the variable sYA in the case that a RL is not the initial location*)
		    match (relookup#in_RLSE target ) with 
		    				  (*we create here the variables sYA*)
		    | true -> localSimpleCounters <- IDSet.add (_sY ^ templateName.Name.text) localSimpleCounters;
		    					relookup#getAllCounters localSimpleCounters;
		    					(*we create here the update sYA:=1*)
		    					Update.add_expr update (Assignment(Var(_sY ^ templateName.Name.text),Assign, Num 1))
		    | _    -> update (* no extra expression necessary *)
    in
    super#transform_edge { edge with Edge.update = new_update } 
    
  end
  |> fun transformer -> transformer#transform nta
