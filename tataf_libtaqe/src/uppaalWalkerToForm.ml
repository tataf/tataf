(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

open Tataf_libta.UppaalSystem
open Tataf_libta.UppaalSystem.Expression
open Tataf_libta.UppaalWalker

(*----------------------------------------------------------------------------*)

module Form = struct
  (** 
  This module contains helper to transform Uppaal expressions into Form.form 
  types (isabelle lie formula). This is needed, if the use of the Decider is
  wanted.
  *)
  open Tataf_libform.FormUtil
  open Tataf_libcore.Util
  open Edge
  open Tataf_libform.PrintForm

    
  class nta2form =
    object (self)
    inherit [Tataf_libform.Form.form option] walker None ~concat:(fun a b -> None) (*concat must be explicit*)


    val mutable errors = []
    method errors = errors
    method error msg =
      errors <- msg :: errors;
      failwith(msg)
    
    method! visit_expression e =
      let ve f e =
        match self#visit_expression e with
        | None -> None
        | Some fe -> f fe
      in 
      let ve2 f e1 e2 =
        match self#visit_expression e1, self#visit_expression e2 with | Some f1, Some f2 -> f f1 f2
        | _ -> None
      in
      match e with
      | Var id         -> Some (mk_var id)
      | Num n          -> Some (mk_real(float_of_int(n)))
      | Bool b         -> Some (mk_bool b)
      | Call (_,_)     -> self#error "No calling supported."
      | Array (_,_)    -> self#error "No arrays supported."
      | Parenthesis e  -> self#visit_expression e
      | IncPost e      -> self#error "no post increment supported."
      | IncPre e       -> ve (fun f -> Some (mk_plus (f,(mk_int 1)))) e
      | DecPost e      -> self#error "no post decrement supported."
      | DecPre e       -> ve (fun f -> Some (mk_minus (f,(mk_int 1)))) e
      | Assignment (_,_,_)    -> self#error "No assignments supported."
      | Unary(UnaryMinus,e)   -> ve (fun f -> Some (mk_uminus f)) e
      | Unary(UnaryNot,e)     -> ve (fun f -> Some (mk_not f)) e
      | Unary(UnaryPlus,e)    -> self#visit_expression e
      | Relation(e1,Lt,e2)    -> ve2 (fun f1 f2 -> Some (mk_lt (f1,f2))) e1 e2
      | Relation(e1,LtEq,e2)  -> ve2 (fun f1 f2 -> Some (mk_lteq (f1,f2))) e1 e2
      | Relation(e1,Eq,e2)    -> ve2 (fun f1 f2 -> Some (mk_eq (f1,f2))) e1 e2
      | Relation(e1,NotEq,e2) -> ve2 (fun f1 f2 -> Some (mk_neq (f1,f2))) e1 e2
      | Relation(e1,GtEq,e2)  -> ve2 (fun f1 f2 -> Some (mk_gteq (f1,f2))) e1 e2
      | Relation(e1,Gt,e2)    -> ve2 (fun f1 f2 -> Some (mk_gt (f1,f2))) e1 e2
      | Relation(e1,_,e2)     -> self#error "'>?' and '<?' not supported."
      | IntOperation(e1,Plus,e2)    -> ve2 (fun f1 f2 -> Some (mk_plus (f1,f2))) e1 e2
      | IntOperation(e1,Minus,e2)   -> ve2 (fun f1 f2 -> Some (mk_minus (f2,f2))) e1 e2
      | IntOperation(e1,Mult,e2)    -> ve2 (fun f1 f2 -> Some (mk_mult (f1,f2))) e1 e2
      | IntOperation(e1,Div,e2)     -> ve2 (fun f1 f2 -> Some (mk_div (f1,f2))) e1 e2 
      | IntOperation(e1,Mod,e2)     -> ve2 (fun f1 f2 -> Some (mk_mod (f1,f2))) e1 e2 
      | IntOperation(e1,_,e2)       -> self#error 
                                        "IntOperations and, or, xor, 
                                            lshift and rshift not supported."
      | BoolOperation(e1,AndAnd,e2) -> ve2 (fun f1 f2 -> Some (mk_and [f1; f2])) e1 e2
      | BoolOperation(e1,OrOr,e2)   -> ve2 (fun f1 f2 -> Some (mk_or [f1; f2])) e1 e2
      | IfExpr(_,_,_)               -> self#error "No if-expression supported."
      | Dot(_,_)                    -> self#error "Currently dot expressions not supported."
      | _                           -> self#error "Unsupported (sub)expression."
  end
  
  let to_some_form (expr : Tataf_libta.UppaalSystem.Expression.t) =
    (new nta2form)#visit_expression expr

  class clockVariableMutator borrowed_frame =
    object (self)
    inherit [Tataf_libform.Form.form option] walker None ~concat:(fun a b -> None) (*concat must be explicit*)

    val mutable errors = []
    method errors = errors
    method error msg =
      errors <- msg :: errors;
      failwith(msg)
    method! visit_expression e =
      let ve f e =
        match self#visit_expression e with
        | None -> None
        | Some fe -> f fe
      in 
      let ve2 f e1 e2 =
        match self#visit_expression e1, self#visit_expression e2 with
        | Some f1, Some f2 -> f f1 f2
        | _ -> None
      in
      let vis_ex = self#visit_expression in
      let ve3 f e1 e2 e3 =
        match vis_ex e1, vis_ex e2, vis_ex e3 with 
        | Some f1, Some f2, Some f3 -> f f1 f2 f3
        | _ -> None
      in
      match e with
      (*TODO: FIND A WAY TO USE A SYMBOL TABLE TO DIFFERENTATE BETWEEN CLOCKS
       * AND INTS/BOOLS BELOW *)
      | Var id         -> if (Tataf_libta.SymbolTable.is_clock borrowed_frame id ||
                             Tataf_libta.SymbolTable.is_clock_ref borrowed_frame id) 
                          then Some (mk_plus (mk_var id, mk_var("d")))
                          else Some (mk_var id)
      | Num n          -> Some (mk_real (float_of_int(n)))
      | Bool b         -> Some (mk_bool b)
      | Call (_,_)     -> self#error "No calling supported."

      (*| Array(_,_)     -> Some (mk_map (TYPE FIRST THING, TYPE SECOND THING))  <-- how to get the types ? *)
      | Array (_,_)    -> self#error "No arrays supported."
      | Parenthesis e  -> self#visit_expression e 
      | IncPost e      -> self#error "no post increment supported."
      | IncPre e       -> ve (fun f -> Some (mk_plus (f,(mk_int 1)))) e
      | DecPost e      -> self#error "no post decrement supported."
      | DecPre e       -> ve (fun f -> Some (mk_minus (f,(mk_int 1)))) e
      | Assignment (_,_,_)    -> self#error "No assignments supported."
      | Unary(UnaryMinus,e)   -> ve (fun f -> Some (mk_uminus f)) e
      | Unary(UnaryNot,e)     -> ve (fun f -> Some (mk_not f)) e
      | Unary(UnaryPlus,e)    -> self#visit_expression e 
      | Relation(e1,Lt,e2)    -> ve2 (fun f1 f2 -> Some (mk_lt (f1,f2))) e1 e2
      | Relation(e1,LtEq,e2)  -> ve2 (fun f1 f2 -> Some (mk_lteq (f1,f2))) e1 e2
      | Relation(e1,Eq,e2)    -> ve2 (fun f1 f2 -> Some (mk_eq (f1,f2))) e1 e2
      | Relation(e1,NotEq,e2) -> ve2 (fun f1 f2 -> Some (mk_neq (f1,f2))) e1 e2
      | Relation(e1,GtEq,e2)  -> ve2 (fun f1 f2 -> Some (mk_gteq (f1,f2))) e1 e2
      | Relation(e1,Gt,e2)    -> ve2 (fun f1 f2 -> Some (mk_gt (f1,f2))) e1 e2
      | Relation(e1,_,e2)     -> self#error "'>?' and '<?' not supported."
      | IntOperation(e1,Plus,e2)    -> ve2 (fun f1 f2 -> Some (mk_plus (f1,f2))) e1 e2
      | IntOperation(e1,Minus,e2)   -> ve2 (fun f1 f2 -> Some (mk_minus (f2,f2))) e1 e2
      | IntOperation(e1,Mult,e2)    -> ve2 (fun f1 f2 -> Some (mk_mult (f1,f2))) e1 e2
      | IntOperation(e1,Div,e2)     -> ve2 (fun f1 f2 -> Some (mk_div (f1,f2))) e1 e2 
      | IntOperation(e1,Mod,e2)     -> ve2 (fun f1 f2 -> Some (mk_mod (f1,f2))) e1 e2
      | IntOperation(e1,_,e2)       -> self#error "IntOperations and, or, xor, lshift and rshift not supported."
      | BoolOperation(e1,AndAnd,e2) -> ve2 (fun f1 f2 -> Some (mk_and [f1; f2])) e1 e2
      | BoolOperation(e1,OrOr,e2)   -> ve2 (fun f1 f2 -> Some (mk_or [f1; f2])) e1 e2
      | IfExpr(e1,e2,e3)   -> ve3 (fun f1 f2 f3 -> Some (mk_ite (f1, f2, f3))) e1 e2 e3
      | Dot(_,_)                    -> self#error "Currently dot expressions not supported."
      | _                           -> self#error "Unsupported (sub)expression."
  end


  let enhance_guard_expression (expr : Tataf_libta.UppaalSystem.Expression.t) frame = 
      (new clockVariableMutator frame)#visit_expression expr
end

