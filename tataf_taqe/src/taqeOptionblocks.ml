(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
open Tataf_libcore.CmdLine
open TaqeOptions

(* Seems as if

    validate_empty_or_file_exists "<FLAG>" !<VARIABLE>

   also exists; seems that it checks that the argument to <FLAG> is given and
   that the denoted file does not exist; not used any more because we are
   following the standard UNIX tool convention: do what the user requested, 
   if the given file is there, it's overwritten (of course).
*)

(*----------------------------------------------------------------------------*)
(*- utataf -------------------------------------------------------------------*)
(*----------------------------------------------------------------------------*)

(*----------------------------------------------------------------------------*)
(*- analyze ------------------------------------------------------------------*)

(*- analyze -> prepostdelay --------------------------------------------------*)

let opt_prepostdelay : optionblock = "prepostdelay",
  "[OPTION]... SOURCE",
  SCSet prepostdelay,
  "Detect pre-/post-delayed edges in the given Uppaal model.",
  [("-o","--output", "OUTPUT", Arg.Set_string prepostdelay_output,
    "Specifies where we print the output of this subcommand (ID's of ppd edges).");
  ("", "--prime-suffix", "CLOCKSUFFIX", Arg.Set_string
    prepostdelay_prime_suffix,
    "String specifying the suffix to append to primed variables in ppd-detection
    formulae when the variables are reset in the update of the incoming edge 
    (default: primed)");],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string prepostdelay_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
     (Xml format).");
  ]),
  (fun () ->
    validate_notempty "SOURCE" !prepostdelay_source;
  ),
  [("Example containing pre post delayed edges",
    " examples/Uppaal/PrePostDelay/SimplePrePostDelay.xml");
  ("Example containing no pre post delayed edges",
    " examples/Uppaal/PrePostDelay/OnlyPreDelay.xml");

  ]

(*----------------------------------------------------------------------------*)
(*- qeClockReduction ---------------------------------------------------------*)

let opt_qeClockReduction : optionblock = "qeClockReduction",
  "[OPTION]... SOURCE QECLOCKSET",
  SCSet qeClockReduction,
  "Perform the QE clock reduction transformation (under construction).",
  [("-broadcast","--broadcast","", Arg.Set qeClockReduction_broadcast_trafo,
    (* TODO: Speak with Christian about how to document this option 
     * properly *)
    "If specifed, the famous broadcast trafo is performed.");
    ("","--ppd","", Arg.Set qeClockReduction_usePPD,
    "If specifed, the PPD set (set of pre/post delayed locations) is used to
     determine whether an edge is a simple resetting edge.  Remember, that the
     ppd sets must be given in QECLOCKSET.  Otherwise no simple resetting edge
     will be recognized.");
     ("","--edges","", Arg.Set_string qeClockReduction_PPD_edges,
    "Specifies the PPD edge IDs to use for QEClockReduction.");
   ("-q","--query", "FILENAMES", Arg.Set_string qeClockReduction_queryFilesIn,
    "Query-Files for the given Uppaal model as comma-seperated list.  
    If this option is used, a list with equal length of output
     filenames for the transformed query must be specified with '-p'.");
   ("-p","--queryout", "FILENAMES", Arg.Set_string qeClockReduction_queryFilesOut,
    "Comma-seperated list of filenames for the transformed query output (only effective with '-q').");
  ],
  AnonymousParameters([
    ("SOURCE", Arg.Set_string qeClockReduction_source,
     "Required: Specifies the source Uppaal file. This file must be in XTA 4
      (Xml format).");
    ("QECLOCKSET", Arg.Set_string qeClockReduction_qeClocks,
     "Required: Specifies the quasi equal clock equivalence classes in the
      following format: 
      'Y = {x, y, z}, W = {w, w1}'.
      Each equivalence class definition can be extended with a ppd set (cf. '--ppd'): 
      'Y = {x, y, z}[A, B, C]', where A, B and C are globally unique location names.");
  ]),
  (fun () ->
    validate_file_exists "SOURCE" !qeClockReduction_source;
    validate_notempty "QECLOCKSET" !qeClockReduction_qeClocks; 
    (*
    if not (!qeClockReduction_queryFileIn = "")
    && !qeClockReduction_queryFileOut = ""  
     then raise (InvalidOption("-p","Output filename for the transformed query missing."));
    if not (!qeClockReduction_queryFileOut = "")
    && !qeClockReduction_queryFileIn = ""  
    then raise (InvalidOption("-q","Query file missing."));*)
    validate_list_lengths "QUERYIN" "QUERYOUT" !qeClockReduction_queryFilesIn
                                               !qeClockReduction_queryFilesOut
  ),
  []

(*----------------------------------------------------------------------------*)
(*- utataf options -----------------------------------------------------------*)

(* Reference of user input; used for validity check *)
let utataf_debug_modules = ref ""
let opt_utataf : optionblock = "",
  "[OPTION]... SUBCOMMAND [...]",
  SCNothing,
  "tataf for Uppaal is a tool capable to work with networks of timed automata.",
  [
   ("","--debuglevel", "LEVEL", Arg.Int (fun debuglevel -> 
      Tataf_libcore.Debug.set_debug_level debuglevel
    ),
    "Sets the verbosity of the debug messages.  Higher is more.  
     Only effective together with --debugmodules.
     ");    
    ("","--debugmodules", "M[,M]",  Arg.String (fun modules ->
      (* Keep track of modules requested on the commandline for 
       * validity check below. *)
      utataf_debug_modules := modules;
      if modules = ""
      then Tataf_libcore.Debug.debugOn := false
      else Tataf_libcore.Debug.debugOn := true;
           Tataf_libcore.Debug.set_debug_modules modules
    ),
    "Output debugging information for specified modules (listed below).  The
     modules must be given as comma seperated list.  Supported modules: "
     ^ Tataf_libcore.Debug.registered_debug_modules ());
   ("-D","--default-dtd","VERSION", Arg.Set_string Tataf_libta.LibtaOptions.dtd_version_string,
    "Parse wrt. DTD version VERSION if the given XTA file does not specify one.
     Accepted values: 1.1 (default), 1.2.");
   ("","--halt-on-warning","", Arg.Set Tataf_libcore.Debug.halt_on_warning,
    "If given, the program will immediately halt if a warning occurs.");
   ("","--nocolor","", Arg.Clear Tataf_libcore.Color.colors_enabled,
    "Disables ANSI escape codes in console output.");    
   ("-v","--verbose","", Arg.Set Tataf_libta.LibtaOptions.verbose,
    "explain what is being done.");
   (*-----------------*)
   ("","--help","", Arg.Unit (fun () -> raise (Arg.Help "command line argument") ),
    "Display the best matching help page.  This flag is always applicable.");
   ("","--version","", Arg.Unit
      (fun () -> print_endline TaqeVersion.version_string; 
	 		           print_endline Tataf_libtaqe.LibtaqeVersion.version_string;
	 		           print_endline Tataf_libform.LibformVersion.version_string;
	 		           print_endline Tataf_libta.LibtaVersion.version_string;
			           print_endline Tataf_libcore.LibcoreVersion.version_string;
								 exit 0 ),
    "Display version information and exit.");
  ],
  Subcommands([
    opt_prepostdelay;
    opt_qeClockReduction;
    opt_help;
  ]),
  (* "validate_modules_exist" validates that all modules requested by the user 
   * actually exist (are registered) in the Debugging Module. *)
  (fun () ->
    validate_modules_exist 
    "MODULES" !utataf_debug_modules (Tataf_libcore.Debug.registered_debug_modules ());
    let open Tataf_libta.UppaalSystem.DtdVersion in
    match !Tataf_libta.LibtaOptions.dtd_version_string with
    | ""    -> ()
    | "1.1" -> Tataf_libta.LibtaOptions.dtd_version := DTD_1_1 ()
    | "1.2" -> Tataf_libta.LibtaOptions.dtd_version := DTD_1_2 ()
    | _     -> raise_invalid_option "-D"
                 ("'" ^ !Tataf_libta.LibtaOptions.dtd_version_string ^ "': invalid DTD version")
  ),
  [("Output debug information for the workflow of the Uppaal module:",
    "--debuglevel 1 --debugmodules UppaalPrint,UppaalParser ...");
   ("Disable ANSI escape codes:",
    "--nocolor ...");
   ("Display how the help system works:",
    "help");
  ]

