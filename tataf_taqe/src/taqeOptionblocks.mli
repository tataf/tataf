(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)
(** 
  This modules defines the command line options and the subcommands.
  Variables, which are filled by the command line parser {!CmdLine} are
  defined in module {!Options}.

  {3 See {!CmdLine} documentation for an overview over the interplay of these
  three modules and a HOWTO for adding new subcommands, flags, etc.}
*)
open Tataf_libcore.CmdLine

val opt_utataf : optionblock
(** 
  main [optionblock] for command line parsing of tataf for Uppaal options.
*)

