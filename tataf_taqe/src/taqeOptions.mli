(*
 * Copyright (C) 2021 University of Freiburg
 *
 * This file is part of tataf.
 *
 * Tataf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tataf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tataf.  If not, see <https://www.gnu.org/licenses/>.
 *)

val qeClockReduction : bool ref
val qeClockReduction_source : string ref
val qeClockReduction_qeClocks : string ref
val qeClockReduction_broadcast_trafo : bool ref
val qeClockReduction_usePPD : bool ref
val qeClockReduction_PPD_edges : string ref
val qeClockReduction_queryFilesIn : string ref
val qeClockReduction_queryFilesOut : string ref

(**
  {2 prepostdelay}
*)

val prepostdelay: bool ref
(** Indicates if the [prepostdelay] subcommand was specified. *)

val prepostdelay_source : string ref
(** Filename of the source Uppaal xml file. *)

val prepostdelay_output : string ref
(** Specifies where we want to print the output of this subcommand *)

val prepostdelay_prime_suffix : string ref
(** Suffix to append to clock variables during ppd detection *)


